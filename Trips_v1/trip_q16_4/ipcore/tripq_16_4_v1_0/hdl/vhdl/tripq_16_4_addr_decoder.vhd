-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\ccerd\Desktop\ip_fernando\Trips_v1\trip_q16_4\hdlsrc\Trip_Q16_4\tripq_16_4_addr_decoder.vhd
-- Created: 2021-11-11 14:20:02
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: tripq_16_4_addr_decoder
-- Source Path: tripq_16_4/tripq_16_4_axi_lite/tripq_16_4_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY tripq_16_4_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        read_Salida_Trips_procesa         :   IN    std_logic;  -- ufix1
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic;  -- ufix1
        write_Trip_1                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_2                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_3                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_4                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_5                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_6                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_7                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_8                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_9                      :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_10                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_11                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_12                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_13                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_14                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_15                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_16                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_17                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_18                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_19                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_20                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_21                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_22                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_23                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_24                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_25                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_26                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_27                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_28                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_29                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_30                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_31                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_Trip_32                     :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En4
        write_reset                       :   OUT   std_logic  -- ufix1
        );
END tripq_16_4_addr_decoder;


ARCHITECTURE rtl OF tripq_16_4_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp_1_1      : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL decode_sel_Salida_Trips_procesa_1_1 : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp_1_1       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_Salida_Trips_procesa    : std_logic;  -- ufix1
  SIGNAL data_slice_Salida_Trips_procesa_1 : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_Salida_Trips_procesa_1_1 : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable_1_1        : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable_1_1           : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_reg_axi_enable_1_1          : std_logic;  -- ufix1
  SIGNAL write_concats_axi_enable_1       : std_logic;  -- ufix1
  SIGNAL decode_sel_Trip_1_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_1_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_1                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_1_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_2_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_2_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_2                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_2_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_3_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_3_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_3                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_3_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_4_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_4_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_4                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_4_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_5_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_5_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_5                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_5_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_6_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_6_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_6                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_6_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_7_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_7_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_7                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_7_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_8_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_8_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_8                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_8_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_9_1_1            : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_9_1_1               : std_logic;  -- ufix1
  SIGNAL data_in_Trip_9                   : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_9_1_1              : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_10_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_10_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_10                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_10_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_11_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_11_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_11                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_11_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_12_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_12_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_12                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_12_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_13_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_13_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_13                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_13_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_14_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_14_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_14                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_14_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_15_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_15_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_15                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_15_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_16_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_16_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_16                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_16_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_17_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_17_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_17                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_17_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_18_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_18_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_18                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_18_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_19_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_19_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_19                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_19_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_20_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_20_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_20                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_20_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_21_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_21_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_21                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_21_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_22_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_22_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_22                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_22_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_23_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_23_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_23                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_23_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_24_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_24_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_24                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_24_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_25_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_25_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_25                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_25_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_26_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_26_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_26                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_26_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_27_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_27_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_27                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_27_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_28_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_28_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_28                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_28_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_29_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_29_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_29                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_29_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_30_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_30_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_30                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_30_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_31_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_31_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_31                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_31_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_Trip_32_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Trip_32_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Trip_32                  : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL data_reg_Trip_32_1_1             : signed(15 DOWNTO 0);  -- sfix16_En4
  SIGNAL decode_sel_reset_1_1             : std_logic;  -- ufix1
  SIGNAL reg_enb_reset_1_1                : std_logic;  -- ufix1
  SIGNAL data_in_reset                    : std_logic;  -- ufix1
  SIGNAL data_reg_reset_1_1               : std_logic;  -- ufix1
  SIGNAL write_concats_reset_1            : std_logic;  -- ufix1

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  
  decode_sel_Salida_Trips_procesa_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0061#, 14) ELSE
      '0';

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_ip_timestamp <= to_unsigned(0, 32);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp_1_1 <= const_0 WHEN decode_sel_ip_timestamp_1_1 = '0' ELSE
      read_reg_ip_timestamp;

  reg_Salida_Trips_procesa_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_Salida_Trips_procesa <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_Salida_Trips_procesa <= read_Salida_Trips_procesa;
      END IF;
    END IF;
  END PROCESS reg_Salida_Trips_procesa_process;


  data_slice_Salida_Trips_procesa_1 <= '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & read_reg_Salida_Trips_procesa;

  
  decode_rd_Salida_Trips_procesa_1_1 <= decode_rd_ip_timestamp_1_1 WHEN decode_sel_Salida_Trips_procesa_1_1 = '0' ELSE
      data_slice_Salida_Trips_procesa_1;

  data_read <= std_logic_vector(decode_rd_Salida_Trips_procesa_1_1);

  
  decode_sel_axi_enable_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable_1_1 <= decode_sel_axi_enable_1_1 AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_axi_enable_1_1 <= '1';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_axi_enable_1_1 = '1' THEN
        data_reg_axi_enable_1_1 <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_1_1_process;


  write_concats_axi_enable_1 <= data_reg_axi_enable_1_1;

  write_axi_enable <= write_concats_axi_enable_1;

  
  decode_sel_Trip_1_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  reg_enb_Trip_1_1_1 <= decode_sel_Trip_1_1_1 AND wr_enb;

  data_in_Trip_1 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_1_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_1_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_1_1_1 = '1' THEN
        data_reg_Trip_1_1_1 <= data_in_Trip_1;
      END IF;
    END IF;
  END PROCESS reg_Trip_1_1_1_process;


  write_Trip_1 <= std_logic_vector(data_reg_Trip_1_1_1);

  
  decode_sel_Trip_2_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  reg_enb_Trip_2_1_1 <= decode_sel_Trip_2_1_1 AND wr_enb;

  data_in_Trip_2 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_2_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_2_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_2_1_1 = '1' THEN
        data_reg_Trip_2_1_1 <= data_in_Trip_2;
      END IF;
    END IF;
  END PROCESS reg_Trip_2_1_1_process;


  write_Trip_2 <= std_logic_vector(data_reg_Trip_2_1_1);

  
  decode_sel_Trip_3_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0042#, 14) ELSE
      '0';

  reg_enb_Trip_3_1_1 <= decode_sel_Trip_3_1_1 AND wr_enb;

  data_in_Trip_3 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_3_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_3_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_3_1_1 = '1' THEN
        data_reg_Trip_3_1_1 <= data_in_Trip_3;
      END IF;
    END IF;
  END PROCESS reg_Trip_3_1_1_process;


  write_Trip_3 <= std_logic_vector(data_reg_Trip_3_1_1);

  
  decode_sel_Trip_4_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0043#, 14) ELSE
      '0';

  reg_enb_Trip_4_1_1 <= decode_sel_Trip_4_1_1 AND wr_enb;

  data_in_Trip_4 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_4_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_4_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_4_1_1 = '1' THEN
        data_reg_Trip_4_1_1 <= data_in_Trip_4;
      END IF;
    END IF;
  END PROCESS reg_Trip_4_1_1_process;


  write_Trip_4 <= std_logic_vector(data_reg_Trip_4_1_1);

  
  decode_sel_Trip_5_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0044#, 14) ELSE
      '0';

  reg_enb_Trip_5_1_1 <= decode_sel_Trip_5_1_1 AND wr_enb;

  data_in_Trip_5 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_5_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_5_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_5_1_1 = '1' THEN
        data_reg_Trip_5_1_1 <= data_in_Trip_5;
      END IF;
    END IF;
  END PROCESS reg_Trip_5_1_1_process;


  write_Trip_5 <= std_logic_vector(data_reg_Trip_5_1_1);

  
  decode_sel_Trip_6_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0045#, 14) ELSE
      '0';

  reg_enb_Trip_6_1_1 <= decode_sel_Trip_6_1_1 AND wr_enb;

  data_in_Trip_6 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_6_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_6_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_6_1_1 = '1' THEN
        data_reg_Trip_6_1_1 <= data_in_Trip_6;
      END IF;
    END IF;
  END PROCESS reg_Trip_6_1_1_process;


  write_Trip_6 <= std_logic_vector(data_reg_Trip_6_1_1);

  
  decode_sel_Trip_7_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0046#, 14) ELSE
      '0';

  reg_enb_Trip_7_1_1 <= decode_sel_Trip_7_1_1 AND wr_enb;

  data_in_Trip_7 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_7_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_7_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_7_1_1 = '1' THEN
        data_reg_Trip_7_1_1 <= data_in_Trip_7;
      END IF;
    END IF;
  END PROCESS reg_Trip_7_1_1_process;


  write_Trip_7 <= std_logic_vector(data_reg_Trip_7_1_1);

  
  decode_sel_Trip_8_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0047#, 14) ELSE
      '0';

  reg_enb_Trip_8_1_1 <= decode_sel_Trip_8_1_1 AND wr_enb;

  data_in_Trip_8 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_8_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_8_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_8_1_1 = '1' THEN
        data_reg_Trip_8_1_1 <= data_in_Trip_8;
      END IF;
    END IF;
  END PROCESS reg_Trip_8_1_1_process;


  write_Trip_8 <= std_logic_vector(data_reg_Trip_8_1_1);

  
  decode_sel_Trip_9_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0048#, 14) ELSE
      '0';

  reg_enb_Trip_9_1_1 <= decode_sel_Trip_9_1_1 AND wr_enb;

  data_in_Trip_9 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_9_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_9_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_9_1_1 = '1' THEN
        data_reg_Trip_9_1_1 <= data_in_Trip_9;
      END IF;
    END IF;
  END PROCESS reg_Trip_9_1_1_process;


  write_Trip_9 <= std_logic_vector(data_reg_Trip_9_1_1);

  
  decode_sel_Trip_10_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0049#, 14) ELSE
      '0';

  reg_enb_Trip_10_1_1 <= decode_sel_Trip_10_1_1 AND wr_enb;

  data_in_Trip_10 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_10_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_10_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_10_1_1 = '1' THEN
        data_reg_Trip_10_1_1 <= data_in_Trip_10;
      END IF;
    END IF;
  END PROCESS reg_Trip_10_1_1_process;


  write_Trip_10 <= std_logic_vector(data_reg_Trip_10_1_1);

  
  decode_sel_Trip_11_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004A#, 14) ELSE
      '0';

  reg_enb_Trip_11_1_1 <= decode_sel_Trip_11_1_1 AND wr_enb;

  data_in_Trip_11 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_11_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_11_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_11_1_1 = '1' THEN
        data_reg_Trip_11_1_1 <= data_in_Trip_11;
      END IF;
    END IF;
  END PROCESS reg_Trip_11_1_1_process;


  write_Trip_11 <= std_logic_vector(data_reg_Trip_11_1_1);

  
  decode_sel_Trip_12_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004B#, 14) ELSE
      '0';

  reg_enb_Trip_12_1_1 <= decode_sel_Trip_12_1_1 AND wr_enb;

  data_in_Trip_12 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_12_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_12_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_12_1_1 = '1' THEN
        data_reg_Trip_12_1_1 <= data_in_Trip_12;
      END IF;
    END IF;
  END PROCESS reg_Trip_12_1_1_process;


  write_Trip_12 <= std_logic_vector(data_reg_Trip_12_1_1);

  
  decode_sel_Trip_13_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004C#, 14) ELSE
      '0';

  reg_enb_Trip_13_1_1 <= decode_sel_Trip_13_1_1 AND wr_enb;

  data_in_Trip_13 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_13_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_13_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_13_1_1 = '1' THEN
        data_reg_Trip_13_1_1 <= data_in_Trip_13;
      END IF;
    END IF;
  END PROCESS reg_Trip_13_1_1_process;


  write_Trip_13 <= std_logic_vector(data_reg_Trip_13_1_1);

  
  decode_sel_Trip_14_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004D#, 14) ELSE
      '0';

  reg_enb_Trip_14_1_1 <= decode_sel_Trip_14_1_1 AND wr_enb;

  data_in_Trip_14 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_14_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_14_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_14_1_1 = '1' THEN
        data_reg_Trip_14_1_1 <= data_in_Trip_14;
      END IF;
    END IF;
  END PROCESS reg_Trip_14_1_1_process;


  write_Trip_14 <= std_logic_vector(data_reg_Trip_14_1_1);

  
  decode_sel_Trip_15_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004E#, 14) ELSE
      '0';

  reg_enb_Trip_15_1_1 <= decode_sel_Trip_15_1_1 AND wr_enb;

  data_in_Trip_15 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_15_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_15_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_15_1_1 = '1' THEN
        data_reg_Trip_15_1_1 <= data_in_Trip_15;
      END IF;
    END IF;
  END PROCESS reg_Trip_15_1_1_process;


  write_Trip_15 <= std_logic_vector(data_reg_Trip_15_1_1);

  
  decode_sel_Trip_16_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004F#, 14) ELSE
      '0';

  reg_enb_Trip_16_1_1 <= decode_sel_Trip_16_1_1 AND wr_enb;

  data_in_Trip_16 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_16_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_16_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_16_1_1 = '1' THEN
        data_reg_Trip_16_1_1 <= data_in_Trip_16;
      END IF;
    END IF;
  END PROCESS reg_Trip_16_1_1_process;


  write_Trip_16 <= std_logic_vector(data_reg_Trip_16_1_1);

  
  decode_sel_Trip_17_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0050#, 14) ELSE
      '0';

  reg_enb_Trip_17_1_1 <= decode_sel_Trip_17_1_1 AND wr_enb;

  data_in_Trip_17 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_17_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_17_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_17_1_1 = '1' THEN
        data_reg_Trip_17_1_1 <= data_in_Trip_17;
      END IF;
    END IF;
  END PROCESS reg_Trip_17_1_1_process;


  write_Trip_17 <= std_logic_vector(data_reg_Trip_17_1_1);

  
  decode_sel_Trip_18_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0051#, 14) ELSE
      '0';

  reg_enb_Trip_18_1_1 <= decode_sel_Trip_18_1_1 AND wr_enb;

  data_in_Trip_18 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_18_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_18_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_18_1_1 = '1' THEN
        data_reg_Trip_18_1_1 <= data_in_Trip_18;
      END IF;
    END IF;
  END PROCESS reg_Trip_18_1_1_process;


  write_Trip_18 <= std_logic_vector(data_reg_Trip_18_1_1);

  
  decode_sel_Trip_19_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0052#, 14) ELSE
      '0';

  reg_enb_Trip_19_1_1 <= decode_sel_Trip_19_1_1 AND wr_enb;

  data_in_Trip_19 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_19_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_19_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_19_1_1 = '1' THEN
        data_reg_Trip_19_1_1 <= data_in_Trip_19;
      END IF;
    END IF;
  END PROCESS reg_Trip_19_1_1_process;


  write_Trip_19 <= std_logic_vector(data_reg_Trip_19_1_1);

  
  decode_sel_Trip_20_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0053#, 14) ELSE
      '0';

  reg_enb_Trip_20_1_1 <= decode_sel_Trip_20_1_1 AND wr_enb;

  data_in_Trip_20 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_20_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_20_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_20_1_1 = '1' THEN
        data_reg_Trip_20_1_1 <= data_in_Trip_20;
      END IF;
    END IF;
  END PROCESS reg_Trip_20_1_1_process;


  write_Trip_20 <= std_logic_vector(data_reg_Trip_20_1_1);

  
  decode_sel_Trip_21_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0054#, 14) ELSE
      '0';

  reg_enb_Trip_21_1_1 <= decode_sel_Trip_21_1_1 AND wr_enb;

  data_in_Trip_21 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_21_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_21_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_21_1_1 = '1' THEN
        data_reg_Trip_21_1_1 <= data_in_Trip_21;
      END IF;
    END IF;
  END PROCESS reg_Trip_21_1_1_process;


  write_Trip_21 <= std_logic_vector(data_reg_Trip_21_1_1);

  
  decode_sel_Trip_22_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0055#, 14) ELSE
      '0';

  reg_enb_Trip_22_1_1 <= decode_sel_Trip_22_1_1 AND wr_enb;

  data_in_Trip_22 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_22_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_22_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_22_1_1 = '1' THEN
        data_reg_Trip_22_1_1 <= data_in_Trip_22;
      END IF;
    END IF;
  END PROCESS reg_Trip_22_1_1_process;


  write_Trip_22 <= std_logic_vector(data_reg_Trip_22_1_1);

  
  decode_sel_Trip_23_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0056#, 14) ELSE
      '0';

  reg_enb_Trip_23_1_1 <= decode_sel_Trip_23_1_1 AND wr_enb;

  data_in_Trip_23 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_23_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_23_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_23_1_1 = '1' THEN
        data_reg_Trip_23_1_1 <= data_in_Trip_23;
      END IF;
    END IF;
  END PROCESS reg_Trip_23_1_1_process;


  write_Trip_23 <= std_logic_vector(data_reg_Trip_23_1_1);

  
  decode_sel_Trip_24_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0057#, 14) ELSE
      '0';

  reg_enb_Trip_24_1_1 <= decode_sel_Trip_24_1_1 AND wr_enb;

  data_in_Trip_24 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_24_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_24_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_24_1_1 = '1' THEN
        data_reg_Trip_24_1_1 <= data_in_Trip_24;
      END IF;
    END IF;
  END PROCESS reg_Trip_24_1_1_process;


  write_Trip_24 <= std_logic_vector(data_reg_Trip_24_1_1);

  
  decode_sel_Trip_25_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0058#, 14) ELSE
      '0';

  reg_enb_Trip_25_1_1 <= decode_sel_Trip_25_1_1 AND wr_enb;

  data_in_Trip_25 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_25_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_25_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_25_1_1 = '1' THEN
        data_reg_Trip_25_1_1 <= data_in_Trip_25;
      END IF;
    END IF;
  END PROCESS reg_Trip_25_1_1_process;


  write_Trip_25 <= std_logic_vector(data_reg_Trip_25_1_1);

  
  decode_sel_Trip_26_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0059#, 14) ELSE
      '0';

  reg_enb_Trip_26_1_1 <= decode_sel_Trip_26_1_1 AND wr_enb;

  data_in_Trip_26 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_26_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_26_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_26_1_1 = '1' THEN
        data_reg_Trip_26_1_1 <= data_in_Trip_26;
      END IF;
    END IF;
  END PROCESS reg_Trip_26_1_1_process;


  write_Trip_26 <= std_logic_vector(data_reg_Trip_26_1_1);

  
  decode_sel_Trip_27_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005A#, 14) ELSE
      '0';

  reg_enb_Trip_27_1_1 <= decode_sel_Trip_27_1_1 AND wr_enb;

  data_in_Trip_27 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_27_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_27_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_27_1_1 = '1' THEN
        data_reg_Trip_27_1_1 <= data_in_Trip_27;
      END IF;
    END IF;
  END PROCESS reg_Trip_27_1_1_process;


  write_Trip_27 <= std_logic_vector(data_reg_Trip_27_1_1);

  
  decode_sel_Trip_28_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005B#, 14) ELSE
      '0';

  reg_enb_Trip_28_1_1 <= decode_sel_Trip_28_1_1 AND wr_enb;

  data_in_Trip_28 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_28_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_28_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_28_1_1 = '1' THEN
        data_reg_Trip_28_1_1 <= data_in_Trip_28;
      END IF;
    END IF;
  END PROCESS reg_Trip_28_1_1_process;


  write_Trip_28 <= std_logic_vector(data_reg_Trip_28_1_1);

  
  decode_sel_Trip_29_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005C#, 14) ELSE
      '0';

  reg_enb_Trip_29_1_1 <= decode_sel_Trip_29_1_1 AND wr_enb;

  data_in_Trip_29 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_29_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_29_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_29_1_1 = '1' THEN
        data_reg_Trip_29_1_1 <= data_in_Trip_29;
      END IF;
    END IF;
  END PROCESS reg_Trip_29_1_1_process;


  write_Trip_29 <= std_logic_vector(data_reg_Trip_29_1_1);

  
  decode_sel_Trip_30_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005D#, 14) ELSE
      '0';

  reg_enb_Trip_30_1_1 <= decode_sel_Trip_30_1_1 AND wr_enb;

  data_in_Trip_30 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_30_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_30_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_30_1_1 = '1' THEN
        data_reg_Trip_30_1_1 <= data_in_Trip_30;
      END IF;
    END IF;
  END PROCESS reg_Trip_30_1_1_process;


  write_Trip_30 <= std_logic_vector(data_reg_Trip_30_1_1);

  
  decode_sel_Trip_31_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005E#, 14) ELSE
      '0';

  reg_enb_Trip_31_1_1 <= decode_sel_Trip_31_1_1 AND wr_enb;

  data_in_Trip_31 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_31_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_31_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_31_1_1 = '1' THEN
        data_reg_Trip_31_1_1 <= data_in_Trip_31;
      END IF;
    END IF;
  END PROCESS reg_Trip_31_1_1_process;


  write_Trip_31 <= std_logic_vector(data_reg_Trip_31_1_1);

  
  decode_sel_Trip_32_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#005F#, 14) ELSE
      '0';

  reg_enb_Trip_32_1_1 <= decode_sel_Trip_32_1_1 AND wr_enb;

  data_in_Trip_32 <= signed(data_write_unsigned(15 DOWNTO 0));

  reg_Trip_32_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_Trip_32_1_1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_Trip_32_1_1 = '1' THEN
        data_reg_Trip_32_1_1 <= data_in_Trip_32;
      END IF;
    END IF;
  END PROCESS reg_Trip_32_1_1_process;


  write_Trip_32 <= std_logic_vector(data_reg_Trip_32_1_1);

  
  decode_sel_reset_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0060#, 14) ELSE
      '0';

  reg_enb_reset_1_1 <= decode_sel_reset_1_1 AND wr_enb;

  data_in_reset <= data_write_unsigned(0);

  reg_reset_1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      data_reg_reset_1_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_reset_1_1 = '1' THEN
        data_reg_reset_1_1 <= data_in_reset;
      END IF;
    END IF;
  END PROCESS reg_reset_1_1_process;


  write_concats_reset_1 <= data_reg_reset_1_1;

  write_reset <= write_concats_reset_1;

END rtl;

