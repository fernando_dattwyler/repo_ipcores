clear;
%hdlsetuptoolpath('ToolName','Xilinx Vivado','ToolPath',...
%'C:\Xilinx\Vivado\2020.1\bin\vivado.bat');
hdlsetuptoolpath('ToolName','Xilinx Vivado','ToolPath','C:\Xilinx\Vivado\2017.4\bin\vivado.bat');

T_fpga = 0.01e-6;
T_sim = 0.001e-6;
T_ipcore = 1e-7;
count  = 2000;
oversampling_factor = 10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLECS Simulation data
% Power and modulation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%		GENERAL PARAMETERS

Tsim = 11;		% Simulation time
Ts = 0.0001;	% Sample time

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%		RECTIFIER
% Grid 
Vg = 24*sqrt(2);
wg = 2*pi*50;
Lg = 1e-3;
Rlg = 1e-3;

% DC Bus
nc = 2;
C = 0.0441;
Rc = 1e-3;
Vdcref = 45;
Vdc0 = Vg;
Rcarga = 1;

% Carrier frequency
fsw = 5e3;

% Capacitor Voltage Controller
wn_Vdc 	= 2*pi*9;
xhi_Vdc 	= 0.7071;
Kp_Vdc 	= 2*xhi_Vdc*wn_Vdc*C-1/Rcarga;
Ki_Vdc 	= wn_Vdc^2*C;

% Input Current Controller
xhi_g = 0.7071;
wn_g = 2*pi*100;
Kp_g = 2*xhi_g*wn_g*Lg*2-Rlg;
Ki_g = wn_g*wn_g*Lg;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		MOSFET PARAMETERS
Ron = 1.6e-3;	% Ron mosfet


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%		PARALLEL CONVERTER

Rmod = 1e-3;
%Rmod_a = [0.9 1.1 1 1 1]*Rmod;
%Rmod_b = [1.1 1 0.9 1 1]*Rmod;
Rmod_a = Rmod;
Rmod_b = Rmod;

Lmod = 10e-6;
%Lmod_a = [1.1 1 0.9]*Lmod;
%Lmod_b = [0.9 1.1 1]*Lmod;
Lmod_a = Lmod;
Lmod_b = Lmod;

%Matrices segun numero de modulos

%m3 = [];
n = 2;
for i = 1:1:n-1
    for j = 1:1:n
        if j==i
            m3(i,j)=1;
        elseif j==i+1
            m3(i,j)=-1;
        end
    end
end
M3 = [ones(1,n) ; m3]


m = n-1;
M4 = M3^(-1);

%Parametros de entrada/salida
Io_ref = 1500*1.4142/11*n; 
Rout = 0.05e-3*5;

%Interleave Carrier Fsw
fc = 5e3;
fc_low = -1*ones(1,n);
fc_up  = 1*ones(1,n);
dphase = 1/fc/n;
%dphase = 0.000018; %para N=11
%dphase = 0.000015; %para N=13
fc_phase = 0:dphase:dphase*(n-1);
fc_phase_a = fc_phase;
fc_phase_b = fc_phase;

%Output Current controller
xhi = 0.7071;
wn = 2*pi*50; 
Kp = 2*xhi*wn*Lmod/2;
Ki = wn*wn*Lmod/2;

%Circulating Current Controller
wn2 = 2*pi*50;		%10
Kp2 = 2*xhi*wn2*Lmod/n;
Ki2 = wn2*wn2*Lmod/n;
w0 = 2*pi*50;