#ifndef __c4_PWM_28_HalfB_h__
#define __c4_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc4_PWM_28_HalfBInstanceStruct
#define typedef_SFc4_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c4_sfEvent;
  boolean_T c4_doneDoubleBufferReInit;
  uint8_T c4_is_active_c4_PWM_28_HalfB;
  uint8_T c4_JITStateAnimation[1];
  uint8_T c4_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c4_emlrtLocationLoggingDataTables[24];
  int32_T c4_IsDebuggerActive;
  int32_T c4_IsSequenceViewerPresent;
  int32_T c4_SequenceViewerOptimization;
  void *c4_RuntimeVar;
  emlrtLocationLoggingHistogramType c4_emlrtLocLogHistTables[24];
  boolean_T c4_emlrtLocLogSimulated;
  uint32_T c4_mlFcnLineNumber;
  void *c4_fcnDataPtrs[7];
  char_T *c4_dataNames[7];
  uint32_T c4_numFcnVars;
  uint32_T c4_ssIds[7];
  uint32_T c4_statuses[7];
  void *c4_outMexFcns[7];
  void *c4_inMexFcns[7];
  CovrtStateflowInstance *c4_covrtInstance;
  void *c4_fEmlrtCtx;
  uint8_T *c4_enable;
  int16_T *c4_offset;
  int16_T *c4_max;
  int16_T *c4_sum;
  uint16_T *c4_cont;
  uint8_T *c4_init;
  uint8_T *c4_out_init;
} SFc4_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc4_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c4_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c4_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c4_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
