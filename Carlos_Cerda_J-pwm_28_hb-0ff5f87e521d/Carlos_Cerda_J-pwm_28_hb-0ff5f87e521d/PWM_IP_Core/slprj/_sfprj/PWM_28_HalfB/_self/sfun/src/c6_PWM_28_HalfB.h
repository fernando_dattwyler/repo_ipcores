#ifndef __c6_PWM_28_HalfB_h__
#define __c6_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc6_PWM_28_HalfBInstanceStruct
#define typedef_SFc6_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c6_sfEvent;
  boolean_T c6_doneDoubleBufferReInit;
  uint8_T c6_is_active_c6_PWM_28_HalfB;
  uint8_T c6_JITStateAnimation[1];
  uint8_T c6_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c6_emlrtLocationLoggingDataTables[24];
  int32_T c6_IsDebuggerActive;
  int32_T c6_IsSequenceViewerPresent;
  int32_T c6_SequenceViewerOptimization;
  void *c6_RuntimeVar;
  emlrtLocationLoggingHistogramType c6_emlrtLocLogHistTables[24];
  boolean_T c6_emlrtLocLogSimulated;
  uint32_T c6_mlFcnLineNumber;
  void *c6_fcnDataPtrs[7];
  char_T *c6_dataNames[7];
  uint32_T c6_numFcnVars;
  uint32_T c6_ssIds[7];
  uint32_T c6_statuses[7];
  void *c6_outMexFcns[7];
  void *c6_inMexFcns[7];
  CovrtStateflowInstance *c6_covrtInstance;
  void *c6_fEmlrtCtx;
  uint8_T *c6_enable;
  int16_T *c6_offset;
  int16_T *c6_max;
  int16_T *c6_sum;
  uint16_T *c6_cont;
  uint8_T *c6_init;
  uint8_T *c6_out_init;
} SFc6_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc6_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c6_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c6_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c6_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
