/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c13_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c13_eml_mx;
static const mxArray *c13_b_eml_mx;
static const mxArray *c13_c_eml_mx;

/* Function Declarations */
static void initialize_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c13_update_jit_animation_state_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance);
static void c13_do_animation_call_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_st);
static void sf_gateway_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c13_emlrt_update_log_1(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static int16_T c13_emlrt_update_log_2(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static int16_T c13_emlrt_update_log_3(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static boolean_T c13_emlrt_update_log_4(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static uint16_T c13_emlrt_update_log_5(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static int32_T c13_emlrt_update_log_6(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index);
static void c13_emlrtInitVarDataTables(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c13_dataTables[24],
  emlrtLocationLoggingHistogramType c13_histTables[24]);
static uint16_T c13_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_b_cont, const
  char_T *c13_identifier);
static uint16_T c13_b_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_u, const
  emlrtMsgIdentifier *c13_parentId);
static uint8_T c13_c_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_b_out_init, const
  char_T *c13_identifier);
static uint8_T c13_d_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_u, const
  emlrtMsgIdentifier *c13_parentId);
static uint8_T c13_e_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_b_is_active_c13_PWM_28_HalfB, const char_T *
  c13_identifier);
static uint8_T c13_f_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId);
static const mxArray *c13_chart_data_browse_helper
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c13_ssIdNumber);
static int32_T c13__s32_add__(SFc13_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c13_b, int32_T c13_c, int32_T c13_EMLOvCount_src_loc, uint32_T
  c13_ssid_src_loc, int32_T c13_offset_src_loc, int32_T c13_length_src_loc);
static void init_dsm_address_info(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c13_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c13_st.tls = chartInstance->c13_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c13_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c13_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c13_is_active_c13_PWM_28_HalfB = 0U;
  sf_mex_assign(&c13_c_eml_mx, sf_mex_call(&c13_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c13_b_eml_mx, sf_mex_call(&c13_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c13_eml_mx, sf_mex_call(&c13_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c13_emlrtLocLogSimulated = false;
  c13_emlrtInitVarDataTables(chartInstance,
    chartInstance->c13_emlrtLocationLoggingDataTables,
    chartInstance->c13_emlrtLocLogHistTables);
}

static void initialize_params_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c13_update_jit_animation_state_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c13_do_animation_call_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c13_st;
  const mxArray *c13_y = NULL;
  const mxArray *c13_b_y = NULL;
  uint16_T c13_u;
  const mxArray *c13_c_y = NULL;
  const mxArray *c13_d_y = NULL;
  uint8_T c13_b_u;
  const mxArray *c13_e_y = NULL;
  const mxArray *c13_f_y = NULL;
  c13_st = NULL;
  c13_st = NULL;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_createcellmatrix(3, 1), false);
  c13_b_y = NULL;
  c13_u = *chartInstance->c13_cont;
  c13_c_y = NULL;
  sf_mex_assign(&c13_c_y, sf_mex_create("y", &c13_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c13_b_y, sf_mex_create_fi(sf_mex_dup(c13_eml_mx), sf_mex_dup
    (c13_b_eml_mx), "simulinkarray", c13_c_y, false, false), false);
  sf_mex_setcell(c13_y, 0, c13_b_y);
  c13_d_y = NULL;
  c13_b_u = *chartInstance->c13_out_init;
  c13_e_y = NULL;
  sf_mex_assign(&c13_e_y, sf_mex_create("y", &c13_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c13_d_y, sf_mex_create_fi(sf_mex_dup(c13_eml_mx), sf_mex_dup
    (c13_c_eml_mx), "simulinkarray", c13_e_y, false, false), false);
  sf_mex_setcell(c13_y, 1, c13_d_y);
  c13_f_y = NULL;
  sf_mex_assign(&c13_f_y, sf_mex_create("y",
    &chartInstance->c13_is_active_c13_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c13_y, 2, c13_f_y);
  sf_mex_assign(&c13_st, c13_y, false);
  return c13_st;
}

static void set_sim_state_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_st)
{
  emlrtStack c13_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c13_u;
  c13_b_st.tls = chartInstance->c13_fEmlrtCtx;
  chartInstance->c13_doneDoubleBufferReInit = true;
  c13_u = sf_mex_dup(c13_st);
  *chartInstance->c13_cont = c13_emlrt_marshallIn(chartInstance, &c13_b_st,
    sf_mex_dup(sf_mex_getcell(c13_u, 0)), "cont");
  *chartInstance->c13_out_init = c13_c_emlrt_marshallIn(chartInstance, &c13_b_st,
    sf_mex_dup(sf_mex_getcell(c13_u, 1)), "out_init");
  chartInstance->c13_is_active_c13_PWM_28_HalfB = c13_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c13_u, 2)),
     "is_active_c13_PWM_28_HalfB");
  sf_mex_destroy(&c13_u);
  sf_mex_destroy(&c13_st);
}

static void sf_gateway_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c13_PICOffset;
  uint8_T c13_b_enable;
  int16_T c13_b_offset;
  int16_T c13_b_max;
  int16_T c13_b_sum;
  uint8_T c13_b_init;
  uint8_T c13_a0;
  uint8_T c13_a;
  uint8_T c13_b_a0;
  uint8_T c13_a1;
  uint8_T c13_b_a1;
  boolean_T c13_c;
  int8_T c13_i;
  int8_T c13_i1;
  real_T c13_d;
  uint8_T c13_c_a0;
  uint16_T c13_b_cont;
  uint8_T c13_b_a;
  uint8_T c13_b_out_init;
  uint8_T c13_d_a0;
  uint8_T c13_c_a1;
  uint8_T c13_d_a1;
  boolean_T c13_b_c;
  int8_T c13_i2;
  int8_T c13_i3;
  real_T c13_d1;
  int16_T c13_varargin_1;
  int16_T c13_b_varargin_1;
  int16_T c13_c_varargin_1;
  int16_T c13_d_varargin_1;
  int16_T c13_var1;
  int16_T c13_b_var1;
  int16_T c13_i4;
  int16_T c13_i5;
  boolean_T c13_covSaturation;
  boolean_T c13_b_covSaturation;
  uint16_T c13_hfi;
  uint16_T c13_b_hfi;
  uint16_T c13_u;
  uint16_T c13_u1;
  int16_T c13_e_varargin_1;
  int16_T c13_f_varargin_1;
  int16_T c13_g_varargin_1;
  int16_T c13_h_varargin_1;
  int16_T c13_c_var1;
  int16_T c13_d_var1;
  int16_T c13_i6;
  int16_T c13_i7;
  boolean_T c13_c_covSaturation;
  boolean_T c13_d_covSaturation;
  uint16_T c13_c_hfi;
  uint16_T c13_d_hfi;
  uint16_T c13_u2;
  uint16_T c13_u3;
  uint16_T c13_e_a0;
  uint16_T c13_f_a0;
  uint16_T c13_b0;
  uint16_T c13_b_b0;
  uint16_T c13_c_a;
  uint16_T c13_d_a;
  uint16_T c13_b;
  uint16_T c13_b_b;
  uint16_T c13_g_a0;
  uint16_T c13_h_a0;
  uint16_T c13_c_b0;
  uint16_T c13_d_b0;
  uint16_T c13_e_a1;
  uint16_T c13_f_a1;
  uint16_T c13_b1;
  uint16_T c13_b_b1;
  uint16_T c13_g_a1;
  uint16_T c13_h_a1;
  uint16_T c13_c_b1;
  uint16_T c13_d_b1;
  boolean_T c13_c_c;
  boolean_T c13_d_c;
  int16_T c13_i8;
  int16_T c13_i9;
  int16_T c13_i10;
  int16_T c13_i11;
  int16_T c13_i12;
  int16_T c13_i13;
  int16_T c13_i14;
  int16_T c13_i15;
  int16_T c13_i16;
  int16_T c13_i17;
  int16_T c13_i18;
  int16_T c13_i19;
  int32_T c13_i20;
  int32_T c13_i21;
  int16_T c13_i22;
  int16_T c13_i23;
  int16_T c13_i24;
  int16_T c13_i25;
  int16_T c13_i26;
  int16_T c13_i27;
  int16_T c13_i28;
  int16_T c13_i29;
  int16_T c13_i30;
  int16_T c13_i31;
  int16_T c13_i32;
  int16_T c13_i33;
  int32_T c13_i34;
  int32_T c13_i35;
  int16_T c13_i36;
  int16_T c13_i37;
  int16_T c13_i38;
  int16_T c13_i39;
  int16_T c13_i40;
  int16_T c13_i41;
  int16_T c13_i42;
  int16_T c13_i43;
  real_T c13_d2;
  real_T c13_d3;
  int16_T c13_i_varargin_1;
  int16_T c13_i_a0;
  int16_T c13_j_varargin_1;
  int16_T c13_e_b0;
  int16_T c13_e_var1;
  int16_T c13_k_varargin_1;
  int16_T c13_i44;
  int16_T c13_v;
  boolean_T c13_e_covSaturation;
  int16_T c13_val;
  int16_T c13_c_b;
  int32_T c13_i45;
  uint16_T c13_e_hfi;
  real_T c13_d4;
  int32_T c13_i46;
  real_T c13_d5;
  real_T c13_d6;
  real_T c13_d7;
  int32_T c13_i47;
  int32_T c13_i48;
  int32_T c13_i49;
  real_T c13_d8;
  real_T c13_d9;
  int32_T c13_e_c;
  int32_T c13_l_varargin_1;
  int32_T c13_m_varargin_1;
  int32_T c13_f_var1;
  int32_T c13_i50;
  boolean_T c13_f_covSaturation;
  uint16_T c13_f_hfi;
  observerLogReadPIC(&c13_PICOffset);
  chartInstance->c13_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 4U, (real_T)
                    *chartInstance->c13_init);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 3U, (real_T)
                    *chartInstance->c13_sum);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 2U, (real_T)
                    *chartInstance->c13_max);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 1U, (real_T)
                    *chartInstance->c13_offset);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 0U, (real_T)
                    *chartInstance->c13_enable);
  chartInstance->c13_sfEvent = CALL_EVENT;
  c13_b_enable = *chartInstance->c13_enable;
  c13_b_offset = *chartInstance->c13_offset;
  c13_b_max = *chartInstance->c13_max;
  c13_b_sum = *chartInstance->c13_sum;
  c13_b_init = *chartInstance->c13_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c13_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c13_emlrt_update_log_1(chartInstance, c13_b_enable,
    chartInstance->c13_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c13_emlrt_update_log_2(chartInstance, c13_b_offset,
    chartInstance->c13_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c13_emlrt_update_log_3(chartInstance, c13_b_max,
    chartInstance->c13_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c13_emlrt_update_log_3(chartInstance, c13_b_sum,
    chartInstance->c13_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c13_emlrt_update_log_1(chartInstance, c13_b_init,
    chartInstance->c13_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c13_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c13_covrtInstance, 4U, 0, 0, false);
  c13_a0 = c13_b_enable;
  c13_a = c13_a0;
  c13_b_a0 = c13_a;
  c13_a1 = c13_b_a0;
  c13_b_a1 = c13_a1;
  c13_c = (c13_b_a1 == 0);
  c13_i = (int8_T)c13_b_enable;
  if ((int8_T)(c13_i & 2) != 0) {
    c13_i1 = (int8_T)(c13_i | -2);
  } else {
    c13_i1 = (int8_T)(c13_i & 1);
  }

  if (c13_i1 > 0) {
    c13_d = 3.0;
  } else {
    c13_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c13_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c13_covrtInstance,
        4U, 0U, 0U, c13_d, 0.0, -2, 0U, (int32_T)c13_emlrt_update_log_4
        (chartInstance, c13_c, chartInstance->c13_emlrtLocationLoggingDataTables,
         5)))) {
    c13_b_cont = c13_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c13_emlrtLocationLoggingDataTables, 6);
    c13_b_out_init = c13_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c13_emlrtLocationLoggingDataTables, 7);
  } else {
    c13_c_a0 = c13_b_init;
    c13_b_a = c13_c_a0;
    c13_d_a0 = c13_b_a;
    c13_c_a1 = c13_d_a0;
    c13_d_a1 = c13_c_a1;
    c13_b_c = (c13_d_a1 == 0);
    c13_i2 = (int8_T)c13_b_init;
    if ((int8_T)(c13_i2 & 2) != 0) {
      c13_i3 = (int8_T)(c13_i2 | -2);
    } else {
      c13_i3 = (int8_T)(c13_i2 & 1);
    }

    if (c13_i3 > 0) {
      c13_d1 = 3.0;
    } else {
      c13_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c13_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c13_covrtInstance, 4U, 0U, 1U, c13_d1,
                        0.0, -2, 0U, (int32_T)c13_emlrt_update_log_4
                        (chartInstance, c13_b_c,
                         chartInstance->c13_emlrtLocationLoggingDataTables, 8))))
    {
      c13_b_varargin_1 = c13_b_sum;
      c13_d_varargin_1 = c13_b_varargin_1;
      c13_b_var1 = c13_d_varargin_1;
      c13_i5 = c13_b_var1;
      c13_b_covSaturation = false;
      if (c13_i5 < 0) {
        c13_i5 = 0;
      } else {
        if (c13_i5 > 4095) {
          c13_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 0, 0,
          c13_b_covSaturation);
      }

      c13_b_hfi = (uint16_T)c13_i5;
      c13_u1 = c13_emlrt_update_log_5(chartInstance, c13_b_hfi,
        chartInstance->c13_emlrtLocationLoggingDataTables, 10);
      c13_f_varargin_1 = c13_b_max;
      c13_h_varargin_1 = c13_f_varargin_1;
      c13_d_var1 = c13_h_varargin_1;
      c13_i7 = c13_d_var1;
      c13_d_covSaturation = false;
      if (c13_i7 < 0) {
        c13_i7 = 0;
      } else {
        if (c13_i7 > 4095) {
          c13_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 1, 0,
          c13_d_covSaturation);
      }

      c13_d_hfi = (uint16_T)c13_i7;
      c13_u3 = c13_emlrt_update_log_5(chartInstance, c13_d_hfi,
        chartInstance->c13_emlrtLocationLoggingDataTables, 11);
      c13_f_a0 = c13_u1;
      c13_b_b0 = c13_u3;
      c13_d_a = c13_f_a0;
      c13_b_b = c13_b_b0;
      c13_h_a0 = c13_d_a;
      c13_d_b0 = c13_b_b;
      c13_f_a1 = c13_h_a0;
      c13_b_b1 = c13_d_b0;
      c13_h_a1 = c13_f_a1;
      c13_d_b1 = c13_b_b1;
      c13_d_c = (c13_h_a1 < c13_d_b1);
      c13_i9 = (int16_T)c13_u1;
      c13_i11 = (int16_T)c13_u3;
      c13_i13 = (int16_T)c13_u3;
      c13_i15 = (int16_T)c13_u1;
      if ((int16_T)(c13_i13 & 4096) != 0) {
        c13_i17 = (int16_T)(c13_i13 | -4096);
      } else {
        c13_i17 = (int16_T)(c13_i13 & 4095);
      }

      if ((int16_T)(c13_i15 & 4096) != 0) {
        c13_i19 = (int16_T)(c13_i15 | -4096);
      } else {
        c13_i19 = (int16_T)(c13_i15 & 4095);
      }

      c13_i21 = c13_i17 - c13_i19;
      if (c13_i21 > 4095) {
        c13_i21 = 4095;
      } else {
        if (c13_i21 < -4096) {
          c13_i21 = -4096;
        }
      }

      c13_i23 = (int16_T)c13_u1;
      c13_i25 = (int16_T)c13_u3;
      c13_i27 = (int16_T)c13_u1;
      c13_i29 = (int16_T)c13_u3;
      if ((int16_T)(c13_i27 & 4096) != 0) {
        c13_i31 = (int16_T)(c13_i27 | -4096);
      } else {
        c13_i31 = (int16_T)(c13_i27 & 4095);
      }

      if ((int16_T)(c13_i29 & 4096) != 0) {
        c13_i33 = (int16_T)(c13_i29 | -4096);
      } else {
        c13_i33 = (int16_T)(c13_i29 & 4095);
      }

      c13_i35 = c13_i31 - c13_i33;
      if (c13_i35 > 4095) {
        c13_i35 = 4095;
      } else {
        if (c13_i35 < -4096) {
          c13_i35 = -4096;
        }
      }

      if ((int16_T)(c13_i9 & 4096) != 0) {
        c13_i37 = (int16_T)(c13_i9 | -4096);
      } else {
        c13_i37 = (int16_T)(c13_i9 & 4095);
      }

      if ((int16_T)(c13_i11 & 4096) != 0) {
        c13_i39 = (int16_T)(c13_i11 | -4096);
      } else {
        c13_i39 = (int16_T)(c13_i11 & 4095);
      }

      if ((int16_T)(c13_i23 & 4096) != 0) {
        c13_i41 = (int16_T)(c13_i23 | -4096);
      } else {
        c13_i41 = (int16_T)(c13_i23 & 4095);
      }

      if ((int16_T)(c13_i25 & 4096) != 0) {
        c13_i43 = (int16_T)(c13_i25 | -4096);
      } else {
        c13_i43 = (int16_T)(c13_i25 & 4095);
      }

      if (c13_i37 < c13_i39) {
        c13_d3 = (real_T)((int16_T)c13_i21 <= 1);
      } else if (c13_i41 > c13_i43) {
        if ((int16_T)c13_i35 <= 1) {
          c13_d3 = 3.0;
        } else {
          c13_d3 = 0.0;
        }
      } else {
        c13_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c13_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c13_covrtInstance, 4U, 0U, 2U, c13_d3,
                          0.0, -2, 2U, (int32_T)c13_emlrt_update_log_4
                          (chartInstance, c13_d_c,
                           chartInstance->c13_emlrtLocationLoggingDataTables, 9))))
      {
        c13_i_a0 = c13_b_sum;
        c13_e_b0 = c13_b_offset;
        c13_k_varargin_1 = c13_e_b0;
        c13_v = c13_k_varargin_1;
        c13_val = c13_v;
        c13_c_b = c13_val;
        c13_i45 = c13_i_a0;
        if (c13_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d4 = 1.0;
          observerLog(225 + c13_PICOffset, &c13_d4, 1);
        }

        if (c13_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d5 = 1.0;
          observerLog(225 + c13_PICOffset, &c13_d5, 1);
        }

        c13_i46 = c13_c_b;
        if (c13_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d6 = 1.0;
          observerLog(228 + c13_PICOffset, &c13_d6, 1);
        }

        if (c13_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d7 = 1.0;
          observerLog(228 + c13_PICOffset, &c13_d7, 1);
        }

        if ((c13_i45 & 65536) != 0) {
          c13_i47 = c13_i45 | -65536;
        } else {
          c13_i47 = c13_i45 & 65535;
        }

        if ((c13_i46 & 65536) != 0) {
          c13_i48 = c13_i46 | -65536;
        } else {
          c13_i48 = c13_i46 & 65535;
        }

        c13_i49 = c13__s32_add__(chartInstance, c13_i47, c13_i48, 227, 1U, 323,
          12);
        if (c13_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d8 = 1.0;
          observerLog(233 + c13_PICOffset, &c13_d8, 1);
        }

        if (c13_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c13_d9 = 1.0;
          observerLog(233 + c13_PICOffset, &c13_d9, 1);
        }

        if ((c13_i49 & 65536) != 0) {
          c13_e_c = c13_i49 | -65536;
        } else {
          c13_e_c = c13_i49 & 65535;
        }

        c13_l_varargin_1 = c13_emlrt_update_log_6(chartInstance, c13_e_c,
          chartInstance->c13_emlrtLocationLoggingDataTables, 13);
        c13_m_varargin_1 = c13_l_varargin_1;
        c13_f_var1 = c13_m_varargin_1;
        c13_i50 = c13_f_var1;
        c13_f_covSaturation = false;
        if (c13_i50 < 0) {
          c13_i50 = 0;
        } else {
          if (c13_i50 > 4095) {
            c13_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 2, 0,
            c13_f_covSaturation);
        }

        c13_f_hfi = (uint16_T)c13_i50;
        c13_b_cont = c13_emlrt_update_log_5(chartInstance, c13_f_hfi,
          chartInstance->c13_emlrtLocationLoggingDataTables, 12);
        c13_b_out_init = c13_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 14);
      } else {
        c13_b_cont = c13_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 15);
        c13_b_out_init = c13_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c13_varargin_1 = c13_b_sum;
      c13_c_varargin_1 = c13_varargin_1;
      c13_var1 = c13_c_varargin_1;
      c13_i4 = c13_var1;
      c13_covSaturation = false;
      if (c13_i4 < 0) {
        c13_i4 = 0;
      } else {
        if (c13_i4 > 4095) {
          c13_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 3, 0,
          c13_covSaturation);
      }

      c13_hfi = (uint16_T)c13_i4;
      c13_u = c13_emlrt_update_log_5(chartInstance, c13_hfi,
        chartInstance->c13_emlrtLocationLoggingDataTables, 18);
      c13_e_varargin_1 = c13_b_max;
      c13_g_varargin_1 = c13_e_varargin_1;
      c13_c_var1 = c13_g_varargin_1;
      c13_i6 = c13_c_var1;
      c13_c_covSaturation = false;
      if (c13_i6 < 0) {
        c13_i6 = 0;
      } else {
        if (c13_i6 > 4095) {
          c13_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 4, 0,
          c13_c_covSaturation);
      }

      c13_c_hfi = (uint16_T)c13_i6;
      c13_u2 = c13_emlrt_update_log_5(chartInstance, c13_c_hfi,
        chartInstance->c13_emlrtLocationLoggingDataTables, 19);
      c13_e_a0 = c13_u;
      c13_b0 = c13_u2;
      c13_c_a = c13_e_a0;
      c13_b = c13_b0;
      c13_g_a0 = c13_c_a;
      c13_c_b0 = c13_b;
      c13_e_a1 = c13_g_a0;
      c13_b1 = c13_c_b0;
      c13_g_a1 = c13_e_a1;
      c13_c_b1 = c13_b1;
      c13_c_c = (c13_g_a1 < c13_c_b1);
      c13_i8 = (int16_T)c13_u;
      c13_i10 = (int16_T)c13_u2;
      c13_i12 = (int16_T)c13_u2;
      c13_i14 = (int16_T)c13_u;
      if ((int16_T)(c13_i12 & 4096) != 0) {
        c13_i16 = (int16_T)(c13_i12 | -4096);
      } else {
        c13_i16 = (int16_T)(c13_i12 & 4095);
      }

      if ((int16_T)(c13_i14 & 4096) != 0) {
        c13_i18 = (int16_T)(c13_i14 | -4096);
      } else {
        c13_i18 = (int16_T)(c13_i14 & 4095);
      }

      c13_i20 = c13_i16 - c13_i18;
      if (c13_i20 > 4095) {
        c13_i20 = 4095;
      } else {
        if (c13_i20 < -4096) {
          c13_i20 = -4096;
        }
      }

      c13_i22 = (int16_T)c13_u;
      c13_i24 = (int16_T)c13_u2;
      c13_i26 = (int16_T)c13_u;
      c13_i28 = (int16_T)c13_u2;
      if ((int16_T)(c13_i26 & 4096) != 0) {
        c13_i30 = (int16_T)(c13_i26 | -4096);
      } else {
        c13_i30 = (int16_T)(c13_i26 & 4095);
      }

      if ((int16_T)(c13_i28 & 4096) != 0) {
        c13_i32 = (int16_T)(c13_i28 | -4096);
      } else {
        c13_i32 = (int16_T)(c13_i28 & 4095);
      }

      c13_i34 = c13_i30 - c13_i32;
      if (c13_i34 > 4095) {
        c13_i34 = 4095;
      } else {
        if (c13_i34 < -4096) {
          c13_i34 = -4096;
        }
      }

      if ((int16_T)(c13_i8 & 4096) != 0) {
        c13_i36 = (int16_T)(c13_i8 | -4096);
      } else {
        c13_i36 = (int16_T)(c13_i8 & 4095);
      }

      if ((int16_T)(c13_i10 & 4096) != 0) {
        c13_i38 = (int16_T)(c13_i10 | -4096);
      } else {
        c13_i38 = (int16_T)(c13_i10 & 4095);
      }

      if ((int16_T)(c13_i22 & 4096) != 0) {
        c13_i40 = (int16_T)(c13_i22 | -4096);
      } else {
        c13_i40 = (int16_T)(c13_i22 & 4095);
      }

      if ((int16_T)(c13_i24 & 4096) != 0) {
        c13_i42 = (int16_T)(c13_i24 | -4096);
      } else {
        c13_i42 = (int16_T)(c13_i24 & 4095);
      }

      if (c13_i36 < c13_i38) {
        c13_d2 = (real_T)((int16_T)c13_i20 <= 1);
      } else if (c13_i40 > c13_i42) {
        if ((int16_T)c13_i34 <= 1) {
          c13_d2 = 3.0;
        } else {
          c13_d2 = 0.0;
        }
      } else {
        c13_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c13_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c13_covrtInstance, 4U, 0U, 3U, c13_d2,
                          0.0, -2, 2U, (int32_T)c13_emlrt_update_log_4
                          (chartInstance, c13_c_c,
                           chartInstance->c13_emlrtLocationLoggingDataTables, 17))))
      {
        c13_i_varargin_1 = c13_b_sum;
        c13_j_varargin_1 = c13_i_varargin_1;
        c13_e_var1 = c13_j_varargin_1;
        c13_i44 = c13_e_var1;
        c13_e_covSaturation = false;
        if (c13_i44 < 0) {
          c13_i44 = 0;
        } else {
          if (c13_i44 > 4095) {
            c13_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c13_covrtInstance, 4, 0, 5, 0,
            c13_e_covSaturation);
        }

        c13_e_hfi = (uint16_T)c13_i44;
        c13_b_cont = c13_emlrt_update_log_5(chartInstance, c13_e_hfi,
          chartInstance->c13_emlrtLocationLoggingDataTables, 20);
        c13_b_out_init = c13_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 21);
      } else {
        c13_b_cont = c13_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 22);
        c13_b_out_init = c13_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c13_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c13_cont = c13_b_cont;
  *chartInstance->c13_out_init = c13_b_out_init;
  c13_do_animation_call_c13_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 5U, (real_T)
                    *chartInstance->c13_cont);
  covrtSigUpdateFcn(chartInstance->c13_covrtInstance, 6U, (real_T)
                    *chartInstance->c13_out_init);
}

static void mdl_start_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c13_decisionTxtStartIdx = 0U;
  static const uint32_T c13_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c13_chart_data_browse_helper);
  chartInstance->c13_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c13_RuntimeVar,
    &chartInstance->c13_IsDebuggerActive,
    &chartInstance->c13_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c13_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c13_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c13_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c13_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c13_decisionTxtStartIdx, &c13_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c13_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c13_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c13_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c13_PWM_28_HalfB
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c13_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7507",              /* mexFileName */
    "Thu May 27 10:27:22 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c13_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c13_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c13_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c13_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7507");
    emlrtLocationLoggingPushLog(&c13_emlrtLocationLoggingFileInfo,
      c13_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c13_emlrtLocationLoggingDataTables, c13_emlrtLocationInfo,
      NULL, 0U, c13_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7507");
  }

  sfListenerLightTerminate(chartInstance->c13_RuntimeVar);
  sf_mex_destroy(&c13_eml_mx);
  sf_mex_destroy(&c13_b_eml_mx);
  sf_mex_destroy(&c13_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c13_covrtInstance);
}

static void initSimStructsc13_PWM_28_HalfB(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c13_emlrt_update_log_1(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  real_T c13_d;
  uint8_T c13_u;
  uint8_T c13_localMin;
  real_T c13_d1;
  uint8_T c13_u1;
  uint8_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_d = c13_b_table[0U].SimMin;
    if (c13_d < 2.0) {
      if (c13_d >= 0.0) {
        c13_u = (uint8_T)c13_d;
      } else {
        c13_u = 0U;
      }
    } else if (c13_d >= 2.0) {
      c13_u = 1U;
    } else {
      c13_u = 0U;
    }

    c13_localMin = c13_u;
    c13_d1 = c13_b_table[0U].SimMax;
    if (c13_d1 < 2.0) {
      if (c13_d1 >= 0.0) {
        c13_u1 = (uint8_T)c13_d1;
      } else {
        c13_u1 = 0U;
      }
    } else if (c13_d1 >= 2.0) {
      c13_u1 = 1U;
    } else {
      c13_u1 = 0U;
    }

    c13_localMax = c13_u1;
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c13_in < c13_localMin) {
      c13_localMin = c13_in;
    }

    if (c13_in > c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static int16_T c13_emlrt_update_log_2(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  real_T c13_d;
  int16_T c13_i;
  int16_T c13_localMin;
  real_T c13_d1;
  int16_T c13_i1;
  int16_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_d = muDoubleScalarFloor(c13_b_table[0U].SimMin);
    if (c13_d < 32768.0) {
      if (c13_d >= -32768.0) {
        c13_i = (int16_T)c13_d;
      } else {
        c13_i = MIN_int16_T;
      }
    } else if (c13_d >= 32768.0) {
      c13_i = MAX_int16_T;
    } else {
      c13_i = 0;
    }

    c13_localMin = c13_i;
    c13_d1 = muDoubleScalarFloor(c13_b_table[0U].SimMax);
    if (c13_d1 < 32768.0) {
      if (c13_d1 >= -32768.0) {
        c13_i1 = (int16_T)c13_d1;
      } else {
        c13_i1 = MIN_int16_T;
      }
    } else if (c13_d1 >= 32768.0) {
      c13_i1 = MAX_int16_T;
    } else {
      c13_i1 = 0;
    }

    c13_localMax = c13_i1;
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c13_in < c13_localMin) {
      c13_localMin = c13_in;
    }

    if (c13_in > c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static int16_T c13_emlrt_update_log_3(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  real_T c13_d;
  int16_T c13_i;
  int16_T c13_localMin;
  real_T c13_d1;
  int16_T c13_i1;
  int16_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_d = muDoubleScalarFloor(c13_b_table[0U].SimMin);
    if (c13_d < 2048.0) {
      if (c13_d >= -2048.0) {
        c13_i = (int16_T)c13_d;
      } else {
        c13_i = -2048;
      }
    } else if (c13_d >= 2048.0) {
      c13_i = 2047;
    } else {
      c13_i = 0;
    }

    c13_localMin = c13_i;
    c13_d1 = muDoubleScalarFloor(c13_b_table[0U].SimMax);
    if (c13_d1 < 2048.0) {
      if (c13_d1 >= -2048.0) {
        c13_i1 = (int16_T)c13_d1;
      } else {
        c13_i1 = -2048;
      }
    } else if (c13_d1 >= 2048.0) {
      c13_i1 = 2047;
    } else {
      c13_i1 = 0;
    }

    c13_localMax = c13_i1;
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c13_in < c13_localMin) {
      c13_localMin = c13_in;
    }

    if (c13_in > c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static boolean_T c13_emlrt_update_log_4(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  boolean_T c13_localMin;
  boolean_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_localMin = (c13_b_table[0U].SimMin > 0.0);
    c13_localMax = (c13_b_table[0U].SimMax > 0.0);
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c13_in < (int32_T)c13_localMin) {
      c13_localMin = c13_in;
    }

    if ((int32_T)c13_in > (int32_T)c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static uint16_T c13_emlrt_update_log_5(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  real_T c13_d;
  uint16_T c13_u;
  uint16_T c13_localMin;
  real_T c13_d1;
  uint16_T c13_u1;
  uint16_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_d = c13_b_table[0U].SimMin;
    if (c13_d < 4096.0) {
      if (c13_d >= 0.0) {
        c13_u = (uint16_T)c13_d;
      } else {
        c13_u = 0U;
      }
    } else if (c13_d >= 4096.0) {
      c13_u = 4095U;
    } else {
      c13_u = 0U;
    }

    c13_localMin = c13_u;
    c13_d1 = c13_b_table[0U].SimMax;
    if (c13_d1 < 4096.0) {
      if (c13_d1 >= 0.0) {
        c13_u1 = (uint16_T)c13_d1;
      } else {
        c13_u1 = 0U;
      }
    } else if (c13_d1 >= 4096.0) {
      c13_u1 = 4095U;
    } else {
      c13_u1 = 0U;
    }

    c13_localMax = c13_u1;
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c13_in < c13_localMin) {
      c13_localMin = c13_in;
    }

    if (c13_in > c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static int32_T c13_emlrt_update_log_6(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c13_in, emlrtLocationLoggingDataType c13_table[],
  int32_T c13_index)
{
  boolean_T c13_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c13_b_table;
  real_T c13_d;
  int32_T c13_i;
  int32_T c13_localMin;
  real_T c13_d1;
  int32_T c13_i1;
  int32_T c13_localMax;
  emlrtLocationLoggingHistogramType *c13_histTable;
  real_T c13_inDouble;
  real_T c13_significand;
  int32_T c13_exponent;
  (void)chartInstance;
  c13_isLoggingEnabledHere = (c13_index >= 0);
  if (c13_isLoggingEnabledHere) {
    c13_b_table = (emlrtLocationLoggingDataType *)&c13_table[c13_index];
    c13_d = muDoubleScalarFloor(c13_b_table[0U].SimMin);
    if (c13_d < 65536.0) {
      if (c13_d >= -65536.0) {
        c13_i = (int32_T)c13_d;
      } else {
        c13_i = -65536;
      }
    } else if (c13_d >= 65536.0) {
      c13_i = 65535;
    } else {
      c13_i = 0;
    }

    c13_localMin = c13_i;
    c13_d1 = muDoubleScalarFloor(c13_b_table[0U].SimMax);
    if (c13_d1 < 65536.0) {
      if (c13_d1 >= -65536.0) {
        c13_i1 = (int32_T)c13_d1;
      } else {
        c13_i1 = -65536;
      }
    } else if (c13_d1 >= 65536.0) {
      c13_i1 = 65535;
    } else {
      c13_i1 = 0;
    }

    c13_localMax = c13_i1;
    c13_histTable = c13_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c13_in < c13_localMin) {
      c13_localMin = c13_in;
    }

    if (c13_in > c13_localMax) {
      c13_localMax = c13_in;
    }

    /* Histogram logging. */
    c13_inDouble = (real_T)c13_in;
    c13_histTable->TotalNumberOfValues++;
    if (c13_inDouble == 0.0) {
      c13_histTable->NumberOfZeros++;
    } else {
      c13_histTable->SimSum += c13_inDouble;
      if ((!muDoubleScalarIsInf(c13_inDouble)) && (!muDoubleScalarIsNaN
           (c13_inDouble))) {
        c13_significand = frexp(c13_inDouble, &c13_exponent);
        if (c13_exponent > 128) {
          c13_exponent = 128;
        }

        if (c13_exponent < -127) {
          c13_exponent = -127;
        }

        if (c13_significand < 0.0) {
          c13_histTable->NumberOfNegativeValues++;
          c13_histTable->HistogramOfNegativeValues[127 + c13_exponent]++;
        } else {
          c13_histTable->NumberOfPositiveValues++;
          c13_histTable->HistogramOfPositiveValues[127 + c13_exponent]++;
        }
      }
    }

    c13_b_table[0U].SimMin = (real_T)c13_localMin;
    c13_b_table[0U].SimMax = (real_T)c13_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c13_in;
}

static void c13_emlrtInitVarDataTables(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c13_dataTables[24],
  emlrtLocationLoggingHistogramType c13_histTables[24])
{
  int32_T c13_i;
  int32_T c13_iH;
  (void)chartInstance;
  for (c13_i = 0; c13_i < 24; c13_i++) {
    c13_dataTables[c13_i].SimMin = rtInf;
    c13_dataTables[c13_i].SimMax = rtMinusInf;
    c13_dataTables[c13_i].OverflowWraps = 0;
    c13_dataTables[c13_i].Saturations = 0;
    c13_dataTables[c13_i].IsAlwaysInteger = true;
    c13_dataTables[c13_i].HistogramTable = &c13_histTables[c13_i];
    c13_histTables[c13_i].NumberOfZeros = 0.0;
    c13_histTables[c13_i].NumberOfPositiveValues = 0.0;
    c13_histTables[c13_i].NumberOfNegativeValues = 0.0;
    c13_histTables[c13_i].TotalNumberOfValues = 0.0;
    c13_histTables[c13_i].SimSum = 0.0;
    for (c13_iH = 0; c13_iH < 256; c13_iH++) {
      c13_histTables[c13_i].HistogramOfPositiveValues[c13_iH] = 0.0;
      c13_histTables[c13_i].HistogramOfNegativeValues[c13_iH] = 0.0;
    }
  }
}

const mxArray *sf_c13_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c13_nameCaptureInfo = NULL;
  c13_nameCaptureInfo = NULL;
  sf_mex_assign(&c13_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c13_nameCaptureInfo;
}

static uint16_T c13_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_b_cont, const
  char_T *c13_identifier)
{
  uint16_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = (const char *)c13_identifier;
  c13_thisId.fParent = NULL;
  c13_thisId.bParentIsCell = false;
  c13_y = c13_b_emlrt_marshallIn(chartInstance, c13_sp, sf_mex_dup(c13_b_cont),
    &c13_thisId);
  sf_mex_destroy(&c13_b_cont);
  return c13_y;
}

static uint16_T c13_b_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_u, const
  emlrtMsgIdentifier *c13_parentId)
{
  uint16_T c13_y;
  const mxArray *c13_mxFi = NULL;
  const mxArray *c13_mxInt = NULL;
  uint16_T c13_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c13_parentId, c13_u, false, 0U, NULL, c13_eml_mx, c13_b_eml_mx);
  sf_mex_assign(&c13_mxFi, sf_mex_dup(c13_u), false);
  sf_mex_assign(&c13_mxInt, sf_mex_call(c13_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c13_mxFi)), false);
  sf_mex_import(c13_parentId, sf_mex_dup(c13_mxInt), &c13_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c13_mxFi);
  sf_mex_destroy(&c13_mxInt);
  c13_y = c13_b_u;
  sf_mex_destroy(&c13_mxFi);
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static uint8_T c13_c_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_b_out_init, const
  char_T *c13_identifier)
{
  uint8_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = (const char *)c13_identifier;
  c13_thisId.fParent = NULL;
  c13_thisId.bParentIsCell = false;
  c13_y = c13_d_emlrt_marshallIn(chartInstance, c13_sp, sf_mex_dup
    (c13_b_out_init), &c13_thisId);
  sf_mex_destroy(&c13_b_out_init);
  return c13_y;
}

static uint8_T c13_d_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c13_sp, const mxArray *c13_u, const
  emlrtMsgIdentifier *c13_parentId)
{
  uint8_T c13_y;
  const mxArray *c13_mxFi = NULL;
  const mxArray *c13_mxInt = NULL;
  uint8_T c13_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c13_parentId, c13_u, false, 0U, NULL, c13_eml_mx, c13_c_eml_mx);
  sf_mex_assign(&c13_mxFi, sf_mex_dup(c13_u), false);
  sf_mex_assign(&c13_mxInt, sf_mex_call(c13_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c13_mxFi)), false);
  sf_mex_import(c13_parentId, sf_mex_dup(c13_mxInt), &c13_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c13_mxFi);
  sf_mex_destroy(&c13_mxInt);
  c13_y = c13_b_u;
  sf_mex_destroy(&c13_mxFi);
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static uint8_T c13_e_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_b_is_active_c13_PWM_28_HalfB, const char_T *
  c13_identifier)
{
  uint8_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = (const char *)c13_identifier;
  c13_thisId.fParent = NULL;
  c13_thisId.bParentIsCell = false;
  c13_y = c13_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c13_b_is_active_c13_PWM_28_HalfB), &c13_thisId);
  sf_mex_destroy(&c13_b_is_active_c13_PWM_28_HalfB);
  return c13_y;
}

static uint8_T c13_f_emlrt_marshallIn(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId)
{
  uint8_T c13_y;
  uint8_T c13_b_u;
  (void)chartInstance;
  sf_mex_import(c13_parentId, sf_mex_dup(c13_u), &c13_b_u, 1, 3, 0U, 0, 0U, 0);
  c13_y = c13_b_u;
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static const mxArray *c13_chart_data_browse_helper
  (SFc13_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c13_ssIdNumber)
{
  const mxArray *c13_mxData = NULL;
  uint8_T c13_u;
  int16_T c13_i;
  int16_T c13_i1;
  int16_T c13_i2;
  uint16_T c13_u1;
  uint8_T c13_u2;
  uint8_T c13_u3;
  real_T c13_d;
  real_T c13_d1;
  real_T c13_d2;
  real_T c13_d3;
  real_T c13_d4;
  real_T c13_d5;
  c13_mxData = NULL;
  switch (c13_ssIdNumber) {
   case 18U:
    c13_u = *chartInstance->c13_enable;
    c13_d = (real_T)c13_u;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c13_i = *chartInstance->c13_offset;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c13_i1 = *chartInstance->c13_max;
    c13_d1 = (real_T)c13_i1;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c13_i2 = *chartInstance->c13_sum;
    c13_d2 = (real_T)c13_i2;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c13_u1 = *chartInstance->c13_cont;
    c13_d3 = (real_T)c13_u1;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c13_u2 = *chartInstance->c13_init;
    c13_d4 = (real_T)c13_u2;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c13_u3 = *chartInstance->c13_out_init;
    c13_d5 = (real_T)c13_u3;
    sf_mex_assign(&c13_mxData, sf_mex_create("mxData", &c13_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c13_mxData;
}

static int32_T c13__s32_add__(SFc13_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c13_b, int32_T c13_c, int32_T c13_EMLOvCount_src_loc, uint32_T
  c13_ssid_src_loc, int32_T c13_offset_src_loc, int32_T c13_length_src_loc)
{
  int32_T c13_a;
  int32_T c13_PICOffset;
  real_T c13_d;
  observerLogReadPIC(&c13_PICOffset);
  c13_a = c13_b + c13_c;
  if (((c13_a ^ c13_b) & (c13_a ^ c13_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c13_ssid_src_loc,
      c13_offset_src_loc, c13_length_src_loc);
    c13_d = 1.0;
    observerLog(c13_EMLOvCount_src_loc + c13_PICOffset, &c13_d, 1);
  }

  return c13_a;
}

static void init_dsm_address_info(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc13_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c13_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c13_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c13_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c13_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c13_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c13_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c13_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c13_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c13_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c13_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c13_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c13_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c13_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c13_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoXF8QLhvvJFFv"
    "EdiTpoTwlwQAADrmCCQ"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c13_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c13_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c13_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c13_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc13_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c13_PWM_28_HalfB
      ((SFc13_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c13_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c13_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc13_PWM_28_HalfB((SFc13_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c13_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5SNDEcLopEBRods0qiJM28KpxTEm1AKlWQudnJ4yHT+JAwxlmfmS758gl0hN",
    "02UUv0F0XRW9QoEfIG4qSZYqk4igxUqADUMQMv/fN+5+RV+v0PByb+Lz60vOu4vsaPnVvOjayeW",
    "3hma43vG+z+WMUEjbuE0Vi7VUOQWJ4Blpya5gUHTGUhTAmhqBAUMQmUpkyNs1iy5kYt62gjk+/j",
    "BiNgkhaHu6hLAkPBD9FtsSaPvI0mQJq2gChiZS0o6jNyWiusTLHfgR0rG1cZYIGE9jEqaV7lhuW",
    "cGidAO0IbQhqrM90Cwwx4JuTUjOdpTqYAWWccEZEobUR0QEk6GADz5MQfw+sQaPyMBoRZfYgIhP",
    "QXTZOOaWAPCfT+OGICWKkYoS3Yu47wWXd+hz16ckQeIVDULc9BWScSCZMefyDNlraEuSIQxOO7K",
    "icLYDX1gX/BYNjUKV+G/pyAoqM4ECUbpo6pHWSRmueJcsww2J4QdQTivHTEJZmL2aODgjGCQ5Ro",
    "gwGqZEdfajYBN1bymbjjsvMVSVj42mw9SpYytaaQFUU5mxtKnzCuS6FHcqkCxPgKWuTGFINm7IW",
    "47Rm4aFEB7v0Lq8GKxgGPoP5UoSsMFyTHCDtOz9hYzmPpFYbGfuYvM1ud/nzMqwjDKghoVDUBRR",
    "hGtBnqXvL2UKmXewRiFqZVL0i8DRDVqE8PbSieSzVGH1S0UTOTHARLQXGeoSxxEp4rrFoqmAulq",
    "twlNAIQtdgGIcelg1iC3yiXWt7gnU3Yea0CZoqlhRF1Z0/972z8+fGe5w/M7n8++4CT62Ax1t4O",
    "/zOAv56/Tx+I7dvfbbmRia/uyB/M7dfIyfvcFuuct78/svbW/988dfTX//o/A0mb39ej9qSHjVv",
    "tn9y5WLn9mY2/3rWIOcJP1nKM4fdX9CrUcD/1QL/VjbX3+23g9741fbrZ9vwozL851iJR37K91u",
    "9Wt8rOX1n63dcpz5N0r6rFe2E2YXCzYmdHrP5eF5d4Y/r2fp0/Pt4Pfl7u/k4Fvmrcc5fDY9KYT",
    "ZL8vFy9f9+Ny9fpP+1XLzdXFozYIJ9JDvu7K4nP92/v8KO2zk7bqf3igFx3QoGdPvhoP+yN3iwM",
    "9gnfLi33Gc+tF4vKuddstx/Rc///fLp7Xufc3jjA+Xqa577lyW3rn0XvY98bviq88zL4bc+YzvW",
    "vSd+avyf3sXucd9k8x/mf7H8iPGw4Ladfe4CGRZ9vQT73gEYVqB9",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c13_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c13_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c13_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c13_PWM_28_HalfB(SimStruct *S)
{
  SFc13_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc13_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc13_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc13_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c13_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c13_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c13_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c13_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c13_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c13_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c13_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c13_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c13_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c13_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c13_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c13_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c13_JITStateAnimation,
    chartInstance->c13_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c13_PWM_28_HalfB(chartInstance);
}

void c13_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c13_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c13_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c13_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c13_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
