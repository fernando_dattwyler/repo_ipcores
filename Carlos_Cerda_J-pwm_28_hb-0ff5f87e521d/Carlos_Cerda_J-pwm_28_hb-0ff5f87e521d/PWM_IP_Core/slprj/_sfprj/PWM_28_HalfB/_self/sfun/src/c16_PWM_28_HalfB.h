#ifndef __c16_PWM_28_HalfB_h__
#define __c16_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc16_PWM_28_HalfBInstanceStruct
#define typedef_SFc16_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c16_sfEvent;
  boolean_T c16_doneDoubleBufferReInit;
  uint8_T c16_is_active_c16_PWM_28_HalfB;
  uint8_T c16_JITStateAnimation[1];
  uint8_T c16_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c16_emlrtLocationLoggingDataTables[24];
  int32_T c16_IsDebuggerActive;
  int32_T c16_IsSequenceViewerPresent;
  int32_T c16_SequenceViewerOptimization;
  void *c16_RuntimeVar;
  emlrtLocationLoggingHistogramType c16_emlrtLocLogHistTables[24];
  boolean_T c16_emlrtLocLogSimulated;
  uint32_T c16_mlFcnLineNumber;
  void *c16_fcnDataPtrs[7];
  char_T *c16_dataNames[7];
  uint32_T c16_numFcnVars;
  uint32_T c16_ssIds[7];
  uint32_T c16_statuses[7];
  void *c16_outMexFcns[7];
  void *c16_inMexFcns[7];
  CovrtStateflowInstance *c16_covrtInstance;
  void *c16_fEmlrtCtx;
  uint8_T *c16_enable;
  int16_T *c16_offset;
  int16_T *c16_max;
  int16_T *c16_sum;
  uint16_T *c16_cont;
  uint8_T *c16_init;
  uint8_T *c16_out_init;
} SFc16_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc16_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c16_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c16_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c16_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
