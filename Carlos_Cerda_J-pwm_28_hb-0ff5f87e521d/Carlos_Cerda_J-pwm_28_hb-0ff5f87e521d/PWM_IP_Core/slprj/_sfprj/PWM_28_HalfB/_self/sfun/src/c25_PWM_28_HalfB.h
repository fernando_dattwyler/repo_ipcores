#ifndef __c25_PWM_28_HalfB_h__
#define __c25_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc25_PWM_28_HalfBInstanceStruct
#define typedef_SFc25_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c25_sfEvent;
  boolean_T c25_doneDoubleBufferReInit;
  uint8_T c25_is_active_c25_PWM_28_HalfB;
  uint8_T c25_JITStateAnimation[1];
  uint8_T c25_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c25_emlrtLocationLoggingDataTables[24];
  int32_T c25_IsDebuggerActive;
  int32_T c25_IsSequenceViewerPresent;
  int32_T c25_SequenceViewerOptimization;
  void *c25_RuntimeVar;
  emlrtLocationLoggingHistogramType c25_emlrtLocLogHistTables[24];
  boolean_T c25_emlrtLocLogSimulated;
  uint32_T c25_mlFcnLineNumber;
  void *c25_fcnDataPtrs[7];
  char_T *c25_dataNames[7];
  uint32_T c25_numFcnVars;
  uint32_T c25_ssIds[7];
  uint32_T c25_statuses[7];
  void *c25_outMexFcns[7];
  void *c25_inMexFcns[7];
  CovrtStateflowInstance *c25_covrtInstance;
  void *c25_fEmlrtCtx;
  uint8_T *c25_enable;
  int16_T *c25_offset;
  int16_T *c25_max;
  int16_T *c25_sum;
  uint16_T *c25_cont;
  uint8_T *c25_init;
  uint8_T *c25_out_init;
} SFc25_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc25_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c25_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c25_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c25_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
