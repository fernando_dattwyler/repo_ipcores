#ifndef __c13_PWM_28_HalfB_h__
#define __c13_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc13_PWM_28_HalfBInstanceStruct
#define typedef_SFc13_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c13_sfEvent;
  boolean_T c13_doneDoubleBufferReInit;
  uint8_T c13_is_active_c13_PWM_28_HalfB;
  uint8_T c13_JITStateAnimation[1];
  uint8_T c13_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c13_emlrtLocationLoggingDataTables[24];
  int32_T c13_IsDebuggerActive;
  int32_T c13_IsSequenceViewerPresent;
  int32_T c13_SequenceViewerOptimization;
  void *c13_RuntimeVar;
  emlrtLocationLoggingHistogramType c13_emlrtLocLogHistTables[24];
  boolean_T c13_emlrtLocLogSimulated;
  uint32_T c13_mlFcnLineNumber;
  void *c13_fcnDataPtrs[7];
  char_T *c13_dataNames[7];
  uint32_T c13_numFcnVars;
  uint32_T c13_ssIds[7];
  uint32_T c13_statuses[7];
  void *c13_outMexFcns[7];
  void *c13_inMexFcns[7];
  CovrtStateflowInstance *c13_covrtInstance;
  void *c13_fEmlrtCtx;
  uint8_T *c13_enable;
  int16_T *c13_offset;
  int16_T *c13_max;
  int16_T *c13_sum;
  uint16_T *c13_cont;
  uint8_T *c13_init;
  uint8_T *c13_out_init;
} SFc13_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc13_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c13_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c13_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c13_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
