/*
 * File Name:         C:\Users\ccerd\Desktop\ip_fernando\Trips_v1\trip_v3\ipcore\Trip_ip_v3_0\include\Trip_ip_addr.h
 * Description:       C Header File
 * Created:           2021-11-15 10:39:13
*/

#ifndef TRIP_IP_H_
#define TRIP_IP_H_

#define  IPCore_Reset_Trip_ip                0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Trip_ip               0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Trip_ip            0x8  //contains unique IP timestamp (yymmddHHMM): 2111151039
#define  Trip_1_Data_Trip_ip                 0x100  //data register for Inport Trip_1
#define  Trip_2_Data_Trip_ip                 0x104  //data register for Inport Trip_2
#define  Trip_3_Data_Trip_ip                 0x108  //data register for Inport Trip_3
#define  Trip_4_Data_Trip_ip                 0x10C  //data register for Inport Trip_4
#define  Trip_5_Data_Trip_ip                 0x110  //data register for Inport Trip_5
#define  Trip_6_Data_Trip_ip                 0x114  //data register for Inport Trip_6
#define  Trip_7_Data_Trip_ip                 0x118  //data register for Inport Trip_7
#define  Trip_8_Data_Trip_ip                 0x11C  //data register for Inport Trip_8
#define  Trip_9_Data_Trip_ip                 0x120  //data register for Inport Trip_9
#define  Trip_10_Data_Trip_ip                0x124  //data register for Inport Trip_10
#define  Trip_11_Data_Trip_ip                0x128  //data register for Inport Trip_11
#define  Trip_12_Data_Trip_ip                0x12C  //data register for Inport Trip_12
#define  Trip_13_Data_Trip_ip                0x130  //data register for Inport Trip_13
#define  Trip_14_Data_Trip_ip                0x134  //data register for Inport Trip_14
#define  Trip_15_Data_Trip_ip                0x138  //data register for Inport Trip_15
#define  Trip_16_Data_Trip_ip                0x13C  //data register for Inport Trip_16
#define  Trip_17_Data_Trip_ip                0x140  //data register for Inport Trip_17
#define  Trip_18_Data_Trip_ip                0x144  //data register for Inport Trip_18
#define  Trip_19_Data_Trip_ip                0x148  //data register for Inport Trip_19
#define  Trip_20_Data_Trip_ip                0x14C  //data register for Inport Trip_20
#define  Trip_21_Data_Trip_ip                0x150  //data register for Inport Trip_21
#define  Trip_22_Data_Trip_ip                0x154  //data register for Inport Trip_22
#define  Trip_23_Data_Trip_ip                0x158  //data register for Inport Trip_23
#define  Trip_24_Data_Trip_ip                0x15C  //data register for Inport Trip_24
#define  Trip_25_Data_Trip_ip                0x160  //data register for Inport Trip_25
#define  Trip_26_Data_Trip_ip                0x164  //data register for Inport Trip_26
#define  Trip_27_Data_Trip_ip                0x168  //data register for Inport Trip_27
#define  Trip_28_Data_Trip_ip                0x16C  //data register for Inport Trip_28
#define  Trip_29_Data_Trip_ip                0x170  //data register for Inport Trip_29
#define  Trip_30_Data_Trip_ip                0x174  //data register for Inport Trip_30
#define  Trip_31_Data_Trip_ip                0x178  //data register for Inport Trip_31
#define  Trip_32_Data_Trip_ip                0x17C  //data register for Inport Trip_32
#define  Salida_Trips_procesa_Data_Trip_ip   0x180  //data register for Outport Salida_Trips_procesa

#endif /* TRIP_IP_H_ */
