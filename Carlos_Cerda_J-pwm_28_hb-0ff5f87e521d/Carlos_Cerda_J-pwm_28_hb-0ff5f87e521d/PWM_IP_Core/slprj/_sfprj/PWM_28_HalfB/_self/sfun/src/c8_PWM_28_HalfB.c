/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c8_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c8_eml_mx;
static const mxArray *c8_b_eml_mx;
static const mxArray *c8_c_eml_mx;

/* Function Declarations */
static void initialize_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c8_update_jit_animation_state_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance);
static void c8_do_animation_call_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_st);
static void sf_gateway_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c8_emlrt_update_log_1(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static int16_T c8_emlrt_update_log_2(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static int16_T c8_emlrt_update_log_3(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static boolean_T c8_emlrt_update_log_4(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static uint16_T c8_emlrt_update_log_5(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static int32_T c8_emlrt_update_log_6(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index);
static void c8_emlrtInitVarDataTables(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c8_dataTables[24],
  emlrtLocationLoggingHistogramType c8_histTables[24]);
static uint16_T c8_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_b_cont, const
  char_T *c8_identifier);
static uint16_T c8_b_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_u, const
  emlrtMsgIdentifier *c8_parentId);
static uint8_T c8_c_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_b_out_init, const
  char_T *c8_identifier);
static uint8_T c8_d_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_u, const
  emlrtMsgIdentifier *c8_parentId);
static uint8_T c8_e_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_b_is_active_c8_PWM_28_HalfB, const char_T
  *c8_identifier);
static uint8_T c8_f_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static const mxArray *c8_chart_data_browse_helper
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c8_ssIdNumber);
static int32_T c8__s32_add__(SFc8_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c8_b, int32_T c8_c, int32_T c8_EMLOvCount_src_loc, uint32_T
  c8_ssid_src_loc, int32_T c8_offset_src_loc, int32_T c8_length_src_loc);
static void init_dsm_address_info(SFc8_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c8_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c8_st.tls = chartInstance->c8_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c8_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c8_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c8_is_active_c8_PWM_28_HalfB = 0U;
  sf_mex_assign(&c8_c_eml_mx, sf_mex_call(&c8_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c8_b_eml_mx, sf_mex_call(&c8_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c8_eml_mx, sf_mex_call(&c8_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c8_emlrtLocLogSimulated = false;
  c8_emlrtInitVarDataTables(chartInstance,
    chartInstance->c8_emlrtLocationLoggingDataTables,
    chartInstance->c8_emlrtLocLogHistTables);
}

static void initialize_params_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c8_update_jit_animation_state_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c8_do_animation_call_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c8_st;
  const mxArray *c8_y = NULL;
  const mxArray *c8_b_y = NULL;
  uint16_T c8_u;
  const mxArray *c8_c_y = NULL;
  const mxArray *c8_d_y = NULL;
  uint8_T c8_b_u;
  const mxArray *c8_e_y = NULL;
  const mxArray *c8_f_y = NULL;
  c8_st = NULL;
  c8_st = NULL;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createcellmatrix(3, 1), false);
  c8_b_y = NULL;
  c8_u = *chartInstance->c8_cont;
  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c8_b_y, sf_mex_create_fi(sf_mex_dup(c8_eml_mx), sf_mex_dup
    (c8_b_eml_mx), "simulinkarray", c8_c_y, false, false), false);
  sf_mex_setcell(c8_y, 0, c8_b_y);
  c8_d_y = NULL;
  c8_b_u = *chartInstance->c8_out_init;
  c8_e_y = NULL;
  sf_mex_assign(&c8_e_y, sf_mex_create("y", &c8_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c8_d_y, sf_mex_create_fi(sf_mex_dup(c8_eml_mx), sf_mex_dup
    (c8_c_eml_mx), "simulinkarray", c8_e_y, false, false), false);
  sf_mex_setcell(c8_y, 1, c8_d_y);
  c8_f_y = NULL;
  sf_mex_assign(&c8_f_y, sf_mex_create("y",
    &chartInstance->c8_is_active_c8_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c8_y, 2, c8_f_y);
  sf_mex_assign(&c8_st, c8_y, false);
  return c8_st;
}

static void set_sim_state_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_st)
{
  emlrtStack c8_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c8_u;
  c8_b_st.tls = chartInstance->c8_fEmlrtCtx;
  chartInstance->c8_doneDoubleBufferReInit = true;
  c8_u = sf_mex_dup(c8_st);
  *chartInstance->c8_cont = c8_emlrt_marshallIn(chartInstance, &c8_b_st,
    sf_mex_dup(sf_mex_getcell(c8_u, 0)), "cont");
  *chartInstance->c8_out_init = c8_c_emlrt_marshallIn(chartInstance, &c8_b_st,
    sf_mex_dup(sf_mex_getcell(c8_u, 1)), "out_init");
  chartInstance->c8_is_active_c8_PWM_28_HalfB = c8_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c8_u, 2)),
     "is_active_c8_PWM_28_HalfB");
  sf_mex_destroy(&c8_u);
  sf_mex_destroy(&c8_st);
}

static void sf_gateway_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c8_PICOffset;
  uint8_T c8_b_enable;
  int16_T c8_b_offset;
  int16_T c8_b_max;
  int16_T c8_b_sum;
  uint8_T c8_b_init;
  uint8_T c8_a0;
  uint8_T c8_a;
  uint8_T c8_b_a0;
  uint8_T c8_a1;
  uint8_T c8_b_a1;
  boolean_T c8_c;
  int8_T c8_i;
  int8_T c8_i1;
  real_T c8_d;
  uint8_T c8_c_a0;
  uint16_T c8_b_cont;
  uint8_T c8_b_a;
  uint8_T c8_b_out_init;
  uint8_T c8_d_a0;
  uint8_T c8_c_a1;
  uint8_T c8_d_a1;
  boolean_T c8_b_c;
  int8_T c8_i2;
  int8_T c8_i3;
  real_T c8_d1;
  int16_T c8_varargin_1;
  int16_T c8_b_varargin_1;
  int16_T c8_c_varargin_1;
  int16_T c8_d_varargin_1;
  int16_T c8_var1;
  int16_T c8_b_var1;
  int16_T c8_i4;
  int16_T c8_i5;
  boolean_T c8_covSaturation;
  boolean_T c8_b_covSaturation;
  uint16_T c8_hfi;
  uint16_T c8_b_hfi;
  uint16_T c8_u;
  uint16_T c8_u1;
  int16_T c8_e_varargin_1;
  int16_T c8_f_varargin_1;
  int16_T c8_g_varargin_1;
  int16_T c8_h_varargin_1;
  int16_T c8_c_var1;
  int16_T c8_d_var1;
  int16_T c8_i6;
  int16_T c8_i7;
  boolean_T c8_c_covSaturation;
  boolean_T c8_d_covSaturation;
  uint16_T c8_c_hfi;
  uint16_T c8_d_hfi;
  uint16_T c8_u2;
  uint16_T c8_u3;
  uint16_T c8_e_a0;
  uint16_T c8_f_a0;
  uint16_T c8_b0;
  uint16_T c8_b_b0;
  uint16_T c8_c_a;
  uint16_T c8_d_a;
  uint16_T c8_b;
  uint16_T c8_b_b;
  uint16_T c8_g_a0;
  uint16_T c8_h_a0;
  uint16_T c8_c_b0;
  uint16_T c8_d_b0;
  uint16_T c8_e_a1;
  uint16_T c8_f_a1;
  uint16_T c8_b1;
  uint16_T c8_b_b1;
  uint16_T c8_g_a1;
  uint16_T c8_h_a1;
  uint16_T c8_c_b1;
  uint16_T c8_d_b1;
  boolean_T c8_c_c;
  boolean_T c8_d_c;
  int16_T c8_i8;
  int16_T c8_i9;
  int16_T c8_i10;
  int16_T c8_i11;
  int16_T c8_i12;
  int16_T c8_i13;
  int16_T c8_i14;
  int16_T c8_i15;
  int16_T c8_i16;
  int16_T c8_i17;
  int16_T c8_i18;
  int16_T c8_i19;
  int32_T c8_i20;
  int32_T c8_i21;
  int16_T c8_i22;
  int16_T c8_i23;
  int16_T c8_i24;
  int16_T c8_i25;
  int16_T c8_i26;
  int16_T c8_i27;
  int16_T c8_i28;
  int16_T c8_i29;
  int16_T c8_i30;
  int16_T c8_i31;
  int16_T c8_i32;
  int16_T c8_i33;
  int32_T c8_i34;
  int32_T c8_i35;
  int16_T c8_i36;
  int16_T c8_i37;
  int16_T c8_i38;
  int16_T c8_i39;
  int16_T c8_i40;
  int16_T c8_i41;
  int16_T c8_i42;
  int16_T c8_i43;
  real_T c8_d2;
  real_T c8_d3;
  int16_T c8_i_varargin_1;
  int16_T c8_i_a0;
  int16_T c8_j_varargin_1;
  int16_T c8_e_b0;
  int16_T c8_e_var1;
  int16_T c8_k_varargin_1;
  int16_T c8_i44;
  int16_T c8_v;
  boolean_T c8_e_covSaturation;
  int16_T c8_val;
  int16_T c8_c_b;
  int32_T c8_i45;
  uint16_T c8_e_hfi;
  real_T c8_d4;
  int32_T c8_i46;
  real_T c8_d5;
  real_T c8_d6;
  real_T c8_d7;
  int32_T c8_i47;
  int32_T c8_i48;
  int32_T c8_i49;
  real_T c8_d8;
  real_T c8_d9;
  int32_T c8_e_c;
  int32_T c8_l_varargin_1;
  int32_T c8_m_varargin_1;
  int32_T c8_f_var1;
  int32_T c8_i50;
  boolean_T c8_f_covSaturation;
  uint16_T c8_f_hfi;
  observerLogReadPIC(&c8_PICOffset);
  chartInstance->c8_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 4U, (real_T)
                    *chartInstance->c8_init);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 3U, (real_T)
                    *chartInstance->c8_sum);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 2U, (real_T)
                    *chartInstance->c8_max);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 1U, (real_T)
                    *chartInstance->c8_offset);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 0U, (real_T)
                    *chartInstance->c8_enable);
  chartInstance->c8_sfEvent = CALL_EVENT;
  c8_b_enable = *chartInstance->c8_enable;
  c8_b_offset = *chartInstance->c8_offset;
  c8_b_max = *chartInstance->c8_max;
  c8_b_sum = *chartInstance->c8_sum;
  c8_b_init = *chartInstance->c8_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c8_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c8_emlrt_update_log_1(chartInstance, c8_b_enable,
                        chartInstance->c8_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c8_emlrt_update_log_2(chartInstance, c8_b_offset,
                        chartInstance->c8_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c8_emlrt_update_log_3(chartInstance, c8_b_max,
                        chartInstance->c8_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c8_emlrt_update_log_3(chartInstance, c8_b_sum,
                        chartInstance->c8_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c8_emlrt_update_log_1(chartInstance, c8_b_init,
                        chartInstance->c8_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c8_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c8_covrtInstance, 4U, 0, 0, false);
  c8_a0 = c8_b_enable;
  c8_a = c8_a0;
  c8_b_a0 = c8_a;
  c8_a1 = c8_b_a0;
  c8_b_a1 = c8_a1;
  c8_c = (c8_b_a1 == 0);
  c8_i = (int8_T)c8_b_enable;
  if ((int8_T)(c8_i & 2) != 0) {
    c8_i1 = (int8_T)(c8_i | -2);
  } else {
    c8_i1 = (int8_T)(c8_i & 1);
  }

  if (c8_i1 > 0) {
    c8_d = 3.0;
  } else {
    c8_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c8_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c8_covrtInstance,
        4U, 0U, 0U, c8_d, 0.0, -2, 0U, (int32_T)c8_emlrt_update_log_4
        (chartInstance, c8_c, chartInstance->c8_emlrtLocationLoggingDataTables,
         5)))) {
    c8_b_cont = c8_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c8_emlrtLocationLoggingDataTables, 6);
    c8_b_out_init = c8_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c8_emlrtLocationLoggingDataTables, 7);
  } else {
    c8_c_a0 = c8_b_init;
    c8_b_a = c8_c_a0;
    c8_d_a0 = c8_b_a;
    c8_c_a1 = c8_d_a0;
    c8_d_a1 = c8_c_a1;
    c8_b_c = (c8_d_a1 == 0);
    c8_i2 = (int8_T)c8_b_init;
    if ((int8_T)(c8_i2 & 2) != 0) {
      c8_i3 = (int8_T)(c8_i2 | -2);
    } else {
      c8_i3 = (int8_T)(c8_i2 & 1);
    }

    if (c8_i3 > 0) {
      c8_d1 = 3.0;
    } else {
      c8_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c8_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c8_covrtInstance, 4U, 0U, 1U, c8_d1, 0.0,
                        -2, 0U, (int32_T)c8_emlrt_update_log_4(chartInstance,
           c8_b_c, chartInstance->c8_emlrtLocationLoggingDataTables, 8)))) {
      c8_b_varargin_1 = c8_b_sum;
      c8_d_varargin_1 = c8_b_varargin_1;
      c8_b_var1 = c8_d_varargin_1;
      c8_i5 = c8_b_var1;
      c8_b_covSaturation = false;
      if (c8_i5 < 0) {
        c8_i5 = 0;
      } else {
        if (c8_i5 > 4095) {
          c8_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 0, 0,
          c8_b_covSaturation);
      }

      c8_b_hfi = (uint16_T)c8_i5;
      c8_u1 = c8_emlrt_update_log_5(chartInstance, c8_b_hfi,
        chartInstance->c8_emlrtLocationLoggingDataTables, 10);
      c8_f_varargin_1 = c8_b_max;
      c8_h_varargin_1 = c8_f_varargin_1;
      c8_d_var1 = c8_h_varargin_1;
      c8_i7 = c8_d_var1;
      c8_d_covSaturation = false;
      if (c8_i7 < 0) {
        c8_i7 = 0;
      } else {
        if (c8_i7 > 4095) {
          c8_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 1, 0,
          c8_d_covSaturation);
      }

      c8_d_hfi = (uint16_T)c8_i7;
      c8_u3 = c8_emlrt_update_log_5(chartInstance, c8_d_hfi,
        chartInstance->c8_emlrtLocationLoggingDataTables, 11);
      c8_f_a0 = c8_u1;
      c8_b_b0 = c8_u3;
      c8_d_a = c8_f_a0;
      c8_b_b = c8_b_b0;
      c8_h_a0 = c8_d_a;
      c8_d_b0 = c8_b_b;
      c8_f_a1 = c8_h_a0;
      c8_b_b1 = c8_d_b0;
      c8_h_a1 = c8_f_a1;
      c8_d_b1 = c8_b_b1;
      c8_d_c = (c8_h_a1 < c8_d_b1);
      c8_i9 = (int16_T)c8_u1;
      c8_i11 = (int16_T)c8_u3;
      c8_i13 = (int16_T)c8_u3;
      c8_i15 = (int16_T)c8_u1;
      if ((int16_T)(c8_i13 & 4096) != 0) {
        c8_i17 = (int16_T)(c8_i13 | -4096);
      } else {
        c8_i17 = (int16_T)(c8_i13 & 4095);
      }

      if ((int16_T)(c8_i15 & 4096) != 0) {
        c8_i19 = (int16_T)(c8_i15 | -4096);
      } else {
        c8_i19 = (int16_T)(c8_i15 & 4095);
      }

      c8_i21 = c8_i17 - c8_i19;
      if (c8_i21 > 4095) {
        c8_i21 = 4095;
      } else {
        if (c8_i21 < -4096) {
          c8_i21 = -4096;
        }
      }

      c8_i23 = (int16_T)c8_u1;
      c8_i25 = (int16_T)c8_u3;
      c8_i27 = (int16_T)c8_u1;
      c8_i29 = (int16_T)c8_u3;
      if ((int16_T)(c8_i27 & 4096) != 0) {
        c8_i31 = (int16_T)(c8_i27 | -4096);
      } else {
        c8_i31 = (int16_T)(c8_i27 & 4095);
      }

      if ((int16_T)(c8_i29 & 4096) != 0) {
        c8_i33 = (int16_T)(c8_i29 | -4096);
      } else {
        c8_i33 = (int16_T)(c8_i29 & 4095);
      }

      c8_i35 = c8_i31 - c8_i33;
      if (c8_i35 > 4095) {
        c8_i35 = 4095;
      } else {
        if (c8_i35 < -4096) {
          c8_i35 = -4096;
        }
      }

      if ((int16_T)(c8_i9 & 4096) != 0) {
        c8_i37 = (int16_T)(c8_i9 | -4096);
      } else {
        c8_i37 = (int16_T)(c8_i9 & 4095);
      }

      if ((int16_T)(c8_i11 & 4096) != 0) {
        c8_i39 = (int16_T)(c8_i11 | -4096);
      } else {
        c8_i39 = (int16_T)(c8_i11 & 4095);
      }

      if ((int16_T)(c8_i23 & 4096) != 0) {
        c8_i41 = (int16_T)(c8_i23 | -4096);
      } else {
        c8_i41 = (int16_T)(c8_i23 & 4095);
      }

      if ((int16_T)(c8_i25 & 4096) != 0) {
        c8_i43 = (int16_T)(c8_i25 | -4096);
      } else {
        c8_i43 = (int16_T)(c8_i25 & 4095);
      }

      if (c8_i37 < c8_i39) {
        c8_d3 = (real_T)((int16_T)c8_i21 <= 1);
      } else if (c8_i41 > c8_i43) {
        if ((int16_T)c8_i35 <= 1) {
          c8_d3 = 3.0;
        } else {
          c8_d3 = 0.0;
        }
      } else {
        c8_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c8_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c8_covrtInstance, 4U, 0U, 2U, c8_d3,
                          0.0, -2, 2U, (int32_T)c8_emlrt_update_log_4
                          (chartInstance, c8_d_c,
                           chartInstance->c8_emlrtLocationLoggingDataTables, 9))))
      {
        c8_i_a0 = c8_b_sum;
        c8_e_b0 = c8_b_offset;
        c8_k_varargin_1 = c8_e_b0;
        c8_v = c8_k_varargin_1;
        c8_val = c8_v;
        c8_c_b = c8_val;
        c8_i45 = c8_i_a0;
        if (c8_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d4 = 1.0;
          observerLog(135 + c8_PICOffset, &c8_d4, 1);
        }

        if (c8_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d5 = 1.0;
          observerLog(135 + c8_PICOffset, &c8_d5, 1);
        }

        c8_i46 = c8_c_b;
        if (c8_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d6 = 1.0;
          observerLog(138 + c8_PICOffset, &c8_d6, 1);
        }

        if (c8_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d7 = 1.0;
          observerLog(138 + c8_PICOffset, &c8_d7, 1);
        }

        if ((c8_i45 & 65536) != 0) {
          c8_i47 = c8_i45 | -65536;
        } else {
          c8_i47 = c8_i45 & 65535;
        }

        if ((c8_i46 & 65536) != 0) {
          c8_i48 = c8_i46 | -65536;
        } else {
          c8_i48 = c8_i46 & 65535;
        }

        c8_i49 = c8__s32_add__(chartInstance, c8_i47, c8_i48, 137, 1U, 323, 12);
        if (c8_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d8 = 1.0;
          observerLog(143 + c8_PICOffset, &c8_d8, 1);
        }

        if (c8_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c8_d9 = 1.0;
          observerLog(143 + c8_PICOffset, &c8_d9, 1);
        }

        if ((c8_i49 & 65536) != 0) {
          c8_e_c = c8_i49 | -65536;
        } else {
          c8_e_c = c8_i49 & 65535;
        }

        c8_l_varargin_1 = c8_emlrt_update_log_6(chartInstance, c8_e_c,
          chartInstance->c8_emlrtLocationLoggingDataTables, 13);
        c8_m_varargin_1 = c8_l_varargin_1;
        c8_f_var1 = c8_m_varargin_1;
        c8_i50 = c8_f_var1;
        c8_f_covSaturation = false;
        if (c8_i50 < 0) {
          c8_i50 = 0;
        } else {
          if (c8_i50 > 4095) {
            c8_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 2, 0,
            c8_f_covSaturation);
        }

        c8_f_hfi = (uint16_T)c8_i50;
        c8_b_cont = c8_emlrt_update_log_5(chartInstance, c8_f_hfi,
          chartInstance->c8_emlrtLocationLoggingDataTables, 12);
        c8_b_out_init = c8_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 14);
      } else {
        c8_b_cont = c8_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 15);
        c8_b_out_init = c8_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c8_varargin_1 = c8_b_sum;
      c8_c_varargin_1 = c8_varargin_1;
      c8_var1 = c8_c_varargin_1;
      c8_i4 = c8_var1;
      c8_covSaturation = false;
      if (c8_i4 < 0) {
        c8_i4 = 0;
      } else {
        if (c8_i4 > 4095) {
          c8_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 3, 0,
          c8_covSaturation);
      }

      c8_hfi = (uint16_T)c8_i4;
      c8_u = c8_emlrt_update_log_5(chartInstance, c8_hfi,
        chartInstance->c8_emlrtLocationLoggingDataTables, 18);
      c8_e_varargin_1 = c8_b_max;
      c8_g_varargin_1 = c8_e_varargin_1;
      c8_c_var1 = c8_g_varargin_1;
      c8_i6 = c8_c_var1;
      c8_c_covSaturation = false;
      if (c8_i6 < 0) {
        c8_i6 = 0;
      } else {
        if (c8_i6 > 4095) {
          c8_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 4, 0,
          c8_c_covSaturation);
      }

      c8_c_hfi = (uint16_T)c8_i6;
      c8_u2 = c8_emlrt_update_log_5(chartInstance, c8_c_hfi,
        chartInstance->c8_emlrtLocationLoggingDataTables, 19);
      c8_e_a0 = c8_u;
      c8_b0 = c8_u2;
      c8_c_a = c8_e_a0;
      c8_b = c8_b0;
      c8_g_a0 = c8_c_a;
      c8_c_b0 = c8_b;
      c8_e_a1 = c8_g_a0;
      c8_b1 = c8_c_b0;
      c8_g_a1 = c8_e_a1;
      c8_c_b1 = c8_b1;
      c8_c_c = (c8_g_a1 < c8_c_b1);
      c8_i8 = (int16_T)c8_u;
      c8_i10 = (int16_T)c8_u2;
      c8_i12 = (int16_T)c8_u2;
      c8_i14 = (int16_T)c8_u;
      if ((int16_T)(c8_i12 & 4096) != 0) {
        c8_i16 = (int16_T)(c8_i12 | -4096);
      } else {
        c8_i16 = (int16_T)(c8_i12 & 4095);
      }

      if ((int16_T)(c8_i14 & 4096) != 0) {
        c8_i18 = (int16_T)(c8_i14 | -4096);
      } else {
        c8_i18 = (int16_T)(c8_i14 & 4095);
      }

      c8_i20 = c8_i16 - c8_i18;
      if (c8_i20 > 4095) {
        c8_i20 = 4095;
      } else {
        if (c8_i20 < -4096) {
          c8_i20 = -4096;
        }
      }

      c8_i22 = (int16_T)c8_u;
      c8_i24 = (int16_T)c8_u2;
      c8_i26 = (int16_T)c8_u;
      c8_i28 = (int16_T)c8_u2;
      if ((int16_T)(c8_i26 & 4096) != 0) {
        c8_i30 = (int16_T)(c8_i26 | -4096);
      } else {
        c8_i30 = (int16_T)(c8_i26 & 4095);
      }

      if ((int16_T)(c8_i28 & 4096) != 0) {
        c8_i32 = (int16_T)(c8_i28 | -4096);
      } else {
        c8_i32 = (int16_T)(c8_i28 & 4095);
      }

      c8_i34 = c8_i30 - c8_i32;
      if (c8_i34 > 4095) {
        c8_i34 = 4095;
      } else {
        if (c8_i34 < -4096) {
          c8_i34 = -4096;
        }
      }

      if ((int16_T)(c8_i8 & 4096) != 0) {
        c8_i36 = (int16_T)(c8_i8 | -4096);
      } else {
        c8_i36 = (int16_T)(c8_i8 & 4095);
      }

      if ((int16_T)(c8_i10 & 4096) != 0) {
        c8_i38 = (int16_T)(c8_i10 | -4096);
      } else {
        c8_i38 = (int16_T)(c8_i10 & 4095);
      }

      if ((int16_T)(c8_i22 & 4096) != 0) {
        c8_i40 = (int16_T)(c8_i22 | -4096);
      } else {
        c8_i40 = (int16_T)(c8_i22 & 4095);
      }

      if ((int16_T)(c8_i24 & 4096) != 0) {
        c8_i42 = (int16_T)(c8_i24 | -4096);
      } else {
        c8_i42 = (int16_T)(c8_i24 & 4095);
      }

      if (c8_i36 < c8_i38) {
        c8_d2 = (real_T)((int16_T)c8_i20 <= 1);
      } else if (c8_i40 > c8_i42) {
        if ((int16_T)c8_i34 <= 1) {
          c8_d2 = 3.0;
        } else {
          c8_d2 = 0.0;
        }
      } else {
        c8_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c8_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c8_covrtInstance, 4U, 0U, 3U, c8_d2,
                          0.0, -2, 2U, (int32_T)c8_emlrt_update_log_4
                          (chartInstance, c8_c_c,
                           chartInstance->c8_emlrtLocationLoggingDataTables, 17))))
      {
        c8_i_varargin_1 = c8_b_sum;
        c8_j_varargin_1 = c8_i_varargin_1;
        c8_e_var1 = c8_j_varargin_1;
        c8_i44 = c8_e_var1;
        c8_e_covSaturation = false;
        if (c8_i44 < 0) {
          c8_i44 = 0;
        } else {
          if (c8_i44 > 4095) {
            c8_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c8_covrtInstance, 4, 0, 5, 0,
            c8_e_covSaturation);
        }

        c8_e_hfi = (uint16_T)c8_i44;
        c8_b_cont = c8_emlrt_update_log_5(chartInstance, c8_e_hfi,
          chartInstance->c8_emlrtLocationLoggingDataTables, 20);
        c8_b_out_init = c8_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 21);
      } else {
        c8_b_cont = c8_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 22);
        c8_b_out_init = c8_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c8_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c8_cont = c8_b_cont;
  *chartInstance->c8_out_init = c8_b_out_init;
  c8_do_animation_call_c8_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 5U, (real_T)
                    *chartInstance->c8_cont);
  covrtSigUpdateFcn(chartInstance->c8_covrtInstance, 6U, (real_T)
                    *chartInstance->c8_out_init);
}

static void mdl_start_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c8_decisionTxtStartIdx = 0U;
  static const uint32_T c8_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c8_chart_data_browse_helper);
  chartInstance->c8_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c8_RuntimeVar,
    &chartInstance->c8_IsDebuggerActive,
    &chartInstance->c8_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c8_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c8_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c8_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c8_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c8_decisionTxtStartIdx, &c8_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c8_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c8_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c8_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c8_PWM_28_HalfB
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c8_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7397",              /* mexFileName */
    "Thu May 27 10:27:19 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c8_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c8_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c8_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c8_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7397");
    emlrtLocationLoggingPushLog(&c8_emlrtLocationLoggingFileInfo,
      c8_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c8_emlrtLocationLoggingDataTables, c8_emlrtLocationInfo,
      NULL, 0U, c8_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7397");
  }

  sfListenerLightTerminate(chartInstance->c8_RuntimeVar);
  sf_mex_destroy(&c8_eml_mx);
  sf_mex_destroy(&c8_b_eml_mx);
  sf_mex_destroy(&c8_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c8_covrtInstance);
}

static void initSimStructsc8_PWM_28_HalfB(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c8_emlrt_update_log_1(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  real_T c8_d;
  uint8_T c8_u;
  uint8_T c8_localMin;
  real_T c8_d1;
  uint8_T c8_u1;
  uint8_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_d = c8_b_table[0U].SimMin;
    if (c8_d < 2.0) {
      if (c8_d >= 0.0) {
        c8_u = (uint8_T)c8_d;
      } else {
        c8_u = 0U;
      }
    } else if (c8_d >= 2.0) {
      c8_u = 1U;
    } else {
      c8_u = 0U;
    }

    c8_localMin = c8_u;
    c8_d1 = c8_b_table[0U].SimMax;
    if (c8_d1 < 2.0) {
      if (c8_d1 >= 0.0) {
        c8_u1 = (uint8_T)c8_d1;
      } else {
        c8_u1 = 0U;
      }
    } else if (c8_d1 >= 2.0) {
      c8_u1 = 1U;
    } else {
      c8_u1 = 0U;
    }

    c8_localMax = c8_u1;
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c8_in < c8_localMin) {
      c8_localMin = c8_in;
    }

    if (c8_in > c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static int16_T c8_emlrt_update_log_2(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  real_T c8_d;
  int16_T c8_i;
  int16_T c8_localMin;
  real_T c8_d1;
  int16_T c8_i1;
  int16_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_d = muDoubleScalarFloor(c8_b_table[0U].SimMin);
    if (c8_d < 32768.0) {
      if (c8_d >= -32768.0) {
        c8_i = (int16_T)c8_d;
      } else {
        c8_i = MIN_int16_T;
      }
    } else if (c8_d >= 32768.0) {
      c8_i = MAX_int16_T;
    } else {
      c8_i = 0;
    }

    c8_localMin = c8_i;
    c8_d1 = muDoubleScalarFloor(c8_b_table[0U].SimMax);
    if (c8_d1 < 32768.0) {
      if (c8_d1 >= -32768.0) {
        c8_i1 = (int16_T)c8_d1;
      } else {
        c8_i1 = MIN_int16_T;
      }
    } else if (c8_d1 >= 32768.0) {
      c8_i1 = MAX_int16_T;
    } else {
      c8_i1 = 0;
    }

    c8_localMax = c8_i1;
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c8_in < c8_localMin) {
      c8_localMin = c8_in;
    }

    if (c8_in > c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static int16_T c8_emlrt_update_log_3(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  real_T c8_d;
  int16_T c8_i;
  int16_T c8_localMin;
  real_T c8_d1;
  int16_T c8_i1;
  int16_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_d = muDoubleScalarFloor(c8_b_table[0U].SimMin);
    if (c8_d < 2048.0) {
      if (c8_d >= -2048.0) {
        c8_i = (int16_T)c8_d;
      } else {
        c8_i = -2048;
      }
    } else if (c8_d >= 2048.0) {
      c8_i = 2047;
    } else {
      c8_i = 0;
    }

    c8_localMin = c8_i;
    c8_d1 = muDoubleScalarFloor(c8_b_table[0U].SimMax);
    if (c8_d1 < 2048.0) {
      if (c8_d1 >= -2048.0) {
        c8_i1 = (int16_T)c8_d1;
      } else {
        c8_i1 = -2048;
      }
    } else if (c8_d1 >= 2048.0) {
      c8_i1 = 2047;
    } else {
      c8_i1 = 0;
    }

    c8_localMax = c8_i1;
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c8_in < c8_localMin) {
      c8_localMin = c8_in;
    }

    if (c8_in > c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static boolean_T c8_emlrt_update_log_4(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  boolean_T c8_localMin;
  boolean_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_localMin = (c8_b_table[0U].SimMin > 0.0);
    c8_localMax = (c8_b_table[0U].SimMax > 0.0);
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c8_in < (int32_T)c8_localMin) {
      c8_localMin = c8_in;
    }

    if ((int32_T)c8_in > (int32_T)c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static uint16_T c8_emlrt_update_log_5(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  real_T c8_d;
  uint16_T c8_u;
  uint16_T c8_localMin;
  real_T c8_d1;
  uint16_T c8_u1;
  uint16_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_d = c8_b_table[0U].SimMin;
    if (c8_d < 4096.0) {
      if (c8_d >= 0.0) {
        c8_u = (uint16_T)c8_d;
      } else {
        c8_u = 0U;
      }
    } else if (c8_d >= 4096.0) {
      c8_u = 4095U;
    } else {
      c8_u = 0U;
    }

    c8_localMin = c8_u;
    c8_d1 = c8_b_table[0U].SimMax;
    if (c8_d1 < 4096.0) {
      if (c8_d1 >= 0.0) {
        c8_u1 = (uint16_T)c8_d1;
      } else {
        c8_u1 = 0U;
      }
    } else if (c8_d1 >= 4096.0) {
      c8_u1 = 4095U;
    } else {
      c8_u1 = 0U;
    }

    c8_localMax = c8_u1;
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c8_in < c8_localMin) {
      c8_localMin = c8_in;
    }

    if (c8_in > c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static int32_T c8_emlrt_update_log_6(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c8_in, emlrtLocationLoggingDataType c8_table[],
  int32_T c8_index)
{
  boolean_T c8_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c8_b_table;
  real_T c8_d;
  int32_T c8_i;
  int32_T c8_localMin;
  real_T c8_d1;
  int32_T c8_i1;
  int32_T c8_localMax;
  emlrtLocationLoggingHistogramType *c8_histTable;
  real_T c8_inDouble;
  real_T c8_significand;
  int32_T c8_exponent;
  (void)chartInstance;
  c8_isLoggingEnabledHere = (c8_index >= 0);
  if (c8_isLoggingEnabledHere) {
    c8_b_table = (emlrtLocationLoggingDataType *)&c8_table[c8_index];
    c8_d = muDoubleScalarFloor(c8_b_table[0U].SimMin);
    if (c8_d < 65536.0) {
      if (c8_d >= -65536.0) {
        c8_i = (int32_T)c8_d;
      } else {
        c8_i = -65536;
      }
    } else if (c8_d >= 65536.0) {
      c8_i = 65535;
    } else {
      c8_i = 0;
    }

    c8_localMin = c8_i;
    c8_d1 = muDoubleScalarFloor(c8_b_table[0U].SimMax);
    if (c8_d1 < 65536.0) {
      if (c8_d1 >= -65536.0) {
        c8_i1 = (int32_T)c8_d1;
      } else {
        c8_i1 = -65536;
      }
    } else if (c8_d1 >= 65536.0) {
      c8_i1 = 65535;
    } else {
      c8_i1 = 0;
    }

    c8_localMax = c8_i1;
    c8_histTable = c8_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c8_in < c8_localMin) {
      c8_localMin = c8_in;
    }

    if (c8_in > c8_localMax) {
      c8_localMax = c8_in;
    }

    /* Histogram logging. */
    c8_inDouble = (real_T)c8_in;
    c8_histTable->TotalNumberOfValues++;
    if (c8_inDouble == 0.0) {
      c8_histTable->NumberOfZeros++;
    } else {
      c8_histTable->SimSum += c8_inDouble;
      if ((!muDoubleScalarIsInf(c8_inDouble)) && (!muDoubleScalarIsNaN
           (c8_inDouble))) {
        c8_significand = frexp(c8_inDouble, &c8_exponent);
        if (c8_exponent > 128) {
          c8_exponent = 128;
        }

        if (c8_exponent < -127) {
          c8_exponent = -127;
        }

        if (c8_significand < 0.0) {
          c8_histTable->NumberOfNegativeValues++;
          c8_histTable->HistogramOfNegativeValues[127 + c8_exponent]++;
        } else {
          c8_histTable->NumberOfPositiveValues++;
          c8_histTable->HistogramOfPositiveValues[127 + c8_exponent]++;
        }
      }
    }

    c8_b_table[0U].SimMin = (real_T)c8_localMin;
    c8_b_table[0U].SimMax = (real_T)c8_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c8_in;
}

static void c8_emlrtInitVarDataTables(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c8_dataTables[24],
  emlrtLocationLoggingHistogramType c8_histTables[24])
{
  int32_T c8_i;
  int32_T c8_iH;
  (void)chartInstance;
  for (c8_i = 0; c8_i < 24; c8_i++) {
    c8_dataTables[c8_i].SimMin = rtInf;
    c8_dataTables[c8_i].SimMax = rtMinusInf;
    c8_dataTables[c8_i].OverflowWraps = 0;
    c8_dataTables[c8_i].Saturations = 0;
    c8_dataTables[c8_i].IsAlwaysInteger = true;
    c8_dataTables[c8_i].HistogramTable = &c8_histTables[c8_i];
    c8_histTables[c8_i].NumberOfZeros = 0.0;
    c8_histTables[c8_i].NumberOfPositiveValues = 0.0;
    c8_histTables[c8_i].NumberOfNegativeValues = 0.0;
    c8_histTables[c8_i].TotalNumberOfValues = 0.0;
    c8_histTables[c8_i].SimSum = 0.0;
    for (c8_iH = 0; c8_iH < 256; c8_iH++) {
      c8_histTables[c8_i].HistogramOfPositiveValues[c8_iH] = 0.0;
      c8_histTables[c8_i].HistogramOfNegativeValues[c8_iH] = 0.0;
    }
  }
}

const mxArray *sf_c8_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c8_nameCaptureInfo = NULL;
  c8_nameCaptureInfo = NULL;
  sf_mex_assign(&c8_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c8_nameCaptureInfo;
}

static uint16_T c8_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_b_cont, const
  char_T *c8_identifier)
{
  uint16_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = (const char *)c8_identifier;
  c8_thisId.fParent = NULL;
  c8_thisId.bParentIsCell = false;
  c8_y = c8_b_emlrt_marshallIn(chartInstance, c8_sp, sf_mex_dup(c8_b_cont),
    &c8_thisId);
  sf_mex_destroy(&c8_b_cont);
  return c8_y;
}

static uint16_T c8_b_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_u, const
  emlrtMsgIdentifier *c8_parentId)
{
  uint16_T c8_y;
  const mxArray *c8_mxFi = NULL;
  const mxArray *c8_mxInt = NULL;
  uint16_T c8_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c8_parentId, c8_u, false, 0U, NULL, c8_eml_mx, c8_b_eml_mx);
  sf_mex_assign(&c8_mxFi, sf_mex_dup(c8_u), false);
  sf_mex_assign(&c8_mxInt, sf_mex_call(c8_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c8_mxFi)), false);
  sf_mex_import(c8_parentId, sf_mex_dup(c8_mxInt), &c8_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c8_mxFi);
  sf_mex_destroy(&c8_mxInt);
  c8_y = c8_b_u;
  sf_mex_destroy(&c8_mxFi);
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static uint8_T c8_c_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_b_out_init, const
  char_T *c8_identifier)
{
  uint8_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = (const char *)c8_identifier;
  c8_thisId.fParent = NULL;
  c8_thisId.bParentIsCell = false;
  c8_y = c8_d_emlrt_marshallIn(chartInstance, c8_sp, sf_mex_dup(c8_b_out_init),
    &c8_thisId);
  sf_mex_destroy(&c8_b_out_init);
  return c8_y;
}

static uint8_T c8_d_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c8_sp, const mxArray *c8_u, const
  emlrtMsgIdentifier *c8_parentId)
{
  uint8_T c8_y;
  const mxArray *c8_mxFi = NULL;
  const mxArray *c8_mxInt = NULL;
  uint8_T c8_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c8_parentId, c8_u, false, 0U, NULL, c8_eml_mx, c8_c_eml_mx);
  sf_mex_assign(&c8_mxFi, sf_mex_dup(c8_u), false);
  sf_mex_assign(&c8_mxInt, sf_mex_call(c8_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c8_mxFi)), false);
  sf_mex_import(c8_parentId, sf_mex_dup(c8_mxInt), &c8_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c8_mxFi);
  sf_mex_destroy(&c8_mxInt);
  c8_y = c8_b_u;
  sf_mex_destroy(&c8_mxFi);
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static uint8_T c8_e_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_b_is_active_c8_PWM_28_HalfB, const char_T
  *c8_identifier)
{
  uint8_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = (const char *)c8_identifier;
  c8_thisId.fParent = NULL;
  c8_thisId.bParentIsCell = false;
  c8_y = c8_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_b_is_active_c8_PWM_28_HalfB), &c8_thisId);
  sf_mex_destroy(&c8_b_is_active_c8_PWM_28_HalfB);
  return c8_y;
}

static uint8_T c8_f_emlrt_marshallIn(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  uint8_T c8_y;
  uint8_T c8_b_u;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_b_u, 1, 3, 0U, 0, 0U, 0);
  c8_y = c8_b_u;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static const mxArray *c8_chart_data_browse_helper
  (SFc8_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c8_ssIdNumber)
{
  const mxArray *c8_mxData = NULL;
  uint8_T c8_u;
  int16_T c8_i;
  int16_T c8_i1;
  int16_T c8_i2;
  uint16_T c8_u1;
  uint8_T c8_u2;
  uint8_T c8_u3;
  real_T c8_d;
  real_T c8_d1;
  real_T c8_d2;
  real_T c8_d3;
  real_T c8_d4;
  real_T c8_d5;
  c8_mxData = NULL;
  switch (c8_ssIdNumber) {
   case 18U:
    c8_u = *chartInstance->c8_enable;
    c8_d = (real_T)c8_u;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c8_i = *chartInstance->c8_offset;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c8_i1 = *chartInstance->c8_max;
    c8_d1 = (real_T)c8_i1;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c8_i2 = *chartInstance->c8_sum;
    c8_d2 = (real_T)c8_i2;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c8_u1 = *chartInstance->c8_cont;
    c8_d3 = (real_T)c8_u1;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c8_u2 = *chartInstance->c8_init;
    c8_d4 = (real_T)c8_u2;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c8_u3 = *chartInstance->c8_out_init;
    c8_d5 = (real_T)c8_u3;
    sf_mex_assign(&c8_mxData, sf_mex_create("mxData", &c8_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c8_mxData;
}

static int32_T c8__s32_add__(SFc8_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c8_b, int32_T c8_c, int32_T c8_EMLOvCount_src_loc, uint32_T
  c8_ssid_src_loc, int32_T c8_offset_src_loc, int32_T c8_length_src_loc)
{
  int32_T c8_a;
  int32_T c8_PICOffset;
  real_T c8_d;
  observerLogReadPIC(&c8_PICOffset);
  c8_a = c8_b + c8_c;
  if (((c8_a ^ c8_b) & (c8_a ^ c8_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c8_ssid_src_loc, c8_offset_src_loc,
      c8_length_src_loc);
    c8_d = 1.0;
    observerLog(c8_EMLOvCount_src_loc + c8_PICOffset, &c8_d, 1);
  }

  return c8_a;
}

static void init_dsm_address_info(SFc8_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc8_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c8_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c8_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c8_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c8_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c8_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c8_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c8_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c8_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c8_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c8_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c8_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c8_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyyRXxAuG+8kUW8R"
    "2JOmhOSuSAAAOqfIGI="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c8_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c8_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c8_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c8_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c8_PWM_28_HalfB
      ((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c8_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c8_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc8_PWM_28_HalfB((SFc8_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c8_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iNHGcLopDARodu2qSIom8CpxTEmxAKlWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4igxEiADUMQMv/fN+5+RV+v0PBxb+Lz8yvOu4/sGPnVvOjayeW",
    "3hma43vG+z+SMUEjbuE0Vi7VUOQWJ4Clpya5gUHTGUhTAmhqBAUMQmUpkyNs1iy5kYt62gjk+/i",
    "BiNgkhaHu6jLAkPBT9DtsSaPvI0mQJq2gChiZS0o6jNyWiusTInfgR0rG1cZYIGE9jEqaV7lhuW",
    "cGidAu0IbQhqrM91Cwwx4JvTUjOdpTqYAWWccEZEobUR0QEk6GADz5IQfw+tQaPyMBoRZfYhIhP",
    "QXTZOOaWAPCfT+OGYCWKkYoS3Yu47wWXd+hz16ckQeIVDULd9BWScSCZMefyDNlraEuSYQxOO7a",
    "icLYBX1gX/OYMTUKV+G/pyAoqM4FCUbpo6pHWaRmueJcsww2J4TtRjivHTEJZmL2aODgjGCY5Qo",
    "gwGqZEdfaTYBN1bymbjjsvMVSVj42mw9SpYytaaQFUU5mxtKnzCuS6FHcmkCxPgKWuTGFINm7IW",
    "47Rm4ZFEB7v0Lq8GKxgGPoP5UoSsMFyTHCDtOz9hY7mIpFYbGfuYvM1ud/nzMqwjDKghoVDUBRR",
    "hGtBnqXvL2UKmXewRiFqZVL0i8DRDVqE8PbSieSLVGH1S0UTOTXARLQXGeoSxxEp4prFoqmAulq",
    "twlNAIQtdgGIcelg1iC3yiXWt7jHU3YeasCZoqlhRF1Z0/d73z8+fLtzh/ZnL593cLPLUCHm/h7",
    "fC7C/jN+kX8Rm7f+mzNjUx+b0H+Zm6/Rk7e4bZd5bz549dfbv37xd8///Zn5x8wefvzetSW9Kh5",
    "s/2Ta5c7t7ey+e1Zg5wn/GQpzxz2YEGvRgH/1wv829lc/3jQDnrjl/dePb0HT5Thr2MlHvgp3+/",
    "1an2v5fSdrd9xnfosSfuuVrQTZhcKNyd2eszm43l9hT82s/Xp+O/RevLf7+XjWOSvxgV/NTwqhd",
    "kqycer1f/+Xl6+SP8buXi7ubRmwAR7T3bc2VtPfrp/f4UdOzk7dtJ7xYC4bgUDujvov+gNftgdH",
    "BA+3C/oM+9ar5eV865Y7lPR87NfPrx9b3MOb7yjXH3Nc/+q5Na177L3kY8NX3WeeTn89kdsx7r3",
    "xA+N/8u73D3um2z+cP4Xy48YDwtu29nnLpBh0dcrsO9/zJ+gTw==",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c8_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c8_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c8_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c8_PWM_28_HalfB(SimStruct *S)
{
  SFc8_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc8_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc8_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc8_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c8_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c8_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c8_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c8_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c8_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c8_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c8_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c8_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c8_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c8_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c8_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c8_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c8_JITStateAnimation,
    chartInstance->c8_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c8_PWM_28_HalfB(chartInstance);
}

void c8_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c8_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c8_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c8_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c8_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
