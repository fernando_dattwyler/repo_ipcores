/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c20_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c20_eml_mx;
static const mxArray *c20_b_eml_mx;
static const mxArray *c20_c_eml_mx;

/* Function Declarations */
static void initialize_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c20_update_jit_animation_state_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance);
static void c20_do_animation_call_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_st);
static void sf_gateway_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c20_emlrt_update_log_1(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static int16_T c20_emlrt_update_log_2(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static int16_T c20_emlrt_update_log_3(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static boolean_T c20_emlrt_update_log_4(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static uint16_T c20_emlrt_update_log_5(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static int32_T c20_emlrt_update_log_6(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index);
static void c20_emlrtInitVarDataTables(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c20_dataTables[24],
  emlrtLocationLoggingHistogramType c20_histTables[24]);
static uint16_T c20_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_b_cont, const
  char_T *c20_identifier);
static uint16_T c20_b_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_u, const
  emlrtMsgIdentifier *c20_parentId);
static uint8_T c20_c_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_b_out_init, const
  char_T *c20_identifier);
static uint8_T c20_d_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_u, const
  emlrtMsgIdentifier *c20_parentId);
static uint8_T c20_e_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_b_is_active_c20_PWM_28_HalfB, const char_T *
  c20_identifier);
static uint8_T c20_f_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_u, const emlrtMsgIdentifier *c20_parentId);
static const mxArray *c20_chart_data_browse_helper
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c20_ssIdNumber);
static int32_T c20__s32_add__(SFc20_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c20_b, int32_T c20_c, int32_T c20_EMLOvCount_src_loc, uint32_T
  c20_ssid_src_loc, int32_T c20_offset_src_loc, int32_T c20_length_src_loc);
static void init_dsm_address_info(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c20_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c20_st.tls = chartInstance->c20_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c20_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c20_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c20_is_active_c20_PWM_28_HalfB = 0U;
  sf_mex_assign(&c20_c_eml_mx, sf_mex_call(&c20_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c20_b_eml_mx, sf_mex_call(&c20_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c20_eml_mx, sf_mex_call(&c20_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c20_emlrtLocLogSimulated = false;
  c20_emlrtInitVarDataTables(chartInstance,
    chartInstance->c20_emlrtLocationLoggingDataTables,
    chartInstance->c20_emlrtLocLogHistTables);
}

static void initialize_params_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c20_update_jit_animation_state_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c20_do_animation_call_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c20_st;
  const mxArray *c20_y = NULL;
  const mxArray *c20_b_y = NULL;
  uint16_T c20_u;
  const mxArray *c20_c_y = NULL;
  const mxArray *c20_d_y = NULL;
  uint8_T c20_b_u;
  const mxArray *c20_e_y = NULL;
  const mxArray *c20_f_y = NULL;
  c20_st = NULL;
  c20_st = NULL;
  c20_y = NULL;
  sf_mex_assign(&c20_y, sf_mex_createcellmatrix(3, 1), false);
  c20_b_y = NULL;
  c20_u = *chartInstance->c20_cont;
  c20_c_y = NULL;
  sf_mex_assign(&c20_c_y, sf_mex_create("y", &c20_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c20_b_y, sf_mex_create_fi(sf_mex_dup(c20_eml_mx), sf_mex_dup
    (c20_b_eml_mx), "simulinkarray", c20_c_y, false, false), false);
  sf_mex_setcell(c20_y, 0, c20_b_y);
  c20_d_y = NULL;
  c20_b_u = *chartInstance->c20_out_init;
  c20_e_y = NULL;
  sf_mex_assign(&c20_e_y, sf_mex_create("y", &c20_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c20_d_y, sf_mex_create_fi(sf_mex_dup(c20_eml_mx), sf_mex_dup
    (c20_c_eml_mx), "simulinkarray", c20_e_y, false, false), false);
  sf_mex_setcell(c20_y, 1, c20_d_y);
  c20_f_y = NULL;
  sf_mex_assign(&c20_f_y, sf_mex_create("y",
    &chartInstance->c20_is_active_c20_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c20_y, 2, c20_f_y);
  sf_mex_assign(&c20_st, c20_y, false);
  return c20_st;
}

static void set_sim_state_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_st)
{
  emlrtStack c20_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c20_u;
  c20_b_st.tls = chartInstance->c20_fEmlrtCtx;
  chartInstance->c20_doneDoubleBufferReInit = true;
  c20_u = sf_mex_dup(c20_st);
  *chartInstance->c20_cont = c20_emlrt_marshallIn(chartInstance, &c20_b_st,
    sf_mex_dup(sf_mex_getcell(c20_u, 0)), "cont");
  *chartInstance->c20_out_init = c20_c_emlrt_marshallIn(chartInstance, &c20_b_st,
    sf_mex_dup(sf_mex_getcell(c20_u, 1)), "out_init");
  chartInstance->c20_is_active_c20_PWM_28_HalfB = c20_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c20_u, 2)),
     "is_active_c20_PWM_28_HalfB");
  sf_mex_destroy(&c20_u);
  sf_mex_destroy(&c20_st);
}

static void sf_gateway_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c20_PICOffset;
  uint8_T c20_b_enable;
  int16_T c20_b_offset;
  int16_T c20_b_max;
  int16_T c20_b_sum;
  uint8_T c20_b_init;
  uint8_T c20_a0;
  uint8_T c20_a;
  uint8_T c20_b_a0;
  uint8_T c20_a1;
  uint8_T c20_b_a1;
  boolean_T c20_c;
  int8_T c20_i;
  int8_T c20_i1;
  real_T c20_d;
  uint8_T c20_c_a0;
  uint16_T c20_b_cont;
  uint8_T c20_b_a;
  uint8_T c20_b_out_init;
  uint8_T c20_d_a0;
  uint8_T c20_c_a1;
  uint8_T c20_d_a1;
  boolean_T c20_b_c;
  int8_T c20_i2;
  int8_T c20_i3;
  real_T c20_d1;
  int16_T c20_varargin_1;
  int16_T c20_b_varargin_1;
  int16_T c20_c_varargin_1;
  int16_T c20_d_varargin_1;
  int16_T c20_var1;
  int16_T c20_b_var1;
  int16_T c20_i4;
  int16_T c20_i5;
  boolean_T c20_covSaturation;
  boolean_T c20_b_covSaturation;
  uint16_T c20_hfi;
  uint16_T c20_b_hfi;
  uint16_T c20_u;
  uint16_T c20_u1;
  int16_T c20_e_varargin_1;
  int16_T c20_f_varargin_1;
  int16_T c20_g_varargin_1;
  int16_T c20_h_varargin_1;
  int16_T c20_c_var1;
  int16_T c20_d_var1;
  int16_T c20_i6;
  int16_T c20_i7;
  boolean_T c20_c_covSaturation;
  boolean_T c20_d_covSaturation;
  uint16_T c20_c_hfi;
  uint16_T c20_d_hfi;
  uint16_T c20_u2;
  uint16_T c20_u3;
  uint16_T c20_e_a0;
  uint16_T c20_f_a0;
  uint16_T c20_b0;
  uint16_T c20_b_b0;
  uint16_T c20_c_a;
  uint16_T c20_d_a;
  uint16_T c20_b;
  uint16_T c20_b_b;
  uint16_T c20_g_a0;
  uint16_T c20_h_a0;
  uint16_T c20_c_b0;
  uint16_T c20_d_b0;
  uint16_T c20_e_a1;
  uint16_T c20_f_a1;
  uint16_T c20_b1;
  uint16_T c20_b_b1;
  uint16_T c20_g_a1;
  uint16_T c20_h_a1;
  uint16_T c20_c_b1;
  uint16_T c20_d_b1;
  boolean_T c20_c_c;
  boolean_T c20_d_c;
  int16_T c20_i8;
  int16_T c20_i9;
  int16_T c20_i10;
  int16_T c20_i11;
  int16_T c20_i12;
  int16_T c20_i13;
  int16_T c20_i14;
  int16_T c20_i15;
  int16_T c20_i16;
  int16_T c20_i17;
  int16_T c20_i18;
  int16_T c20_i19;
  int32_T c20_i20;
  int32_T c20_i21;
  int16_T c20_i22;
  int16_T c20_i23;
  int16_T c20_i24;
  int16_T c20_i25;
  int16_T c20_i26;
  int16_T c20_i27;
  int16_T c20_i28;
  int16_T c20_i29;
  int16_T c20_i30;
  int16_T c20_i31;
  int16_T c20_i32;
  int16_T c20_i33;
  int32_T c20_i34;
  int32_T c20_i35;
  int16_T c20_i36;
  int16_T c20_i37;
  int16_T c20_i38;
  int16_T c20_i39;
  int16_T c20_i40;
  int16_T c20_i41;
  int16_T c20_i42;
  int16_T c20_i43;
  real_T c20_d2;
  real_T c20_d3;
  int16_T c20_i_varargin_1;
  int16_T c20_i_a0;
  int16_T c20_j_varargin_1;
  int16_T c20_e_b0;
  int16_T c20_e_var1;
  int16_T c20_k_varargin_1;
  int16_T c20_i44;
  int16_T c20_v;
  boolean_T c20_e_covSaturation;
  int16_T c20_val;
  int16_T c20_c_b;
  int32_T c20_i45;
  uint16_T c20_e_hfi;
  real_T c20_d4;
  int32_T c20_i46;
  real_T c20_d5;
  real_T c20_d6;
  real_T c20_d7;
  int32_T c20_i47;
  int32_T c20_i48;
  int32_T c20_i49;
  real_T c20_d8;
  real_T c20_d9;
  int32_T c20_e_c;
  int32_T c20_l_varargin_1;
  int32_T c20_m_varargin_1;
  int32_T c20_f_var1;
  int32_T c20_i50;
  boolean_T c20_f_covSaturation;
  uint16_T c20_f_hfi;
  observerLogReadPIC(&c20_PICOffset);
  chartInstance->c20_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 4U, (real_T)
                    *chartInstance->c20_init);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 3U, (real_T)
                    *chartInstance->c20_sum);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 2U, (real_T)
                    *chartInstance->c20_max);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 1U, (real_T)
                    *chartInstance->c20_offset);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 0U, (real_T)
                    *chartInstance->c20_enable);
  chartInstance->c20_sfEvent = CALL_EVENT;
  c20_b_enable = *chartInstance->c20_enable;
  c20_b_offset = *chartInstance->c20_offset;
  c20_b_max = *chartInstance->c20_max;
  c20_b_sum = *chartInstance->c20_sum;
  c20_b_init = *chartInstance->c20_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c20_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c20_emlrt_update_log_1(chartInstance, c20_b_enable,
    chartInstance->c20_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c20_emlrt_update_log_2(chartInstance, c20_b_offset,
    chartInstance->c20_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c20_emlrt_update_log_3(chartInstance, c20_b_max,
    chartInstance->c20_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c20_emlrt_update_log_3(chartInstance, c20_b_sum,
    chartInstance->c20_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c20_emlrt_update_log_1(chartInstance, c20_b_init,
    chartInstance->c20_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c20_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c20_covrtInstance, 4U, 0, 0, false);
  c20_a0 = c20_b_enable;
  c20_a = c20_a0;
  c20_b_a0 = c20_a;
  c20_a1 = c20_b_a0;
  c20_b_a1 = c20_a1;
  c20_c = (c20_b_a1 == 0);
  c20_i = (int8_T)c20_b_enable;
  if ((int8_T)(c20_i & 2) != 0) {
    c20_i1 = (int8_T)(c20_i | -2);
  } else {
    c20_i1 = (int8_T)(c20_i & 1);
  }

  if (c20_i1 > 0) {
    c20_d = 3.0;
  } else {
    c20_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c20_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c20_covrtInstance,
        4U, 0U, 0U, c20_d, 0.0, -2, 0U, (int32_T)c20_emlrt_update_log_4
        (chartInstance, c20_c, chartInstance->c20_emlrtLocationLoggingDataTables,
         5)))) {
    c20_b_cont = c20_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c20_emlrtLocationLoggingDataTables, 6);
    c20_b_out_init = c20_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c20_emlrtLocationLoggingDataTables, 7);
  } else {
    c20_c_a0 = c20_b_init;
    c20_b_a = c20_c_a0;
    c20_d_a0 = c20_b_a;
    c20_c_a1 = c20_d_a0;
    c20_d_a1 = c20_c_a1;
    c20_b_c = (c20_d_a1 == 0);
    c20_i2 = (int8_T)c20_b_init;
    if ((int8_T)(c20_i2 & 2) != 0) {
      c20_i3 = (int8_T)(c20_i2 | -2);
    } else {
      c20_i3 = (int8_T)(c20_i2 & 1);
    }

    if (c20_i3 > 0) {
      c20_d1 = 3.0;
    } else {
      c20_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c20_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c20_covrtInstance, 4U, 0U, 1U, c20_d1,
                        0.0, -2, 0U, (int32_T)c20_emlrt_update_log_4
                        (chartInstance, c20_b_c,
                         chartInstance->c20_emlrtLocationLoggingDataTables, 8))))
    {
      c20_b_varargin_1 = c20_b_sum;
      c20_d_varargin_1 = c20_b_varargin_1;
      c20_b_var1 = c20_d_varargin_1;
      c20_i5 = c20_b_var1;
      c20_b_covSaturation = false;
      if (c20_i5 < 0) {
        c20_i5 = 0;
      } else {
        if (c20_i5 > 4095) {
          c20_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 0, 0,
          c20_b_covSaturation);
      }

      c20_b_hfi = (uint16_T)c20_i5;
      c20_u1 = c20_emlrt_update_log_5(chartInstance, c20_b_hfi,
        chartInstance->c20_emlrtLocationLoggingDataTables, 10);
      c20_f_varargin_1 = c20_b_max;
      c20_h_varargin_1 = c20_f_varargin_1;
      c20_d_var1 = c20_h_varargin_1;
      c20_i7 = c20_d_var1;
      c20_d_covSaturation = false;
      if (c20_i7 < 0) {
        c20_i7 = 0;
      } else {
        if (c20_i7 > 4095) {
          c20_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 1, 0,
          c20_d_covSaturation);
      }

      c20_d_hfi = (uint16_T)c20_i7;
      c20_u3 = c20_emlrt_update_log_5(chartInstance, c20_d_hfi,
        chartInstance->c20_emlrtLocationLoggingDataTables, 11);
      c20_f_a0 = c20_u1;
      c20_b_b0 = c20_u3;
      c20_d_a = c20_f_a0;
      c20_b_b = c20_b_b0;
      c20_h_a0 = c20_d_a;
      c20_d_b0 = c20_b_b;
      c20_f_a1 = c20_h_a0;
      c20_b_b1 = c20_d_b0;
      c20_h_a1 = c20_f_a1;
      c20_d_b1 = c20_b_b1;
      c20_d_c = (c20_h_a1 < c20_d_b1);
      c20_i9 = (int16_T)c20_u1;
      c20_i11 = (int16_T)c20_u3;
      c20_i13 = (int16_T)c20_u3;
      c20_i15 = (int16_T)c20_u1;
      if ((int16_T)(c20_i13 & 4096) != 0) {
        c20_i17 = (int16_T)(c20_i13 | -4096);
      } else {
        c20_i17 = (int16_T)(c20_i13 & 4095);
      }

      if ((int16_T)(c20_i15 & 4096) != 0) {
        c20_i19 = (int16_T)(c20_i15 | -4096);
      } else {
        c20_i19 = (int16_T)(c20_i15 & 4095);
      }

      c20_i21 = c20_i17 - c20_i19;
      if (c20_i21 > 4095) {
        c20_i21 = 4095;
      } else {
        if (c20_i21 < -4096) {
          c20_i21 = -4096;
        }
      }

      c20_i23 = (int16_T)c20_u1;
      c20_i25 = (int16_T)c20_u3;
      c20_i27 = (int16_T)c20_u1;
      c20_i29 = (int16_T)c20_u3;
      if ((int16_T)(c20_i27 & 4096) != 0) {
        c20_i31 = (int16_T)(c20_i27 | -4096);
      } else {
        c20_i31 = (int16_T)(c20_i27 & 4095);
      }

      if ((int16_T)(c20_i29 & 4096) != 0) {
        c20_i33 = (int16_T)(c20_i29 | -4096);
      } else {
        c20_i33 = (int16_T)(c20_i29 & 4095);
      }

      c20_i35 = c20_i31 - c20_i33;
      if (c20_i35 > 4095) {
        c20_i35 = 4095;
      } else {
        if (c20_i35 < -4096) {
          c20_i35 = -4096;
        }
      }

      if ((int16_T)(c20_i9 & 4096) != 0) {
        c20_i37 = (int16_T)(c20_i9 | -4096);
      } else {
        c20_i37 = (int16_T)(c20_i9 & 4095);
      }

      if ((int16_T)(c20_i11 & 4096) != 0) {
        c20_i39 = (int16_T)(c20_i11 | -4096);
      } else {
        c20_i39 = (int16_T)(c20_i11 & 4095);
      }

      if ((int16_T)(c20_i23 & 4096) != 0) {
        c20_i41 = (int16_T)(c20_i23 | -4096);
      } else {
        c20_i41 = (int16_T)(c20_i23 & 4095);
      }

      if ((int16_T)(c20_i25 & 4096) != 0) {
        c20_i43 = (int16_T)(c20_i25 | -4096);
      } else {
        c20_i43 = (int16_T)(c20_i25 & 4095);
      }

      if (c20_i37 < c20_i39) {
        c20_d3 = (real_T)((int16_T)c20_i21 <= 1);
      } else if (c20_i41 > c20_i43) {
        if ((int16_T)c20_i35 <= 1) {
          c20_d3 = 3.0;
        } else {
          c20_d3 = 0.0;
        }
      } else {
        c20_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c20_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c20_covrtInstance, 4U, 0U, 2U, c20_d3,
                          0.0, -2, 2U, (int32_T)c20_emlrt_update_log_4
                          (chartInstance, c20_d_c,
                           chartInstance->c20_emlrtLocationLoggingDataTables, 9))))
      {
        c20_i_a0 = c20_b_sum;
        c20_e_b0 = c20_b_offset;
        c20_k_varargin_1 = c20_e_b0;
        c20_v = c20_k_varargin_1;
        c20_val = c20_v;
        c20_c_b = c20_val;
        c20_i45 = c20_i_a0;
        if (c20_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d4 = 1.0;
          observerLog(351 + c20_PICOffset, &c20_d4, 1);
        }

        if (c20_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d5 = 1.0;
          observerLog(351 + c20_PICOffset, &c20_d5, 1);
        }

        c20_i46 = c20_c_b;
        if (c20_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d6 = 1.0;
          observerLog(354 + c20_PICOffset, &c20_d6, 1);
        }

        if (c20_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d7 = 1.0;
          observerLog(354 + c20_PICOffset, &c20_d7, 1);
        }

        if ((c20_i45 & 65536) != 0) {
          c20_i47 = c20_i45 | -65536;
        } else {
          c20_i47 = c20_i45 & 65535;
        }

        if ((c20_i46 & 65536) != 0) {
          c20_i48 = c20_i46 | -65536;
        } else {
          c20_i48 = c20_i46 & 65535;
        }

        c20_i49 = c20__s32_add__(chartInstance, c20_i47, c20_i48, 353, 1U, 323,
          12);
        if (c20_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d8 = 1.0;
          observerLog(359 + c20_PICOffset, &c20_d8, 1);
        }

        if (c20_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c20_d9 = 1.0;
          observerLog(359 + c20_PICOffset, &c20_d9, 1);
        }

        if ((c20_i49 & 65536) != 0) {
          c20_e_c = c20_i49 | -65536;
        } else {
          c20_e_c = c20_i49 & 65535;
        }

        c20_l_varargin_1 = c20_emlrt_update_log_6(chartInstance, c20_e_c,
          chartInstance->c20_emlrtLocationLoggingDataTables, 13);
        c20_m_varargin_1 = c20_l_varargin_1;
        c20_f_var1 = c20_m_varargin_1;
        c20_i50 = c20_f_var1;
        c20_f_covSaturation = false;
        if (c20_i50 < 0) {
          c20_i50 = 0;
        } else {
          if (c20_i50 > 4095) {
            c20_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 2, 0,
            c20_f_covSaturation);
        }

        c20_f_hfi = (uint16_T)c20_i50;
        c20_b_cont = c20_emlrt_update_log_5(chartInstance, c20_f_hfi,
          chartInstance->c20_emlrtLocationLoggingDataTables, 12);
        c20_b_out_init = c20_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 14);
      } else {
        c20_b_cont = c20_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 15);
        c20_b_out_init = c20_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c20_varargin_1 = c20_b_sum;
      c20_c_varargin_1 = c20_varargin_1;
      c20_var1 = c20_c_varargin_1;
      c20_i4 = c20_var1;
      c20_covSaturation = false;
      if (c20_i4 < 0) {
        c20_i4 = 0;
      } else {
        if (c20_i4 > 4095) {
          c20_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 3, 0,
          c20_covSaturation);
      }

      c20_hfi = (uint16_T)c20_i4;
      c20_u = c20_emlrt_update_log_5(chartInstance, c20_hfi,
        chartInstance->c20_emlrtLocationLoggingDataTables, 18);
      c20_e_varargin_1 = c20_b_max;
      c20_g_varargin_1 = c20_e_varargin_1;
      c20_c_var1 = c20_g_varargin_1;
      c20_i6 = c20_c_var1;
      c20_c_covSaturation = false;
      if (c20_i6 < 0) {
        c20_i6 = 0;
      } else {
        if (c20_i6 > 4095) {
          c20_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 4, 0,
          c20_c_covSaturation);
      }

      c20_c_hfi = (uint16_T)c20_i6;
      c20_u2 = c20_emlrt_update_log_5(chartInstance, c20_c_hfi,
        chartInstance->c20_emlrtLocationLoggingDataTables, 19);
      c20_e_a0 = c20_u;
      c20_b0 = c20_u2;
      c20_c_a = c20_e_a0;
      c20_b = c20_b0;
      c20_g_a0 = c20_c_a;
      c20_c_b0 = c20_b;
      c20_e_a1 = c20_g_a0;
      c20_b1 = c20_c_b0;
      c20_g_a1 = c20_e_a1;
      c20_c_b1 = c20_b1;
      c20_c_c = (c20_g_a1 < c20_c_b1);
      c20_i8 = (int16_T)c20_u;
      c20_i10 = (int16_T)c20_u2;
      c20_i12 = (int16_T)c20_u2;
      c20_i14 = (int16_T)c20_u;
      if ((int16_T)(c20_i12 & 4096) != 0) {
        c20_i16 = (int16_T)(c20_i12 | -4096);
      } else {
        c20_i16 = (int16_T)(c20_i12 & 4095);
      }

      if ((int16_T)(c20_i14 & 4096) != 0) {
        c20_i18 = (int16_T)(c20_i14 | -4096);
      } else {
        c20_i18 = (int16_T)(c20_i14 & 4095);
      }

      c20_i20 = c20_i16 - c20_i18;
      if (c20_i20 > 4095) {
        c20_i20 = 4095;
      } else {
        if (c20_i20 < -4096) {
          c20_i20 = -4096;
        }
      }

      c20_i22 = (int16_T)c20_u;
      c20_i24 = (int16_T)c20_u2;
      c20_i26 = (int16_T)c20_u;
      c20_i28 = (int16_T)c20_u2;
      if ((int16_T)(c20_i26 & 4096) != 0) {
        c20_i30 = (int16_T)(c20_i26 | -4096);
      } else {
        c20_i30 = (int16_T)(c20_i26 & 4095);
      }

      if ((int16_T)(c20_i28 & 4096) != 0) {
        c20_i32 = (int16_T)(c20_i28 | -4096);
      } else {
        c20_i32 = (int16_T)(c20_i28 & 4095);
      }

      c20_i34 = c20_i30 - c20_i32;
      if (c20_i34 > 4095) {
        c20_i34 = 4095;
      } else {
        if (c20_i34 < -4096) {
          c20_i34 = -4096;
        }
      }

      if ((int16_T)(c20_i8 & 4096) != 0) {
        c20_i36 = (int16_T)(c20_i8 | -4096);
      } else {
        c20_i36 = (int16_T)(c20_i8 & 4095);
      }

      if ((int16_T)(c20_i10 & 4096) != 0) {
        c20_i38 = (int16_T)(c20_i10 | -4096);
      } else {
        c20_i38 = (int16_T)(c20_i10 & 4095);
      }

      if ((int16_T)(c20_i22 & 4096) != 0) {
        c20_i40 = (int16_T)(c20_i22 | -4096);
      } else {
        c20_i40 = (int16_T)(c20_i22 & 4095);
      }

      if ((int16_T)(c20_i24 & 4096) != 0) {
        c20_i42 = (int16_T)(c20_i24 | -4096);
      } else {
        c20_i42 = (int16_T)(c20_i24 & 4095);
      }

      if (c20_i36 < c20_i38) {
        c20_d2 = (real_T)((int16_T)c20_i20 <= 1);
      } else if (c20_i40 > c20_i42) {
        if ((int16_T)c20_i34 <= 1) {
          c20_d2 = 3.0;
        } else {
          c20_d2 = 0.0;
        }
      } else {
        c20_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c20_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c20_covrtInstance, 4U, 0U, 3U, c20_d2,
                          0.0, -2, 2U, (int32_T)c20_emlrt_update_log_4
                          (chartInstance, c20_c_c,
                           chartInstance->c20_emlrtLocationLoggingDataTables, 17))))
      {
        c20_i_varargin_1 = c20_b_sum;
        c20_j_varargin_1 = c20_i_varargin_1;
        c20_e_var1 = c20_j_varargin_1;
        c20_i44 = c20_e_var1;
        c20_e_covSaturation = false;
        if (c20_i44 < 0) {
          c20_i44 = 0;
        } else {
          if (c20_i44 > 4095) {
            c20_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c20_covrtInstance, 4, 0, 5, 0,
            c20_e_covSaturation);
        }

        c20_e_hfi = (uint16_T)c20_i44;
        c20_b_cont = c20_emlrt_update_log_5(chartInstance, c20_e_hfi,
          chartInstance->c20_emlrtLocationLoggingDataTables, 20);
        c20_b_out_init = c20_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 21);
      } else {
        c20_b_cont = c20_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 22);
        c20_b_out_init = c20_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c20_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c20_cont = c20_b_cont;
  *chartInstance->c20_out_init = c20_b_out_init;
  c20_do_animation_call_c20_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 5U, (real_T)
                    *chartInstance->c20_cont);
  covrtSigUpdateFcn(chartInstance->c20_covrtInstance, 6U, (real_T)
                    *chartInstance->c20_out_init);
}

static void mdl_start_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c20_decisionTxtStartIdx = 0U;
  static const uint32_T c20_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c20_chart_data_browse_helper);
  chartInstance->c20_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c20_RuntimeVar,
    &chartInstance->c20_IsDebuggerActive,
    &chartInstance->c20_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c20_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c20_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c20_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c20_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c20_decisionTxtStartIdx, &c20_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c20_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c20_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c20_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c20_PWM_28_HalfB
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c20_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7661",              /* mexFileName */
    "Thu May 27 10:27:26 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c20_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c20_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c20_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c20_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7661");
    emlrtLocationLoggingPushLog(&c20_emlrtLocationLoggingFileInfo,
      c20_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c20_emlrtLocationLoggingDataTables, c20_emlrtLocationInfo,
      NULL, 0U, c20_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7661");
  }

  sfListenerLightTerminate(chartInstance->c20_RuntimeVar);
  sf_mex_destroy(&c20_eml_mx);
  sf_mex_destroy(&c20_b_eml_mx);
  sf_mex_destroy(&c20_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c20_covrtInstance);
}

static void initSimStructsc20_PWM_28_HalfB(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c20_emlrt_update_log_1(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  real_T c20_d;
  uint8_T c20_u;
  uint8_T c20_localMin;
  real_T c20_d1;
  uint8_T c20_u1;
  uint8_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_d = c20_b_table[0U].SimMin;
    if (c20_d < 2.0) {
      if (c20_d >= 0.0) {
        c20_u = (uint8_T)c20_d;
      } else {
        c20_u = 0U;
      }
    } else if (c20_d >= 2.0) {
      c20_u = 1U;
    } else {
      c20_u = 0U;
    }

    c20_localMin = c20_u;
    c20_d1 = c20_b_table[0U].SimMax;
    if (c20_d1 < 2.0) {
      if (c20_d1 >= 0.0) {
        c20_u1 = (uint8_T)c20_d1;
      } else {
        c20_u1 = 0U;
      }
    } else if (c20_d1 >= 2.0) {
      c20_u1 = 1U;
    } else {
      c20_u1 = 0U;
    }

    c20_localMax = c20_u1;
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c20_in < c20_localMin) {
      c20_localMin = c20_in;
    }

    if (c20_in > c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static int16_T c20_emlrt_update_log_2(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  real_T c20_d;
  int16_T c20_i;
  int16_T c20_localMin;
  real_T c20_d1;
  int16_T c20_i1;
  int16_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_d = muDoubleScalarFloor(c20_b_table[0U].SimMin);
    if (c20_d < 32768.0) {
      if (c20_d >= -32768.0) {
        c20_i = (int16_T)c20_d;
      } else {
        c20_i = MIN_int16_T;
      }
    } else if (c20_d >= 32768.0) {
      c20_i = MAX_int16_T;
    } else {
      c20_i = 0;
    }

    c20_localMin = c20_i;
    c20_d1 = muDoubleScalarFloor(c20_b_table[0U].SimMax);
    if (c20_d1 < 32768.0) {
      if (c20_d1 >= -32768.0) {
        c20_i1 = (int16_T)c20_d1;
      } else {
        c20_i1 = MIN_int16_T;
      }
    } else if (c20_d1 >= 32768.0) {
      c20_i1 = MAX_int16_T;
    } else {
      c20_i1 = 0;
    }

    c20_localMax = c20_i1;
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c20_in < c20_localMin) {
      c20_localMin = c20_in;
    }

    if (c20_in > c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static int16_T c20_emlrt_update_log_3(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  real_T c20_d;
  int16_T c20_i;
  int16_T c20_localMin;
  real_T c20_d1;
  int16_T c20_i1;
  int16_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_d = muDoubleScalarFloor(c20_b_table[0U].SimMin);
    if (c20_d < 2048.0) {
      if (c20_d >= -2048.0) {
        c20_i = (int16_T)c20_d;
      } else {
        c20_i = -2048;
      }
    } else if (c20_d >= 2048.0) {
      c20_i = 2047;
    } else {
      c20_i = 0;
    }

    c20_localMin = c20_i;
    c20_d1 = muDoubleScalarFloor(c20_b_table[0U].SimMax);
    if (c20_d1 < 2048.0) {
      if (c20_d1 >= -2048.0) {
        c20_i1 = (int16_T)c20_d1;
      } else {
        c20_i1 = -2048;
      }
    } else if (c20_d1 >= 2048.0) {
      c20_i1 = 2047;
    } else {
      c20_i1 = 0;
    }

    c20_localMax = c20_i1;
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c20_in < c20_localMin) {
      c20_localMin = c20_in;
    }

    if (c20_in > c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static boolean_T c20_emlrt_update_log_4(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  boolean_T c20_localMin;
  boolean_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_localMin = (c20_b_table[0U].SimMin > 0.0);
    c20_localMax = (c20_b_table[0U].SimMax > 0.0);
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c20_in < (int32_T)c20_localMin) {
      c20_localMin = c20_in;
    }

    if ((int32_T)c20_in > (int32_T)c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static uint16_T c20_emlrt_update_log_5(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  real_T c20_d;
  uint16_T c20_u;
  uint16_T c20_localMin;
  real_T c20_d1;
  uint16_T c20_u1;
  uint16_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_d = c20_b_table[0U].SimMin;
    if (c20_d < 4096.0) {
      if (c20_d >= 0.0) {
        c20_u = (uint16_T)c20_d;
      } else {
        c20_u = 0U;
      }
    } else if (c20_d >= 4096.0) {
      c20_u = 4095U;
    } else {
      c20_u = 0U;
    }

    c20_localMin = c20_u;
    c20_d1 = c20_b_table[0U].SimMax;
    if (c20_d1 < 4096.0) {
      if (c20_d1 >= 0.0) {
        c20_u1 = (uint16_T)c20_d1;
      } else {
        c20_u1 = 0U;
      }
    } else if (c20_d1 >= 4096.0) {
      c20_u1 = 4095U;
    } else {
      c20_u1 = 0U;
    }

    c20_localMax = c20_u1;
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c20_in < c20_localMin) {
      c20_localMin = c20_in;
    }

    if (c20_in > c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static int32_T c20_emlrt_update_log_6(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c20_in, emlrtLocationLoggingDataType c20_table[],
  int32_T c20_index)
{
  boolean_T c20_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c20_b_table;
  real_T c20_d;
  int32_T c20_i;
  int32_T c20_localMin;
  real_T c20_d1;
  int32_T c20_i1;
  int32_T c20_localMax;
  emlrtLocationLoggingHistogramType *c20_histTable;
  real_T c20_inDouble;
  real_T c20_significand;
  int32_T c20_exponent;
  (void)chartInstance;
  c20_isLoggingEnabledHere = (c20_index >= 0);
  if (c20_isLoggingEnabledHere) {
    c20_b_table = (emlrtLocationLoggingDataType *)&c20_table[c20_index];
    c20_d = muDoubleScalarFloor(c20_b_table[0U].SimMin);
    if (c20_d < 65536.0) {
      if (c20_d >= -65536.0) {
        c20_i = (int32_T)c20_d;
      } else {
        c20_i = -65536;
      }
    } else if (c20_d >= 65536.0) {
      c20_i = 65535;
    } else {
      c20_i = 0;
    }

    c20_localMin = c20_i;
    c20_d1 = muDoubleScalarFloor(c20_b_table[0U].SimMax);
    if (c20_d1 < 65536.0) {
      if (c20_d1 >= -65536.0) {
        c20_i1 = (int32_T)c20_d1;
      } else {
        c20_i1 = -65536;
      }
    } else if (c20_d1 >= 65536.0) {
      c20_i1 = 65535;
    } else {
      c20_i1 = 0;
    }

    c20_localMax = c20_i1;
    c20_histTable = c20_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c20_in < c20_localMin) {
      c20_localMin = c20_in;
    }

    if (c20_in > c20_localMax) {
      c20_localMax = c20_in;
    }

    /* Histogram logging. */
    c20_inDouble = (real_T)c20_in;
    c20_histTable->TotalNumberOfValues++;
    if (c20_inDouble == 0.0) {
      c20_histTable->NumberOfZeros++;
    } else {
      c20_histTable->SimSum += c20_inDouble;
      if ((!muDoubleScalarIsInf(c20_inDouble)) && (!muDoubleScalarIsNaN
           (c20_inDouble))) {
        c20_significand = frexp(c20_inDouble, &c20_exponent);
        if (c20_exponent > 128) {
          c20_exponent = 128;
        }

        if (c20_exponent < -127) {
          c20_exponent = -127;
        }

        if (c20_significand < 0.0) {
          c20_histTable->NumberOfNegativeValues++;
          c20_histTable->HistogramOfNegativeValues[127 + c20_exponent]++;
        } else {
          c20_histTable->NumberOfPositiveValues++;
          c20_histTable->HistogramOfPositiveValues[127 + c20_exponent]++;
        }
      }
    }

    c20_b_table[0U].SimMin = (real_T)c20_localMin;
    c20_b_table[0U].SimMax = (real_T)c20_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c20_in;
}

static void c20_emlrtInitVarDataTables(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c20_dataTables[24],
  emlrtLocationLoggingHistogramType c20_histTables[24])
{
  int32_T c20_i;
  int32_T c20_iH;
  (void)chartInstance;
  for (c20_i = 0; c20_i < 24; c20_i++) {
    c20_dataTables[c20_i].SimMin = rtInf;
    c20_dataTables[c20_i].SimMax = rtMinusInf;
    c20_dataTables[c20_i].OverflowWraps = 0;
    c20_dataTables[c20_i].Saturations = 0;
    c20_dataTables[c20_i].IsAlwaysInteger = true;
    c20_dataTables[c20_i].HistogramTable = &c20_histTables[c20_i];
    c20_histTables[c20_i].NumberOfZeros = 0.0;
    c20_histTables[c20_i].NumberOfPositiveValues = 0.0;
    c20_histTables[c20_i].NumberOfNegativeValues = 0.0;
    c20_histTables[c20_i].TotalNumberOfValues = 0.0;
    c20_histTables[c20_i].SimSum = 0.0;
    for (c20_iH = 0; c20_iH < 256; c20_iH++) {
      c20_histTables[c20_i].HistogramOfPositiveValues[c20_iH] = 0.0;
      c20_histTables[c20_i].HistogramOfNegativeValues[c20_iH] = 0.0;
    }
  }
}

const mxArray *sf_c20_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c20_nameCaptureInfo = NULL;
  c20_nameCaptureInfo = NULL;
  sf_mex_assign(&c20_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c20_nameCaptureInfo;
}

static uint16_T c20_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_b_cont, const
  char_T *c20_identifier)
{
  uint16_T c20_y;
  emlrtMsgIdentifier c20_thisId;
  c20_thisId.fIdentifier = (const char *)c20_identifier;
  c20_thisId.fParent = NULL;
  c20_thisId.bParentIsCell = false;
  c20_y = c20_b_emlrt_marshallIn(chartInstance, c20_sp, sf_mex_dup(c20_b_cont),
    &c20_thisId);
  sf_mex_destroy(&c20_b_cont);
  return c20_y;
}

static uint16_T c20_b_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_u, const
  emlrtMsgIdentifier *c20_parentId)
{
  uint16_T c20_y;
  const mxArray *c20_mxFi = NULL;
  const mxArray *c20_mxInt = NULL;
  uint16_T c20_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c20_parentId, c20_u, false, 0U, NULL, c20_eml_mx, c20_b_eml_mx);
  sf_mex_assign(&c20_mxFi, sf_mex_dup(c20_u), false);
  sf_mex_assign(&c20_mxInt, sf_mex_call(c20_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c20_mxFi)), false);
  sf_mex_import(c20_parentId, sf_mex_dup(c20_mxInt), &c20_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c20_mxFi);
  sf_mex_destroy(&c20_mxInt);
  c20_y = c20_b_u;
  sf_mex_destroy(&c20_mxFi);
  sf_mex_destroy(&c20_u);
  return c20_y;
}

static uint8_T c20_c_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_b_out_init, const
  char_T *c20_identifier)
{
  uint8_T c20_y;
  emlrtMsgIdentifier c20_thisId;
  c20_thisId.fIdentifier = (const char *)c20_identifier;
  c20_thisId.fParent = NULL;
  c20_thisId.bParentIsCell = false;
  c20_y = c20_d_emlrt_marshallIn(chartInstance, c20_sp, sf_mex_dup
    (c20_b_out_init), &c20_thisId);
  sf_mex_destroy(&c20_b_out_init);
  return c20_y;
}

static uint8_T c20_d_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c20_sp, const mxArray *c20_u, const
  emlrtMsgIdentifier *c20_parentId)
{
  uint8_T c20_y;
  const mxArray *c20_mxFi = NULL;
  const mxArray *c20_mxInt = NULL;
  uint8_T c20_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c20_parentId, c20_u, false, 0U, NULL, c20_eml_mx, c20_c_eml_mx);
  sf_mex_assign(&c20_mxFi, sf_mex_dup(c20_u), false);
  sf_mex_assign(&c20_mxInt, sf_mex_call(c20_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c20_mxFi)), false);
  sf_mex_import(c20_parentId, sf_mex_dup(c20_mxInt), &c20_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c20_mxFi);
  sf_mex_destroy(&c20_mxInt);
  c20_y = c20_b_u;
  sf_mex_destroy(&c20_mxFi);
  sf_mex_destroy(&c20_u);
  return c20_y;
}

static uint8_T c20_e_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_b_is_active_c20_PWM_28_HalfB, const char_T *
  c20_identifier)
{
  uint8_T c20_y;
  emlrtMsgIdentifier c20_thisId;
  c20_thisId.fIdentifier = (const char *)c20_identifier;
  c20_thisId.fParent = NULL;
  c20_thisId.bParentIsCell = false;
  c20_y = c20_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c20_b_is_active_c20_PWM_28_HalfB), &c20_thisId);
  sf_mex_destroy(&c20_b_is_active_c20_PWM_28_HalfB);
  return c20_y;
}

static uint8_T c20_f_emlrt_marshallIn(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c20_u, const emlrtMsgIdentifier *c20_parentId)
{
  uint8_T c20_y;
  uint8_T c20_b_u;
  (void)chartInstance;
  sf_mex_import(c20_parentId, sf_mex_dup(c20_u), &c20_b_u, 1, 3, 0U, 0, 0U, 0);
  c20_y = c20_b_u;
  sf_mex_destroy(&c20_u);
  return c20_y;
}

static const mxArray *c20_chart_data_browse_helper
  (SFc20_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c20_ssIdNumber)
{
  const mxArray *c20_mxData = NULL;
  uint8_T c20_u;
  int16_T c20_i;
  int16_T c20_i1;
  int16_T c20_i2;
  uint16_T c20_u1;
  uint8_T c20_u2;
  uint8_T c20_u3;
  real_T c20_d;
  real_T c20_d1;
  real_T c20_d2;
  real_T c20_d3;
  real_T c20_d4;
  real_T c20_d5;
  c20_mxData = NULL;
  switch (c20_ssIdNumber) {
   case 18U:
    c20_u = *chartInstance->c20_enable;
    c20_d = (real_T)c20_u;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c20_i = *chartInstance->c20_offset;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c20_i1 = *chartInstance->c20_max;
    c20_d1 = (real_T)c20_i1;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c20_i2 = *chartInstance->c20_sum;
    c20_d2 = (real_T)c20_i2;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c20_u1 = *chartInstance->c20_cont;
    c20_d3 = (real_T)c20_u1;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c20_u2 = *chartInstance->c20_init;
    c20_d4 = (real_T)c20_u2;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c20_u3 = *chartInstance->c20_out_init;
    c20_d5 = (real_T)c20_u3;
    sf_mex_assign(&c20_mxData, sf_mex_create("mxData", &c20_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c20_mxData;
}

static int32_T c20__s32_add__(SFc20_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c20_b, int32_T c20_c, int32_T c20_EMLOvCount_src_loc, uint32_T
  c20_ssid_src_loc, int32_T c20_offset_src_loc, int32_T c20_length_src_loc)
{
  int32_T c20_a;
  int32_T c20_PICOffset;
  real_T c20_d;
  observerLogReadPIC(&c20_PICOffset);
  c20_a = c20_b + c20_c;
  if (((c20_a ^ c20_b) & (c20_a ^ c20_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c20_ssid_src_loc,
      c20_offset_src_loc, c20_length_src_loc);
    c20_d = 1.0;
    observerLog(c20_EMLOvCount_src_loc + c20_PICOffset, &c20_d, 1);
  }

  return c20_a;
}

static void init_dsm_address_info(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc20_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c20_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c20_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c20_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c20_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c20_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c20_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c20_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c20_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c20_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c20_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c20_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c20_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c20_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c20_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyykUF8QLhvvJFFv"
    "EdiTpoTwlwQAADrYSCO"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c20_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c20_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c20_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c20_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc20_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c20_PWM_28_HalfB
      ((SFc20_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c20_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c20_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc20_PWM_28_HalfB((SFc20_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c20_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5SNDGcLopYARodu2qSII0yCp2TEmxAKlWQ+dnJ4yHT+JAwxl6fmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4qgxEiADUMQMv/fN+5+RV+v0PByb+Lz+yvOu4/sGPnVvOjayeW",
    "3hma43vG+z+Q4KCRv3iSKx9iqHIDE8By25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "YxGQSQtD/dQloQHgp8hW2JNH3maTAE1bYDQREraUdTmZDTXWJkTPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgXaENgQ11ue6BYYY8M1pqZnOUh3MgDJOOCOi0NqI6AASdLCBF0mIvwfWoFF5GI2IMnsQkQn",
    "oLhunnFJAnpNp/HDEBDFSMcJbMfed4LJufY769GQIvMIhqNueAjJOJBOmPP5BGy1tCXLEoQlHdl",
    "TOFsCxdcF/yeAEVKnfhr6cgCIjOBClm6YOaZ2m0ZpnyTLMsBheEvWUYvw0hKXZi5mjA4JxgkOUK",
    "INBamRHHyo2QfeWstm44zJzVcnYeBpsvQqWsrUmUBWFOVubCp9wrkthhzLpwgR4ytokhlTDpqzF",
    "OK1ZeCjRwS69y6vBCoaBz2C+FCErDNckB0j7zo/YWC4iqdVGxj4mb7PbXf68DOsIA2pIKBR1AUW",
    "YBvRZ6t5ytpBpF3sEolYmVa8IPM2QVShPD61onkg1Rp9UNJFzE1xES4GxHmEssRJeaCyaKpiL5S",
    "ocJTSC0DUYxqGHZYPYAp9o19qeYt1NmDlrgqaKJUVRdefPPe/8/PnyHc6fmVz+/d0CT62Ax1t4O",
    "/zjBfzN+kX8Rm7f+mzNjUx+d0H+Vm6/Rk7e4bZc5bz9/Zefb//zxV8//fpH528wefvzetSW9Kh5",
    "s/2Ta5c7tzez+Z1Zg5wn/GQpzxx2f0GvRgH/1wv8W9lcP9xvB73x6/vHz+/DM2X4m1iJR37K91u",
    "9Wt9rOX1n63ddpz5L0r6rFe2E2YXCzYmdHrP5eF5f4Y+b2fp0/Luznvz3u/k4FvmrccFfDY9KYT",
    "ZL8vFq9f9hNy9fpP+NXLzdXFozYIL9T3bc3V1Pfrp/f4Ud2zk7ttN7xYC4bgUD+uDeoP+qN3jwe",
    "LBP+HBvuc+8b71eVs67YrlPRc/Pfvnw9r3LObzxnnL1Nc/9q5Jb177L3kc+NnzVeebl8FsfsR3r",
    "3hM/NP5P73L3uG+y+ZP5Xyw/YjwsuG1nn7tAhkVfr8C+/wAJv6B7",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c20_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c20_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c20_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c20_PWM_28_HalfB(SimStruct *S)
{
  SFc20_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc20_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc20_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc20_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c20_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c20_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c20_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c20_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c20_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c20_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c20_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c20_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c20_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c20_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c20_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c20_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c20_JITStateAnimation,
    chartInstance->c20_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c20_PWM_28_HalfB(chartInstance);
}

void c20_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c20_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c20_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c20_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c20_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
