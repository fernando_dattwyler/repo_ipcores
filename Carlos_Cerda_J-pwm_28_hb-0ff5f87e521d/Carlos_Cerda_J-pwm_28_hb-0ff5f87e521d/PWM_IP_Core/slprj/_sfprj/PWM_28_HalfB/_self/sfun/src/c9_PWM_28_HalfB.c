/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c9_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c9_eml_mx;
static const mxArray *c9_b_eml_mx;
static const mxArray *c9_c_eml_mx;

/* Function Declarations */
static void initialize_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c9_update_jit_animation_state_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance);
static void c9_do_animation_call_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_st);
static void sf_gateway_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c9_emlrt_update_log_1(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static int16_T c9_emlrt_update_log_2(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static int16_T c9_emlrt_update_log_3(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static boolean_T c9_emlrt_update_log_4(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static uint16_T c9_emlrt_update_log_5(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static int32_T c9_emlrt_update_log_6(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index);
static void c9_emlrtInitVarDataTables(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c9_dataTables[24],
  emlrtLocationLoggingHistogramType c9_histTables[24]);
static uint16_T c9_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_b_cont, const
  char_T *c9_identifier);
static uint16_T c9_b_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_u, const
  emlrtMsgIdentifier *c9_parentId);
static uint8_T c9_c_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_b_out_init, const
  char_T *c9_identifier);
static uint8_T c9_d_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_u, const
  emlrtMsgIdentifier *c9_parentId);
static uint8_T c9_e_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_b_is_active_c9_PWM_28_HalfB, const char_T
  *c9_identifier);
static uint8_T c9_f_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static const mxArray *c9_chart_data_browse_helper
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c9_ssIdNumber);
static int32_T c9__s32_add__(SFc9_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c9_b, int32_T c9_c, int32_T c9_EMLOvCount_src_loc, uint32_T
  c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T c9_length_src_loc);
static void init_dsm_address_info(SFc9_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c9_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c9_st.tls = chartInstance->c9_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c9_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c9_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c9_is_active_c9_PWM_28_HalfB = 0U;
  sf_mex_assign(&c9_c_eml_mx, sf_mex_call(&c9_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c9_b_eml_mx, sf_mex_call(&c9_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c9_eml_mx, sf_mex_call(&c9_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c9_emlrtLocLogSimulated = false;
  c9_emlrtInitVarDataTables(chartInstance,
    chartInstance->c9_emlrtLocationLoggingDataTables,
    chartInstance->c9_emlrtLocLogHistTables);
}

static void initialize_params_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c9_update_jit_animation_state_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c9_do_animation_call_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c9_st;
  const mxArray *c9_y = NULL;
  const mxArray *c9_b_y = NULL;
  uint16_T c9_u;
  const mxArray *c9_c_y = NULL;
  const mxArray *c9_d_y = NULL;
  uint8_T c9_b_u;
  const mxArray *c9_e_y = NULL;
  const mxArray *c9_f_y = NULL;
  c9_st = NULL;
  c9_st = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createcellmatrix(3, 1), false);
  c9_b_y = NULL;
  c9_u = *chartInstance->c9_cont;
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_b_y, sf_mex_create_fi(sf_mex_dup(c9_eml_mx), sf_mex_dup
    (c9_b_eml_mx), "simulinkarray", c9_c_y, false, false), false);
  sf_mex_setcell(c9_y, 0, c9_b_y);
  c9_d_y = NULL;
  c9_b_u = *chartInstance->c9_out_init;
  c9_e_y = NULL;
  sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_d_y, sf_mex_create_fi(sf_mex_dup(c9_eml_mx), sf_mex_dup
    (c9_c_eml_mx), "simulinkarray", c9_e_y, false, false), false);
  sf_mex_setcell(c9_y, 1, c9_d_y);
  c9_f_y = NULL;
  sf_mex_assign(&c9_f_y, sf_mex_create("y",
    &chartInstance->c9_is_active_c9_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 2, c9_f_y);
  sf_mex_assign(&c9_st, c9_y, false);
  return c9_st;
}

static void set_sim_state_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_st)
{
  emlrtStack c9_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c9_u;
  c9_b_st.tls = chartInstance->c9_fEmlrtCtx;
  chartInstance->c9_doneDoubleBufferReInit = true;
  c9_u = sf_mex_dup(c9_st);
  *chartInstance->c9_cont = c9_emlrt_marshallIn(chartInstance, &c9_b_st,
    sf_mex_dup(sf_mex_getcell(c9_u, 0)), "cont");
  *chartInstance->c9_out_init = c9_c_emlrt_marshallIn(chartInstance, &c9_b_st,
    sf_mex_dup(sf_mex_getcell(c9_u, 1)), "out_init");
  chartInstance->c9_is_active_c9_PWM_28_HalfB = c9_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 2)),
     "is_active_c9_PWM_28_HalfB");
  sf_mex_destroy(&c9_u);
  sf_mex_destroy(&c9_st);
}

static void sf_gateway_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c9_PICOffset;
  uint8_T c9_b_enable;
  int16_T c9_b_offset;
  int16_T c9_b_max;
  int16_T c9_b_sum;
  uint8_T c9_b_init;
  uint8_T c9_a0;
  uint8_T c9_a;
  uint8_T c9_b_a0;
  uint8_T c9_a1;
  uint8_T c9_b_a1;
  boolean_T c9_c;
  int8_T c9_i;
  int8_T c9_i1;
  real_T c9_d;
  uint8_T c9_c_a0;
  uint16_T c9_b_cont;
  uint8_T c9_b_a;
  uint8_T c9_b_out_init;
  uint8_T c9_d_a0;
  uint8_T c9_c_a1;
  uint8_T c9_d_a1;
  boolean_T c9_b_c;
  int8_T c9_i2;
  int8_T c9_i3;
  real_T c9_d1;
  int16_T c9_varargin_1;
  int16_T c9_b_varargin_1;
  int16_T c9_c_varargin_1;
  int16_T c9_d_varargin_1;
  int16_T c9_var1;
  int16_T c9_b_var1;
  int16_T c9_i4;
  int16_T c9_i5;
  boolean_T c9_covSaturation;
  boolean_T c9_b_covSaturation;
  uint16_T c9_hfi;
  uint16_T c9_b_hfi;
  uint16_T c9_u;
  uint16_T c9_u1;
  int16_T c9_e_varargin_1;
  int16_T c9_f_varargin_1;
  int16_T c9_g_varargin_1;
  int16_T c9_h_varargin_1;
  int16_T c9_c_var1;
  int16_T c9_d_var1;
  int16_T c9_i6;
  int16_T c9_i7;
  boolean_T c9_c_covSaturation;
  boolean_T c9_d_covSaturation;
  uint16_T c9_c_hfi;
  uint16_T c9_d_hfi;
  uint16_T c9_u2;
  uint16_T c9_u3;
  uint16_T c9_e_a0;
  uint16_T c9_f_a0;
  uint16_T c9_b0;
  uint16_T c9_b_b0;
  uint16_T c9_c_a;
  uint16_T c9_d_a;
  uint16_T c9_b;
  uint16_T c9_b_b;
  uint16_T c9_g_a0;
  uint16_T c9_h_a0;
  uint16_T c9_c_b0;
  uint16_T c9_d_b0;
  uint16_T c9_e_a1;
  uint16_T c9_f_a1;
  uint16_T c9_b1;
  uint16_T c9_b_b1;
  uint16_T c9_g_a1;
  uint16_T c9_h_a1;
  uint16_T c9_c_b1;
  uint16_T c9_d_b1;
  boolean_T c9_c_c;
  boolean_T c9_d_c;
  int16_T c9_i8;
  int16_T c9_i9;
  int16_T c9_i10;
  int16_T c9_i11;
  int16_T c9_i12;
  int16_T c9_i13;
  int16_T c9_i14;
  int16_T c9_i15;
  int16_T c9_i16;
  int16_T c9_i17;
  int16_T c9_i18;
  int16_T c9_i19;
  int32_T c9_i20;
  int32_T c9_i21;
  int16_T c9_i22;
  int16_T c9_i23;
  int16_T c9_i24;
  int16_T c9_i25;
  int16_T c9_i26;
  int16_T c9_i27;
  int16_T c9_i28;
  int16_T c9_i29;
  int16_T c9_i30;
  int16_T c9_i31;
  int16_T c9_i32;
  int16_T c9_i33;
  int32_T c9_i34;
  int32_T c9_i35;
  int16_T c9_i36;
  int16_T c9_i37;
  int16_T c9_i38;
  int16_T c9_i39;
  int16_T c9_i40;
  int16_T c9_i41;
  int16_T c9_i42;
  int16_T c9_i43;
  real_T c9_d2;
  real_T c9_d3;
  int16_T c9_i_varargin_1;
  int16_T c9_i_a0;
  int16_T c9_j_varargin_1;
  int16_T c9_e_b0;
  int16_T c9_e_var1;
  int16_T c9_k_varargin_1;
  int16_T c9_i44;
  int16_T c9_v;
  boolean_T c9_e_covSaturation;
  int16_T c9_val;
  int16_T c9_c_b;
  int32_T c9_i45;
  uint16_T c9_e_hfi;
  real_T c9_d4;
  int32_T c9_i46;
  real_T c9_d5;
  real_T c9_d6;
  real_T c9_d7;
  int32_T c9_i47;
  int32_T c9_i48;
  int32_T c9_i49;
  real_T c9_d8;
  real_T c9_d9;
  int32_T c9_e_c;
  int32_T c9_l_varargin_1;
  int32_T c9_m_varargin_1;
  int32_T c9_f_var1;
  int32_T c9_i50;
  boolean_T c9_f_covSaturation;
  uint16_T c9_f_hfi;
  observerLogReadPIC(&c9_PICOffset);
  chartInstance->c9_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 4U, (real_T)
                    *chartInstance->c9_init);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 3U, (real_T)
                    *chartInstance->c9_sum);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 2U, (real_T)
                    *chartInstance->c9_max);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 1U, (real_T)
                    *chartInstance->c9_offset);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 0U, (real_T)
                    *chartInstance->c9_enable);
  chartInstance->c9_sfEvent = CALL_EVENT;
  c9_b_enable = *chartInstance->c9_enable;
  c9_b_offset = *chartInstance->c9_offset;
  c9_b_max = *chartInstance->c9_max;
  c9_b_sum = *chartInstance->c9_sum;
  c9_b_init = *chartInstance->c9_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c9_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c9_emlrt_update_log_1(chartInstance, c9_b_enable,
                        chartInstance->c9_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c9_emlrt_update_log_2(chartInstance, c9_b_offset,
                        chartInstance->c9_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c9_emlrt_update_log_3(chartInstance, c9_b_max,
                        chartInstance->c9_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c9_emlrt_update_log_3(chartInstance, c9_b_sum,
                        chartInstance->c9_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c9_emlrt_update_log_1(chartInstance, c9_b_init,
                        chartInstance->c9_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c9_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c9_covrtInstance, 4U, 0, 0, false);
  c9_a0 = c9_b_enable;
  c9_a = c9_a0;
  c9_b_a0 = c9_a;
  c9_a1 = c9_b_a0;
  c9_b_a1 = c9_a1;
  c9_c = (c9_b_a1 == 0);
  c9_i = (int8_T)c9_b_enable;
  if ((int8_T)(c9_i & 2) != 0) {
    c9_i1 = (int8_T)(c9_i | -2);
  } else {
    c9_i1 = (int8_T)(c9_i & 1);
  }

  if (c9_i1 > 0) {
    c9_d = 3.0;
  } else {
    c9_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c9_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c9_covrtInstance,
        4U, 0U, 0U, c9_d, 0.0, -2, 0U, (int32_T)c9_emlrt_update_log_4
        (chartInstance, c9_c, chartInstance->c9_emlrtLocationLoggingDataTables,
         5)))) {
    c9_b_cont = c9_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c9_emlrtLocationLoggingDataTables, 6);
    c9_b_out_init = c9_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c9_emlrtLocationLoggingDataTables, 7);
  } else {
    c9_c_a0 = c9_b_init;
    c9_b_a = c9_c_a0;
    c9_d_a0 = c9_b_a;
    c9_c_a1 = c9_d_a0;
    c9_d_a1 = c9_c_a1;
    c9_b_c = (c9_d_a1 == 0);
    c9_i2 = (int8_T)c9_b_init;
    if ((int8_T)(c9_i2 & 2) != 0) {
      c9_i3 = (int8_T)(c9_i2 | -2);
    } else {
      c9_i3 = (int8_T)(c9_i2 & 1);
    }

    if (c9_i3 > 0) {
      c9_d1 = 3.0;
    } else {
      c9_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c9_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c9_covrtInstance, 4U, 0U, 1U, c9_d1, 0.0,
                        -2, 0U, (int32_T)c9_emlrt_update_log_4(chartInstance,
           c9_b_c, chartInstance->c9_emlrtLocationLoggingDataTables, 8)))) {
      c9_b_varargin_1 = c9_b_sum;
      c9_d_varargin_1 = c9_b_varargin_1;
      c9_b_var1 = c9_d_varargin_1;
      c9_i5 = c9_b_var1;
      c9_b_covSaturation = false;
      if (c9_i5 < 0) {
        c9_i5 = 0;
      } else {
        if (c9_i5 > 4095) {
          c9_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 0, 0,
          c9_b_covSaturation);
      }

      c9_b_hfi = (uint16_T)c9_i5;
      c9_u1 = c9_emlrt_update_log_5(chartInstance, c9_b_hfi,
        chartInstance->c9_emlrtLocationLoggingDataTables, 10);
      c9_f_varargin_1 = c9_b_max;
      c9_h_varargin_1 = c9_f_varargin_1;
      c9_d_var1 = c9_h_varargin_1;
      c9_i7 = c9_d_var1;
      c9_d_covSaturation = false;
      if (c9_i7 < 0) {
        c9_i7 = 0;
      } else {
        if (c9_i7 > 4095) {
          c9_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 1, 0,
          c9_d_covSaturation);
      }

      c9_d_hfi = (uint16_T)c9_i7;
      c9_u3 = c9_emlrt_update_log_5(chartInstance, c9_d_hfi,
        chartInstance->c9_emlrtLocationLoggingDataTables, 11);
      c9_f_a0 = c9_u1;
      c9_b_b0 = c9_u3;
      c9_d_a = c9_f_a0;
      c9_b_b = c9_b_b0;
      c9_h_a0 = c9_d_a;
      c9_d_b0 = c9_b_b;
      c9_f_a1 = c9_h_a0;
      c9_b_b1 = c9_d_b0;
      c9_h_a1 = c9_f_a1;
      c9_d_b1 = c9_b_b1;
      c9_d_c = (c9_h_a1 < c9_d_b1);
      c9_i9 = (int16_T)c9_u1;
      c9_i11 = (int16_T)c9_u3;
      c9_i13 = (int16_T)c9_u3;
      c9_i15 = (int16_T)c9_u1;
      if ((int16_T)(c9_i13 & 4096) != 0) {
        c9_i17 = (int16_T)(c9_i13 | -4096);
      } else {
        c9_i17 = (int16_T)(c9_i13 & 4095);
      }

      if ((int16_T)(c9_i15 & 4096) != 0) {
        c9_i19 = (int16_T)(c9_i15 | -4096);
      } else {
        c9_i19 = (int16_T)(c9_i15 & 4095);
      }

      c9_i21 = c9_i17 - c9_i19;
      if (c9_i21 > 4095) {
        c9_i21 = 4095;
      } else {
        if (c9_i21 < -4096) {
          c9_i21 = -4096;
        }
      }

      c9_i23 = (int16_T)c9_u1;
      c9_i25 = (int16_T)c9_u3;
      c9_i27 = (int16_T)c9_u1;
      c9_i29 = (int16_T)c9_u3;
      if ((int16_T)(c9_i27 & 4096) != 0) {
        c9_i31 = (int16_T)(c9_i27 | -4096);
      } else {
        c9_i31 = (int16_T)(c9_i27 & 4095);
      }

      if ((int16_T)(c9_i29 & 4096) != 0) {
        c9_i33 = (int16_T)(c9_i29 | -4096);
      } else {
        c9_i33 = (int16_T)(c9_i29 & 4095);
      }

      c9_i35 = c9_i31 - c9_i33;
      if (c9_i35 > 4095) {
        c9_i35 = 4095;
      } else {
        if (c9_i35 < -4096) {
          c9_i35 = -4096;
        }
      }

      if ((int16_T)(c9_i9 & 4096) != 0) {
        c9_i37 = (int16_T)(c9_i9 | -4096);
      } else {
        c9_i37 = (int16_T)(c9_i9 & 4095);
      }

      if ((int16_T)(c9_i11 & 4096) != 0) {
        c9_i39 = (int16_T)(c9_i11 | -4096);
      } else {
        c9_i39 = (int16_T)(c9_i11 & 4095);
      }

      if ((int16_T)(c9_i23 & 4096) != 0) {
        c9_i41 = (int16_T)(c9_i23 | -4096);
      } else {
        c9_i41 = (int16_T)(c9_i23 & 4095);
      }

      if ((int16_T)(c9_i25 & 4096) != 0) {
        c9_i43 = (int16_T)(c9_i25 | -4096);
      } else {
        c9_i43 = (int16_T)(c9_i25 & 4095);
      }

      if (c9_i37 < c9_i39) {
        c9_d3 = (real_T)((int16_T)c9_i21 <= 1);
      } else if (c9_i41 > c9_i43) {
        if ((int16_T)c9_i35 <= 1) {
          c9_d3 = 3.0;
        } else {
          c9_d3 = 0.0;
        }
      } else {
        c9_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c9_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c9_covrtInstance, 4U, 0U, 2U, c9_d3,
                          0.0, -2, 2U, (int32_T)c9_emlrt_update_log_4
                          (chartInstance, c9_d_c,
                           chartInstance->c9_emlrtLocationLoggingDataTables, 9))))
      {
        c9_i_a0 = c9_b_sum;
        c9_e_b0 = c9_b_offset;
        c9_k_varargin_1 = c9_e_b0;
        c9_v = c9_k_varargin_1;
        c9_val = c9_v;
        c9_c_b = c9_val;
        c9_i45 = c9_i_a0;
        if (c9_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d4 = 1.0;
          observerLog(153 + c9_PICOffset, &c9_d4, 1);
        }

        if (c9_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d5 = 1.0;
          observerLog(153 + c9_PICOffset, &c9_d5, 1);
        }

        c9_i46 = c9_c_b;
        if (c9_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d6 = 1.0;
          observerLog(156 + c9_PICOffset, &c9_d6, 1);
        }

        if (c9_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d7 = 1.0;
          observerLog(156 + c9_PICOffset, &c9_d7, 1);
        }

        if ((c9_i45 & 65536) != 0) {
          c9_i47 = c9_i45 | -65536;
        } else {
          c9_i47 = c9_i45 & 65535;
        }

        if ((c9_i46 & 65536) != 0) {
          c9_i48 = c9_i46 | -65536;
        } else {
          c9_i48 = c9_i46 & 65535;
        }

        c9_i49 = c9__s32_add__(chartInstance, c9_i47, c9_i48, 155, 1U, 323, 12);
        if (c9_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d8 = 1.0;
          observerLog(161 + c9_PICOffset, &c9_d8, 1);
        }

        if (c9_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c9_d9 = 1.0;
          observerLog(161 + c9_PICOffset, &c9_d9, 1);
        }

        if ((c9_i49 & 65536) != 0) {
          c9_e_c = c9_i49 | -65536;
        } else {
          c9_e_c = c9_i49 & 65535;
        }

        c9_l_varargin_1 = c9_emlrt_update_log_6(chartInstance, c9_e_c,
          chartInstance->c9_emlrtLocationLoggingDataTables, 13);
        c9_m_varargin_1 = c9_l_varargin_1;
        c9_f_var1 = c9_m_varargin_1;
        c9_i50 = c9_f_var1;
        c9_f_covSaturation = false;
        if (c9_i50 < 0) {
          c9_i50 = 0;
        } else {
          if (c9_i50 > 4095) {
            c9_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 2, 0,
            c9_f_covSaturation);
        }

        c9_f_hfi = (uint16_T)c9_i50;
        c9_b_cont = c9_emlrt_update_log_5(chartInstance, c9_f_hfi,
          chartInstance->c9_emlrtLocationLoggingDataTables, 12);
        c9_b_out_init = c9_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 14);
      } else {
        c9_b_cont = c9_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 15);
        c9_b_out_init = c9_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c9_varargin_1 = c9_b_sum;
      c9_c_varargin_1 = c9_varargin_1;
      c9_var1 = c9_c_varargin_1;
      c9_i4 = c9_var1;
      c9_covSaturation = false;
      if (c9_i4 < 0) {
        c9_i4 = 0;
      } else {
        if (c9_i4 > 4095) {
          c9_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 3, 0,
          c9_covSaturation);
      }

      c9_hfi = (uint16_T)c9_i4;
      c9_u = c9_emlrt_update_log_5(chartInstance, c9_hfi,
        chartInstance->c9_emlrtLocationLoggingDataTables, 18);
      c9_e_varargin_1 = c9_b_max;
      c9_g_varargin_1 = c9_e_varargin_1;
      c9_c_var1 = c9_g_varargin_1;
      c9_i6 = c9_c_var1;
      c9_c_covSaturation = false;
      if (c9_i6 < 0) {
        c9_i6 = 0;
      } else {
        if (c9_i6 > 4095) {
          c9_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 4, 0,
          c9_c_covSaturation);
      }

      c9_c_hfi = (uint16_T)c9_i6;
      c9_u2 = c9_emlrt_update_log_5(chartInstance, c9_c_hfi,
        chartInstance->c9_emlrtLocationLoggingDataTables, 19);
      c9_e_a0 = c9_u;
      c9_b0 = c9_u2;
      c9_c_a = c9_e_a0;
      c9_b = c9_b0;
      c9_g_a0 = c9_c_a;
      c9_c_b0 = c9_b;
      c9_e_a1 = c9_g_a0;
      c9_b1 = c9_c_b0;
      c9_g_a1 = c9_e_a1;
      c9_c_b1 = c9_b1;
      c9_c_c = (c9_g_a1 < c9_c_b1);
      c9_i8 = (int16_T)c9_u;
      c9_i10 = (int16_T)c9_u2;
      c9_i12 = (int16_T)c9_u2;
      c9_i14 = (int16_T)c9_u;
      if ((int16_T)(c9_i12 & 4096) != 0) {
        c9_i16 = (int16_T)(c9_i12 | -4096);
      } else {
        c9_i16 = (int16_T)(c9_i12 & 4095);
      }

      if ((int16_T)(c9_i14 & 4096) != 0) {
        c9_i18 = (int16_T)(c9_i14 | -4096);
      } else {
        c9_i18 = (int16_T)(c9_i14 & 4095);
      }

      c9_i20 = c9_i16 - c9_i18;
      if (c9_i20 > 4095) {
        c9_i20 = 4095;
      } else {
        if (c9_i20 < -4096) {
          c9_i20 = -4096;
        }
      }

      c9_i22 = (int16_T)c9_u;
      c9_i24 = (int16_T)c9_u2;
      c9_i26 = (int16_T)c9_u;
      c9_i28 = (int16_T)c9_u2;
      if ((int16_T)(c9_i26 & 4096) != 0) {
        c9_i30 = (int16_T)(c9_i26 | -4096);
      } else {
        c9_i30 = (int16_T)(c9_i26 & 4095);
      }

      if ((int16_T)(c9_i28 & 4096) != 0) {
        c9_i32 = (int16_T)(c9_i28 | -4096);
      } else {
        c9_i32 = (int16_T)(c9_i28 & 4095);
      }

      c9_i34 = c9_i30 - c9_i32;
      if (c9_i34 > 4095) {
        c9_i34 = 4095;
      } else {
        if (c9_i34 < -4096) {
          c9_i34 = -4096;
        }
      }

      if ((int16_T)(c9_i8 & 4096) != 0) {
        c9_i36 = (int16_T)(c9_i8 | -4096);
      } else {
        c9_i36 = (int16_T)(c9_i8 & 4095);
      }

      if ((int16_T)(c9_i10 & 4096) != 0) {
        c9_i38 = (int16_T)(c9_i10 | -4096);
      } else {
        c9_i38 = (int16_T)(c9_i10 & 4095);
      }

      if ((int16_T)(c9_i22 & 4096) != 0) {
        c9_i40 = (int16_T)(c9_i22 | -4096);
      } else {
        c9_i40 = (int16_T)(c9_i22 & 4095);
      }

      if ((int16_T)(c9_i24 & 4096) != 0) {
        c9_i42 = (int16_T)(c9_i24 | -4096);
      } else {
        c9_i42 = (int16_T)(c9_i24 & 4095);
      }

      if (c9_i36 < c9_i38) {
        c9_d2 = (real_T)((int16_T)c9_i20 <= 1);
      } else if (c9_i40 > c9_i42) {
        if ((int16_T)c9_i34 <= 1) {
          c9_d2 = 3.0;
        } else {
          c9_d2 = 0.0;
        }
      } else {
        c9_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c9_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c9_covrtInstance, 4U, 0U, 3U, c9_d2,
                          0.0, -2, 2U, (int32_T)c9_emlrt_update_log_4
                          (chartInstance, c9_c_c,
                           chartInstance->c9_emlrtLocationLoggingDataTables, 17))))
      {
        c9_i_varargin_1 = c9_b_sum;
        c9_j_varargin_1 = c9_i_varargin_1;
        c9_e_var1 = c9_j_varargin_1;
        c9_i44 = c9_e_var1;
        c9_e_covSaturation = false;
        if (c9_i44 < 0) {
          c9_i44 = 0;
        } else {
          if (c9_i44 > 4095) {
            c9_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c9_covrtInstance, 4, 0, 5, 0,
            c9_e_covSaturation);
        }

        c9_e_hfi = (uint16_T)c9_i44;
        c9_b_cont = c9_emlrt_update_log_5(chartInstance, c9_e_hfi,
          chartInstance->c9_emlrtLocationLoggingDataTables, 20);
        c9_b_out_init = c9_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 21);
      } else {
        c9_b_cont = c9_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 22);
        c9_b_out_init = c9_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c9_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c9_cont = c9_b_cont;
  *chartInstance->c9_out_init = c9_b_out_init;
  c9_do_animation_call_c9_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 5U, (real_T)
                    *chartInstance->c9_cont);
  covrtSigUpdateFcn(chartInstance->c9_covrtInstance, 6U, (real_T)
                    *chartInstance->c9_out_init);
}

static void mdl_start_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c9_decisionTxtStartIdx = 0U;
  static const uint32_T c9_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c9_chart_data_browse_helper);
  chartInstance->c9_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c9_RuntimeVar,
    &chartInstance->c9_IsDebuggerActive,
    &chartInstance->c9_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c9_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c9_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c9_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c9_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c9_decisionTxtStartIdx, &c9_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c9_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c9_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c9_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c9_PWM_28_HalfB
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c9_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7419",              /* mexFileName */
    "Thu May 27 10:27:20 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c9_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c9_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c9_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c9_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7419");
    emlrtLocationLoggingPushLog(&c9_emlrtLocationLoggingFileInfo,
      c9_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c9_emlrtLocationLoggingDataTables, c9_emlrtLocationInfo,
      NULL, 0U, c9_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7419");
  }

  sfListenerLightTerminate(chartInstance->c9_RuntimeVar);
  sf_mex_destroy(&c9_eml_mx);
  sf_mex_destroy(&c9_b_eml_mx);
  sf_mex_destroy(&c9_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c9_covrtInstance);
}

static void initSimStructsc9_PWM_28_HalfB(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c9_emlrt_update_log_1(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  real_T c9_d;
  uint8_T c9_u;
  uint8_T c9_localMin;
  real_T c9_d1;
  uint8_T c9_u1;
  uint8_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_d = c9_b_table[0U].SimMin;
    if (c9_d < 2.0) {
      if (c9_d >= 0.0) {
        c9_u = (uint8_T)c9_d;
      } else {
        c9_u = 0U;
      }
    } else if (c9_d >= 2.0) {
      c9_u = 1U;
    } else {
      c9_u = 0U;
    }

    c9_localMin = c9_u;
    c9_d1 = c9_b_table[0U].SimMax;
    if (c9_d1 < 2.0) {
      if (c9_d1 >= 0.0) {
        c9_u1 = (uint8_T)c9_d1;
      } else {
        c9_u1 = 0U;
      }
    } else if (c9_d1 >= 2.0) {
      c9_u1 = 1U;
    } else {
      c9_u1 = 0U;
    }

    c9_localMax = c9_u1;
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c9_in < c9_localMin) {
      c9_localMin = c9_in;
    }

    if (c9_in > c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static int16_T c9_emlrt_update_log_2(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  real_T c9_d;
  int16_T c9_i;
  int16_T c9_localMin;
  real_T c9_d1;
  int16_T c9_i1;
  int16_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_d = muDoubleScalarFloor(c9_b_table[0U].SimMin);
    if (c9_d < 32768.0) {
      if (c9_d >= -32768.0) {
        c9_i = (int16_T)c9_d;
      } else {
        c9_i = MIN_int16_T;
      }
    } else if (c9_d >= 32768.0) {
      c9_i = MAX_int16_T;
    } else {
      c9_i = 0;
    }

    c9_localMin = c9_i;
    c9_d1 = muDoubleScalarFloor(c9_b_table[0U].SimMax);
    if (c9_d1 < 32768.0) {
      if (c9_d1 >= -32768.0) {
        c9_i1 = (int16_T)c9_d1;
      } else {
        c9_i1 = MIN_int16_T;
      }
    } else if (c9_d1 >= 32768.0) {
      c9_i1 = MAX_int16_T;
    } else {
      c9_i1 = 0;
    }

    c9_localMax = c9_i1;
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c9_in < c9_localMin) {
      c9_localMin = c9_in;
    }

    if (c9_in > c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static int16_T c9_emlrt_update_log_3(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  real_T c9_d;
  int16_T c9_i;
  int16_T c9_localMin;
  real_T c9_d1;
  int16_T c9_i1;
  int16_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_d = muDoubleScalarFloor(c9_b_table[0U].SimMin);
    if (c9_d < 2048.0) {
      if (c9_d >= -2048.0) {
        c9_i = (int16_T)c9_d;
      } else {
        c9_i = -2048;
      }
    } else if (c9_d >= 2048.0) {
      c9_i = 2047;
    } else {
      c9_i = 0;
    }

    c9_localMin = c9_i;
    c9_d1 = muDoubleScalarFloor(c9_b_table[0U].SimMax);
    if (c9_d1 < 2048.0) {
      if (c9_d1 >= -2048.0) {
        c9_i1 = (int16_T)c9_d1;
      } else {
        c9_i1 = -2048;
      }
    } else if (c9_d1 >= 2048.0) {
      c9_i1 = 2047;
    } else {
      c9_i1 = 0;
    }

    c9_localMax = c9_i1;
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c9_in < c9_localMin) {
      c9_localMin = c9_in;
    }

    if (c9_in > c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static boolean_T c9_emlrt_update_log_4(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  boolean_T c9_localMin;
  boolean_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_localMin = (c9_b_table[0U].SimMin > 0.0);
    c9_localMax = (c9_b_table[0U].SimMax > 0.0);
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c9_in < (int32_T)c9_localMin) {
      c9_localMin = c9_in;
    }

    if ((int32_T)c9_in > (int32_T)c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static uint16_T c9_emlrt_update_log_5(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  real_T c9_d;
  uint16_T c9_u;
  uint16_T c9_localMin;
  real_T c9_d1;
  uint16_T c9_u1;
  uint16_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_d = c9_b_table[0U].SimMin;
    if (c9_d < 4096.0) {
      if (c9_d >= 0.0) {
        c9_u = (uint16_T)c9_d;
      } else {
        c9_u = 0U;
      }
    } else if (c9_d >= 4096.0) {
      c9_u = 4095U;
    } else {
      c9_u = 0U;
    }

    c9_localMin = c9_u;
    c9_d1 = c9_b_table[0U].SimMax;
    if (c9_d1 < 4096.0) {
      if (c9_d1 >= 0.0) {
        c9_u1 = (uint16_T)c9_d1;
      } else {
        c9_u1 = 0U;
      }
    } else if (c9_d1 >= 4096.0) {
      c9_u1 = 4095U;
    } else {
      c9_u1 = 0U;
    }

    c9_localMax = c9_u1;
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c9_in < c9_localMin) {
      c9_localMin = c9_in;
    }

    if (c9_in > c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static int32_T c9_emlrt_update_log_6(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c9_in, emlrtLocationLoggingDataType c9_table[],
  int32_T c9_index)
{
  boolean_T c9_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c9_b_table;
  real_T c9_d;
  int32_T c9_i;
  int32_T c9_localMin;
  real_T c9_d1;
  int32_T c9_i1;
  int32_T c9_localMax;
  emlrtLocationLoggingHistogramType *c9_histTable;
  real_T c9_inDouble;
  real_T c9_significand;
  int32_T c9_exponent;
  (void)chartInstance;
  c9_isLoggingEnabledHere = (c9_index >= 0);
  if (c9_isLoggingEnabledHere) {
    c9_b_table = (emlrtLocationLoggingDataType *)&c9_table[c9_index];
    c9_d = muDoubleScalarFloor(c9_b_table[0U].SimMin);
    if (c9_d < 65536.0) {
      if (c9_d >= -65536.0) {
        c9_i = (int32_T)c9_d;
      } else {
        c9_i = -65536;
      }
    } else if (c9_d >= 65536.0) {
      c9_i = 65535;
    } else {
      c9_i = 0;
    }

    c9_localMin = c9_i;
    c9_d1 = muDoubleScalarFloor(c9_b_table[0U].SimMax);
    if (c9_d1 < 65536.0) {
      if (c9_d1 >= -65536.0) {
        c9_i1 = (int32_T)c9_d1;
      } else {
        c9_i1 = -65536;
      }
    } else if (c9_d1 >= 65536.0) {
      c9_i1 = 65535;
    } else {
      c9_i1 = 0;
    }

    c9_localMax = c9_i1;
    c9_histTable = c9_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c9_in < c9_localMin) {
      c9_localMin = c9_in;
    }

    if (c9_in > c9_localMax) {
      c9_localMax = c9_in;
    }

    /* Histogram logging. */
    c9_inDouble = (real_T)c9_in;
    c9_histTable->TotalNumberOfValues++;
    if (c9_inDouble == 0.0) {
      c9_histTable->NumberOfZeros++;
    } else {
      c9_histTable->SimSum += c9_inDouble;
      if ((!muDoubleScalarIsInf(c9_inDouble)) && (!muDoubleScalarIsNaN
           (c9_inDouble))) {
        c9_significand = frexp(c9_inDouble, &c9_exponent);
        if (c9_exponent > 128) {
          c9_exponent = 128;
        }

        if (c9_exponent < -127) {
          c9_exponent = -127;
        }

        if (c9_significand < 0.0) {
          c9_histTable->NumberOfNegativeValues++;
          c9_histTable->HistogramOfNegativeValues[127 + c9_exponent]++;
        } else {
          c9_histTable->NumberOfPositiveValues++;
          c9_histTable->HistogramOfPositiveValues[127 + c9_exponent]++;
        }
      }
    }

    c9_b_table[0U].SimMin = (real_T)c9_localMin;
    c9_b_table[0U].SimMax = (real_T)c9_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c9_in;
}

static void c9_emlrtInitVarDataTables(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c9_dataTables[24],
  emlrtLocationLoggingHistogramType c9_histTables[24])
{
  int32_T c9_i;
  int32_T c9_iH;
  (void)chartInstance;
  for (c9_i = 0; c9_i < 24; c9_i++) {
    c9_dataTables[c9_i].SimMin = rtInf;
    c9_dataTables[c9_i].SimMax = rtMinusInf;
    c9_dataTables[c9_i].OverflowWraps = 0;
    c9_dataTables[c9_i].Saturations = 0;
    c9_dataTables[c9_i].IsAlwaysInteger = true;
    c9_dataTables[c9_i].HistogramTable = &c9_histTables[c9_i];
    c9_histTables[c9_i].NumberOfZeros = 0.0;
    c9_histTables[c9_i].NumberOfPositiveValues = 0.0;
    c9_histTables[c9_i].NumberOfNegativeValues = 0.0;
    c9_histTables[c9_i].TotalNumberOfValues = 0.0;
    c9_histTables[c9_i].SimSum = 0.0;
    for (c9_iH = 0; c9_iH < 256; c9_iH++) {
      c9_histTables[c9_i].HistogramOfPositiveValues[c9_iH] = 0.0;
      c9_histTables[c9_i].HistogramOfNegativeValues[c9_iH] = 0.0;
    }
  }
}

const mxArray *sf_c9_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c9_nameCaptureInfo = NULL;
  c9_nameCaptureInfo = NULL;
  sf_mex_assign(&c9_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c9_nameCaptureInfo;
}

static uint16_T c9_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_b_cont, const
  char_T *c9_identifier)
{
  uint16_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_b_emlrt_marshallIn(chartInstance, c9_sp, sf_mex_dup(c9_b_cont),
    &c9_thisId);
  sf_mex_destroy(&c9_b_cont);
  return c9_y;
}

static uint16_T c9_b_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_u, const
  emlrtMsgIdentifier *c9_parentId)
{
  uint16_T c9_y;
  const mxArray *c9_mxFi = NULL;
  const mxArray *c9_mxInt = NULL;
  uint16_T c9_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c9_parentId, c9_u, false, 0U, NULL, c9_eml_mx, c9_b_eml_mx);
  sf_mex_assign(&c9_mxFi, sf_mex_dup(c9_u), false);
  sf_mex_assign(&c9_mxInt, sf_mex_call(c9_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c9_mxFi)), false);
  sf_mex_import(c9_parentId, sf_mex_dup(c9_mxInt), &c9_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c9_mxFi);
  sf_mex_destroy(&c9_mxInt);
  c9_y = c9_b_u;
  sf_mex_destroy(&c9_mxFi);
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static uint8_T c9_c_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_b_out_init, const
  char_T *c9_identifier)
{
  uint8_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_d_emlrt_marshallIn(chartInstance, c9_sp, sf_mex_dup(c9_b_out_init),
    &c9_thisId);
  sf_mex_destroy(&c9_b_out_init);
  return c9_y;
}

static uint8_T c9_d_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c9_sp, const mxArray *c9_u, const
  emlrtMsgIdentifier *c9_parentId)
{
  uint8_T c9_y;
  const mxArray *c9_mxFi = NULL;
  const mxArray *c9_mxInt = NULL;
  uint8_T c9_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c9_parentId, c9_u, false, 0U, NULL, c9_eml_mx, c9_c_eml_mx);
  sf_mex_assign(&c9_mxFi, sf_mex_dup(c9_u), false);
  sf_mex_assign(&c9_mxInt, sf_mex_call(c9_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c9_mxFi)), false);
  sf_mex_import(c9_parentId, sf_mex_dup(c9_mxInt), &c9_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c9_mxFi);
  sf_mex_destroy(&c9_mxInt);
  c9_y = c9_b_u;
  sf_mex_destroy(&c9_mxFi);
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static uint8_T c9_e_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_b_is_active_c9_PWM_28_HalfB, const char_T
  *c9_identifier)
{
  uint8_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_b_is_active_c9_PWM_28_HalfB), &c9_thisId);
  sf_mex_destroy(&c9_b_is_active_c9_PWM_28_HalfB);
  return c9_y;
}

static uint8_T c9_f_emlrt_marshallIn(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  uint8_T c9_y;
  uint8_T c9_b_u;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_b_u, 1, 3, 0U, 0, 0U, 0);
  c9_y = c9_b_u;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static const mxArray *c9_chart_data_browse_helper
  (SFc9_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c9_ssIdNumber)
{
  const mxArray *c9_mxData = NULL;
  uint8_T c9_u;
  int16_T c9_i;
  int16_T c9_i1;
  int16_T c9_i2;
  uint16_T c9_u1;
  uint8_T c9_u2;
  uint8_T c9_u3;
  real_T c9_d;
  real_T c9_d1;
  real_T c9_d2;
  real_T c9_d3;
  real_T c9_d4;
  real_T c9_d5;
  c9_mxData = NULL;
  switch (c9_ssIdNumber) {
   case 18U:
    c9_u = *chartInstance->c9_enable;
    c9_d = (real_T)c9_u;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c9_i = *chartInstance->c9_offset;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c9_i1 = *chartInstance->c9_max;
    c9_d1 = (real_T)c9_i1;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c9_i2 = *chartInstance->c9_sum;
    c9_d2 = (real_T)c9_i2;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c9_u1 = *chartInstance->c9_cont;
    c9_d3 = (real_T)c9_u1;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c9_u2 = *chartInstance->c9_init;
    c9_d4 = (real_T)c9_u2;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c9_u3 = *chartInstance->c9_out_init;
    c9_d5 = (real_T)c9_u3;
    sf_mex_assign(&c9_mxData, sf_mex_create("mxData", &c9_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c9_mxData;
}

static int32_T c9__s32_add__(SFc9_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c9_b, int32_T c9_c, int32_T c9_EMLOvCount_src_loc, uint32_T
  c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T c9_length_src_loc)
{
  int32_T c9_a;
  int32_T c9_PICOffset;
  real_T c9_d;
  observerLogReadPIC(&c9_PICOffset);
  c9_a = c9_b + c9_c;
  if (((c9_a ^ c9_b) & (c9_a ^ c9_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c9_ssid_src_loc, c9_offset_src_loc,
      c9_length_src_loc);
    c9_d = 1.0;
    observerLog(c9_EMLOvCount_src_loc + c9_PICOffset, &c9_d, 1);
  }

  return c9_a;
}

static void init_dsm_address_info(SFc9_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc9_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c9_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c9_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c9_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c9_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c9_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c9_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c9_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c9_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c9_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c9_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c9_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c9_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyyZXxAuG+8kUW8R"
    "2JOmhOSuSAAAOq8IGM="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c9_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c9_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c9_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c9_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c9_PWM_28_HalfB
      ((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c9_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c9_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc9_PWM_28_HalfB((SFc9_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c9_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iJHGcLspDBRIds2qSIKkSDeNY0qKBUixGjo/O2E8fBIHGs4w8yPbPUcukZ6",
    "gyyx6ge66KHqDAj1C31CULFMkFUeJkQIdgCJm+L1v3v+MvFqn5+HYwufVV553Fd/X8Kl707GRzW",
    "sLz3S94X2bzR+hkLBxnygSa69yCBLDM9CSW8Ok6IihLIQxMQQFgiI2kcqUsWkWW87EuG0FdXz6Z",
    "cRoFETS8nAPZUl4IPgpsiXW9JGnyRRQ0wYITaSkHUVtTkZzjZU59iOgY23jKhM0mMAmTi3ds9yw",
    "hEPrBGhHaENQY32mW2CIAd+clJrpLNXBDCjjhDMiCq2NiA4gQQcbeJ6E+HtgDRqVh9GIKLMHEZm",
    "A7rJxyikF5DmZxg9HTBAjFSO8FXPfCS7r1ueoT0+GwCscgrrtKSDjRDJhyuMftNHSliBHHJpwZE",
    "flbAG8ti74Lxgcgyr129CXE1BkBAeidNPUIa2TNFrzLFmGGRbDC6IeU4yfhrA0ezFzdEAwTnCIE",
    "mUwSI3s6EPFJujeUjYbd1xmrioZG0+DrVfBUrbWBKqiMGdrU+ETznUp7FAmXZgAT1mbxJBq2JS1",
    "GKc1Cw8lOtild3k1WMEw8BnMlyJkheGa5ABp33mKjeU8klptZOxj8ja73eXPy7COMKCGhEJRF1C",
    "EaUCfpe4tZwuZdrFHIGplUvWKwNMMWYXy9NCK5rFUY/RJRRM5M8FFtBQY6xHGEivhucaiqYK5WK",
    "7CUUIjCF2DYRx6WDaILfCJdq3tMdbdhJnTJmiqWFIUVXf+3PHOzp8v3uP8mcnl37cXeGoFPN7C2",
    "+EfLuA36+fxG7l967M1NzL53QX5G7n9Gjl5h9t2lfPmt1/efvn39T9/+vX3zl9g8vbn9agt6VHz",
    "ZvsnVy52bm9l829mDXKe8JOlPHPY/QW9GgX8Xy/wb2dzfX+/HfTGr+6+fnYXnijDf46V+N5P+d7",
    "Vq/W9ktN3tn7LderTJO27WtFOmF0o3JzY6TGbj+fVFf7YzNan459H68l/t5uPY5G/Guf81fCoFG",
    "arJB8vV/8Hu3n5Iv2v5eLt5tKaARPsI9lxa3c9+en+/RV27OTs2EnvFQPiuhUM6A+D/sve4N7Dw",
    "T7hw72CPvOh9XpROe+S5f4rev7vl09v3/ucwxsfKFdf89y/LLl17bvofeRzw1edZ14Ov/0Z27Hu",
    "PfFT4//wLnaPu5nNf5z/xfIjxsOC23b2uQtkWPT1Euz7F9PsoFA=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c9_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c9_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c9_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c9_PWM_28_HalfB(SimStruct *S)
{
  SFc9_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc9_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc9_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc9_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c9_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c9_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c9_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c9_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c9_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c9_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c9_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c9_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c9_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c9_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c9_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c9_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c9_JITStateAnimation,
    chartInstance->c9_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c9_PWM_28_HalfB(chartInstance);
}

void c9_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c9_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c9_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c9_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c9_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
