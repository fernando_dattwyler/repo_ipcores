#ifndef __c23_PWM_28_HalfB_h__
#define __c23_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc23_PWM_28_HalfBInstanceStruct
#define typedef_SFc23_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c23_sfEvent;
  boolean_T c23_doneDoubleBufferReInit;
  uint8_T c23_is_active_c23_PWM_28_HalfB;
  uint8_T c23_JITStateAnimation[1];
  uint8_T c23_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c23_emlrtLocationLoggingDataTables[24];
  int32_T c23_IsDebuggerActive;
  int32_T c23_IsSequenceViewerPresent;
  int32_T c23_SequenceViewerOptimization;
  void *c23_RuntimeVar;
  emlrtLocationLoggingHistogramType c23_emlrtLocLogHistTables[24];
  boolean_T c23_emlrtLocLogSimulated;
  uint32_T c23_mlFcnLineNumber;
  void *c23_fcnDataPtrs[7];
  char_T *c23_dataNames[7];
  uint32_T c23_numFcnVars;
  uint32_T c23_ssIds[7];
  uint32_T c23_statuses[7];
  void *c23_outMexFcns[7];
  void *c23_inMexFcns[7];
  CovrtStateflowInstance *c23_covrtInstance;
  void *c23_fEmlrtCtx;
  uint8_T *c23_enable;
  int16_T *c23_offset;
  int16_T *c23_max;
  int16_T *c23_sum;
  uint16_T *c23_cont;
  uint8_T *c23_init;
  uint8_T *c23_out_init;
} SFc23_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc23_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c23_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c23_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c23_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
