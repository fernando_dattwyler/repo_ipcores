#ifndef __c24_PWM_28_HalfB_h__
#define __c24_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc24_PWM_28_HalfBInstanceStruct
#define typedef_SFc24_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c24_sfEvent;
  boolean_T c24_doneDoubleBufferReInit;
  uint8_T c24_is_active_c24_PWM_28_HalfB;
  uint8_T c24_JITStateAnimation[1];
  uint8_T c24_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c24_emlrtLocationLoggingDataTables[24];
  int32_T c24_IsDebuggerActive;
  int32_T c24_IsSequenceViewerPresent;
  int32_T c24_SequenceViewerOptimization;
  void *c24_RuntimeVar;
  emlrtLocationLoggingHistogramType c24_emlrtLocLogHistTables[24];
  boolean_T c24_emlrtLocLogSimulated;
  uint32_T c24_mlFcnLineNumber;
  void *c24_fcnDataPtrs[7];
  char_T *c24_dataNames[7];
  uint32_T c24_numFcnVars;
  uint32_T c24_ssIds[7];
  uint32_T c24_statuses[7];
  void *c24_outMexFcns[7];
  void *c24_inMexFcns[7];
  CovrtStateflowInstance *c24_covrtInstance;
  void *c24_fEmlrtCtx;
  uint8_T *c24_enable;
  int16_T *c24_offset;
  int16_T *c24_max;
  int16_T *c24_sum;
  uint16_T *c24_cont;
  uint8_T *c24_init;
  uint8_T *c24_out_init;
} SFc24_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc24_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c24_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c24_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c24_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
