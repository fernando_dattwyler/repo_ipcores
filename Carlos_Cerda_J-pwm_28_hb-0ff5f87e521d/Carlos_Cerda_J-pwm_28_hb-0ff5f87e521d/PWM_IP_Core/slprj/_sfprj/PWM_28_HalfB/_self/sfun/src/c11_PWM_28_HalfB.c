/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c11_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c11_eml_mx;
static const mxArray *c11_b_eml_mx;
static const mxArray *c11_c_eml_mx;

/* Function Declarations */
static void initialize_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c11_update_jit_animation_state_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance);
static void c11_do_animation_call_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_st);
static void sf_gateway_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c11_emlrt_update_log_1(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static int16_T c11_emlrt_update_log_2(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static int16_T c11_emlrt_update_log_3(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static boolean_T c11_emlrt_update_log_4(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static uint16_T c11_emlrt_update_log_5(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static int32_T c11_emlrt_update_log_6(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index);
static void c11_emlrtInitVarDataTables(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c11_dataTables[24],
  emlrtLocationLoggingHistogramType c11_histTables[24]);
static uint16_T c11_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_b_cont, const
  char_T *c11_identifier);
static uint16_T c11_b_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_u, const
  emlrtMsgIdentifier *c11_parentId);
static uint8_T c11_c_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_b_out_init, const
  char_T *c11_identifier);
static uint8_T c11_d_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_u, const
  emlrtMsgIdentifier *c11_parentId);
static uint8_T c11_e_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_b_is_active_c11_PWM_28_HalfB, const char_T *
  c11_identifier);
static uint8_T c11_f_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static const mxArray *c11_chart_data_browse_helper
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c11_ssIdNumber);
static int32_T c11__s32_add__(SFc11_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c11_b, int32_T c11_c, int32_T c11_EMLOvCount_src_loc, uint32_T
  c11_ssid_src_loc, int32_T c11_offset_src_loc, int32_T c11_length_src_loc);
static void init_dsm_address_info(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c11_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c11_st.tls = chartInstance->c11_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c11_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c11_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c11_is_active_c11_PWM_28_HalfB = 0U;
  sf_mex_assign(&c11_c_eml_mx, sf_mex_call(&c11_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c11_b_eml_mx, sf_mex_call(&c11_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c11_eml_mx, sf_mex_call(&c11_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c11_emlrtLocLogSimulated = false;
  c11_emlrtInitVarDataTables(chartInstance,
    chartInstance->c11_emlrtLocationLoggingDataTables,
    chartInstance->c11_emlrtLocLogHistTables);
}

static void initialize_params_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c11_update_jit_animation_state_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c11_do_animation_call_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c11_st;
  const mxArray *c11_y = NULL;
  const mxArray *c11_b_y = NULL;
  uint16_T c11_u;
  const mxArray *c11_c_y = NULL;
  const mxArray *c11_d_y = NULL;
  uint8_T c11_b_u;
  const mxArray *c11_e_y = NULL;
  const mxArray *c11_f_y = NULL;
  c11_st = NULL;
  c11_st = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createcellmatrix(3, 1), false);
  c11_b_y = NULL;
  c11_u = *chartInstance->c11_cont;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_b_y, sf_mex_create_fi(sf_mex_dup(c11_eml_mx), sf_mex_dup
    (c11_b_eml_mx), "simulinkarray", c11_c_y, false, false), false);
  sf_mex_setcell(c11_y, 0, c11_b_y);
  c11_d_y = NULL;
  c11_b_u = *chartInstance->c11_out_init;
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_d_y, sf_mex_create_fi(sf_mex_dup(c11_eml_mx), sf_mex_dup
    (c11_c_eml_mx), "simulinkarray", c11_e_y, false, false), false);
  sf_mex_setcell(c11_y, 1, c11_d_y);
  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y",
    &chartInstance->c11_is_active_c11_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 2, c11_f_y);
  sf_mex_assign(&c11_st, c11_y, false);
  return c11_st;
}

static void set_sim_state_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_st)
{
  emlrtStack c11_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c11_u;
  c11_b_st.tls = chartInstance->c11_fEmlrtCtx;
  chartInstance->c11_doneDoubleBufferReInit = true;
  c11_u = sf_mex_dup(c11_st);
  *chartInstance->c11_cont = c11_emlrt_marshallIn(chartInstance, &c11_b_st,
    sf_mex_dup(sf_mex_getcell(c11_u, 0)), "cont");
  *chartInstance->c11_out_init = c11_c_emlrt_marshallIn(chartInstance, &c11_b_st,
    sf_mex_dup(sf_mex_getcell(c11_u, 1)), "out_init");
  chartInstance->c11_is_active_c11_PWM_28_HalfB = c11_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 2)),
     "is_active_c11_PWM_28_HalfB");
  sf_mex_destroy(&c11_u);
  sf_mex_destroy(&c11_st);
}

static void sf_gateway_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c11_PICOffset;
  uint8_T c11_b_enable;
  int16_T c11_b_offset;
  int16_T c11_b_max;
  int16_T c11_b_sum;
  uint8_T c11_b_init;
  uint8_T c11_a0;
  uint8_T c11_a;
  uint8_T c11_b_a0;
  uint8_T c11_a1;
  uint8_T c11_b_a1;
  boolean_T c11_c;
  int8_T c11_i;
  int8_T c11_i1;
  real_T c11_d;
  uint8_T c11_c_a0;
  uint16_T c11_b_cont;
  uint8_T c11_b_a;
  uint8_T c11_b_out_init;
  uint8_T c11_d_a0;
  uint8_T c11_c_a1;
  uint8_T c11_d_a1;
  boolean_T c11_b_c;
  int8_T c11_i2;
  int8_T c11_i3;
  real_T c11_d1;
  int16_T c11_varargin_1;
  int16_T c11_b_varargin_1;
  int16_T c11_c_varargin_1;
  int16_T c11_d_varargin_1;
  int16_T c11_var1;
  int16_T c11_b_var1;
  int16_T c11_i4;
  int16_T c11_i5;
  boolean_T c11_covSaturation;
  boolean_T c11_b_covSaturation;
  uint16_T c11_hfi;
  uint16_T c11_b_hfi;
  uint16_T c11_u;
  uint16_T c11_u1;
  int16_T c11_e_varargin_1;
  int16_T c11_f_varargin_1;
  int16_T c11_g_varargin_1;
  int16_T c11_h_varargin_1;
  int16_T c11_c_var1;
  int16_T c11_d_var1;
  int16_T c11_i6;
  int16_T c11_i7;
  boolean_T c11_c_covSaturation;
  boolean_T c11_d_covSaturation;
  uint16_T c11_c_hfi;
  uint16_T c11_d_hfi;
  uint16_T c11_u2;
  uint16_T c11_u3;
  uint16_T c11_e_a0;
  uint16_T c11_f_a0;
  uint16_T c11_b0;
  uint16_T c11_b_b0;
  uint16_T c11_c_a;
  uint16_T c11_d_a;
  uint16_T c11_b;
  uint16_T c11_b_b;
  uint16_T c11_g_a0;
  uint16_T c11_h_a0;
  uint16_T c11_c_b0;
  uint16_T c11_d_b0;
  uint16_T c11_e_a1;
  uint16_T c11_f_a1;
  uint16_T c11_b1;
  uint16_T c11_b_b1;
  uint16_T c11_g_a1;
  uint16_T c11_h_a1;
  uint16_T c11_c_b1;
  uint16_T c11_d_b1;
  boolean_T c11_c_c;
  boolean_T c11_d_c;
  int16_T c11_i8;
  int16_T c11_i9;
  int16_T c11_i10;
  int16_T c11_i11;
  int16_T c11_i12;
  int16_T c11_i13;
  int16_T c11_i14;
  int16_T c11_i15;
  int16_T c11_i16;
  int16_T c11_i17;
  int16_T c11_i18;
  int16_T c11_i19;
  int32_T c11_i20;
  int32_T c11_i21;
  int16_T c11_i22;
  int16_T c11_i23;
  int16_T c11_i24;
  int16_T c11_i25;
  int16_T c11_i26;
  int16_T c11_i27;
  int16_T c11_i28;
  int16_T c11_i29;
  int16_T c11_i30;
  int16_T c11_i31;
  int16_T c11_i32;
  int16_T c11_i33;
  int32_T c11_i34;
  int32_T c11_i35;
  int16_T c11_i36;
  int16_T c11_i37;
  int16_T c11_i38;
  int16_T c11_i39;
  int16_T c11_i40;
  int16_T c11_i41;
  int16_T c11_i42;
  int16_T c11_i43;
  real_T c11_d2;
  real_T c11_d3;
  int16_T c11_i_varargin_1;
  int16_T c11_i_a0;
  int16_T c11_j_varargin_1;
  int16_T c11_e_b0;
  int16_T c11_e_var1;
  int16_T c11_k_varargin_1;
  int16_T c11_i44;
  int16_T c11_v;
  boolean_T c11_e_covSaturation;
  int16_T c11_val;
  int16_T c11_c_b;
  int32_T c11_i45;
  uint16_T c11_e_hfi;
  real_T c11_d4;
  int32_T c11_i46;
  real_T c11_d5;
  real_T c11_d6;
  real_T c11_d7;
  int32_T c11_i47;
  int32_T c11_i48;
  int32_T c11_i49;
  real_T c11_d8;
  real_T c11_d9;
  int32_T c11_e_c;
  int32_T c11_l_varargin_1;
  int32_T c11_m_varargin_1;
  int32_T c11_f_var1;
  int32_T c11_i50;
  boolean_T c11_f_covSaturation;
  uint16_T c11_f_hfi;
  observerLogReadPIC(&c11_PICOffset);
  chartInstance->c11_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 4U, (real_T)
                    *chartInstance->c11_init);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 3U, (real_T)
                    *chartInstance->c11_sum);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 2U, (real_T)
                    *chartInstance->c11_max);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 1U, (real_T)
                    *chartInstance->c11_offset);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 0U, (real_T)
                    *chartInstance->c11_enable);
  chartInstance->c11_sfEvent = CALL_EVENT;
  c11_b_enable = *chartInstance->c11_enable;
  c11_b_offset = *chartInstance->c11_offset;
  c11_b_max = *chartInstance->c11_max;
  c11_b_sum = *chartInstance->c11_sum;
  c11_b_init = *chartInstance->c11_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c11_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c11_emlrt_update_log_1(chartInstance, c11_b_enable,
    chartInstance->c11_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c11_emlrt_update_log_2(chartInstance, c11_b_offset,
    chartInstance->c11_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c11_emlrt_update_log_3(chartInstance, c11_b_max,
    chartInstance->c11_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c11_emlrt_update_log_3(chartInstance, c11_b_sum,
    chartInstance->c11_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c11_emlrt_update_log_1(chartInstance, c11_b_init,
    chartInstance->c11_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c11_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c11_covrtInstance, 4U, 0, 0, false);
  c11_a0 = c11_b_enable;
  c11_a = c11_a0;
  c11_b_a0 = c11_a;
  c11_a1 = c11_b_a0;
  c11_b_a1 = c11_a1;
  c11_c = (c11_b_a1 == 0);
  c11_i = (int8_T)c11_b_enable;
  if ((int8_T)(c11_i & 2) != 0) {
    c11_i1 = (int8_T)(c11_i | -2);
  } else {
    c11_i1 = (int8_T)(c11_i & 1);
  }

  if (c11_i1 > 0) {
    c11_d = 3.0;
  } else {
    c11_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c11_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c11_covrtInstance,
        4U, 0U, 0U, c11_d, 0.0, -2, 0U, (int32_T)c11_emlrt_update_log_4
        (chartInstance, c11_c, chartInstance->c11_emlrtLocationLoggingDataTables,
         5)))) {
    c11_b_cont = c11_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c11_emlrtLocationLoggingDataTables, 6);
    c11_b_out_init = c11_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c11_emlrtLocationLoggingDataTables, 7);
  } else {
    c11_c_a0 = c11_b_init;
    c11_b_a = c11_c_a0;
    c11_d_a0 = c11_b_a;
    c11_c_a1 = c11_d_a0;
    c11_d_a1 = c11_c_a1;
    c11_b_c = (c11_d_a1 == 0);
    c11_i2 = (int8_T)c11_b_init;
    if ((int8_T)(c11_i2 & 2) != 0) {
      c11_i3 = (int8_T)(c11_i2 | -2);
    } else {
      c11_i3 = (int8_T)(c11_i2 & 1);
    }

    if (c11_i3 > 0) {
      c11_d1 = 3.0;
    } else {
      c11_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c11_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c11_covrtInstance, 4U, 0U, 1U, c11_d1,
                        0.0, -2, 0U, (int32_T)c11_emlrt_update_log_4
                        (chartInstance, c11_b_c,
                         chartInstance->c11_emlrtLocationLoggingDataTables, 8))))
    {
      c11_b_varargin_1 = c11_b_sum;
      c11_d_varargin_1 = c11_b_varargin_1;
      c11_b_var1 = c11_d_varargin_1;
      c11_i5 = c11_b_var1;
      c11_b_covSaturation = false;
      if (c11_i5 < 0) {
        c11_i5 = 0;
      } else {
        if (c11_i5 > 4095) {
          c11_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 0, 0,
          c11_b_covSaturation);
      }

      c11_b_hfi = (uint16_T)c11_i5;
      c11_u1 = c11_emlrt_update_log_5(chartInstance, c11_b_hfi,
        chartInstance->c11_emlrtLocationLoggingDataTables, 10);
      c11_f_varargin_1 = c11_b_max;
      c11_h_varargin_1 = c11_f_varargin_1;
      c11_d_var1 = c11_h_varargin_1;
      c11_i7 = c11_d_var1;
      c11_d_covSaturation = false;
      if (c11_i7 < 0) {
        c11_i7 = 0;
      } else {
        if (c11_i7 > 4095) {
          c11_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 1, 0,
          c11_d_covSaturation);
      }

      c11_d_hfi = (uint16_T)c11_i7;
      c11_u3 = c11_emlrt_update_log_5(chartInstance, c11_d_hfi,
        chartInstance->c11_emlrtLocationLoggingDataTables, 11);
      c11_f_a0 = c11_u1;
      c11_b_b0 = c11_u3;
      c11_d_a = c11_f_a0;
      c11_b_b = c11_b_b0;
      c11_h_a0 = c11_d_a;
      c11_d_b0 = c11_b_b;
      c11_f_a1 = c11_h_a0;
      c11_b_b1 = c11_d_b0;
      c11_h_a1 = c11_f_a1;
      c11_d_b1 = c11_b_b1;
      c11_d_c = (c11_h_a1 < c11_d_b1);
      c11_i9 = (int16_T)c11_u1;
      c11_i11 = (int16_T)c11_u3;
      c11_i13 = (int16_T)c11_u3;
      c11_i15 = (int16_T)c11_u1;
      if ((int16_T)(c11_i13 & 4096) != 0) {
        c11_i17 = (int16_T)(c11_i13 | -4096);
      } else {
        c11_i17 = (int16_T)(c11_i13 & 4095);
      }

      if ((int16_T)(c11_i15 & 4096) != 0) {
        c11_i19 = (int16_T)(c11_i15 | -4096);
      } else {
        c11_i19 = (int16_T)(c11_i15 & 4095);
      }

      c11_i21 = c11_i17 - c11_i19;
      if (c11_i21 > 4095) {
        c11_i21 = 4095;
      } else {
        if (c11_i21 < -4096) {
          c11_i21 = -4096;
        }
      }

      c11_i23 = (int16_T)c11_u1;
      c11_i25 = (int16_T)c11_u3;
      c11_i27 = (int16_T)c11_u1;
      c11_i29 = (int16_T)c11_u3;
      if ((int16_T)(c11_i27 & 4096) != 0) {
        c11_i31 = (int16_T)(c11_i27 | -4096);
      } else {
        c11_i31 = (int16_T)(c11_i27 & 4095);
      }

      if ((int16_T)(c11_i29 & 4096) != 0) {
        c11_i33 = (int16_T)(c11_i29 | -4096);
      } else {
        c11_i33 = (int16_T)(c11_i29 & 4095);
      }

      c11_i35 = c11_i31 - c11_i33;
      if (c11_i35 > 4095) {
        c11_i35 = 4095;
      } else {
        if (c11_i35 < -4096) {
          c11_i35 = -4096;
        }
      }

      if ((int16_T)(c11_i9 & 4096) != 0) {
        c11_i37 = (int16_T)(c11_i9 | -4096);
      } else {
        c11_i37 = (int16_T)(c11_i9 & 4095);
      }

      if ((int16_T)(c11_i11 & 4096) != 0) {
        c11_i39 = (int16_T)(c11_i11 | -4096);
      } else {
        c11_i39 = (int16_T)(c11_i11 & 4095);
      }

      if ((int16_T)(c11_i23 & 4096) != 0) {
        c11_i41 = (int16_T)(c11_i23 | -4096);
      } else {
        c11_i41 = (int16_T)(c11_i23 & 4095);
      }

      if ((int16_T)(c11_i25 & 4096) != 0) {
        c11_i43 = (int16_T)(c11_i25 | -4096);
      } else {
        c11_i43 = (int16_T)(c11_i25 & 4095);
      }

      if (c11_i37 < c11_i39) {
        c11_d3 = (real_T)((int16_T)c11_i21 <= 1);
      } else if (c11_i41 > c11_i43) {
        if ((int16_T)c11_i35 <= 1) {
          c11_d3 = 3.0;
        } else {
          c11_d3 = 0.0;
        }
      } else {
        c11_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c11_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c11_covrtInstance, 4U, 0U, 2U, c11_d3,
                          0.0, -2, 2U, (int32_T)c11_emlrt_update_log_4
                          (chartInstance, c11_d_c,
                           chartInstance->c11_emlrtLocationLoggingDataTables, 9))))
      {
        c11_i_a0 = c11_b_sum;
        c11_e_b0 = c11_b_offset;
        c11_k_varargin_1 = c11_e_b0;
        c11_v = c11_k_varargin_1;
        c11_val = c11_v;
        c11_c_b = c11_val;
        c11_i45 = c11_i_a0;
        if (c11_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d4 = 1.0;
          observerLog(189 + c11_PICOffset, &c11_d4, 1);
        }

        if (c11_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d5 = 1.0;
          observerLog(189 + c11_PICOffset, &c11_d5, 1);
        }

        c11_i46 = c11_c_b;
        if (c11_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d6 = 1.0;
          observerLog(192 + c11_PICOffset, &c11_d6, 1);
        }

        if (c11_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d7 = 1.0;
          observerLog(192 + c11_PICOffset, &c11_d7, 1);
        }

        if ((c11_i45 & 65536) != 0) {
          c11_i47 = c11_i45 | -65536;
        } else {
          c11_i47 = c11_i45 & 65535;
        }

        if ((c11_i46 & 65536) != 0) {
          c11_i48 = c11_i46 | -65536;
        } else {
          c11_i48 = c11_i46 & 65535;
        }

        c11_i49 = c11__s32_add__(chartInstance, c11_i47, c11_i48, 191, 1U, 323,
          12);
        if (c11_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d8 = 1.0;
          observerLog(197 + c11_PICOffset, &c11_d8, 1);
        }

        if (c11_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c11_d9 = 1.0;
          observerLog(197 + c11_PICOffset, &c11_d9, 1);
        }

        if ((c11_i49 & 65536) != 0) {
          c11_e_c = c11_i49 | -65536;
        } else {
          c11_e_c = c11_i49 & 65535;
        }

        c11_l_varargin_1 = c11_emlrt_update_log_6(chartInstance, c11_e_c,
          chartInstance->c11_emlrtLocationLoggingDataTables, 13);
        c11_m_varargin_1 = c11_l_varargin_1;
        c11_f_var1 = c11_m_varargin_1;
        c11_i50 = c11_f_var1;
        c11_f_covSaturation = false;
        if (c11_i50 < 0) {
          c11_i50 = 0;
        } else {
          if (c11_i50 > 4095) {
            c11_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 2, 0,
            c11_f_covSaturation);
        }

        c11_f_hfi = (uint16_T)c11_i50;
        c11_b_cont = c11_emlrt_update_log_5(chartInstance, c11_f_hfi,
          chartInstance->c11_emlrtLocationLoggingDataTables, 12);
        c11_b_out_init = c11_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 14);
      } else {
        c11_b_cont = c11_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 15);
        c11_b_out_init = c11_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c11_varargin_1 = c11_b_sum;
      c11_c_varargin_1 = c11_varargin_1;
      c11_var1 = c11_c_varargin_1;
      c11_i4 = c11_var1;
      c11_covSaturation = false;
      if (c11_i4 < 0) {
        c11_i4 = 0;
      } else {
        if (c11_i4 > 4095) {
          c11_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 3, 0,
          c11_covSaturation);
      }

      c11_hfi = (uint16_T)c11_i4;
      c11_u = c11_emlrt_update_log_5(chartInstance, c11_hfi,
        chartInstance->c11_emlrtLocationLoggingDataTables, 18);
      c11_e_varargin_1 = c11_b_max;
      c11_g_varargin_1 = c11_e_varargin_1;
      c11_c_var1 = c11_g_varargin_1;
      c11_i6 = c11_c_var1;
      c11_c_covSaturation = false;
      if (c11_i6 < 0) {
        c11_i6 = 0;
      } else {
        if (c11_i6 > 4095) {
          c11_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 4, 0,
          c11_c_covSaturation);
      }

      c11_c_hfi = (uint16_T)c11_i6;
      c11_u2 = c11_emlrt_update_log_5(chartInstance, c11_c_hfi,
        chartInstance->c11_emlrtLocationLoggingDataTables, 19);
      c11_e_a0 = c11_u;
      c11_b0 = c11_u2;
      c11_c_a = c11_e_a0;
      c11_b = c11_b0;
      c11_g_a0 = c11_c_a;
      c11_c_b0 = c11_b;
      c11_e_a1 = c11_g_a0;
      c11_b1 = c11_c_b0;
      c11_g_a1 = c11_e_a1;
      c11_c_b1 = c11_b1;
      c11_c_c = (c11_g_a1 < c11_c_b1);
      c11_i8 = (int16_T)c11_u;
      c11_i10 = (int16_T)c11_u2;
      c11_i12 = (int16_T)c11_u2;
      c11_i14 = (int16_T)c11_u;
      if ((int16_T)(c11_i12 & 4096) != 0) {
        c11_i16 = (int16_T)(c11_i12 | -4096);
      } else {
        c11_i16 = (int16_T)(c11_i12 & 4095);
      }

      if ((int16_T)(c11_i14 & 4096) != 0) {
        c11_i18 = (int16_T)(c11_i14 | -4096);
      } else {
        c11_i18 = (int16_T)(c11_i14 & 4095);
      }

      c11_i20 = c11_i16 - c11_i18;
      if (c11_i20 > 4095) {
        c11_i20 = 4095;
      } else {
        if (c11_i20 < -4096) {
          c11_i20 = -4096;
        }
      }

      c11_i22 = (int16_T)c11_u;
      c11_i24 = (int16_T)c11_u2;
      c11_i26 = (int16_T)c11_u;
      c11_i28 = (int16_T)c11_u2;
      if ((int16_T)(c11_i26 & 4096) != 0) {
        c11_i30 = (int16_T)(c11_i26 | -4096);
      } else {
        c11_i30 = (int16_T)(c11_i26 & 4095);
      }

      if ((int16_T)(c11_i28 & 4096) != 0) {
        c11_i32 = (int16_T)(c11_i28 | -4096);
      } else {
        c11_i32 = (int16_T)(c11_i28 & 4095);
      }

      c11_i34 = c11_i30 - c11_i32;
      if (c11_i34 > 4095) {
        c11_i34 = 4095;
      } else {
        if (c11_i34 < -4096) {
          c11_i34 = -4096;
        }
      }

      if ((int16_T)(c11_i8 & 4096) != 0) {
        c11_i36 = (int16_T)(c11_i8 | -4096);
      } else {
        c11_i36 = (int16_T)(c11_i8 & 4095);
      }

      if ((int16_T)(c11_i10 & 4096) != 0) {
        c11_i38 = (int16_T)(c11_i10 | -4096);
      } else {
        c11_i38 = (int16_T)(c11_i10 & 4095);
      }

      if ((int16_T)(c11_i22 & 4096) != 0) {
        c11_i40 = (int16_T)(c11_i22 | -4096);
      } else {
        c11_i40 = (int16_T)(c11_i22 & 4095);
      }

      if ((int16_T)(c11_i24 & 4096) != 0) {
        c11_i42 = (int16_T)(c11_i24 | -4096);
      } else {
        c11_i42 = (int16_T)(c11_i24 & 4095);
      }

      if (c11_i36 < c11_i38) {
        c11_d2 = (real_T)((int16_T)c11_i20 <= 1);
      } else if (c11_i40 > c11_i42) {
        if ((int16_T)c11_i34 <= 1) {
          c11_d2 = 3.0;
        } else {
          c11_d2 = 0.0;
        }
      } else {
        c11_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c11_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c11_covrtInstance, 4U, 0U, 3U, c11_d2,
                          0.0, -2, 2U, (int32_T)c11_emlrt_update_log_4
                          (chartInstance, c11_c_c,
                           chartInstance->c11_emlrtLocationLoggingDataTables, 17))))
      {
        c11_i_varargin_1 = c11_b_sum;
        c11_j_varargin_1 = c11_i_varargin_1;
        c11_e_var1 = c11_j_varargin_1;
        c11_i44 = c11_e_var1;
        c11_e_covSaturation = false;
        if (c11_i44 < 0) {
          c11_i44 = 0;
        } else {
          if (c11_i44 > 4095) {
            c11_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c11_covrtInstance, 4, 0, 5, 0,
            c11_e_covSaturation);
        }

        c11_e_hfi = (uint16_T)c11_i44;
        c11_b_cont = c11_emlrt_update_log_5(chartInstance, c11_e_hfi,
          chartInstance->c11_emlrtLocationLoggingDataTables, 20);
        c11_b_out_init = c11_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 21);
      } else {
        c11_b_cont = c11_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 22);
        c11_b_out_init = c11_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c11_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c11_cont = c11_b_cont;
  *chartInstance->c11_out_init = c11_b_out_init;
  c11_do_animation_call_c11_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 5U, (real_T)
                    *chartInstance->c11_cont);
  covrtSigUpdateFcn(chartInstance->c11_covrtInstance, 6U, (real_T)
                    *chartInstance->c11_out_init);
}

static void mdl_start_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c11_decisionTxtStartIdx = 0U;
  static const uint32_T c11_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c11_chart_data_browse_helper);
  chartInstance->c11_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c11_RuntimeVar,
    &chartInstance->c11_IsDebuggerActive,
    &chartInstance->c11_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c11_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c11_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c11_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c11_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c11_decisionTxtStartIdx, &c11_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c11_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c11_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c11_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c11_PWM_28_HalfB
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c11_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7463",              /* mexFileName */
    "Thu May 27 10:27:21 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c11_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c11_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c11_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c11_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7463");
    emlrtLocationLoggingPushLog(&c11_emlrtLocationLoggingFileInfo,
      c11_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c11_emlrtLocationLoggingDataTables, c11_emlrtLocationInfo,
      NULL, 0U, c11_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7463");
  }

  sfListenerLightTerminate(chartInstance->c11_RuntimeVar);
  sf_mex_destroy(&c11_eml_mx);
  sf_mex_destroy(&c11_b_eml_mx);
  sf_mex_destroy(&c11_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c11_covrtInstance);
}

static void initSimStructsc11_PWM_28_HalfB(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c11_emlrt_update_log_1(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  real_T c11_d;
  uint8_T c11_u;
  uint8_T c11_localMin;
  real_T c11_d1;
  uint8_T c11_u1;
  uint8_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_d = c11_b_table[0U].SimMin;
    if (c11_d < 2.0) {
      if (c11_d >= 0.0) {
        c11_u = (uint8_T)c11_d;
      } else {
        c11_u = 0U;
      }
    } else if (c11_d >= 2.0) {
      c11_u = 1U;
    } else {
      c11_u = 0U;
    }

    c11_localMin = c11_u;
    c11_d1 = c11_b_table[0U].SimMax;
    if (c11_d1 < 2.0) {
      if (c11_d1 >= 0.0) {
        c11_u1 = (uint8_T)c11_d1;
      } else {
        c11_u1 = 0U;
      }
    } else if (c11_d1 >= 2.0) {
      c11_u1 = 1U;
    } else {
      c11_u1 = 0U;
    }

    c11_localMax = c11_u1;
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c11_in < c11_localMin) {
      c11_localMin = c11_in;
    }

    if (c11_in > c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static int16_T c11_emlrt_update_log_2(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  real_T c11_d;
  int16_T c11_i;
  int16_T c11_localMin;
  real_T c11_d1;
  int16_T c11_i1;
  int16_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_d = muDoubleScalarFloor(c11_b_table[0U].SimMin);
    if (c11_d < 32768.0) {
      if (c11_d >= -32768.0) {
        c11_i = (int16_T)c11_d;
      } else {
        c11_i = MIN_int16_T;
      }
    } else if (c11_d >= 32768.0) {
      c11_i = MAX_int16_T;
    } else {
      c11_i = 0;
    }

    c11_localMin = c11_i;
    c11_d1 = muDoubleScalarFloor(c11_b_table[0U].SimMax);
    if (c11_d1 < 32768.0) {
      if (c11_d1 >= -32768.0) {
        c11_i1 = (int16_T)c11_d1;
      } else {
        c11_i1 = MIN_int16_T;
      }
    } else if (c11_d1 >= 32768.0) {
      c11_i1 = MAX_int16_T;
    } else {
      c11_i1 = 0;
    }

    c11_localMax = c11_i1;
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c11_in < c11_localMin) {
      c11_localMin = c11_in;
    }

    if (c11_in > c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static int16_T c11_emlrt_update_log_3(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  real_T c11_d;
  int16_T c11_i;
  int16_T c11_localMin;
  real_T c11_d1;
  int16_T c11_i1;
  int16_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_d = muDoubleScalarFloor(c11_b_table[0U].SimMin);
    if (c11_d < 2048.0) {
      if (c11_d >= -2048.0) {
        c11_i = (int16_T)c11_d;
      } else {
        c11_i = -2048;
      }
    } else if (c11_d >= 2048.0) {
      c11_i = 2047;
    } else {
      c11_i = 0;
    }

    c11_localMin = c11_i;
    c11_d1 = muDoubleScalarFloor(c11_b_table[0U].SimMax);
    if (c11_d1 < 2048.0) {
      if (c11_d1 >= -2048.0) {
        c11_i1 = (int16_T)c11_d1;
      } else {
        c11_i1 = -2048;
      }
    } else if (c11_d1 >= 2048.0) {
      c11_i1 = 2047;
    } else {
      c11_i1 = 0;
    }

    c11_localMax = c11_i1;
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c11_in < c11_localMin) {
      c11_localMin = c11_in;
    }

    if (c11_in > c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static boolean_T c11_emlrt_update_log_4(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  boolean_T c11_localMin;
  boolean_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_localMin = (c11_b_table[0U].SimMin > 0.0);
    c11_localMax = (c11_b_table[0U].SimMax > 0.0);
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c11_in < (int32_T)c11_localMin) {
      c11_localMin = c11_in;
    }

    if ((int32_T)c11_in > (int32_T)c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static uint16_T c11_emlrt_update_log_5(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  real_T c11_d;
  uint16_T c11_u;
  uint16_T c11_localMin;
  real_T c11_d1;
  uint16_T c11_u1;
  uint16_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_d = c11_b_table[0U].SimMin;
    if (c11_d < 4096.0) {
      if (c11_d >= 0.0) {
        c11_u = (uint16_T)c11_d;
      } else {
        c11_u = 0U;
      }
    } else if (c11_d >= 4096.0) {
      c11_u = 4095U;
    } else {
      c11_u = 0U;
    }

    c11_localMin = c11_u;
    c11_d1 = c11_b_table[0U].SimMax;
    if (c11_d1 < 4096.0) {
      if (c11_d1 >= 0.0) {
        c11_u1 = (uint16_T)c11_d1;
      } else {
        c11_u1 = 0U;
      }
    } else if (c11_d1 >= 4096.0) {
      c11_u1 = 4095U;
    } else {
      c11_u1 = 0U;
    }

    c11_localMax = c11_u1;
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c11_in < c11_localMin) {
      c11_localMin = c11_in;
    }

    if (c11_in > c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static int32_T c11_emlrt_update_log_6(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c11_in, emlrtLocationLoggingDataType c11_table[],
  int32_T c11_index)
{
  boolean_T c11_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c11_b_table;
  real_T c11_d;
  int32_T c11_i;
  int32_T c11_localMin;
  real_T c11_d1;
  int32_T c11_i1;
  int32_T c11_localMax;
  emlrtLocationLoggingHistogramType *c11_histTable;
  real_T c11_inDouble;
  real_T c11_significand;
  int32_T c11_exponent;
  (void)chartInstance;
  c11_isLoggingEnabledHere = (c11_index >= 0);
  if (c11_isLoggingEnabledHere) {
    c11_b_table = (emlrtLocationLoggingDataType *)&c11_table[c11_index];
    c11_d = muDoubleScalarFloor(c11_b_table[0U].SimMin);
    if (c11_d < 65536.0) {
      if (c11_d >= -65536.0) {
        c11_i = (int32_T)c11_d;
      } else {
        c11_i = -65536;
      }
    } else if (c11_d >= 65536.0) {
      c11_i = 65535;
    } else {
      c11_i = 0;
    }

    c11_localMin = c11_i;
    c11_d1 = muDoubleScalarFloor(c11_b_table[0U].SimMax);
    if (c11_d1 < 65536.0) {
      if (c11_d1 >= -65536.0) {
        c11_i1 = (int32_T)c11_d1;
      } else {
        c11_i1 = -65536;
      }
    } else if (c11_d1 >= 65536.0) {
      c11_i1 = 65535;
    } else {
      c11_i1 = 0;
    }

    c11_localMax = c11_i1;
    c11_histTable = c11_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c11_in < c11_localMin) {
      c11_localMin = c11_in;
    }

    if (c11_in > c11_localMax) {
      c11_localMax = c11_in;
    }

    /* Histogram logging. */
    c11_inDouble = (real_T)c11_in;
    c11_histTable->TotalNumberOfValues++;
    if (c11_inDouble == 0.0) {
      c11_histTable->NumberOfZeros++;
    } else {
      c11_histTable->SimSum += c11_inDouble;
      if ((!muDoubleScalarIsInf(c11_inDouble)) && (!muDoubleScalarIsNaN
           (c11_inDouble))) {
        c11_significand = frexp(c11_inDouble, &c11_exponent);
        if (c11_exponent > 128) {
          c11_exponent = 128;
        }

        if (c11_exponent < -127) {
          c11_exponent = -127;
        }

        if (c11_significand < 0.0) {
          c11_histTable->NumberOfNegativeValues++;
          c11_histTable->HistogramOfNegativeValues[127 + c11_exponent]++;
        } else {
          c11_histTable->NumberOfPositiveValues++;
          c11_histTable->HistogramOfPositiveValues[127 + c11_exponent]++;
        }
      }
    }

    c11_b_table[0U].SimMin = (real_T)c11_localMin;
    c11_b_table[0U].SimMax = (real_T)c11_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c11_in;
}

static void c11_emlrtInitVarDataTables(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c11_dataTables[24],
  emlrtLocationLoggingHistogramType c11_histTables[24])
{
  int32_T c11_i;
  int32_T c11_iH;
  (void)chartInstance;
  for (c11_i = 0; c11_i < 24; c11_i++) {
    c11_dataTables[c11_i].SimMin = rtInf;
    c11_dataTables[c11_i].SimMax = rtMinusInf;
    c11_dataTables[c11_i].OverflowWraps = 0;
    c11_dataTables[c11_i].Saturations = 0;
    c11_dataTables[c11_i].IsAlwaysInteger = true;
    c11_dataTables[c11_i].HistogramTable = &c11_histTables[c11_i];
    c11_histTables[c11_i].NumberOfZeros = 0.0;
    c11_histTables[c11_i].NumberOfPositiveValues = 0.0;
    c11_histTables[c11_i].NumberOfNegativeValues = 0.0;
    c11_histTables[c11_i].TotalNumberOfValues = 0.0;
    c11_histTables[c11_i].SimSum = 0.0;
    for (c11_iH = 0; c11_iH < 256; c11_iH++) {
      c11_histTables[c11_i].HistogramOfPositiveValues[c11_iH] = 0.0;
      c11_histTables[c11_i].HistogramOfNegativeValues[c11_iH] = 0.0;
    }
  }
}

const mxArray *sf_c11_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c11_nameCaptureInfo = NULL;
  c11_nameCaptureInfo = NULL;
  sf_mex_assign(&c11_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c11_nameCaptureInfo;
}

static uint16_T c11_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_b_cont, const
  char_T *c11_identifier)
{
  uint16_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = (const char *)c11_identifier;
  c11_thisId.fParent = NULL;
  c11_thisId.bParentIsCell = false;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, c11_sp, sf_mex_dup(c11_b_cont),
    &c11_thisId);
  sf_mex_destroy(&c11_b_cont);
  return c11_y;
}

static uint16_T c11_b_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_u, const
  emlrtMsgIdentifier *c11_parentId)
{
  uint16_T c11_y;
  const mxArray *c11_mxFi = NULL;
  const mxArray *c11_mxInt = NULL;
  uint16_T c11_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c11_parentId, c11_u, false, 0U, NULL, c11_eml_mx, c11_b_eml_mx);
  sf_mex_assign(&c11_mxFi, sf_mex_dup(c11_u), false);
  sf_mex_assign(&c11_mxInt, sf_mex_call(c11_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c11_mxFi)), false);
  sf_mex_import(c11_parentId, sf_mex_dup(c11_mxInt), &c11_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c11_mxFi);
  sf_mex_destroy(&c11_mxInt);
  c11_y = c11_b_u;
  sf_mex_destroy(&c11_mxFi);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static uint8_T c11_c_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_b_out_init, const
  char_T *c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = (const char *)c11_identifier;
  c11_thisId.fParent = NULL;
  c11_thisId.bParentIsCell = false;
  c11_y = c11_d_emlrt_marshallIn(chartInstance, c11_sp, sf_mex_dup
    (c11_b_out_init), &c11_thisId);
  sf_mex_destroy(&c11_b_out_init);
  return c11_y;
}

static uint8_T c11_d_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c11_sp, const mxArray *c11_u, const
  emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  const mxArray *c11_mxFi = NULL;
  const mxArray *c11_mxInt = NULL;
  uint8_T c11_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c11_parentId, c11_u, false, 0U, NULL, c11_eml_mx, c11_c_eml_mx);
  sf_mex_assign(&c11_mxFi, sf_mex_dup(c11_u), false);
  sf_mex_assign(&c11_mxInt, sf_mex_call(c11_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c11_mxFi)), false);
  sf_mex_import(c11_parentId, sf_mex_dup(c11_mxInt), &c11_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c11_mxFi);
  sf_mex_destroy(&c11_mxInt);
  c11_y = c11_b_u;
  sf_mex_destroy(&c11_mxFi);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static uint8_T c11_e_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_b_is_active_c11_PWM_28_HalfB, const char_T *
  c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = (const char *)c11_identifier;
  c11_thisId.fParent = NULL;
  c11_thisId.bParentIsCell = false;
  c11_y = c11_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_b_is_active_c11_PWM_28_HalfB), &c11_thisId);
  sf_mex_destroy(&c11_b_is_active_c11_PWM_28_HalfB);
  return c11_y;
}

static uint8_T c11_f_emlrt_marshallIn(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  uint8_T c11_b_u;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_b_u, 1, 3, 0U, 0, 0U, 0);
  c11_y = c11_b_u;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static const mxArray *c11_chart_data_browse_helper
  (SFc11_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c11_ssIdNumber)
{
  const mxArray *c11_mxData = NULL;
  uint8_T c11_u;
  int16_T c11_i;
  int16_T c11_i1;
  int16_T c11_i2;
  uint16_T c11_u1;
  uint8_T c11_u2;
  uint8_T c11_u3;
  real_T c11_d;
  real_T c11_d1;
  real_T c11_d2;
  real_T c11_d3;
  real_T c11_d4;
  real_T c11_d5;
  c11_mxData = NULL;
  switch (c11_ssIdNumber) {
   case 18U:
    c11_u = *chartInstance->c11_enable;
    c11_d = (real_T)c11_u;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c11_i = *chartInstance->c11_offset;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c11_i1 = *chartInstance->c11_max;
    c11_d1 = (real_T)c11_i1;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c11_i2 = *chartInstance->c11_sum;
    c11_d2 = (real_T)c11_i2;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c11_u1 = *chartInstance->c11_cont;
    c11_d3 = (real_T)c11_u1;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c11_u2 = *chartInstance->c11_init;
    c11_d4 = (real_T)c11_u2;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c11_u3 = *chartInstance->c11_out_init;
    c11_d5 = (real_T)c11_u3;
    sf_mex_assign(&c11_mxData, sf_mex_create("mxData", &c11_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c11_mxData;
}

static int32_T c11__s32_add__(SFc11_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c11_b, int32_T c11_c, int32_T c11_EMLOvCount_src_loc, uint32_T
  c11_ssid_src_loc, int32_T c11_offset_src_loc, int32_T c11_length_src_loc)
{
  int32_T c11_a;
  int32_T c11_PICOffset;
  real_T c11_d;
  observerLogReadPIC(&c11_PICOffset);
  c11_a = c11_b + c11_c;
  if (((c11_a ^ c11_b) & (c11_a ^ c11_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c11_ssid_src_loc,
      c11_offset_src_loc, c11_length_src_loc);
    c11_d = 1.0;
    observerLog(c11_EMLOvCount_src_loc + c11_PICOffset, &c11_d, 1);
  }

  return c11_a;
}

static void init_dsm_address_info(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc11_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c11_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c11_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c11_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c11_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c11_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c11_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c11_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c11_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c11_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c11_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c11_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c11_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoWF8QLhvvJFFv"
    "EdiTpoTwlwQAADrYCCO"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c11_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c11_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c11_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c11_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc11_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c11_PWM_28_HalfB
      ((SFc11_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c11_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c11_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc11_PWM_28_HalfB((SFc11_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c11_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV91uE0cUXhsHgWijwA0SQoK79qpqqhZxBSFrm1iyG5cNP3fWZPbYO/LszDI/TsJz8BLwBFx",
    "y0RfoXS+qvkGlPkLPrNeOs95dE1wikBhpvZrZ73xz/mfs1To9D8cmPi9ueN5lfF/Bp+5Nx0Y2ry",
    "080/WG9102f4hCwsZ9okisvcohSAxPQEtuDZOiI4ayEMbEEBQIithEKlPGpllsORPjthXU8ennE",
    "aNREEnLw12UJeG+4CfIlljTR54mU0BNGyA0kZJ2FLU5Gc01VubIj4COtY2rTNBgAps4tXTPcsMS",
    "Dq1joB2hDUGN9alugSEGfHNcaqazVAczoIwTzogotDYiOoAEHWzgaRLi7741aFQeRiOizC5EZAK",
    "6y8YppxSQ52QaPxwyQYxUjPBWzH0nuKxbn6M+PRkCr3AI6rargIwTyYQpj3/QRktbghxyaMKhHZ",
    "WzBfDSuuA/Y3AEqtRvQ19OQJER7IvSTVOHtI7TaM2zZBlmWAzPiHpEMX4awtLsxczRAcE4wQFKl",
    "MEgNbKjDxSboHtL2WzccZm5qmRsPA22XgVL2VoTqIrCnK1NhU8416WwA5l0YQI8ZW0SQ6phU9Zi",
    "nNYsPJDoYJfe5dVgBcPAZzBfipAVhmuSA6R951dsLGeR1GojYx+Tt9ntLn9ehnWEATUkFIq6gCJ",
    "MA/osdW85W8i0iz0CUSuTqlcEnmbIKpSnh1Y0j6Qao08qmsipCS6ipcBYjzCWWAlPNRZNFczFch",
    "WOEhpB6BoM49DDskFsgU+0a22PsO4mzJw0QVPFkqKouvPnR+/0/PnmA86fmVz+/f0CT62Ax1t4O",
    "/z9BfzV+ln8Rm7f+mzNjUx+Z0H+29x+jZy8w225ynn9+9s31/+59tdv7/7o/A0mb39ej9qSHjVv",
    "tn9y6Xzn9mY2vz1rkPOEnyzlmcPuLejVKOC/ucC/lc31z3vtoDd+sf3yyTY8Voa/ipW456d87+v",
    "V+l7K6Ttbv+s69UmS9l2taCfMLhRuTuz0mM3H8/IKf1zN1qfj34fryf+wk49jkb8aZ/zV8KgUZr",
    "MkHy9W/1928vJF+l/JxdvNpTUDJtj/ZMfdnfXkp/v3V9hxK2fHrfReMSCuW8GAbm8P+s97g5/uD",
    "/YIH+4u95mPrdfzynkXLPel6PnVL5/evg85hzc+Uq6+5rl/UXLr2nfe+8jnhq86z7wcfusztmPd",
    "e+Knxv/pne8edyebP5j/xfIjxsOC23b2uQtkWPT1Auz7Dwm+oHs=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c11_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c11_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c11_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c11_PWM_28_HalfB(SimStruct *S)
{
  SFc11_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc11_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc11_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc11_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c11_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c11_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c11_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c11_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c11_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c11_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c11_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c11_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c11_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c11_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c11_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c11_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c11_JITStateAnimation,
    chartInstance->c11_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c11_PWM_28_HalfB(chartInstance);
}

void c11_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c11_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c11_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c11_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c11_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
