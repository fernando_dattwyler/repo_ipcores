#ifndef __c19_PWM_28_HalfB_h__
#define __c19_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc19_PWM_28_HalfBInstanceStruct
#define typedef_SFc19_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c19_sfEvent;
  boolean_T c19_doneDoubleBufferReInit;
  uint8_T c19_is_active_c19_PWM_28_HalfB;
  uint8_T c19_JITStateAnimation[1];
  uint8_T c19_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c19_emlrtLocationLoggingDataTables[24];
  int32_T c19_IsDebuggerActive;
  int32_T c19_IsSequenceViewerPresent;
  int32_T c19_SequenceViewerOptimization;
  void *c19_RuntimeVar;
  emlrtLocationLoggingHistogramType c19_emlrtLocLogHistTables[24];
  boolean_T c19_emlrtLocLogSimulated;
  uint32_T c19_mlFcnLineNumber;
  void *c19_fcnDataPtrs[7];
  char_T *c19_dataNames[7];
  uint32_T c19_numFcnVars;
  uint32_T c19_ssIds[7];
  uint32_T c19_statuses[7];
  void *c19_outMexFcns[7];
  void *c19_inMexFcns[7];
  CovrtStateflowInstance *c19_covrtInstance;
  void *c19_fEmlrtCtx;
  uint8_T *c19_enable;
  int16_T *c19_offset;
  int16_T *c19_max;
  int16_T *c19_sum;
  uint16_T *c19_cont;
  uint8_T *c19_init;
  uint8_T *c19_out_init;
} SFc19_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc19_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c19_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c19_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c19_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
