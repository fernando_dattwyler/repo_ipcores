/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c4_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c4_eml_mx;
static const mxArray *c4_b_eml_mx;
static const mxArray *c4_c_eml_mx;

/* Function Declarations */
static void initialize_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c4_update_jit_animation_state_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance);
static void c4_do_animation_call_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_st);
static void sf_gateway_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c4_emlrt_update_log_1(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static int16_T c4_emlrt_update_log_2(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static int16_T c4_emlrt_update_log_3(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static boolean_T c4_emlrt_update_log_4(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static uint16_T c4_emlrt_update_log_5(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static int32_T c4_emlrt_update_log_6(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index);
static void c4_emlrtInitVarDataTables(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c4_dataTables[24],
  emlrtLocationLoggingHistogramType c4_histTables[24]);
static uint16_T c4_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_b_cont, const
  char_T *c4_identifier);
static uint16_T c4_b_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_u, const
  emlrtMsgIdentifier *c4_parentId);
static uint8_T c4_c_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_b_out_init, const
  char_T *c4_identifier);
static uint8_T c4_d_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_u, const
  emlrtMsgIdentifier *c4_parentId);
static uint8_T c4_e_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_b_is_active_c4_PWM_28_HalfB, const char_T
  *c4_identifier);
static uint8_T c4_f_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId);
static const mxArray *c4_chart_data_browse_helper
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c4_ssIdNumber);
static int32_T c4__s32_add__(SFc4_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c4_b, int32_T c4_c, int32_T c4_EMLOvCount_src_loc, uint32_T
  c4_ssid_src_loc, int32_T c4_offset_src_loc, int32_T c4_length_src_loc);
static void init_dsm_address_info(SFc4_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c4_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c4_st.tls = chartInstance->c4_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c4_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c4_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c4_is_active_c4_PWM_28_HalfB = 0U;
  sf_mex_assign(&c4_c_eml_mx, sf_mex_call(&c4_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c4_b_eml_mx, sf_mex_call(&c4_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c4_eml_mx, sf_mex_call(&c4_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c4_emlrtLocLogSimulated = false;
  c4_emlrtInitVarDataTables(chartInstance,
    chartInstance->c4_emlrtLocationLoggingDataTables,
    chartInstance->c4_emlrtLocLogHistTables);
}

static void initialize_params_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c4_update_jit_animation_state_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c4_do_animation_call_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c4_st;
  const mxArray *c4_y = NULL;
  const mxArray *c4_b_y = NULL;
  uint16_T c4_u;
  const mxArray *c4_c_y = NULL;
  const mxArray *c4_d_y = NULL;
  uint8_T c4_b_u;
  const mxArray *c4_e_y = NULL;
  const mxArray *c4_f_y = NULL;
  c4_st = NULL;
  c4_st = NULL;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_createcellmatrix(3, 1), false);
  c4_b_y = NULL;
  c4_u = *chartInstance->c4_cont;
  c4_c_y = NULL;
  sf_mex_assign(&c4_c_y, sf_mex_create("y", &c4_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c4_b_y, sf_mex_create_fi(sf_mex_dup(c4_eml_mx), sf_mex_dup
    (c4_b_eml_mx), "simulinkarray", c4_c_y, false, false), false);
  sf_mex_setcell(c4_y, 0, c4_b_y);
  c4_d_y = NULL;
  c4_b_u = *chartInstance->c4_out_init;
  c4_e_y = NULL;
  sf_mex_assign(&c4_e_y, sf_mex_create("y", &c4_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c4_d_y, sf_mex_create_fi(sf_mex_dup(c4_eml_mx), sf_mex_dup
    (c4_c_eml_mx), "simulinkarray", c4_e_y, false, false), false);
  sf_mex_setcell(c4_y, 1, c4_d_y);
  c4_f_y = NULL;
  sf_mex_assign(&c4_f_y, sf_mex_create("y",
    &chartInstance->c4_is_active_c4_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c4_y, 2, c4_f_y);
  sf_mex_assign(&c4_st, c4_y, false);
  return c4_st;
}

static void set_sim_state_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_st)
{
  emlrtStack c4_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c4_u;
  c4_b_st.tls = chartInstance->c4_fEmlrtCtx;
  chartInstance->c4_doneDoubleBufferReInit = true;
  c4_u = sf_mex_dup(c4_st);
  *chartInstance->c4_cont = c4_emlrt_marshallIn(chartInstance, &c4_b_st,
    sf_mex_dup(sf_mex_getcell(c4_u, 0)), "cont");
  *chartInstance->c4_out_init = c4_c_emlrt_marshallIn(chartInstance, &c4_b_st,
    sf_mex_dup(sf_mex_getcell(c4_u, 1)), "out_init");
  chartInstance->c4_is_active_c4_PWM_28_HalfB = c4_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c4_u, 2)),
     "is_active_c4_PWM_28_HalfB");
  sf_mex_destroy(&c4_u);
  sf_mex_destroy(&c4_st);
}

static void sf_gateway_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c4_PICOffset;
  uint8_T c4_b_enable;
  int16_T c4_b_offset;
  int16_T c4_b_max;
  int16_T c4_b_sum;
  uint8_T c4_b_init;
  uint8_T c4_a0;
  uint8_T c4_a;
  uint8_T c4_b_a0;
  uint8_T c4_a1;
  uint8_T c4_b_a1;
  boolean_T c4_c;
  int8_T c4_i;
  int8_T c4_i1;
  real_T c4_d;
  uint8_T c4_c_a0;
  uint16_T c4_b_cont;
  uint8_T c4_b_a;
  uint8_T c4_b_out_init;
  uint8_T c4_d_a0;
  uint8_T c4_c_a1;
  uint8_T c4_d_a1;
  boolean_T c4_b_c;
  int8_T c4_i2;
  int8_T c4_i3;
  real_T c4_d1;
  int16_T c4_varargin_1;
  int16_T c4_b_varargin_1;
  int16_T c4_c_varargin_1;
  int16_T c4_d_varargin_1;
  int16_T c4_var1;
  int16_T c4_b_var1;
  int16_T c4_i4;
  int16_T c4_i5;
  boolean_T c4_covSaturation;
  boolean_T c4_b_covSaturation;
  uint16_T c4_hfi;
  uint16_T c4_b_hfi;
  uint16_T c4_u;
  uint16_T c4_u1;
  int16_T c4_e_varargin_1;
  int16_T c4_f_varargin_1;
  int16_T c4_g_varargin_1;
  int16_T c4_h_varargin_1;
  int16_T c4_c_var1;
  int16_T c4_d_var1;
  int16_T c4_i6;
  int16_T c4_i7;
  boolean_T c4_c_covSaturation;
  boolean_T c4_d_covSaturation;
  uint16_T c4_c_hfi;
  uint16_T c4_d_hfi;
  uint16_T c4_u2;
  uint16_T c4_u3;
  uint16_T c4_e_a0;
  uint16_T c4_f_a0;
  uint16_T c4_b0;
  uint16_T c4_b_b0;
  uint16_T c4_c_a;
  uint16_T c4_d_a;
  uint16_T c4_b;
  uint16_T c4_b_b;
  uint16_T c4_g_a0;
  uint16_T c4_h_a0;
  uint16_T c4_c_b0;
  uint16_T c4_d_b0;
  uint16_T c4_e_a1;
  uint16_T c4_f_a1;
  uint16_T c4_b1;
  uint16_T c4_b_b1;
  uint16_T c4_g_a1;
  uint16_T c4_h_a1;
  uint16_T c4_c_b1;
  uint16_T c4_d_b1;
  boolean_T c4_c_c;
  boolean_T c4_d_c;
  int16_T c4_i8;
  int16_T c4_i9;
  int16_T c4_i10;
  int16_T c4_i11;
  int16_T c4_i12;
  int16_T c4_i13;
  int16_T c4_i14;
  int16_T c4_i15;
  int16_T c4_i16;
  int16_T c4_i17;
  int16_T c4_i18;
  int16_T c4_i19;
  int32_T c4_i20;
  int32_T c4_i21;
  int16_T c4_i22;
  int16_T c4_i23;
  int16_T c4_i24;
  int16_T c4_i25;
  int16_T c4_i26;
  int16_T c4_i27;
  int16_T c4_i28;
  int16_T c4_i29;
  int16_T c4_i30;
  int16_T c4_i31;
  int16_T c4_i32;
  int16_T c4_i33;
  int32_T c4_i34;
  int32_T c4_i35;
  int16_T c4_i36;
  int16_T c4_i37;
  int16_T c4_i38;
  int16_T c4_i39;
  int16_T c4_i40;
  int16_T c4_i41;
  int16_T c4_i42;
  int16_T c4_i43;
  real_T c4_d2;
  real_T c4_d3;
  int16_T c4_i_varargin_1;
  int16_T c4_i_a0;
  int16_T c4_j_varargin_1;
  int16_T c4_e_b0;
  int16_T c4_e_var1;
  int16_T c4_k_varargin_1;
  int16_T c4_i44;
  int16_T c4_v;
  boolean_T c4_e_covSaturation;
  int16_T c4_val;
  int16_T c4_c_b;
  int32_T c4_i45;
  uint16_T c4_e_hfi;
  real_T c4_d4;
  int32_T c4_i46;
  real_T c4_d5;
  real_T c4_d6;
  real_T c4_d7;
  int32_T c4_i47;
  int32_T c4_i48;
  int32_T c4_i49;
  real_T c4_d8;
  real_T c4_d9;
  int32_T c4_e_c;
  int32_T c4_l_varargin_1;
  int32_T c4_m_varargin_1;
  int32_T c4_f_var1;
  int32_T c4_i50;
  boolean_T c4_f_covSaturation;
  uint16_T c4_f_hfi;
  observerLogReadPIC(&c4_PICOffset);
  chartInstance->c4_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 4U, (real_T)
                    *chartInstance->c4_init);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 3U, (real_T)
                    *chartInstance->c4_sum);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 2U, (real_T)
                    *chartInstance->c4_max);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 1U, (real_T)
                    *chartInstance->c4_offset);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 0U, (real_T)
                    *chartInstance->c4_enable);
  chartInstance->c4_sfEvent = CALL_EVENT;
  c4_b_enable = *chartInstance->c4_enable;
  c4_b_offset = *chartInstance->c4_offset;
  c4_b_max = *chartInstance->c4_max;
  c4_b_sum = *chartInstance->c4_sum;
  c4_b_init = *chartInstance->c4_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c4_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c4_emlrt_update_log_1(chartInstance, c4_b_enable,
                        chartInstance->c4_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c4_emlrt_update_log_2(chartInstance, c4_b_offset,
                        chartInstance->c4_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c4_emlrt_update_log_3(chartInstance, c4_b_max,
                        chartInstance->c4_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c4_emlrt_update_log_3(chartInstance, c4_b_sum,
                        chartInstance->c4_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c4_emlrt_update_log_1(chartInstance, c4_b_init,
                        chartInstance->c4_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c4_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c4_covrtInstance, 4U, 0, 0, false);
  c4_a0 = c4_b_enable;
  c4_a = c4_a0;
  c4_b_a0 = c4_a;
  c4_a1 = c4_b_a0;
  c4_b_a1 = c4_a1;
  c4_c = (c4_b_a1 == 0);
  c4_i = (int8_T)c4_b_enable;
  if ((int8_T)(c4_i & 2) != 0) {
    c4_i1 = (int8_T)(c4_i | -2);
  } else {
    c4_i1 = (int8_T)(c4_i & 1);
  }

  if (c4_i1 > 0) {
    c4_d = 3.0;
  } else {
    c4_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c4_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c4_covrtInstance,
        4U, 0U, 0U, c4_d, 0.0, -2, 0U, (int32_T)c4_emlrt_update_log_4
        (chartInstance, c4_c, chartInstance->c4_emlrtLocationLoggingDataTables,
         5)))) {
    c4_b_cont = c4_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c4_emlrtLocationLoggingDataTables, 6);
    c4_b_out_init = c4_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c4_emlrtLocationLoggingDataTables, 7);
  } else {
    c4_c_a0 = c4_b_init;
    c4_b_a = c4_c_a0;
    c4_d_a0 = c4_b_a;
    c4_c_a1 = c4_d_a0;
    c4_d_a1 = c4_c_a1;
    c4_b_c = (c4_d_a1 == 0);
    c4_i2 = (int8_T)c4_b_init;
    if ((int8_T)(c4_i2 & 2) != 0) {
      c4_i3 = (int8_T)(c4_i2 | -2);
    } else {
      c4_i3 = (int8_T)(c4_i2 & 1);
    }

    if (c4_i3 > 0) {
      c4_d1 = 3.0;
    } else {
      c4_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c4_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c4_covrtInstance, 4U, 0U, 1U, c4_d1, 0.0,
                        -2, 0U, (int32_T)c4_emlrt_update_log_4(chartInstance,
           c4_b_c, chartInstance->c4_emlrtLocationLoggingDataTables, 8)))) {
      c4_b_varargin_1 = c4_b_sum;
      c4_d_varargin_1 = c4_b_varargin_1;
      c4_b_var1 = c4_d_varargin_1;
      c4_i5 = c4_b_var1;
      c4_b_covSaturation = false;
      if (c4_i5 < 0) {
        c4_i5 = 0;
      } else {
        if (c4_i5 > 4095) {
          c4_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 0, 0,
          c4_b_covSaturation);
      }

      c4_b_hfi = (uint16_T)c4_i5;
      c4_u1 = c4_emlrt_update_log_5(chartInstance, c4_b_hfi,
        chartInstance->c4_emlrtLocationLoggingDataTables, 10);
      c4_f_varargin_1 = c4_b_max;
      c4_h_varargin_1 = c4_f_varargin_1;
      c4_d_var1 = c4_h_varargin_1;
      c4_i7 = c4_d_var1;
      c4_d_covSaturation = false;
      if (c4_i7 < 0) {
        c4_i7 = 0;
      } else {
        if (c4_i7 > 4095) {
          c4_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 1, 0,
          c4_d_covSaturation);
      }

      c4_d_hfi = (uint16_T)c4_i7;
      c4_u3 = c4_emlrt_update_log_5(chartInstance, c4_d_hfi,
        chartInstance->c4_emlrtLocationLoggingDataTables, 11);
      c4_f_a0 = c4_u1;
      c4_b_b0 = c4_u3;
      c4_d_a = c4_f_a0;
      c4_b_b = c4_b_b0;
      c4_h_a0 = c4_d_a;
      c4_d_b0 = c4_b_b;
      c4_f_a1 = c4_h_a0;
      c4_b_b1 = c4_d_b0;
      c4_h_a1 = c4_f_a1;
      c4_d_b1 = c4_b_b1;
      c4_d_c = (c4_h_a1 < c4_d_b1);
      c4_i9 = (int16_T)c4_u1;
      c4_i11 = (int16_T)c4_u3;
      c4_i13 = (int16_T)c4_u3;
      c4_i15 = (int16_T)c4_u1;
      if ((int16_T)(c4_i13 & 4096) != 0) {
        c4_i17 = (int16_T)(c4_i13 | -4096);
      } else {
        c4_i17 = (int16_T)(c4_i13 & 4095);
      }

      if ((int16_T)(c4_i15 & 4096) != 0) {
        c4_i19 = (int16_T)(c4_i15 | -4096);
      } else {
        c4_i19 = (int16_T)(c4_i15 & 4095);
      }

      c4_i21 = c4_i17 - c4_i19;
      if (c4_i21 > 4095) {
        c4_i21 = 4095;
      } else {
        if (c4_i21 < -4096) {
          c4_i21 = -4096;
        }
      }

      c4_i23 = (int16_T)c4_u1;
      c4_i25 = (int16_T)c4_u3;
      c4_i27 = (int16_T)c4_u1;
      c4_i29 = (int16_T)c4_u3;
      if ((int16_T)(c4_i27 & 4096) != 0) {
        c4_i31 = (int16_T)(c4_i27 | -4096);
      } else {
        c4_i31 = (int16_T)(c4_i27 & 4095);
      }

      if ((int16_T)(c4_i29 & 4096) != 0) {
        c4_i33 = (int16_T)(c4_i29 | -4096);
      } else {
        c4_i33 = (int16_T)(c4_i29 & 4095);
      }

      c4_i35 = c4_i31 - c4_i33;
      if (c4_i35 > 4095) {
        c4_i35 = 4095;
      } else {
        if (c4_i35 < -4096) {
          c4_i35 = -4096;
        }
      }

      if ((int16_T)(c4_i9 & 4096) != 0) {
        c4_i37 = (int16_T)(c4_i9 | -4096);
      } else {
        c4_i37 = (int16_T)(c4_i9 & 4095);
      }

      if ((int16_T)(c4_i11 & 4096) != 0) {
        c4_i39 = (int16_T)(c4_i11 | -4096);
      } else {
        c4_i39 = (int16_T)(c4_i11 & 4095);
      }

      if ((int16_T)(c4_i23 & 4096) != 0) {
        c4_i41 = (int16_T)(c4_i23 | -4096);
      } else {
        c4_i41 = (int16_T)(c4_i23 & 4095);
      }

      if ((int16_T)(c4_i25 & 4096) != 0) {
        c4_i43 = (int16_T)(c4_i25 | -4096);
      } else {
        c4_i43 = (int16_T)(c4_i25 & 4095);
      }

      if (c4_i37 < c4_i39) {
        c4_d3 = (real_T)((int16_T)c4_i21 <= 1);
      } else if (c4_i41 > c4_i43) {
        if ((int16_T)c4_i35 <= 1) {
          c4_d3 = 3.0;
        } else {
          c4_d3 = 0.0;
        }
      } else {
        c4_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c4_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c4_covrtInstance, 4U, 0U, 2U, c4_d3,
                          0.0, -2, 2U, (int32_T)c4_emlrt_update_log_4
                          (chartInstance, c4_d_c,
                           chartInstance->c4_emlrtLocationLoggingDataTables, 9))))
      {
        c4_i_a0 = c4_b_sum;
        c4_e_b0 = c4_b_offset;
        c4_k_varargin_1 = c4_e_b0;
        c4_v = c4_k_varargin_1;
        c4_val = c4_v;
        c4_c_b = c4_val;
        c4_i45 = c4_i_a0;
        if (c4_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d4 = 1.0;
          observerLog(63 + c4_PICOffset, &c4_d4, 1);
        }

        if (c4_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d5 = 1.0;
          observerLog(63 + c4_PICOffset, &c4_d5, 1);
        }

        c4_i46 = c4_c_b;
        if (c4_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d6 = 1.0;
          observerLog(66 + c4_PICOffset, &c4_d6, 1);
        }

        if (c4_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d7 = 1.0;
          observerLog(66 + c4_PICOffset, &c4_d7, 1);
        }

        if ((c4_i45 & 65536) != 0) {
          c4_i47 = c4_i45 | -65536;
        } else {
          c4_i47 = c4_i45 & 65535;
        }

        if ((c4_i46 & 65536) != 0) {
          c4_i48 = c4_i46 | -65536;
        } else {
          c4_i48 = c4_i46 & 65535;
        }

        c4_i49 = c4__s32_add__(chartInstance, c4_i47, c4_i48, 65, 1U, 323, 12);
        if (c4_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d8 = 1.0;
          observerLog(71 + c4_PICOffset, &c4_d8, 1);
        }

        if (c4_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c4_d9 = 1.0;
          observerLog(71 + c4_PICOffset, &c4_d9, 1);
        }

        if ((c4_i49 & 65536) != 0) {
          c4_e_c = c4_i49 | -65536;
        } else {
          c4_e_c = c4_i49 & 65535;
        }

        c4_l_varargin_1 = c4_emlrt_update_log_6(chartInstance, c4_e_c,
          chartInstance->c4_emlrtLocationLoggingDataTables, 13);
        c4_m_varargin_1 = c4_l_varargin_1;
        c4_f_var1 = c4_m_varargin_1;
        c4_i50 = c4_f_var1;
        c4_f_covSaturation = false;
        if (c4_i50 < 0) {
          c4_i50 = 0;
        } else {
          if (c4_i50 > 4095) {
            c4_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 2, 0,
            c4_f_covSaturation);
        }

        c4_f_hfi = (uint16_T)c4_i50;
        c4_b_cont = c4_emlrt_update_log_5(chartInstance, c4_f_hfi,
          chartInstance->c4_emlrtLocationLoggingDataTables, 12);
        c4_b_out_init = c4_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 14);
      } else {
        c4_b_cont = c4_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 15);
        c4_b_out_init = c4_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c4_varargin_1 = c4_b_sum;
      c4_c_varargin_1 = c4_varargin_1;
      c4_var1 = c4_c_varargin_1;
      c4_i4 = c4_var1;
      c4_covSaturation = false;
      if (c4_i4 < 0) {
        c4_i4 = 0;
      } else {
        if (c4_i4 > 4095) {
          c4_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 3, 0,
          c4_covSaturation);
      }

      c4_hfi = (uint16_T)c4_i4;
      c4_u = c4_emlrt_update_log_5(chartInstance, c4_hfi,
        chartInstance->c4_emlrtLocationLoggingDataTables, 18);
      c4_e_varargin_1 = c4_b_max;
      c4_g_varargin_1 = c4_e_varargin_1;
      c4_c_var1 = c4_g_varargin_1;
      c4_i6 = c4_c_var1;
      c4_c_covSaturation = false;
      if (c4_i6 < 0) {
        c4_i6 = 0;
      } else {
        if (c4_i6 > 4095) {
          c4_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 4, 0,
          c4_c_covSaturation);
      }

      c4_c_hfi = (uint16_T)c4_i6;
      c4_u2 = c4_emlrt_update_log_5(chartInstance, c4_c_hfi,
        chartInstance->c4_emlrtLocationLoggingDataTables, 19);
      c4_e_a0 = c4_u;
      c4_b0 = c4_u2;
      c4_c_a = c4_e_a0;
      c4_b = c4_b0;
      c4_g_a0 = c4_c_a;
      c4_c_b0 = c4_b;
      c4_e_a1 = c4_g_a0;
      c4_b1 = c4_c_b0;
      c4_g_a1 = c4_e_a1;
      c4_c_b1 = c4_b1;
      c4_c_c = (c4_g_a1 < c4_c_b1);
      c4_i8 = (int16_T)c4_u;
      c4_i10 = (int16_T)c4_u2;
      c4_i12 = (int16_T)c4_u2;
      c4_i14 = (int16_T)c4_u;
      if ((int16_T)(c4_i12 & 4096) != 0) {
        c4_i16 = (int16_T)(c4_i12 | -4096);
      } else {
        c4_i16 = (int16_T)(c4_i12 & 4095);
      }

      if ((int16_T)(c4_i14 & 4096) != 0) {
        c4_i18 = (int16_T)(c4_i14 | -4096);
      } else {
        c4_i18 = (int16_T)(c4_i14 & 4095);
      }

      c4_i20 = c4_i16 - c4_i18;
      if (c4_i20 > 4095) {
        c4_i20 = 4095;
      } else {
        if (c4_i20 < -4096) {
          c4_i20 = -4096;
        }
      }

      c4_i22 = (int16_T)c4_u;
      c4_i24 = (int16_T)c4_u2;
      c4_i26 = (int16_T)c4_u;
      c4_i28 = (int16_T)c4_u2;
      if ((int16_T)(c4_i26 & 4096) != 0) {
        c4_i30 = (int16_T)(c4_i26 | -4096);
      } else {
        c4_i30 = (int16_T)(c4_i26 & 4095);
      }

      if ((int16_T)(c4_i28 & 4096) != 0) {
        c4_i32 = (int16_T)(c4_i28 | -4096);
      } else {
        c4_i32 = (int16_T)(c4_i28 & 4095);
      }

      c4_i34 = c4_i30 - c4_i32;
      if (c4_i34 > 4095) {
        c4_i34 = 4095;
      } else {
        if (c4_i34 < -4096) {
          c4_i34 = -4096;
        }
      }

      if ((int16_T)(c4_i8 & 4096) != 0) {
        c4_i36 = (int16_T)(c4_i8 | -4096);
      } else {
        c4_i36 = (int16_T)(c4_i8 & 4095);
      }

      if ((int16_T)(c4_i10 & 4096) != 0) {
        c4_i38 = (int16_T)(c4_i10 | -4096);
      } else {
        c4_i38 = (int16_T)(c4_i10 & 4095);
      }

      if ((int16_T)(c4_i22 & 4096) != 0) {
        c4_i40 = (int16_T)(c4_i22 | -4096);
      } else {
        c4_i40 = (int16_T)(c4_i22 & 4095);
      }

      if ((int16_T)(c4_i24 & 4096) != 0) {
        c4_i42 = (int16_T)(c4_i24 | -4096);
      } else {
        c4_i42 = (int16_T)(c4_i24 & 4095);
      }

      if (c4_i36 < c4_i38) {
        c4_d2 = (real_T)((int16_T)c4_i20 <= 1);
      } else if (c4_i40 > c4_i42) {
        if ((int16_T)c4_i34 <= 1) {
          c4_d2 = 3.0;
        } else {
          c4_d2 = 0.0;
        }
      } else {
        c4_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c4_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c4_covrtInstance, 4U, 0U, 3U, c4_d2,
                          0.0, -2, 2U, (int32_T)c4_emlrt_update_log_4
                          (chartInstance, c4_c_c,
                           chartInstance->c4_emlrtLocationLoggingDataTables, 17))))
      {
        c4_i_varargin_1 = c4_b_sum;
        c4_j_varargin_1 = c4_i_varargin_1;
        c4_e_var1 = c4_j_varargin_1;
        c4_i44 = c4_e_var1;
        c4_e_covSaturation = false;
        if (c4_i44 < 0) {
          c4_i44 = 0;
        } else {
          if (c4_i44 > 4095) {
            c4_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c4_covrtInstance, 4, 0, 5, 0,
            c4_e_covSaturation);
        }

        c4_e_hfi = (uint16_T)c4_i44;
        c4_b_cont = c4_emlrt_update_log_5(chartInstance, c4_e_hfi,
          chartInstance->c4_emlrtLocationLoggingDataTables, 20);
        c4_b_out_init = c4_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 21);
      } else {
        c4_b_cont = c4_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 22);
        c4_b_out_init = c4_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c4_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c4_cont = c4_b_cont;
  *chartInstance->c4_out_init = c4_b_out_init;
  c4_do_animation_call_c4_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 5U, (real_T)
                    *chartInstance->c4_cont);
  covrtSigUpdateFcn(chartInstance->c4_covrtInstance, 6U, (real_T)
                    *chartInstance->c4_out_init);
}

static void mdl_start_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c4_decisionTxtStartIdx = 0U;
  static const uint32_T c4_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c4_chart_data_browse_helper);
  chartInstance->c4_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c4_RuntimeVar,
    &chartInstance->c4_IsDebuggerActive,
    &chartInstance->c4_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c4_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c4_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c4_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c4_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c4_decisionTxtStartIdx, &c4_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c4_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c4_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c4_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c4_PWM_28_HalfB
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c4_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7164",              /* mexFileName */
    "Thu May 27 10:27:17 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c4_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c4_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c4_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c4_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7164");
    emlrtLocationLoggingPushLog(&c4_emlrtLocationLoggingFileInfo,
      c4_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c4_emlrtLocationLoggingDataTables, c4_emlrtLocationInfo,
      NULL, 0U, c4_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7164");
  }

  sfListenerLightTerminate(chartInstance->c4_RuntimeVar);
  sf_mex_destroy(&c4_eml_mx);
  sf_mex_destroy(&c4_b_eml_mx);
  sf_mex_destroy(&c4_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c4_covrtInstance);
}

static void initSimStructsc4_PWM_28_HalfB(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c4_emlrt_update_log_1(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  real_T c4_d;
  uint8_T c4_u;
  uint8_T c4_localMin;
  real_T c4_d1;
  uint8_T c4_u1;
  uint8_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_d = c4_b_table[0U].SimMin;
    if (c4_d < 2.0) {
      if (c4_d >= 0.0) {
        c4_u = (uint8_T)c4_d;
      } else {
        c4_u = 0U;
      }
    } else if (c4_d >= 2.0) {
      c4_u = 1U;
    } else {
      c4_u = 0U;
    }

    c4_localMin = c4_u;
    c4_d1 = c4_b_table[0U].SimMax;
    if (c4_d1 < 2.0) {
      if (c4_d1 >= 0.0) {
        c4_u1 = (uint8_T)c4_d1;
      } else {
        c4_u1 = 0U;
      }
    } else if (c4_d1 >= 2.0) {
      c4_u1 = 1U;
    } else {
      c4_u1 = 0U;
    }

    c4_localMax = c4_u1;
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c4_in < c4_localMin) {
      c4_localMin = c4_in;
    }

    if (c4_in > c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static int16_T c4_emlrt_update_log_2(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  real_T c4_d;
  int16_T c4_i;
  int16_T c4_localMin;
  real_T c4_d1;
  int16_T c4_i1;
  int16_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_d = muDoubleScalarFloor(c4_b_table[0U].SimMin);
    if (c4_d < 32768.0) {
      if (c4_d >= -32768.0) {
        c4_i = (int16_T)c4_d;
      } else {
        c4_i = MIN_int16_T;
      }
    } else if (c4_d >= 32768.0) {
      c4_i = MAX_int16_T;
    } else {
      c4_i = 0;
    }

    c4_localMin = c4_i;
    c4_d1 = muDoubleScalarFloor(c4_b_table[0U].SimMax);
    if (c4_d1 < 32768.0) {
      if (c4_d1 >= -32768.0) {
        c4_i1 = (int16_T)c4_d1;
      } else {
        c4_i1 = MIN_int16_T;
      }
    } else if (c4_d1 >= 32768.0) {
      c4_i1 = MAX_int16_T;
    } else {
      c4_i1 = 0;
    }

    c4_localMax = c4_i1;
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c4_in < c4_localMin) {
      c4_localMin = c4_in;
    }

    if (c4_in > c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static int16_T c4_emlrt_update_log_3(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  real_T c4_d;
  int16_T c4_i;
  int16_T c4_localMin;
  real_T c4_d1;
  int16_T c4_i1;
  int16_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_d = muDoubleScalarFloor(c4_b_table[0U].SimMin);
    if (c4_d < 2048.0) {
      if (c4_d >= -2048.0) {
        c4_i = (int16_T)c4_d;
      } else {
        c4_i = -2048;
      }
    } else if (c4_d >= 2048.0) {
      c4_i = 2047;
    } else {
      c4_i = 0;
    }

    c4_localMin = c4_i;
    c4_d1 = muDoubleScalarFloor(c4_b_table[0U].SimMax);
    if (c4_d1 < 2048.0) {
      if (c4_d1 >= -2048.0) {
        c4_i1 = (int16_T)c4_d1;
      } else {
        c4_i1 = -2048;
      }
    } else if (c4_d1 >= 2048.0) {
      c4_i1 = 2047;
    } else {
      c4_i1 = 0;
    }

    c4_localMax = c4_i1;
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c4_in < c4_localMin) {
      c4_localMin = c4_in;
    }

    if (c4_in > c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static boolean_T c4_emlrt_update_log_4(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  boolean_T c4_localMin;
  boolean_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_localMin = (c4_b_table[0U].SimMin > 0.0);
    c4_localMax = (c4_b_table[0U].SimMax > 0.0);
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c4_in < (int32_T)c4_localMin) {
      c4_localMin = c4_in;
    }

    if ((int32_T)c4_in > (int32_T)c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static uint16_T c4_emlrt_update_log_5(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  real_T c4_d;
  uint16_T c4_u;
  uint16_T c4_localMin;
  real_T c4_d1;
  uint16_T c4_u1;
  uint16_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_d = c4_b_table[0U].SimMin;
    if (c4_d < 4096.0) {
      if (c4_d >= 0.0) {
        c4_u = (uint16_T)c4_d;
      } else {
        c4_u = 0U;
      }
    } else if (c4_d >= 4096.0) {
      c4_u = 4095U;
    } else {
      c4_u = 0U;
    }

    c4_localMin = c4_u;
    c4_d1 = c4_b_table[0U].SimMax;
    if (c4_d1 < 4096.0) {
      if (c4_d1 >= 0.0) {
        c4_u1 = (uint16_T)c4_d1;
      } else {
        c4_u1 = 0U;
      }
    } else if (c4_d1 >= 4096.0) {
      c4_u1 = 4095U;
    } else {
      c4_u1 = 0U;
    }

    c4_localMax = c4_u1;
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c4_in < c4_localMin) {
      c4_localMin = c4_in;
    }

    if (c4_in > c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static int32_T c4_emlrt_update_log_6(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c4_in, emlrtLocationLoggingDataType c4_table[],
  int32_T c4_index)
{
  boolean_T c4_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c4_b_table;
  real_T c4_d;
  int32_T c4_i;
  int32_T c4_localMin;
  real_T c4_d1;
  int32_T c4_i1;
  int32_T c4_localMax;
  emlrtLocationLoggingHistogramType *c4_histTable;
  real_T c4_inDouble;
  real_T c4_significand;
  int32_T c4_exponent;
  (void)chartInstance;
  c4_isLoggingEnabledHere = (c4_index >= 0);
  if (c4_isLoggingEnabledHere) {
    c4_b_table = (emlrtLocationLoggingDataType *)&c4_table[c4_index];
    c4_d = muDoubleScalarFloor(c4_b_table[0U].SimMin);
    if (c4_d < 65536.0) {
      if (c4_d >= -65536.0) {
        c4_i = (int32_T)c4_d;
      } else {
        c4_i = -65536;
      }
    } else if (c4_d >= 65536.0) {
      c4_i = 65535;
    } else {
      c4_i = 0;
    }

    c4_localMin = c4_i;
    c4_d1 = muDoubleScalarFloor(c4_b_table[0U].SimMax);
    if (c4_d1 < 65536.0) {
      if (c4_d1 >= -65536.0) {
        c4_i1 = (int32_T)c4_d1;
      } else {
        c4_i1 = -65536;
      }
    } else if (c4_d1 >= 65536.0) {
      c4_i1 = 65535;
    } else {
      c4_i1 = 0;
    }

    c4_localMax = c4_i1;
    c4_histTable = c4_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c4_in < c4_localMin) {
      c4_localMin = c4_in;
    }

    if (c4_in > c4_localMax) {
      c4_localMax = c4_in;
    }

    /* Histogram logging. */
    c4_inDouble = (real_T)c4_in;
    c4_histTable->TotalNumberOfValues++;
    if (c4_inDouble == 0.0) {
      c4_histTable->NumberOfZeros++;
    } else {
      c4_histTable->SimSum += c4_inDouble;
      if ((!muDoubleScalarIsInf(c4_inDouble)) && (!muDoubleScalarIsNaN
           (c4_inDouble))) {
        c4_significand = frexp(c4_inDouble, &c4_exponent);
        if (c4_exponent > 128) {
          c4_exponent = 128;
        }

        if (c4_exponent < -127) {
          c4_exponent = -127;
        }

        if (c4_significand < 0.0) {
          c4_histTable->NumberOfNegativeValues++;
          c4_histTable->HistogramOfNegativeValues[127 + c4_exponent]++;
        } else {
          c4_histTable->NumberOfPositiveValues++;
          c4_histTable->HistogramOfPositiveValues[127 + c4_exponent]++;
        }
      }
    }

    c4_b_table[0U].SimMin = (real_T)c4_localMin;
    c4_b_table[0U].SimMax = (real_T)c4_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c4_in;
}

static void c4_emlrtInitVarDataTables(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c4_dataTables[24],
  emlrtLocationLoggingHistogramType c4_histTables[24])
{
  int32_T c4_i;
  int32_T c4_iH;
  (void)chartInstance;
  for (c4_i = 0; c4_i < 24; c4_i++) {
    c4_dataTables[c4_i].SimMin = rtInf;
    c4_dataTables[c4_i].SimMax = rtMinusInf;
    c4_dataTables[c4_i].OverflowWraps = 0;
    c4_dataTables[c4_i].Saturations = 0;
    c4_dataTables[c4_i].IsAlwaysInteger = true;
    c4_dataTables[c4_i].HistogramTable = &c4_histTables[c4_i];
    c4_histTables[c4_i].NumberOfZeros = 0.0;
    c4_histTables[c4_i].NumberOfPositiveValues = 0.0;
    c4_histTables[c4_i].NumberOfNegativeValues = 0.0;
    c4_histTables[c4_i].TotalNumberOfValues = 0.0;
    c4_histTables[c4_i].SimSum = 0.0;
    for (c4_iH = 0; c4_iH < 256; c4_iH++) {
      c4_histTables[c4_i].HistogramOfPositiveValues[c4_iH] = 0.0;
      c4_histTables[c4_i].HistogramOfNegativeValues[c4_iH] = 0.0;
    }
  }
}

const mxArray *sf_c4_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c4_nameCaptureInfo = NULL;
  c4_nameCaptureInfo = NULL;
  sf_mex_assign(&c4_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c4_nameCaptureInfo;
}

static uint16_T c4_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_b_cont, const
  char_T *c4_identifier)
{
  uint16_T c4_y;
  emlrtMsgIdentifier c4_thisId;
  c4_thisId.fIdentifier = (const char *)c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_b_emlrt_marshallIn(chartInstance, c4_sp, sf_mex_dup(c4_b_cont),
    &c4_thisId);
  sf_mex_destroy(&c4_b_cont);
  return c4_y;
}

static uint16_T c4_b_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_u, const
  emlrtMsgIdentifier *c4_parentId)
{
  uint16_T c4_y;
  const mxArray *c4_mxFi = NULL;
  const mxArray *c4_mxInt = NULL;
  uint16_T c4_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c4_parentId, c4_u, false, 0U, NULL, c4_eml_mx, c4_b_eml_mx);
  sf_mex_assign(&c4_mxFi, sf_mex_dup(c4_u), false);
  sf_mex_assign(&c4_mxInt, sf_mex_call(c4_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c4_mxFi)), false);
  sf_mex_import(c4_parentId, sf_mex_dup(c4_mxInt), &c4_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c4_mxFi);
  sf_mex_destroy(&c4_mxInt);
  c4_y = c4_b_u;
  sf_mex_destroy(&c4_mxFi);
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static uint8_T c4_c_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_b_out_init, const
  char_T *c4_identifier)
{
  uint8_T c4_y;
  emlrtMsgIdentifier c4_thisId;
  c4_thisId.fIdentifier = (const char *)c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_d_emlrt_marshallIn(chartInstance, c4_sp, sf_mex_dup(c4_b_out_init),
    &c4_thisId);
  sf_mex_destroy(&c4_b_out_init);
  return c4_y;
}

static uint8_T c4_d_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c4_sp, const mxArray *c4_u, const
  emlrtMsgIdentifier *c4_parentId)
{
  uint8_T c4_y;
  const mxArray *c4_mxFi = NULL;
  const mxArray *c4_mxInt = NULL;
  uint8_T c4_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c4_parentId, c4_u, false, 0U, NULL, c4_eml_mx, c4_c_eml_mx);
  sf_mex_assign(&c4_mxFi, sf_mex_dup(c4_u), false);
  sf_mex_assign(&c4_mxInt, sf_mex_call(c4_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c4_mxFi)), false);
  sf_mex_import(c4_parentId, sf_mex_dup(c4_mxInt), &c4_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c4_mxFi);
  sf_mex_destroy(&c4_mxInt);
  c4_y = c4_b_u;
  sf_mex_destroy(&c4_mxFi);
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static uint8_T c4_e_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_b_is_active_c4_PWM_28_HalfB, const char_T
  *c4_identifier)
{
  uint8_T c4_y;
  emlrtMsgIdentifier c4_thisId;
  c4_thisId.fIdentifier = (const char *)c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c4_b_is_active_c4_PWM_28_HalfB), &c4_thisId);
  sf_mex_destroy(&c4_b_is_active_c4_PWM_28_HalfB);
  return c4_y;
}

static uint8_T c4_f_emlrt_marshallIn(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId)
{
  uint8_T c4_y;
  uint8_T c4_b_u;
  (void)chartInstance;
  sf_mex_import(c4_parentId, sf_mex_dup(c4_u), &c4_b_u, 1, 3, 0U, 0, 0U, 0);
  c4_y = c4_b_u;
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static const mxArray *c4_chart_data_browse_helper
  (SFc4_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c4_ssIdNumber)
{
  const mxArray *c4_mxData = NULL;
  uint8_T c4_u;
  int16_T c4_i;
  int16_T c4_i1;
  int16_T c4_i2;
  uint16_T c4_u1;
  uint8_T c4_u2;
  uint8_T c4_u3;
  real_T c4_d;
  real_T c4_d1;
  real_T c4_d2;
  real_T c4_d3;
  real_T c4_d4;
  real_T c4_d5;
  c4_mxData = NULL;
  switch (c4_ssIdNumber) {
   case 18U:
    c4_u = *chartInstance->c4_enable;
    c4_d = (real_T)c4_u;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c4_i = *chartInstance->c4_offset;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c4_i1 = *chartInstance->c4_max;
    c4_d1 = (real_T)c4_i1;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c4_i2 = *chartInstance->c4_sum;
    c4_d2 = (real_T)c4_i2;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c4_u1 = *chartInstance->c4_cont;
    c4_d3 = (real_T)c4_u1;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c4_u2 = *chartInstance->c4_init;
    c4_d4 = (real_T)c4_u2;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c4_u3 = *chartInstance->c4_out_init;
    c4_d5 = (real_T)c4_u3;
    sf_mex_assign(&c4_mxData, sf_mex_create("mxData", &c4_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c4_mxData;
}

static int32_T c4__s32_add__(SFc4_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c4_b, int32_T c4_c, int32_T c4_EMLOvCount_src_loc, uint32_T
  c4_ssid_src_loc, int32_T c4_offset_src_loc, int32_T c4_length_src_loc)
{
  int32_T c4_a;
  int32_T c4_PICOffset;
  real_T c4_d;
  observerLogReadPIC(&c4_PICOffset);
  c4_a = c4_b + c4_c;
  if (((c4_a ^ c4_b) & (c4_a ^ c4_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c4_ssid_src_loc, c4_offset_src_loc,
      c4_length_src_loc);
    c4_d = 1.0;
    observerLog(c4_EMLOvCount_src_loc + c4_PICOffset, &c4_d, 1);
  }

  return c4_a;
}

static void init_dsm_address_info(SFc4_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc4_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c4_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c4_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c4_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c4_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c4_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c4_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c4_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c4_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c4_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c4_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c4_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c4_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c4_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c4_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyySXxAuG+8kUW8R"
    "2JOmhOSuSAAAOorIF4="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c4_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c4_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c4_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c4_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c4_PWM_28_HalfB
      ((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c4_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c4_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc4_PWM_28_HalfB((SFc4_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c4_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5SNDUcLspDBRIds0qSIqkyKp2TEmxAKlWQudnJ4yHT+JAwxl6fmQ758gl2hN",
    "02UUv0F0XRW9QoEfIG4qSZYqk4igxUqADUMQMv/fN+5+RV+v0PByb+Lz+2vOu4/sGPnVvOjayeW",
    "3hma43vO+y+Q4KCRv3iSKx9iqHIDE8By25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "YxGQSQtD/dQloQHgp8hW2JNH3maTAE1bYDQREraUdTmZDTXWJkTPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgXaENgQ11ue6BYYY8M1pqZnOUh3MgDJOOCOi0NqI6AASdLCBF0mIvwfWoFF5GI2IMnsQkQn",
    "oLhunnFJAnpNp/HDEBDFSMcJbMfed4LJufY769GQIvMIhqNueAjJOJBOmPP5BGy1tCXLEoQlHdl",
    "TOFsCxdcF/yeAEVKnfhr6cgCIjOBClm6YOaZ2m0ZpnyTLMsBheEvWEYvw0hKXZi5mjA4JxgkOUK",
    "INBamRHHyo2QfeWstm44zJzVcnYeBpsvQqWsrUmUBWFOVubCp9wrkthhzLpwgR4ytokhlTDpqzF",
    "OK1ZeCjRwS69y6vBCoaBz2C+FCErDNckB0j7zk/YWC4iqdVGxj4mb7PbXf68DOsIA2pIKBR1AUW",
    "YBvRZ6t5ytpBpF3sEolYmVa8IPM2QVShPD61onkg1Rp9UNJFzE1xES4GxHmEssRJeaCyaKpiL5S",
    "ocJTSC0DUYxqGHZYPYAp9o19qeYN1NmDlrgqaKJUVRdefPfe/8/Ln1HufPTC7/vrvAUyvg8RbeD",
    "v94AX+zfhG/kdu3PltzI5PfXZD/MrdfIyfvcFuuct7+/svPX/3zxV/Pfv2j8zeYvP15PWpLetS8",
    "2f7Jtcud25vZ/NtZg5wn/GQpzxx2f0GvRgH/Nwv8W9lcP9xvB73x6wfHzx/AU2X4m1iJH/yU77d",
    "6tb7XcvrO1u+4Tn2WpH1XK9oJswuFmxM7PWbz8by+wh83s/Xp+HdnPfl7u/k4FvmrccFfDY9KYT",
    "ZL8vFq9X+0m5cv0v9GLt5uLq0ZMME+kh13dteTn+7fX2HHds6O7fReMSCuW8GAPhz0X/UG3z8e7",
    "BM+3CvoMx9ar5eV865Y7r+i5/9++fT2vc85vPGBcvU1z/2rklvXvsveRz43fNV55uXwW5+xHeve",
    "Ez81/k/vcve429n8x/lfLD9iPCy4bWefu0CGRV+vwL53r2ugSw==",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c4_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c4_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c4_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c4_PWM_28_HalfB(SimStruct *S)
{
  SFc4_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc4_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc4_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc4_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c4_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c4_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c4_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c4_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c4_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c4_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c4_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c4_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c4_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c4_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c4_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c4_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c4_JITStateAnimation,
    chartInstance->c4_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c4_PWM_28_HalfB(chartInstance);
}

void c4_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c4_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c4_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c4_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c4_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
