/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c19_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c19_eml_mx;
static const mxArray *c19_b_eml_mx;
static const mxArray *c19_c_eml_mx;

/* Function Declarations */
static void initialize_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c19_update_jit_animation_state_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance);
static void c19_do_animation_call_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_st);
static void sf_gateway_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c19_emlrt_update_log_1(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static int16_T c19_emlrt_update_log_2(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static int16_T c19_emlrt_update_log_3(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static boolean_T c19_emlrt_update_log_4(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static uint16_T c19_emlrt_update_log_5(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static int32_T c19_emlrt_update_log_6(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index);
static void c19_emlrtInitVarDataTables(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c19_dataTables[24],
  emlrtLocationLoggingHistogramType c19_histTables[24]);
static uint16_T c19_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_b_cont, const
  char_T *c19_identifier);
static uint16_T c19_b_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_u, const
  emlrtMsgIdentifier *c19_parentId);
static uint8_T c19_c_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_b_out_init, const
  char_T *c19_identifier);
static uint8_T c19_d_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_u, const
  emlrtMsgIdentifier *c19_parentId);
static uint8_T c19_e_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_b_is_active_c19_PWM_28_HalfB, const char_T *
  c19_identifier);
static uint8_T c19_f_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_u, const emlrtMsgIdentifier *c19_parentId);
static const mxArray *c19_chart_data_browse_helper
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c19_ssIdNumber);
static int32_T c19__s32_add__(SFc19_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c19_b, int32_T c19_c, int32_T c19_EMLOvCount_src_loc, uint32_T
  c19_ssid_src_loc, int32_T c19_offset_src_loc, int32_T c19_length_src_loc);
static void init_dsm_address_info(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c19_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c19_st.tls = chartInstance->c19_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c19_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c19_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c19_is_active_c19_PWM_28_HalfB = 0U;
  sf_mex_assign(&c19_c_eml_mx, sf_mex_call(&c19_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c19_b_eml_mx, sf_mex_call(&c19_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c19_eml_mx, sf_mex_call(&c19_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c19_emlrtLocLogSimulated = false;
  c19_emlrtInitVarDataTables(chartInstance,
    chartInstance->c19_emlrtLocationLoggingDataTables,
    chartInstance->c19_emlrtLocLogHistTables);
}

static void initialize_params_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c19_update_jit_animation_state_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c19_do_animation_call_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c19_st;
  const mxArray *c19_y = NULL;
  const mxArray *c19_b_y = NULL;
  uint16_T c19_u;
  const mxArray *c19_c_y = NULL;
  const mxArray *c19_d_y = NULL;
  uint8_T c19_b_u;
  const mxArray *c19_e_y = NULL;
  const mxArray *c19_f_y = NULL;
  c19_st = NULL;
  c19_st = NULL;
  c19_y = NULL;
  sf_mex_assign(&c19_y, sf_mex_createcellmatrix(3, 1), false);
  c19_b_y = NULL;
  c19_u = *chartInstance->c19_cont;
  c19_c_y = NULL;
  sf_mex_assign(&c19_c_y, sf_mex_create("y", &c19_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c19_b_y, sf_mex_create_fi(sf_mex_dup(c19_eml_mx), sf_mex_dup
    (c19_b_eml_mx), "simulinkarray", c19_c_y, false, false), false);
  sf_mex_setcell(c19_y, 0, c19_b_y);
  c19_d_y = NULL;
  c19_b_u = *chartInstance->c19_out_init;
  c19_e_y = NULL;
  sf_mex_assign(&c19_e_y, sf_mex_create("y", &c19_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c19_d_y, sf_mex_create_fi(sf_mex_dup(c19_eml_mx), sf_mex_dup
    (c19_c_eml_mx), "simulinkarray", c19_e_y, false, false), false);
  sf_mex_setcell(c19_y, 1, c19_d_y);
  c19_f_y = NULL;
  sf_mex_assign(&c19_f_y, sf_mex_create("y",
    &chartInstance->c19_is_active_c19_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c19_y, 2, c19_f_y);
  sf_mex_assign(&c19_st, c19_y, false);
  return c19_st;
}

static void set_sim_state_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_st)
{
  emlrtStack c19_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c19_u;
  c19_b_st.tls = chartInstance->c19_fEmlrtCtx;
  chartInstance->c19_doneDoubleBufferReInit = true;
  c19_u = sf_mex_dup(c19_st);
  *chartInstance->c19_cont = c19_emlrt_marshallIn(chartInstance, &c19_b_st,
    sf_mex_dup(sf_mex_getcell(c19_u, 0)), "cont");
  *chartInstance->c19_out_init = c19_c_emlrt_marshallIn(chartInstance, &c19_b_st,
    sf_mex_dup(sf_mex_getcell(c19_u, 1)), "out_init");
  chartInstance->c19_is_active_c19_PWM_28_HalfB = c19_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c19_u, 2)),
     "is_active_c19_PWM_28_HalfB");
  sf_mex_destroy(&c19_u);
  sf_mex_destroy(&c19_st);
}

static void sf_gateway_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c19_PICOffset;
  uint8_T c19_b_enable;
  int16_T c19_b_offset;
  int16_T c19_b_max;
  int16_T c19_b_sum;
  uint8_T c19_b_init;
  uint8_T c19_a0;
  uint8_T c19_a;
  uint8_T c19_b_a0;
  uint8_T c19_a1;
  uint8_T c19_b_a1;
  boolean_T c19_c;
  int8_T c19_i;
  int8_T c19_i1;
  real_T c19_d;
  uint8_T c19_c_a0;
  uint16_T c19_b_cont;
  uint8_T c19_b_a;
  uint8_T c19_b_out_init;
  uint8_T c19_d_a0;
  uint8_T c19_c_a1;
  uint8_T c19_d_a1;
  boolean_T c19_b_c;
  int8_T c19_i2;
  int8_T c19_i3;
  real_T c19_d1;
  int16_T c19_varargin_1;
  int16_T c19_b_varargin_1;
  int16_T c19_c_varargin_1;
  int16_T c19_d_varargin_1;
  int16_T c19_var1;
  int16_T c19_b_var1;
  int16_T c19_i4;
  int16_T c19_i5;
  boolean_T c19_covSaturation;
  boolean_T c19_b_covSaturation;
  uint16_T c19_hfi;
  uint16_T c19_b_hfi;
  uint16_T c19_u;
  uint16_T c19_u1;
  int16_T c19_e_varargin_1;
  int16_T c19_f_varargin_1;
  int16_T c19_g_varargin_1;
  int16_T c19_h_varargin_1;
  int16_T c19_c_var1;
  int16_T c19_d_var1;
  int16_T c19_i6;
  int16_T c19_i7;
  boolean_T c19_c_covSaturation;
  boolean_T c19_d_covSaturation;
  uint16_T c19_c_hfi;
  uint16_T c19_d_hfi;
  uint16_T c19_u2;
  uint16_T c19_u3;
  uint16_T c19_e_a0;
  uint16_T c19_f_a0;
  uint16_T c19_b0;
  uint16_T c19_b_b0;
  uint16_T c19_c_a;
  uint16_T c19_d_a;
  uint16_T c19_b;
  uint16_T c19_b_b;
  uint16_T c19_g_a0;
  uint16_T c19_h_a0;
  uint16_T c19_c_b0;
  uint16_T c19_d_b0;
  uint16_T c19_e_a1;
  uint16_T c19_f_a1;
  uint16_T c19_b1;
  uint16_T c19_b_b1;
  uint16_T c19_g_a1;
  uint16_T c19_h_a1;
  uint16_T c19_c_b1;
  uint16_T c19_d_b1;
  boolean_T c19_c_c;
  boolean_T c19_d_c;
  int16_T c19_i8;
  int16_T c19_i9;
  int16_T c19_i10;
  int16_T c19_i11;
  int16_T c19_i12;
  int16_T c19_i13;
  int16_T c19_i14;
  int16_T c19_i15;
  int16_T c19_i16;
  int16_T c19_i17;
  int16_T c19_i18;
  int16_T c19_i19;
  int32_T c19_i20;
  int32_T c19_i21;
  int16_T c19_i22;
  int16_T c19_i23;
  int16_T c19_i24;
  int16_T c19_i25;
  int16_T c19_i26;
  int16_T c19_i27;
  int16_T c19_i28;
  int16_T c19_i29;
  int16_T c19_i30;
  int16_T c19_i31;
  int16_T c19_i32;
  int16_T c19_i33;
  int32_T c19_i34;
  int32_T c19_i35;
  int16_T c19_i36;
  int16_T c19_i37;
  int16_T c19_i38;
  int16_T c19_i39;
  int16_T c19_i40;
  int16_T c19_i41;
  int16_T c19_i42;
  int16_T c19_i43;
  real_T c19_d2;
  real_T c19_d3;
  int16_T c19_i_varargin_1;
  int16_T c19_i_a0;
  int16_T c19_j_varargin_1;
  int16_T c19_e_b0;
  int16_T c19_e_var1;
  int16_T c19_k_varargin_1;
  int16_T c19_i44;
  int16_T c19_v;
  boolean_T c19_e_covSaturation;
  int16_T c19_val;
  int16_T c19_c_b;
  int32_T c19_i45;
  uint16_T c19_e_hfi;
  real_T c19_d4;
  int32_T c19_i46;
  real_T c19_d5;
  real_T c19_d6;
  real_T c19_d7;
  int32_T c19_i47;
  int32_T c19_i48;
  int32_T c19_i49;
  real_T c19_d8;
  real_T c19_d9;
  int32_T c19_e_c;
  int32_T c19_l_varargin_1;
  int32_T c19_m_varargin_1;
  int32_T c19_f_var1;
  int32_T c19_i50;
  boolean_T c19_f_covSaturation;
  uint16_T c19_f_hfi;
  observerLogReadPIC(&c19_PICOffset);
  chartInstance->c19_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 4U, (real_T)
                    *chartInstance->c19_init);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 3U, (real_T)
                    *chartInstance->c19_sum);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 2U, (real_T)
                    *chartInstance->c19_max);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 1U, (real_T)
                    *chartInstance->c19_offset);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 0U, (real_T)
                    *chartInstance->c19_enable);
  chartInstance->c19_sfEvent = CALL_EVENT;
  c19_b_enable = *chartInstance->c19_enable;
  c19_b_offset = *chartInstance->c19_offset;
  c19_b_max = *chartInstance->c19_max;
  c19_b_sum = *chartInstance->c19_sum;
  c19_b_init = *chartInstance->c19_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c19_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c19_emlrt_update_log_1(chartInstance, c19_b_enable,
    chartInstance->c19_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c19_emlrt_update_log_2(chartInstance, c19_b_offset,
    chartInstance->c19_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c19_emlrt_update_log_3(chartInstance, c19_b_max,
    chartInstance->c19_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c19_emlrt_update_log_3(chartInstance, c19_b_sum,
    chartInstance->c19_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c19_emlrt_update_log_1(chartInstance, c19_b_init,
    chartInstance->c19_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c19_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c19_covrtInstance, 4U, 0, 0, false);
  c19_a0 = c19_b_enable;
  c19_a = c19_a0;
  c19_b_a0 = c19_a;
  c19_a1 = c19_b_a0;
  c19_b_a1 = c19_a1;
  c19_c = (c19_b_a1 == 0);
  c19_i = (int8_T)c19_b_enable;
  if ((int8_T)(c19_i & 2) != 0) {
    c19_i1 = (int8_T)(c19_i | -2);
  } else {
    c19_i1 = (int8_T)(c19_i & 1);
  }

  if (c19_i1 > 0) {
    c19_d = 3.0;
  } else {
    c19_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c19_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c19_covrtInstance,
        4U, 0U, 0U, c19_d, 0.0, -2, 0U, (int32_T)c19_emlrt_update_log_4
        (chartInstance, c19_c, chartInstance->c19_emlrtLocationLoggingDataTables,
         5)))) {
    c19_b_cont = c19_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c19_emlrtLocationLoggingDataTables, 6);
    c19_b_out_init = c19_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c19_emlrtLocationLoggingDataTables, 7);
  } else {
    c19_c_a0 = c19_b_init;
    c19_b_a = c19_c_a0;
    c19_d_a0 = c19_b_a;
    c19_c_a1 = c19_d_a0;
    c19_d_a1 = c19_c_a1;
    c19_b_c = (c19_d_a1 == 0);
    c19_i2 = (int8_T)c19_b_init;
    if ((int8_T)(c19_i2 & 2) != 0) {
      c19_i3 = (int8_T)(c19_i2 | -2);
    } else {
      c19_i3 = (int8_T)(c19_i2 & 1);
    }

    if (c19_i3 > 0) {
      c19_d1 = 3.0;
    } else {
      c19_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c19_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c19_covrtInstance, 4U, 0U, 1U, c19_d1,
                        0.0, -2, 0U, (int32_T)c19_emlrt_update_log_4
                        (chartInstance, c19_b_c,
                         chartInstance->c19_emlrtLocationLoggingDataTables, 8))))
    {
      c19_b_varargin_1 = c19_b_sum;
      c19_d_varargin_1 = c19_b_varargin_1;
      c19_b_var1 = c19_d_varargin_1;
      c19_i5 = c19_b_var1;
      c19_b_covSaturation = false;
      if (c19_i5 < 0) {
        c19_i5 = 0;
      } else {
        if (c19_i5 > 4095) {
          c19_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 0, 0,
          c19_b_covSaturation);
      }

      c19_b_hfi = (uint16_T)c19_i5;
      c19_u1 = c19_emlrt_update_log_5(chartInstance, c19_b_hfi,
        chartInstance->c19_emlrtLocationLoggingDataTables, 10);
      c19_f_varargin_1 = c19_b_max;
      c19_h_varargin_1 = c19_f_varargin_1;
      c19_d_var1 = c19_h_varargin_1;
      c19_i7 = c19_d_var1;
      c19_d_covSaturation = false;
      if (c19_i7 < 0) {
        c19_i7 = 0;
      } else {
        if (c19_i7 > 4095) {
          c19_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 1, 0,
          c19_d_covSaturation);
      }

      c19_d_hfi = (uint16_T)c19_i7;
      c19_u3 = c19_emlrt_update_log_5(chartInstance, c19_d_hfi,
        chartInstance->c19_emlrtLocationLoggingDataTables, 11);
      c19_f_a0 = c19_u1;
      c19_b_b0 = c19_u3;
      c19_d_a = c19_f_a0;
      c19_b_b = c19_b_b0;
      c19_h_a0 = c19_d_a;
      c19_d_b0 = c19_b_b;
      c19_f_a1 = c19_h_a0;
      c19_b_b1 = c19_d_b0;
      c19_h_a1 = c19_f_a1;
      c19_d_b1 = c19_b_b1;
      c19_d_c = (c19_h_a1 < c19_d_b1);
      c19_i9 = (int16_T)c19_u1;
      c19_i11 = (int16_T)c19_u3;
      c19_i13 = (int16_T)c19_u3;
      c19_i15 = (int16_T)c19_u1;
      if ((int16_T)(c19_i13 & 4096) != 0) {
        c19_i17 = (int16_T)(c19_i13 | -4096);
      } else {
        c19_i17 = (int16_T)(c19_i13 & 4095);
      }

      if ((int16_T)(c19_i15 & 4096) != 0) {
        c19_i19 = (int16_T)(c19_i15 | -4096);
      } else {
        c19_i19 = (int16_T)(c19_i15 & 4095);
      }

      c19_i21 = c19_i17 - c19_i19;
      if (c19_i21 > 4095) {
        c19_i21 = 4095;
      } else {
        if (c19_i21 < -4096) {
          c19_i21 = -4096;
        }
      }

      c19_i23 = (int16_T)c19_u1;
      c19_i25 = (int16_T)c19_u3;
      c19_i27 = (int16_T)c19_u1;
      c19_i29 = (int16_T)c19_u3;
      if ((int16_T)(c19_i27 & 4096) != 0) {
        c19_i31 = (int16_T)(c19_i27 | -4096);
      } else {
        c19_i31 = (int16_T)(c19_i27 & 4095);
      }

      if ((int16_T)(c19_i29 & 4096) != 0) {
        c19_i33 = (int16_T)(c19_i29 | -4096);
      } else {
        c19_i33 = (int16_T)(c19_i29 & 4095);
      }

      c19_i35 = c19_i31 - c19_i33;
      if (c19_i35 > 4095) {
        c19_i35 = 4095;
      } else {
        if (c19_i35 < -4096) {
          c19_i35 = -4096;
        }
      }

      if ((int16_T)(c19_i9 & 4096) != 0) {
        c19_i37 = (int16_T)(c19_i9 | -4096);
      } else {
        c19_i37 = (int16_T)(c19_i9 & 4095);
      }

      if ((int16_T)(c19_i11 & 4096) != 0) {
        c19_i39 = (int16_T)(c19_i11 | -4096);
      } else {
        c19_i39 = (int16_T)(c19_i11 & 4095);
      }

      if ((int16_T)(c19_i23 & 4096) != 0) {
        c19_i41 = (int16_T)(c19_i23 | -4096);
      } else {
        c19_i41 = (int16_T)(c19_i23 & 4095);
      }

      if ((int16_T)(c19_i25 & 4096) != 0) {
        c19_i43 = (int16_T)(c19_i25 | -4096);
      } else {
        c19_i43 = (int16_T)(c19_i25 & 4095);
      }

      if (c19_i37 < c19_i39) {
        c19_d3 = (real_T)((int16_T)c19_i21 <= 1);
      } else if (c19_i41 > c19_i43) {
        if ((int16_T)c19_i35 <= 1) {
          c19_d3 = 3.0;
        } else {
          c19_d3 = 0.0;
        }
      } else {
        c19_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c19_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c19_covrtInstance, 4U, 0U, 2U, c19_d3,
                          0.0, -2, 2U, (int32_T)c19_emlrt_update_log_4
                          (chartInstance, c19_d_c,
                           chartInstance->c19_emlrtLocationLoggingDataTables, 9))))
      {
        c19_i_a0 = c19_b_sum;
        c19_e_b0 = c19_b_offset;
        c19_k_varargin_1 = c19_e_b0;
        c19_v = c19_k_varargin_1;
        c19_val = c19_v;
        c19_c_b = c19_val;
        c19_i45 = c19_i_a0;
        if (c19_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d4 = 1.0;
          observerLog(333 + c19_PICOffset, &c19_d4, 1);
        }

        if (c19_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d5 = 1.0;
          observerLog(333 + c19_PICOffset, &c19_d5, 1);
        }

        c19_i46 = c19_c_b;
        if (c19_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d6 = 1.0;
          observerLog(336 + c19_PICOffset, &c19_d6, 1);
        }

        if (c19_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d7 = 1.0;
          observerLog(336 + c19_PICOffset, &c19_d7, 1);
        }

        if ((c19_i45 & 65536) != 0) {
          c19_i47 = c19_i45 | -65536;
        } else {
          c19_i47 = c19_i45 & 65535;
        }

        if ((c19_i46 & 65536) != 0) {
          c19_i48 = c19_i46 | -65536;
        } else {
          c19_i48 = c19_i46 & 65535;
        }

        c19_i49 = c19__s32_add__(chartInstance, c19_i47, c19_i48, 335, 1U, 323,
          12);
        if (c19_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d8 = 1.0;
          observerLog(341 + c19_PICOffset, &c19_d8, 1);
        }

        if (c19_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c19_d9 = 1.0;
          observerLog(341 + c19_PICOffset, &c19_d9, 1);
        }

        if ((c19_i49 & 65536) != 0) {
          c19_e_c = c19_i49 | -65536;
        } else {
          c19_e_c = c19_i49 & 65535;
        }

        c19_l_varargin_1 = c19_emlrt_update_log_6(chartInstance, c19_e_c,
          chartInstance->c19_emlrtLocationLoggingDataTables, 13);
        c19_m_varargin_1 = c19_l_varargin_1;
        c19_f_var1 = c19_m_varargin_1;
        c19_i50 = c19_f_var1;
        c19_f_covSaturation = false;
        if (c19_i50 < 0) {
          c19_i50 = 0;
        } else {
          if (c19_i50 > 4095) {
            c19_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 2, 0,
            c19_f_covSaturation);
        }

        c19_f_hfi = (uint16_T)c19_i50;
        c19_b_cont = c19_emlrt_update_log_5(chartInstance, c19_f_hfi,
          chartInstance->c19_emlrtLocationLoggingDataTables, 12);
        c19_b_out_init = c19_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 14);
      } else {
        c19_b_cont = c19_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 15);
        c19_b_out_init = c19_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c19_varargin_1 = c19_b_sum;
      c19_c_varargin_1 = c19_varargin_1;
      c19_var1 = c19_c_varargin_1;
      c19_i4 = c19_var1;
      c19_covSaturation = false;
      if (c19_i4 < 0) {
        c19_i4 = 0;
      } else {
        if (c19_i4 > 4095) {
          c19_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 3, 0,
          c19_covSaturation);
      }

      c19_hfi = (uint16_T)c19_i4;
      c19_u = c19_emlrt_update_log_5(chartInstance, c19_hfi,
        chartInstance->c19_emlrtLocationLoggingDataTables, 18);
      c19_e_varargin_1 = c19_b_max;
      c19_g_varargin_1 = c19_e_varargin_1;
      c19_c_var1 = c19_g_varargin_1;
      c19_i6 = c19_c_var1;
      c19_c_covSaturation = false;
      if (c19_i6 < 0) {
        c19_i6 = 0;
      } else {
        if (c19_i6 > 4095) {
          c19_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 4, 0,
          c19_c_covSaturation);
      }

      c19_c_hfi = (uint16_T)c19_i6;
      c19_u2 = c19_emlrt_update_log_5(chartInstance, c19_c_hfi,
        chartInstance->c19_emlrtLocationLoggingDataTables, 19);
      c19_e_a0 = c19_u;
      c19_b0 = c19_u2;
      c19_c_a = c19_e_a0;
      c19_b = c19_b0;
      c19_g_a0 = c19_c_a;
      c19_c_b0 = c19_b;
      c19_e_a1 = c19_g_a0;
      c19_b1 = c19_c_b0;
      c19_g_a1 = c19_e_a1;
      c19_c_b1 = c19_b1;
      c19_c_c = (c19_g_a1 < c19_c_b1);
      c19_i8 = (int16_T)c19_u;
      c19_i10 = (int16_T)c19_u2;
      c19_i12 = (int16_T)c19_u2;
      c19_i14 = (int16_T)c19_u;
      if ((int16_T)(c19_i12 & 4096) != 0) {
        c19_i16 = (int16_T)(c19_i12 | -4096);
      } else {
        c19_i16 = (int16_T)(c19_i12 & 4095);
      }

      if ((int16_T)(c19_i14 & 4096) != 0) {
        c19_i18 = (int16_T)(c19_i14 | -4096);
      } else {
        c19_i18 = (int16_T)(c19_i14 & 4095);
      }

      c19_i20 = c19_i16 - c19_i18;
      if (c19_i20 > 4095) {
        c19_i20 = 4095;
      } else {
        if (c19_i20 < -4096) {
          c19_i20 = -4096;
        }
      }

      c19_i22 = (int16_T)c19_u;
      c19_i24 = (int16_T)c19_u2;
      c19_i26 = (int16_T)c19_u;
      c19_i28 = (int16_T)c19_u2;
      if ((int16_T)(c19_i26 & 4096) != 0) {
        c19_i30 = (int16_T)(c19_i26 | -4096);
      } else {
        c19_i30 = (int16_T)(c19_i26 & 4095);
      }

      if ((int16_T)(c19_i28 & 4096) != 0) {
        c19_i32 = (int16_T)(c19_i28 | -4096);
      } else {
        c19_i32 = (int16_T)(c19_i28 & 4095);
      }

      c19_i34 = c19_i30 - c19_i32;
      if (c19_i34 > 4095) {
        c19_i34 = 4095;
      } else {
        if (c19_i34 < -4096) {
          c19_i34 = -4096;
        }
      }

      if ((int16_T)(c19_i8 & 4096) != 0) {
        c19_i36 = (int16_T)(c19_i8 | -4096);
      } else {
        c19_i36 = (int16_T)(c19_i8 & 4095);
      }

      if ((int16_T)(c19_i10 & 4096) != 0) {
        c19_i38 = (int16_T)(c19_i10 | -4096);
      } else {
        c19_i38 = (int16_T)(c19_i10 & 4095);
      }

      if ((int16_T)(c19_i22 & 4096) != 0) {
        c19_i40 = (int16_T)(c19_i22 | -4096);
      } else {
        c19_i40 = (int16_T)(c19_i22 & 4095);
      }

      if ((int16_T)(c19_i24 & 4096) != 0) {
        c19_i42 = (int16_T)(c19_i24 | -4096);
      } else {
        c19_i42 = (int16_T)(c19_i24 & 4095);
      }

      if (c19_i36 < c19_i38) {
        c19_d2 = (real_T)((int16_T)c19_i20 <= 1);
      } else if (c19_i40 > c19_i42) {
        if ((int16_T)c19_i34 <= 1) {
          c19_d2 = 3.0;
        } else {
          c19_d2 = 0.0;
        }
      } else {
        c19_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c19_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c19_covrtInstance, 4U, 0U, 3U, c19_d2,
                          0.0, -2, 2U, (int32_T)c19_emlrt_update_log_4
                          (chartInstance, c19_c_c,
                           chartInstance->c19_emlrtLocationLoggingDataTables, 17))))
      {
        c19_i_varargin_1 = c19_b_sum;
        c19_j_varargin_1 = c19_i_varargin_1;
        c19_e_var1 = c19_j_varargin_1;
        c19_i44 = c19_e_var1;
        c19_e_covSaturation = false;
        if (c19_i44 < 0) {
          c19_i44 = 0;
        } else {
          if (c19_i44 > 4095) {
            c19_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c19_covrtInstance, 4, 0, 5, 0,
            c19_e_covSaturation);
        }

        c19_e_hfi = (uint16_T)c19_i44;
        c19_b_cont = c19_emlrt_update_log_5(chartInstance, c19_e_hfi,
          chartInstance->c19_emlrtLocationLoggingDataTables, 20);
        c19_b_out_init = c19_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 21);
      } else {
        c19_b_cont = c19_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 22);
        c19_b_out_init = c19_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c19_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c19_cont = c19_b_cont;
  *chartInstance->c19_out_init = c19_b_out_init;
  c19_do_animation_call_c19_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 5U, (real_T)
                    *chartInstance->c19_cont);
  covrtSigUpdateFcn(chartInstance->c19_covrtInstance, 6U, (real_T)
                    *chartInstance->c19_out_init);
}

static void mdl_start_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c19_decisionTxtStartIdx = 0U;
  static const uint32_T c19_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c19_chart_data_browse_helper);
  chartInstance->c19_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c19_RuntimeVar,
    &chartInstance->c19_IsDebuggerActive,
    &chartInstance->c19_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c19_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c19_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c19_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c19_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c19_decisionTxtStartIdx, &c19_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c19_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c19_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c19_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c19_PWM_28_HalfB
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c19_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7639",              /* mexFileName */
    "Thu May 27 10:27:25 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c19_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c19_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c19_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c19_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7639");
    emlrtLocationLoggingPushLog(&c19_emlrtLocationLoggingFileInfo,
      c19_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c19_emlrtLocationLoggingDataTables, c19_emlrtLocationInfo,
      NULL, 0U, c19_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7639");
  }

  sfListenerLightTerminate(chartInstance->c19_RuntimeVar);
  sf_mex_destroy(&c19_eml_mx);
  sf_mex_destroy(&c19_b_eml_mx);
  sf_mex_destroy(&c19_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c19_covrtInstance);
}

static void initSimStructsc19_PWM_28_HalfB(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c19_emlrt_update_log_1(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  real_T c19_d;
  uint8_T c19_u;
  uint8_T c19_localMin;
  real_T c19_d1;
  uint8_T c19_u1;
  uint8_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_d = c19_b_table[0U].SimMin;
    if (c19_d < 2.0) {
      if (c19_d >= 0.0) {
        c19_u = (uint8_T)c19_d;
      } else {
        c19_u = 0U;
      }
    } else if (c19_d >= 2.0) {
      c19_u = 1U;
    } else {
      c19_u = 0U;
    }

    c19_localMin = c19_u;
    c19_d1 = c19_b_table[0U].SimMax;
    if (c19_d1 < 2.0) {
      if (c19_d1 >= 0.0) {
        c19_u1 = (uint8_T)c19_d1;
      } else {
        c19_u1 = 0U;
      }
    } else if (c19_d1 >= 2.0) {
      c19_u1 = 1U;
    } else {
      c19_u1 = 0U;
    }

    c19_localMax = c19_u1;
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c19_in < c19_localMin) {
      c19_localMin = c19_in;
    }

    if (c19_in > c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static int16_T c19_emlrt_update_log_2(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  real_T c19_d;
  int16_T c19_i;
  int16_T c19_localMin;
  real_T c19_d1;
  int16_T c19_i1;
  int16_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_d = muDoubleScalarFloor(c19_b_table[0U].SimMin);
    if (c19_d < 32768.0) {
      if (c19_d >= -32768.0) {
        c19_i = (int16_T)c19_d;
      } else {
        c19_i = MIN_int16_T;
      }
    } else if (c19_d >= 32768.0) {
      c19_i = MAX_int16_T;
    } else {
      c19_i = 0;
    }

    c19_localMin = c19_i;
    c19_d1 = muDoubleScalarFloor(c19_b_table[0U].SimMax);
    if (c19_d1 < 32768.0) {
      if (c19_d1 >= -32768.0) {
        c19_i1 = (int16_T)c19_d1;
      } else {
        c19_i1 = MIN_int16_T;
      }
    } else if (c19_d1 >= 32768.0) {
      c19_i1 = MAX_int16_T;
    } else {
      c19_i1 = 0;
    }

    c19_localMax = c19_i1;
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c19_in < c19_localMin) {
      c19_localMin = c19_in;
    }

    if (c19_in > c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static int16_T c19_emlrt_update_log_3(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  real_T c19_d;
  int16_T c19_i;
  int16_T c19_localMin;
  real_T c19_d1;
  int16_T c19_i1;
  int16_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_d = muDoubleScalarFloor(c19_b_table[0U].SimMin);
    if (c19_d < 2048.0) {
      if (c19_d >= -2048.0) {
        c19_i = (int16_T)c19_d;
      } else {
        c19_i = -2048;
      }
    } else if (c19_d >= 2048.0) {
      c19_i = 2047;
    } else {
      c19_i = 0;
    }

    c19_localMin = c19_i;
    c19_d1 = muDoubleScalarFloor(c19_b_table[0U].SimMax);
    if (c19_d1 < 2048.0) {
      if (c19_d1 >= -2048.0) {
        c19_i1 = (int16_T)c19_d1;
      } else {
        c19_i1 = -2048;
      }
    } else if (c19_d1 >= 2048.0) {
      c19_i1 = 2047;
    } else {
      c19_i1 = 0;
    }

    c19_localMax = c19_i1;
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c19_in < c19_localMin) {
      c19_localMin = c19_in;
    }

    if (c19_in > c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static boolean_T c19_emlrt_update_log_4(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  boolean_T c19_localMin;
  boolean_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_localMin = (c19_b_table[0U].SimMin > 0.0);
    c19_localMax = (c19_b_table[0U].SimMax > 0.0);
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c19_in < (int32_T)c19_localMin) {
      c19_localMin = c19_in;
    }

    if ((int32_T)c19_in > (int32_T)c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static uint16_T c19_emlrt_update_log_5(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  real_T c19_d;
  uint16_T c19_u;
  uint16_T c19_localMin;
  real_T c19_d1;
  uint16_T c19_u1;
  uint16_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_d = c19_b_table[0U].SimMin;
    if (c19_d < 4096.0) {
      if (c19_d >= 0.0) {
        c19_u = (uint16_T)c19_d;
      } else {
        c19_u = 0U;
      }
    } else if (c19_d >= 4096.0) {
      c19_u = 4095U;
    } else {
      c19_u = 0U;
    }

    c19_localMin = c19_u;
    c19_d1 = c19_b_table[0U].SimMax;
    if (c19_d1 < 4096.0) {
      if (c19_d1 >= 0.0) {
        c19_u1 = (uint16_T)c19_d1;
      } else {
        c19_u1 = 0U;
      }
    } else if (c19_d1 >= 4096.0) {
      c19_u1 = 4095U;
    } else {
      c19_u1 = 0U;
    }

    c19_localMax = c19_u1;
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c19_in < c19_localMin) {
      c19_localMin = c19_in;
    }

    if (c19_in > c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static int32_T c19_emlrt_update_log_6(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c19_in, emlrtLocationLoggingDataType c19_table[],
  int32_T c19_index)
{
  boolean_T c19_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c19_b_table;
  real_T c19_d;
  int32_T c19_i;
  int32_T c19_localMin;
  real_T c19_d1;
  int32_T c19_i1;
  int32_T c19_localMax;
  emlrtLocationLoggingHistogramType *c19_histTable;
  real_T c19_inDouble;
  real_T c19_significand;
  int32_T c19_exponent;
  (void)chartInstance;
  c19_isLoggingEnabledHere = (c19_index >= 0);
  if (c19_isLoggingEnabledHere) {
    c19_b_table = (emlrtLocationLoggingDataType *)&c19_table[c19_index];
    c19_d = muDoubleScalarFloor(c19_b_table[0U].SimMin);
    if (c19_d < 65536.0) {
      if (c19_d >= -65536.0) {
        c19_i = (int32_T)c19_d;
      } else {
        c19_i = -65536;
      }
    } else if (c19_d >= 65536.0) {
      c19_i = 65535;
    } else {
      c19_i = 0;
    }

    c19_localMin = c19_i;
    c19_d1 = muDoubleScalarFloor(c19_b_table[0U].SimMax);
    if (c19_d1 < 65536.0) {
      if (c19_d1 >= -65536.0) {
        c19_i1 = (int32_T)c19_d1;
      } else {
        c19_i1 = -65536;
      }
    } else if (c19_d1 >= 65536.0) {
      c19_i1 = 65535;
    } else {
      c19_i1 = 0;
    }

    c19_localMax = c19_i1;
    c19_histTable = c19_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c19_in < c19_localMin) {
      c19_localMin = c19_in;
    }

    if (c19_in > c19_localMax) {
      c19_localMax = c19_in;
    }

    /* Histogram logging. */
    c19_inDouble = (real_T)c19_in;
    c19_histTable->TotalNumberOfValues++;
    if (c19_inDouble == 0.0) {
      c19_histTable->NumberOfZeros++;
    } else {
      c19_histTable->SimSum += c19_inDouble;
      if ((!muDoubleScalarIsInf(c19_inDouble)) && (!muDoubleScalarIsNaN
           (c19_inDouble))) {
        c19_significand = frexp(c19_inDouble, &c19_exponent);
        if (c19_exponent > 128) {
          c19_exponent = 128;
        }

        if (c19_exponent < -127) {
          c19_exponent = -127;
        }

        if (c19_significand < 0.0) {
          c19_histTable->NumberOfNegativeValues++;
          c19_histTable->HistogramOfNegativeValues[127 + c19_exponent]++;
        } else {
          c19_histTable->NumberOfPositiveValues++;
          c19_histTable->HistogramOfPositiveValues[127 + c19_exponent]++;
        }
      }
    }

    c19_b_table[0U].SimMin = (real_T)c19_localMin;
    c19_b_table[0U].SimMax = (real_T)c19_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c19_in;
}

static void c19_emlrtInitVarDataTables(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c19_dataTables[24],
  emlrtLocationLoggingHistogramType c19_histTables[24])
{
  int32_T c19_i;
  int32_T c19_iH;
  (void)chartInstance;
  for (c19_i = 0; c19_i < 24; c19_i++) {
    c19_dataTables[c19_i].SimMin = rtInf;
    c19_dataTables[c19_i].SimMax = rtMinusInf;
    c19_dataTables[c19_i].OverflowWraps = 0;
    c19_dataTables[c19_i].Saturations = 0;
    c19_dataTables[c19_i].IsAlwaysInteger = true;
    c19_dataTables[c19_i].HistogramTable = &c19_histTables[c19_i];
    c19_histTables[c19_i].NumberOfZeros = 0.0;
    c19_histTables[c19_i].NumberOfPositiveValues = 0.0;
    c19_histTables[c19_i].NumberOfNegativeValues = 0.0;
    c19_histTables[c19_i].TotalNumberOfValues = 0.0;
    c19_histTables[c19_i].SimSum = 0.0;
    for (c19_iH = 0; c19_iH < 256; c19_iH++) {
      c19_histTables[c19_i].HistogramOfPositiveValues[c19_iH] = 0.0;
      c19_histTables[c19_i].HistogramOfNegativeValues[c19_iH] = 0.0;
    }
  }
}

const mxArray *sf_c19_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c19_nameCaptureInfo = NULL;
  c19_nameCaptureInfo = NULL;
  sf_mex_assign(&c19_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c19_nameCaptureInfo;
}

static uint16_T c19_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_b_cont, const
  char_T *c19_identifier)
{
  uint16_T c19_y;
  emlrtMsgIdentifier c19_thisId;
  c19_thisId.fIdentifier = (const char *)c19_identifier;
  c19_thisId.fParent = NULL;
  c19_thisId.bParentIsCell = false;
  c19_y = c19_b_emlrt_marshallIn(chartInstance, c19_sp, sf_mex_dup(c19_b_cont),
    &c19_thisId);
  sf_mex_destroy(&c19_b_cont);
  return c19_y;
}

static uint16_T c19_b_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_u, const
  emlrtMsgIdentifier *c19_parentId)
{
  uint16_T c19_y;
  const mxArray *c19_mxFi = NULL;
  const mxArray *c19_mxInt = NULL;
  uint16_T c19_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c19_parentId, c19_u, false, 0U, NULL, c19_eml_mx, c19_b_eml_mx);
  sf_mex_assign(&c19_mxFi, sf_mex_dup(c19_u), false);
  sf_mex_assign(&c19_mxInt, sf_mex_call(c19_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c19_mxFi)), false);
  sf_mex_import(c19_parentId, sf_mex_dup(c19_mxInt), &c19_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c19_mxFi);
  sf_mex_destroy(&c19_mxInt);
  c19_y = c19_b_u;
  sf_mex_destroy(&c19_mxFi);
  sf_mex_destroy(&c19_u);
  return c19_y;
}

static uint8_T c19_c_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_b_out_init, const
  char_T *c19_identifier)
{
  uint8_T c19_y;
  emlrtMsgIdentifier c19_thisId;
  c19_thisId.fIdentifier = (const char *)c19_identifier;
  c19_thisId.fParent = NULL;
  c19_thisId.bParentIsCell = false;
  c19_y = c19_d_emlrt_marshallIn(chartInstance, c19_sp, sf_mex_dup
    (c19_b_out_init), &c19_thisId);
  sf_mex_destroy(&c19_b_out_init);
  return c19_y;
}

static uint8_T c19_d_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c19_sp, const mxArray *c19_u, const
  emlrtMsgIdentifier *c19_parentId)
{
  uint8_T c19_y;
  const mxArray *c19_mxFi = NULL;
  const mxArray *c19_mxInt = NULL;
  uint8_T c19_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c19_parentId, c19_u, false, 0U, NULL, c19_eml_mx, c19_c_eml_mx);
  sf_mex_assign(&c19_mxFi, sf_mex_dup(c19_u), false);
  sf_mex_assign(&c19_mxInt, sf_mex_call(c19_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c19_mxFi)), false);
  sf_mex_import(c19_parentId, sf_mex_dup(c19_mxInt), &c19_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c19_mxFi);
  sf_mex_destroy(&c19_mxInt);
  c19_y = c19_b_u;
  sf_mex_destroy(&c19_mxFi);
  sf_mex_destroy(&c19_u);
  return c19_y;
}

static uint8_T c19_e_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_b_is_active_c19_PWM_28_HalfB, const char_T *
  c19_identifier)
{
  uint8_T c19_y;
  emlrtMsgIdentifier c19_thisId;
  c19_thisId.fIdentifier = (const char *)c19_identifier;
  c19_thisId.fParent = NULL;
  c19_thisId.bParentIsCell = false;
  c19_y = c19_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c19_b_is_active_c19_PWM_28_HalfB), &c19_thisId);
  sf_mex_destroy(&c19_b_is_active_c19_PWM_28_HalfB);
  return c19_y;
}

static uint8_T c19_f_emlrt_marshallIn(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c19_u, const emlrtMsgIdentifier *c19_parentId)
{
  uint8_T c19_y;
  uint8_T c19_b_u;
  (void)chartInstance;
  sf_mex_import(c19_parentId, sf_mex_dup(c19_u), &c19_b_u, 1, 3, 0U, 0, 0U, 0);
  c19_y = c19_b_u;
  sf_mex_destroy(&c19_u);
  return c19_y;
}

static const mxArray *c19_chart_data_browse_helper
  (SFc19_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c19_ssIdNumber)
{
  const mxArray *c19_mxData = NULL;
  uint8_T c19_u;
  int16_T c19_i;
  int16_T c19_i1;
  int16_T c19_i2;
  uint16_T c19_u1;
  uint8_T c19_u2;
  uint8_T c19_u3;
  real_T c19_d;
  real_T c19_d1;
  real_T c19_d2;
  real_T c19_d3;
  real_T c19_d4;
  real_T c19_d5;
  c19_mxData = NULL;
  switch (c19_ssIdNumber) {
   case 18U:
    c19_u = *chartInstance->c19_enable;
    c19_d = (real_T)c19_u;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c19_i = *chartInstance->c19_offset;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c19_i1 = *chartInstance->c19_max;
    c19_d1 = (real_T)c19_i1;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c19_i2 = *chartInstance->c19_sum;
    c19_d2 = (real_T)c19_i2;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c19_u1 = *chartInstance->c19_cont;
    c19_d3 = (real_T)c19_u1;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c19_u2 = *chartInstance->c19_init;
    c19_d4 = (real_T)c19_u2;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c19_u3 = *chartInstance->c19_out_init;
    c19_d5 = (real_T)c19_u3;
    sf_mex_assign(&c19_mxData, sf_mex_create("mxData", &c19_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c19_mxData;
}

static int32_T c19__s32_add__(SFc19_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c19_b, int32_T c19_c, int32_T c19_EMLOvCount_src_loc, uint32_T
  c19_ssid_src_loc, int32_T c19_offset_src_loc, int32_T c19_length_src_loc)
{
  int32_T c19_a;
  int32_T c19_PICOffset;
  real_T c19_d;
  observerLogReadPIC(&c19_PICOffset);
  c19_a = c19_b + c19_c;
  if (((c19_a ^ c19_b) & (c19_a ^ c19_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c19_ssid_src_loc,
      c19_offset_src_loc, c19_length_src_loc);
    c19_d = 1.0;
    observerLog(c19_EMLOvCount_src_loc + c19_PICOffset, &c19_d, 1);
  }

  return c19_a;
}

static void init_dsm_address_info(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc19_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c19_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c19_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c19_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c19_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c19_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c19_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c19_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c19_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c19_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c19_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c19_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c19_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c19_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c19_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoWV8QLhvvJFFv"
    "EdiTpoTwlwQAADsQCCW"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c19_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c19_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c19_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c19_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc19_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c19_PWM_28_HalfB
      ((SFc19_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c19_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c19_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc19_PWM_28_HalfB((SFc19_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c19_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5SNDEcLopEBRods2qiIMkSDaxY0qqBUixGjo/O2E8fBIHGs4w8yPbPUcv0Z6",
    "gyy56ge66KHqDAj1C3lCULFMkFUeJkQIdgCJm+L1v3v+MvFqn5+HYxOfNl553Fd/X8Kl707GRzW",
    "sLz3S94X2bzXdQSNi4TxSJtVc5BInhBWjJrWFSdMRQFsKYGIICQRGbSGXK2DSLLWdi3LaCOj79O",
    "mI0CiJpebiHsiQ8EPwU2RJr+sjTZAqoaQOEJlLSjqI2J6O5xsoc+xHQsbZxlQkaTGATp5buWW5Y",
    "wqF1ArQjtCGosT7TLTDEgG9OSs10lupgBpRxwhkRhdZGRAeQoIMNvExC/D2wBo3Kw2hElNmDiEx",
    "Ad9k45ZQC8pxM44cjJoiRihHeirnvBJd163PUpydD4BUOQd32FJBxIpkw5fEP2mhpS5AjDk04sq",
    "NytgDeWhf8VwyOQZX6bejLCSgyggNRumnqkNZJGq15lizDDIvhFVHPKMZPQ1iavZg5OiAYJzhEi",
    "TIYpEZ29KFiE3RvKZuNOy4zV5WMjafB1qtgKVtrAlVRmLO1qfAJ57oUdiiTLkyAp6xNYkg1bMpa",
    "jNOahYcSHezSu7warGAY+AzmSxGywnBNcoC07zzHxnIeSa02MvYxeZvd7vLnZVhHGFBDQqGoCyj",
    "CNKDPUveWs4VMu9gjELUyqXpF4GmGrEJ5emhF81iqMfqkoomcmeAiWgqM9QhjiZXwUmPRVMFcLF",
    "fhKKERhK7BMA49LBvEFvhEu9b2DOtuwsxpEzRVLCmKqjt/7nln58+N9zh/ZnL5990FnloBj7fwd",
    "vjHC/jr9fP4jdy+9dmaG5n87oL8zdx+jZy8w225yvnp919+vvXPF3/98Osfnb/B5O3P61Fb0qPm",
    "zfZPrlzs3N7M5l/PGuQ84SdLeeaw+wt6NQr4v1rg38rm+sF+O+iN32y/fbEN3yvDf4yVeOSnfL/",
    "Vq/W9ktN3tn7HderTJO27WtFOmF0o3JzY6TGbj+fVFf64nq1Px78768l/t5uPY5G/Guf81fCoFG",
    "azJB8vV/+Hu3n5Iv2v5eLt5tKaARPsI9lxZ3c9+en+/RV23M7ZcTu9VwyI61YwoNtPBv3XvcH9x",
    "4N9wod7y33mQ+v1onLeJcv9V/T83y+f3r73OYc3PlCuvua5f1ly69p30fvI54avOs+8HH7rM7Zj",
    "3Xvip8b/6V3sHvdNNn86/4vlR4yHBbft7HMXyLDo6yXY9w5EHqCD",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c19_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c19_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c19_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c19_PWM_28_HalfB(SimStruct *S)
{
  SFc19_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc19_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc19_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc19_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c19_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c19_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c19_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c19_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c19_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c19_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c19_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c19_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c19_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c19_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c19_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c19_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c19_JITStateAnimation,
    chartInstance->c19_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c19_PWM_28_HalfB(chartInstance);
}

void c19_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c19_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c19_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c19_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c19_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
