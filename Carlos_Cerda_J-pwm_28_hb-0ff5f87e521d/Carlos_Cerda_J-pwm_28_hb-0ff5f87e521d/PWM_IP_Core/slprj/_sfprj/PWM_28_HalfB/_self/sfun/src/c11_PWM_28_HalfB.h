#ifndef __c11_PWM_28_HalfB_h__
#define __c11_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc11_PWM_28_HalfBInstanceStruct
#define typedef_SFc11_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c11_sfEvent;
  boolean_T c11_doneDoubleBufferReInit;
  uint8_T c11_is_active_c11_PWM_28_HalfB;
  uint8_T c11_JITStateAnimation[1];
  uint8_T c11_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c11_emlrtLocationLoggingDataTables[24];
  int32_T c11_IsDebuggerActive;
  int32_T c11_IsSequenceViewerPresent;
  int32_T c11_SequenceViewerOptimization;
  void *c11_RuntimeVar;
  emlrtLocationLoggingHistogramType c11_emlrtLocLogHistTables[24];
  boolean_T c11_emlrtLocLogSimulated;
  uint32_T c11_mlFcnLineNumber;
  void *c11_fcnDataPtrs[7];
  char_T *c11_dataNames[7];
  uint32_T c11_numFcnVars;
  uint32_T c11_ssIds[7];
  uint32_T c11_statuses[7];
  void *c11_outMexFcns[7];
  void *c11_inMexFcns[7];
  CovrtStateflowInstance *c11_covrtInstance;
  void *c11_fEmlrtCtx;
  uint8_T *c11_enable;
  int16_T *c11_offset;
  int16_T *c11_max;
  int16_T *c11_sum;
  uint16_T *c11_cont;
  uint8_T *c11_init;
  uint8_T *c11_out_init;
} SFc11_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc11_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c11_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c11_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
