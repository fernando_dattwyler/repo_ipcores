/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c15_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c15_eml_mx;
static const mxArray *c15_b_eml_mx;
static const mxArray *c15_c_eml_mx;

/* Function Declarations */
static void initialize_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c15_update_jit_animation_state_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance);
static void c15_do_animation_call_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_st);
static void sf_gateway_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c15_emlrt_update_log_1(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static int16_T c15_emlrt_update_log_2(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static int16_T c15_emlrt_update_log_3(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static boolean_T c15_emlrt_update_log_4(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static uint16_T c15_emlrt_update_log_5(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static int32_T c15_emlrt_update_log_6(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index);
static void c15_emlrtInitVarDataTables(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c15_dataTables[24],
  emlrtLocationLoggingHistogramType c15_histTables[24]);
static uint16_T c15_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_b_cont, const
  char_T *c15_identifier);
static uint16_T c15_b_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_u, const
  emlrtMsgIdentifier *c15_parentId);
static uint8_T c15_c_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_b_out_init, const
  char_T *c15_identifier);
static uint8_T c15_d_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_u, const
  emlrtMsgIdentifier *c15_parentId);
static uint8_T c15_e_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_b_is_active_c15_PWM_28_HalfB, const char_T *
  c15_identifier);
static uint8_T c15_f_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId);
static const mxArray *c15_chart_data_browse_helper
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c15_ssIdNumber);
static int32_T c15__s32_add__(SFc15_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c15_b, int32_T c15_c, int32_T c15_EMLOvCount_src_loc, uint32_T
  c15_ssid_src_loc, int32_T c15_offset_src_loc, int32_T c15_length_src_loc);
static void init_dsm_address_info(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c15_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c15_st.tls = chartInstance->c15_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c15_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c15_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c15_is_active_c15_PWM_28_HalfB = 0U;
  sf_mex_assign(&c15_c_eml_mx, sf_mex_call(&c15_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c15_b_eml_mx, sf_mex_call(&c15_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c15_eml_mx, sf_mex_call(&c15_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c15_emlrtLocLogSimulated = false;
  c15_emlrtInitVarDataTables(chartInstance,
    chartInstance->c15_emlrtLocationLoggingDataTables,
    chartInstance->c15_emlrtLocLogHistTables);
}

static void initialize_params_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c15_update_jit_animation_state_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c15_do_animation_call_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c15_st;
  const mxArray *c15_y = NULL;
  const mxArray *c15_b_y = NULL;
  uint16_T c15_u;
  const mxArray *c15_c_y = NULL;
  const mxArray *c15_d_y = NULL;
  uint8_T c15_b_u;
  const mxArray *c15_e_y = NULL;
  const mxArray *c15_f_y = NULL;
  c15_st = NULL;
  c15_st = NULL;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_createcellmatrix(3, 1), false);
  c15_b_y = NULL;
  c15_u = *chartInstance->c15_cont;
  c15_c_y = NULL;
  sf_mex_assign(&c15_c_y, sf_mex_create("y", &c15_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c15_b_y, sf_mex_create_fi(sf_mex_dup(c15_eml_mx), sf_mex_dup
    (c15_b_eml_mx), "simulinkarray", c15_c_y, false, false), false);
  sf_mex_setcell(c15_y, 0, c15_b_y);
  c15_d_y = NULL;
  c15_b_u = *chartInstance->c15_out_init;
  c15_e_y = NULL;
  sf_mex_assign(&c15_e_y, sf_mex_create("y", &c15_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c15_d_y, sf_mex_create_fi(sf_mex_dup(c15_eml_mx), sf_mex_dup
    (c15_c_eml_mx), "simulinkarray", c15_e_y, false, false), false);
  sf_mex_setcell(c15_y, 1, c15_d_y);
  c15_f_y = NULL;
  sf_mex_assign(&c15_f_y, sf_mex_create("y",
    &chartInstance->c15_is_active_c15_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c15_y, 2, c15_f_y);
  sf_mex_assign(&c15_st, c15_y, false);
  return c15_st;
}

static void set_sim_state_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_st)
{
  emlrtStack c15_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c15_u;
  c15_b_st.tls = chartInstance->c15_fEmlrtCtx;
  chartInstance->c15_doneDoubleBufferReInit = true;
  c15_u = sf_mex_dup(c15_st);
  *chartInstance->c15_cont = c15_emlrt_marshallIn(chartInstance, &c15_b_st,
    sf_mex_dup(sf_mex_getcell(c15_u, 0)), "cont");
  *chartInstance->c15_out_init = c15_c_emlrt_marshallIn(chartInstance, &c15_b_st,
    sf_mex_dup(sf_mex_getcell(c15_u, 1)), "out_init");
  chartInstance->c15_is_active_c15_PWM_28_HalfB = c15_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c15_u, 2)),
     "is_active_c15_PWM_28_HalfB");
  sf_mex_destroy(&c15_u);
  sf_mex_destroy(&c15_st);
}

static void sf_gateway_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c15_PICOffset;
  uint8_T c15_b_enable;
  int16_T c15_b_offset;
  int16_T c15_b_max;
  int16_T c15_b_sum;
  uint8_T c15_b_init;
  uint8_T c15_a0;
  uint8_T c15_a;
  uint8_T c15_b_a0;
  uint8_T c15_a1;
  uint8_T c15_b_a1;
  boolean_T c15_c;
  int8_T c15_i;
  int8_T c15_i1;
  real_T c15_d;
  uint8_T c15_c_a0;
  uint16_T c15_b_cont;
  uint8_T c15_b_a;
  uint8_T c15_b_out_init;
  uint8_T c15_d_a0;
  uint8_T c15_c_a1;
  uint8_T c15_d_a1;
  boolean_T c15_b_c;
  int8_T c15_i2;
  int8_T c15_i3;
  real_T c15_d1;
  int16_T c15_varargin_1;
  int16_T c15_b_varargin_1;
  int16_T c15_c_varargin_1;
  int16_T c15_d_varargin_1;
  int16_T c15_var1;
  int16_T c15_b_var1;
  int16_T c15_i4;
  int16_T c15_i5;
  boolean_T c15_covSaturation;
  boolean_T c15_b_covSaturation;
  uint16_T c15_hfi;
  uint16_T c15_b_hfi;
  uint16_T c15_u;
  uint16_T c15_u1;
  int16_T c15_e_varargin_1;
  int16_T c15_f_varargin_1;
  int16_T c15_g_varargin_1;
  int16_T c15_h_varargin_1;
  int16_T c15_c_var1;
  int16_T c15_d_var1;
  int16_T c15_i6;
  int16_T c15_i7;
  boolean_T c15_c_covSaturation;
  boolean_T c15_d_covSaturation;
  uint16_T c15_c_hfi;
  uint16_T c15_d_hfi;
  uint16_T c15_u2;
  uint16_T c15_u3;
  uint16_T c15_e_a0;
  uint16_T c15_f_a0;
  uint16_T c15_b0;
  uint16_T c15_b_b0;
  uint16_T c15_c_a;
  uint16_T c15_d_a;
  uint16_T c15_b;
  uint16_T c15_b_b;
  uint16_T c15_g_a0;
  uint16_T c15_h_a0;
  uint16_T c15_c_b0;
  uint16_T c15_d_b0;
  uint16_T c15_e_a1;
  uint16_T c15_f_a1;
  uint16_T c15_b1;
  uint16_T c15_b_b1;
  uint16_T c15_g_a1;
  uint16_T c15_h_a1;
  uint16_T c15_c_b1;
  uint16_T c15_d_b1;
  boolean_T c15_c_c;
  boolean_T c15_d_c;
  int16_T c15_i8;
  int16_T c15_i9;
  int16_T c15_i10;
  int16_T c15_i11;
  int16_T c15_i12;
  int16_T c15_i13;
  int16_T c15_i14;
  int16_T c15_i15;
  int16_T c15_i16;
  int16_T c15_i17;
  int16_T c15_i18;
  int16_T c15_i19;
  int32_T c15_i20;
  int32_T c15_i21;
  int16_T c15_i22;
  int16_T c15_i23;
  int16_T c15_i24;
  int16_T c15_i25;
  int16_T c15_i26;
  int16_T c15_i27;
  int16_T c15_i28;
  int16_T c15_i29;
  int16_T c15_i30;
  int16_T c15_i31;
  int16_T c15_i32;
  int16_T c15_i33;
  int32_T c15_i34;
  int32_T c15_i35;
  int16_T c15_i36;
  int16_T c15_i37;
  int16_T c15_i38;
  int16_T c15_i39;
  int16_T c15_i40;
  int16_T c15_i41;
  int16_T c15_i42;
  int16_T c15_i43;
  real_T c15_d2;
  real_T c15_d3;
  int16_T c15_i_varargin_1;
  int16_T c15_i_a0;
  int16_T c15_j_varargin_1;
  int16_T c15_e_b0;
  int16_T c15_e_var1;
  int16_T c15_k_varargin_1;
  int16_T c15_i44;
  int16_T c15_v;
  boolean_T c15_e_covSaturation;
  int16_T c15_val;
  int16_T c15_c_b;
  int32_T c15_i45;
  uint16_T c15_e_hfi;
  real_T c15_d4;
  int32_T c15_i46;
  real_T c15_d5;
  real_T c15_d6;
  real_T c15_d7;
  int32_T c15_i47;
  int32_T c15_i48;
  int32_T c15_i49;
  real_T c15_d8;
  real_T c15_d9;
  int32_T c15_e_c;
  int32_T c15_l_varargin_1;
  int32_T c15_m_varargin_1;
  int32_T c15_f_var1;
  int32_T c15_i50;
  boolean_T c15_f_covSaturation;
  uint16_T c15_f_hfi;
  observerLogReadPIC(&c15_PICOffset);
  chartInstance->c15_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 4U, (real_T)
                    *chartInstance->c15_init);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 3U, (real_T)
                    *chartInstance->c15_sum);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 2U, (real_T)
                    *chartInstance->c15_max);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 1U, (real_T)
                    *chartInstance->c15_offset);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 0U, (real_T)
                    *chartInstance->c15_enable);
  chartInstance->c15_sfEvent = CALL_EVENT;
  c15_b_enable = *chartInstance->c15_enable;
  c15_b_offset = *chartInstance->c15_offset;
  c15_b_max = *chartInstance->c15_max;
  c15_b_sum = *chartInstance->c15_sum;
  c15_b_init = *chartInstance->c15_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c15_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c15_emlrt_update_log_1(chartInstance, c15_b_enable,
    chartInstance->c15_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c15_emlrt_update_log_2(chartInstance, c15_b_offset,
    chartInstance->c15_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c15_emlrt_update_log_3(chartInstance, c15_b_max,
    chartInstance->c15_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c15_emlrt_update_log_3(chartInstance, c15_b_sum,
    chartInstance->c15_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c15_emlrt_update_log_1(chartInstance, c15_b_init,
    chartInstance->c15_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c15_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c15_covrtInstance, 4U, 0, 0, false);
  c15_a0 = c15_b_enable;
  c15_a = c15_a0;
  c15_b_a0 = c15_a;
  c15_a1 = c15_b_a0;
  c15_b_a1 = c15_a1;
  c15_c = (c15_b_a1 == 0);
  c15_i = (int8_T)c15_b_enable;
  if ((int8_T)(c15_i & 2) != 0) {
    c15_i1 = (int8_T)(c15_i | -2);
  } else {
    c15_i1 = (int8_T)(c15_i & 1);
  }

  if (c15_i1 > 0) {
    c15_d = 3.0;
  } else {
    c15_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c15_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c15_covrtInstance,
        4U, 0U, 0U, c15_d, 0.0, -2, 0U, (int32_T)c15_emlrt_update_log_4
        (chartInstance, c15_c, chartInstance->c15_emlrtLocationLoggingDataTables,
         5)))) {
    c15_b_cont = c15_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c15_emlrtLocationLoggingDataTables, 6);
    c15_b_out_init = c15_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c15_emlrtLocationLoggingDataTables, 7);
  } else {
    c15_c_a0 = c15_b_init;
    c15_b_a = c15_c_a0;
    c15_d_a0 = c15_b_a;
    c15_c_a1 = c15_d_a0;
    c15_d_a1 = c15_c_a1;
    c15_b_c = (c15_d_a1 == 0);
    c15_i2 = (int8_T)c15_b_init;
    if ((int8_T)(c15_i2 & 2) != 0) {
      c15_i3 = (int8_T)(c15_i2 | -2);
    } else {
      c15_i3 = (int8_T)(c15_i2 & 1);
    }

    if (c15_i3 > 0) {
      c15_d1 = 3.0;
    } else {
      c15_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c15_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c15_covrtInstance, 4U, 0U, 1U, c15_d1,
                        0.0, -2, 0U, (int32_T)c15_emlrt_update_log_4
                        (chartInstance, c15_b_c,
                         chartInstance->c15_emlrtLocationLoggingDataTables, 8))))
    {
      c15_b_varargin_1 = c15_b_sum;
      c15_d_varargin_1 = c15_b_varargin_1;
      c15_b_var1 = c15_d_varargin_1;
      c15_i5 = c15_b_var1;
      c15_b_covSaturation = false;
      if (c15_i5 < 0) {
        c15_i5 = 0;
      } else {
        if (c15_i5 > 4095) {
          c15_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 0, 0,
          c15_b_covSaturation);
      }

      c15_b_hfi = (uint16_T)c15_i5;
      c15_u1 = c15_emlrt_update_log_5(chartInstance, c15_b_hfi,
        chartInstance->c15_emlrtLocationLoggingDataTables, 10);
      c15_f_varargin_1 = c15_b_max;
      c15_h_varargin_1 = c15_f_varargin_1;
      c15_d_var1 = c15_h_varargin_1;
      c15_i7 = c15_d_var1;
      c15_d_covSaturation = false;
      if (c15_i7 < 0) {
        c15_i7 = 0;
      } else {
        if (c15_i7 > 4095) {
          c15_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 1, 0,
          c15_d_covSaturation);
      }

      c15_d_hfi = (uint16_T)c15_i7;
      c15_u3 = c15_emlrt_update_log_5(chartInstance, c15_d_hfi,
        chartInstance->c15_emlrtLocationLoggingDataTables, 11);
      c15_f_a0 = c15_u1;
      c15_b_b0 = c15_u3;
      c15_d_a = c15_f_a0;
      c15_b_b = c15_b_b0;
      c15_h_a0 = c15_d_a;
      c15_d_b0 = c15_b_b;
      c15_f_a1 = c15_h_a0;
      c15_b_b1 = c15_d_b0;
      c15_h_a1 = c15_f_a1;
      c15_d_b1 = c15_b_b1;
      c15_d_c = (c15_h_a1 < c15_d_b1);
      c15_i9 = (int16_T)c15_u1;
      c15_i11 = (int16_T)c15_u3;
      c15_i13 = (int16_T)c15_u3;
      c15_i15 = (int16_T)c15_u1;
      if ((int16_T)(c15_i13 & 4096) != 0) {
        c15_i17 = (int16_T)(c15_i13 | -4096);
      } else {
        c15_i17 = (int16_T)(c15_i13 & 4095);
      }

      if ((int16_T)(c15_i15 & 4096) != 0) {
        c15_i19 = (int16_T)(c15_i15 | -4096);
      } else {
        c15_i19 = (int16_T)(c15_i15 & 4095);
      }

      c15_i21 = c15_i17 - c15_i19;
      if (c15_i21 > 4095) {
        c15_i21 = 4095;
      } else {
        if (c15_i21 < -4096) {
          c15_i21 = -4096;
        }
      }

      c15_i23 = (int16_T)c15_u1;
      c15_i25 = (int16_T)c15_u3;
      c15_i27 = (int16_T)c15_u1;
      c15_i29 = (int16_T)c15_u3;
      if ((int16_T)(c15_i27 & 4096) != 0) {
        c15_i31 = (int16_T)(c15_i27 | -4096);
      } else {
        c15_i31 = (int16_T)(c15_i27 & 4095);
      }

      if ((int16_T)(c15_i29 & 4096) != 0) {
        c15_i33 = (int16_T)(c15_i29 | -4096);
      } else {
        c15_i33 = (int16_T)(c15_i29 & 4095);
      }

      c15_i35 = c15_i31 - c15_i33;
      if (c15_i35 > 4095) {
        c15_i35 = 4095;
      } else {
        if (c15_i35 < -4096) {
          c15_i35 = -4096;
        }
      }

      if ((int16_T)(c15_i9 & 4096) != 0) {
        c15_i37 = (int16_T)(c15_i9 | -4096);
      } else {
        c15_i37 = (int16_T)(c15_i9 & 4095);
      }

      if ((int16_T)(c15_i11 & 4096) != 0) {
        c15_i39 = (int16_T)(c15_i11 | -4096);
      } else {
        c15_i39 = (int16_T)(c15_i11 & 4095);
      }

      if ((int16_T)(c15_i23 & 4096) != 0) {
        c15_i41 = (int16_T)(c15_i23 | -4096);
      } else {
        c15_i41 = (int16_T)(c15_i23 & 4095);
      }

      if ((int16_T)(c15_i25 & 4096) != 0) {
        c15_i43 = (int16_T)(c15_i25 | -4096);
      } else {
        c15_i43 = (int16_T)(c15_i25 & 4095);
      }

      if (c15_i37 < c15_i39) {
        c15_d3 = (real_T)((int16_T)c15_i21 <= 1);
      } else if (c15_i41 > c15_i43) {
        if ((int16_T)c15_i35 <= 1) {
          c15_d3 = 3.0;
        } else {
          c15_d3 = 0.0;
        }
      } else {
        c15_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c15_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c15_covrtInstance, 4U, 0U, 2U, c15_d3,
                          0.0, -2, 2U, (int32_T)c15_emlrt_update_log_4
                          (chartInstance, c15_d_c,
                           chartInstance->c15_emlrtLocationLoggingDataTables, 9))))
      {
        c15_i_a0 = c15_b_sum;
        c15_e_b0 = c15_b_offset;
        c15_k_varargin_1 = c15_e_b0;
        c15_v = c15_k_varargin_1;
        c15_val = c15_v;
        c15_c_b = c15_val;
        c15_i45 = c15_i_a0;
        if (c15_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d4 = 1.0;
          observerLog(261 + c15_PICOffset, &c15_d4, 1);
        }

        if (c15_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d5 = 1.0;
          observerLog(261 + c15_PICOffset, &c15_d5, 1);
        }

        c15_i46 = c15_c_b;
        if (c15_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d6 = 1.0;
          observerLog(264 + c15_PICOffset, &c15_d6, 1);
        }

        if (c15_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d7 = 1.0;
          observerLog(264 + c15_PICOffset, &c15_d7, 1);
        }

        if ((c15_i45 & 65536) != 0) {
          c15_i47 = c15_i45 | -65536;
        } else {
          c15_i47 = c15_i45 & 65535;
        }

        if ((c15_i46 & 65536) != 0) {
          c15_i48 = c15_i46 | -65536;
        } else {
          c15_i48 = c15_i46 & 65535;
        }

        c15_i49 = c15__s32_add__(chartInstance, c15_i47, c15_i48, 263, 1U, 323,
          12);
        if (c15_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d8 = 1.0;
          observerLog(269 + c15_PICOffset, &c15_d8, 1);
        }

        if (c15_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c15_d9 = 1.0;
          observerLog(269 + c15_PICOffset, &c15_d9, 1);
        }

        if ((c15_i49 & 65536) != 0) {
          c15_e_c = c15_i49 | -65536;
        } else {
          c15_e_c = c15_i49 & 65535;
        }

        c15_l_varargin_1 = c15_emlrt_update_log_6(chartInstance, c15_e_c,
          chartInstance->c15_emlrtLocationLoggingDataTables, 13);
        c15_m_varargin_1 = c15_l_varargin_1;
        c15_f_var1 = c15_m_varargin_1;
        c15_i50 = c15_f_var1;
        c15_f_covSaturation = false;
        if (c15_i50 < 0) {
          c15_i50 = 0;
        } else {
          if (c15_i50 > 4095) {
            c15_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 2, 0,
            c15_f_covSaturation);
        }

        c15_f_hfi = (uint16_T)c15_i50;
        c15_b_cont = c15_emlrt_update_log_5(chartInstance, c15_f_hfi,
          chartInstance->c15_emlrtLocationLoggingDataTables, 12);
        c15_b_out_init = c15_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 14);
      } else {
        c15_b_cont = c15_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 15);
        c15_b_out_init = c15_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c15_varargin_1 = c15_b_sum;
      c15_c_varargin_1 = c15_varargin_1;
      c15_var1 = c15_c_varargin_1;
      c15_i4 = c15_var1;
      c15_covSaturation = false;
      if (c15_i4 < 0) {
        c15_i4 = 0;
      } else {
        if (c15_i4 > 4095) {
          c15_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 3, 0,
          c15_covSaturation);
      }

      c15_hfi = (uint16_T)c15_i4;
      c15_u = c15_emlrt_update_log_5(chartInstance, c15_hfi,
        chartInstance->c15_emlrtLocationLoggingDataTables, 18);
      c15_e_varargin_1 = c15_b_max;
      c15_g_varargin_1 = c15_e_varargin_1;
      c15_c_var1 = c15_g_varargin_1;
      c15_i6 = c15_c_var1;
      c15_c_covSaturation = false;
      if (c15_i6 < 0) {
        c15_i6 = 0;
      } else {
        if (c15_i6 > 4095) {
          c15_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 4, 0,
          c15_c_covSaturation);
      }

      c15_c_hfi = (uint16_T)c15_i6;
      c15_u2 = c15_emlrt_update_log_5(chartInstance, c15_c_hfi,
        chartInstance->c15_emlrtLocationLoggingDataTables, 19);
      c15_e_a0 = c15_u;
      c15_b0 = c15_u2;
      c15_c_a = c15_e_a0;
      c15_b = c15_b0;
      c15_g_a0 = c15_c_a;
      c15_c_b0 = c15_b;
      c15_e_a1 = c15_g_a0;
      c15_b1 = c15_c_b0;
      c15_g_a1 = c15_e_a1;
      c15_c_b1 = c15_b1;
      c15_c_c = (c15_g_a1 < c15_c_b1);
      c15_i8 = (int16_T)c15_u;
      c15_i10 = (int16_T)c15_u2;
      c15_i12 = (int16_T)c15_u2;
      c15_i14 = (int16_T)c15_u;
      if ((int16_T)(c15_i12 & 4096) != 0) {
        c15_i16 = (int16_T)(c15_i12 | -4096);
      } else {
        c15_i16 = (int16_T)(c15_i12 & 4095);
      }

      if ((int16_T)(c15_i14 & 4096) != 0) {
        c15_i18 = (int16_T)(c15_i14 | -4096);
      } else {
        c15_i18 = (int16_T)(c15_i14 & 4095);
      }

      c15_i20 = c15_i16 - c15_i18;
      if (c15_i20 > 4095) {
        c15_i20 = 4095;
      } else {
        if (c15_i20 < -4096) {
          c15_i20 = -4096;
        }
      }

      c15_i22 = (int16_T)c15_u;
      c15_i24 = (int16_T)c15_u2;
      c15_i26 = (int16_T)c15_u;
      c15_i28 = (int16_T)c15_u2;
      if ((int16_T)(c15_i26 & 4096) != 0) {
        c15_i30 = (int16_T)(c15_i26 | -4096);
      } else {
        c15_i30 = (int16_T)(c15_i26 & 4095);
      }

      if ((int16_T)(c15_i28 & 4096) != 0) {
        c15_i32 = (int16_T)(c15_i28 | -4096);
      } else {
        c15_i32 = (int16_T)(c15_i28 & 4095);
      }

      c15_i34 = c15_i30 - c15_i32;
      if (c15_i34 > 4095) {
        c15_i34 = 4095;
      } else {
        if (c15_i34 < -4096) {
          c15_i34 = -4096;
        }
      }

      if ((int16_T)(c15_i8 & 4096) != 0) {
        c15_i36 = (int16_T)(c15_i8 | -4096);
      } else {
        c15_i36 = (int16_T)(c15_i8 & 4095);
      }

      if ((int16_T)(c15_i10 & 4096) != 0) {
        c15_i38 = (int16_T)(c15_i10 | -4096);
      } else {
        c15_i38 = (int16_T)(c15_i10 & 4095);
      }

      if ((int16_T)(c15_i22 & 4096) != 0) {
        c15_i40 = (int16_T)(c15_i22 | -4096);
      } else {
        c15_i40 = (int16_T)(c15_i22 & 4095);
      }

      if ((int16_T)(c15_i24 & 4096) != 0) {
        c15_i42 = (int16_T)(c15_i24 | -4096);
      } else {
        c15_i42 = (int16_T)(c15_i24 & 4095);
      }

      if (c15_i36 < c15_i38) {
        c15_d2 = (real_T)((int16_T)c15_i20 <= 1);
      } else if (c15_i40 > c15_i42) {
        if ((int16_T)c15_i34 <= 1) {
          c15_d2 = 3.0;
        } else {
          c15_d2 = 0.0;
        }
      } else {
        c15_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c15_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c15_covrtInstance, 4U, 0U, 3U, c15_d2,
                          0.0, -2, 2U, (int32_T)c15_emlrt_update_log_4
                          (chartInstance, c15_c_c,
                           chartInstance->c15_emlrtLocationLoggingDataTables, 17))))
      {
        c15_i_varargin_1 = c15_b_sum;
        c15_j_varargin_1 = c15_i_varargin_1;
        c15_e_var1 = c15_j_varargin_1;
        c15_i44 = c15_e_var1;
        c15_e_covSaturation = false;
        if (c15_i44 < 0) {
          c15_i44 = 0;
        } else {
          if (c15_i44 > 4095) {
            c15_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c15_covrtInstance, 4, 0, 5, 0,
            c15_e_covSaturation);
        }

        c15_e_hfi = (uint16_T)c15_i44;
        c15_b_cont = c15_emlrt_update_log_5(chartInstance, c15_e_hfi,
          chartInstance->c15_emlrtLocationLoggingDataTables, 20);
        c15_b_out_init = c15_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 21);
      } else {
        c15_b_cont = c15_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 22);
        c15_b_out_init = c15_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c15_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c15_cont = c15_b_cont;
  *chartInstance->c15_out_init = c15_b_out_init;
  c15_do_animation_call_c15_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 5U, (real_T)
                    *chartInstance->c15_cont);
  covrtSigUpdateFcn(chartInstance->c15_covrtInstance, 6U, (real_T)
                    *chartInstance->c15_out_init);
}

static void mdl_start_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c15_decisionTxtStartIdx = 0U;
  static const uint32_T c15_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c15_chart_data_browse_helper);
  chartInstance->c15_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c15_RuntimeVar,
    &chartInstance->c15_IsDebuggerActive,
    &chartInstance->c15_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c15_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c15_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c15_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c15_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c15_decisionTxtStartIdx, &c15_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c15_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c15_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c15_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c15_PWM_28_HalfB
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c15_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7551",              /* mexFileName */
    "Thu May 27 10:27:23 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c15_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c15_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c15_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c15_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7551");
    emlrtLocationLoggingPushLog(&c15_emlrtLocationLoggingFileInfo,
      c15_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c15_emlrtLocationLoggingDataTables, c15_emlrtLocationInfo,
      NULL, 0U, c15_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7551");
  }

  sfListenerLightTerminate(chartInstance->c15_RuntimeVar);
  sf_mex_destroy(&c15_eml_mx);
  sf_mex_destroy(&c15_b_eml_mx);
  sf_mex_destroy(&c15_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c15_covrtInstance);
}

static void initSimStructsc15_PWM_28_HalfB(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c15_emlrt_update_log_1(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  real_T c15_d;
  uint8_T c15_u;
  uint8_T c15_localMin;
  real_T c15_d1;
  uint8_T c15_u1;
  uint8_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_d = c15_b_table[0U].SimMin;
    if (c15_d < 2.0) {
      if (c15_d >= 0.0) {
        c15_u = (uint8_T)c15_d;
      } else {
        c15_u = 0U;
      }
    } else if (c15_d >= 2.0) {
      c15_u = 1U;
    } else {
      c15_u = 0U;
    }

    c15_localMin = c15_u;
    c15_d1 = c15_b_table[0U].SimMax;
    if (c15_d1 < 2.0) {
      if (c15_d1 >= 0.0) {
        c15_u1 = (uint8_T)c15_d1;
      } else {
        c15_u1 = 0U;
      }
    } else if (c15_d1 >= 2.0) {
      c15_u1 = 1U;
    } else {
      c15_u1 = 0U;
    }

    c15_localMax = c15_u1;
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c15_in < c15_localMin) {
      c15_localMin = c15_in;
    }

    if (c15_in > c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static int16_T c15_emlrt_update_log_2(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  real_T c15_d;
  int16_T c15_i;
  int16_T c15_localMin;
  real_T c15_d1;
  int16_T c15_i1;
  int16_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_d = muDoubleScalarFloor(c15_b_table[0U].SimMin);
    if (c15_d < 32768.0) {
      if (c15_d >= -32768.0) {
        c15_i = (int16_T)c15_d;
      } else {
        c15_i = MIN_int16_T;
      }
    } else if (c15_d >= 32768.0) {
      c15_i = MAX_int16_T;
    } else {
      c15_i = 0;
    }

    c15_localMin = c15_i;
    c15_d1 = muDoubleScalarFloor(c15_b_table[0U].SimMax);
    if (c15_d1 < 32768.0) {
      if (c15_d1 >= -32768.0) {
        c15_i1 = (int16_T)c15_d1;
      } else {
        c15_i1 = MIN_int16_T;
      }
    } else if (c15_d1 >= 32768.0) {
      c15_i1 = MAX_int16_T;
    } else {
      c15_i1 = 0;
    }

    c15_localMax = c15_i1;
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c15_in < c15_localMin) {
      c15_localMin = c15_in;
    }

    if (c15_in > c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static int16_T c15_emlrt_update_log_3(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  real_T c15_d;
  int16_T c15_i;
  int16_T c15_localMin;
  real_T c15_d1;
  int16_T c15_i1;
  int16_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_d = muDoubleScalarFloor(c15_b_table[0U].SimMin);
    if (c15_d < 2048.0) {
      if (c15_d >= -2048.0) {
        c15_i = (int16_T)c15_d;
      } else {
        c15_i = -2048;
      }
    } else if (c15_d >= 2048.0) {
      c15_i = 2047;
    } else {
      c15_i = 0;
    }

    c15_localMin = c15_i;
    c15_d1 = muDoubleScalarFloor(c15_b_table[0U].SimMax);
    if (c15_d1 < 2048.0) {
      if (c15_d1 >= -2048.0) {
        c15_i1 = (int16_T)c15_d1;
      } else {
        c15_i1 = -2048;
      }
    } else if (c15_d1 >= 2048.0) {
      c15_i1 = 2047;
    } else {
      c15_i1 = 0;
    }

    c15_localMax = c15_i1;
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c15_in < c15_localMin) {
      c15_localMin = c15_in;
    }

    if (c15_in > c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static boolean_T c15_emlrt_update_log_4(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  boolean_T c15_localMin;
  boolean_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_localMin = (c15_b_table[0U].SimMin > 0.0);
    c15_localMax = (c15_b_table[0U].SimMax > 0.0);
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c15_in < (int32_T)c15_localMin) {
      c15_localMin = c15_in;
    }

    if ((int32_T)c15_in > (int32_T)c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static uint16_T c15_emlrt_update_log_5(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  real_T c15_d;
  uint16_T c15_u;
  uint16_T c15_localMin;
  real_T c15_d1;
  uint16_T c15_u1;
  uint16_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_d = c15_b_table[0U].SimMin;
    if (c15_d < 4096.0) {
      if (c15_d >= 0.0) {
        c15_u = (uint16_T)c15_d;
      } else {
        c15_u = 0U;
      }
    } else if (c15_d >= 4096.0) {
      c15_u = 4095U;
    } else {
      c15_u = 0U;
    }

    c15_localMin = c15_u;
    c15_d1 = c15_b_table[0U].SimMax;
    if (c15_d1 < 4096.0) {
      if (c15_d1 >= 0.0) {
        c15_u1 = (uint16_T)c15_d1;
      } else {
        c15_u1 = 0U;
      }
    } else if (c15_d1 >= 4096.0) {
      c15_u1 = 4095U;
    } else {
      c15_u1 = 0U;
    }

    c15_localMax = c15_u1;
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c15_in < c15_localMin) {
      c15_localMin = c15_in;
    }

    if (c15_in > c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static int32_T c15_emlrt_update_log_6(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c15_in, emlrtLocationLoggingDataType c15_table[],
  int32_T c15_index)
{
  boolean_T c15_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c15_b_table;
  real_T c15_d;
  int32_T c15_i;
  int32_T c15_localMin;
  real_T c15_d1;
  int32_T c15_i1;
  int32_T c15_localMax;
  emlrtLocationLoggingHistogramType *c15_histTable;
  real_T c15_inDouble;
  real_T c15_significand;
  int32_T c15_exponent;
  (void)chartInstance;
  c15_isLoggingEnabledHere = (c15_index >= 0);
  if (c15_isLoggingEnabledHere) {
    c15_b_table = (emlrtLocationLoggingDataType *)&c15_table[c15_index];
    c15_d = muDoubleScalarFloor(c15_b_table[0U].SimMin);
    if (c15_d < 65536.0) {
      if (c15_d >= -65536.0) {
        c15_i = (int32_T)c15_d;
      } else {
        c15_i = -65536;
      }
    } else if (c15_d >= 65536.0) {
      c15_i = 65535;
    } else {
      c15_i = 0;
    }

    c15_localMin = c15_i;
    c15_d1 = muDoubleScalarFloor(c15_b_table[0U].SimMax);
    if (c15_d1 < 65536.0) {
      if (c15_d1 >= -65536.0) {
        c15_i1 = (int32_T)c15_d1;
      } else {
        c15_i1 = -65536;
      }
    } else if (c15_d1 >= 65536.0) {
      c15_i1 = 65535;
    } else {
      c15_i1 = 0;
    }

    c15_localMax = c15_i1;
    c15_histTable = c15_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c15_in < c15_localMin) {
      c15_localMin = c15_in;
    }

    if (c15_in > c15_localMax) {
      c15_localMax = c15_in;
    }

    /* Histogram logging. */
    c15_inDouble = (real_T)c15_in;
    c15_histTable->TotalNumberOfValues++;
    if (c15_inDouble == 0.0) {
      c15_histTable->NumberOfZeros++;
    } else {
      c15_histTable->SimSum += c15_inDouble;
      if ((!muDoubleScalarIsInf(c15_inDouble)) && (!muDoubleScalarIsNaN
           (c15_inDouble))) {
        c15_significand = frexp(c15_inDouble, &c15_exponent);
        if (c15_exponent > 128) {
          c15_exponent = 128;
        }

        if (c15_exponent < -127) {
          c15_exponent = -127;
        }

        if (c15_significand < 0.0) {
          c15_histTable->NumberOfNegativeValues++;
          c15_histTable->HistogramOfNegativeValues[127 + c15_exponent]++;
        } else {
          c15_histTable->NumberOfPositiveValues++;
          c15_histTable->HistogramOfPositiveValues[127 + c15_exponent]++;
        }
      }
    }

    c15_b_table[0U].SimMin = (real_T)c15_localMin;
    c15_b_table[0U].SimMax = (real_T)c15_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c15_in;
}

static void c15_emlrtInitVarDataTables(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c15_dataTables[24],
  emlrtLocationLoggingHistogramType c15_histTables[24])
{
  int32_T c15_i;
  int32_T c15_iH;
  (void)chartInstance;
  for (c15_i = 0; c15_i < 24; c15_i++) {
    c15_dataTables[c15_i].SimMin = rtInf;
    c15_dataTables[c15_i].SimMax = rtMinusInf;
    c15_dataTables[c15_i].OverflowWraps = 0;
    c15_dataTables[c15_i].Saturations = 0;
    c15_dataTables[c15_i].IsAlwaysInteger = true;
    c15_dataTables[c15_i].HistogramTable = &c15_histTables[c15_i];
    c15_histTables[c15_i].NumberOfZeros = 0.0;
    c15_histTables[c15_i].NumberOfPositiveValues = 0.0;
    c15_histTables[c15_i].NumberOfNegativeValues = 0.0;
    c15_histTables[c15_i].TotalNumberOfValues = 0.0;
    c15_histTables[c15_i].SimSum = 0.0;
    for (c15_iH = 0; c15_iH < 256; c15_iH++) {
      c15_histTables[c15_i].HistogramOfPositiveValues[c15_iH] = 0.0;
      c15_histTables[c15_i].HistogramOfNegativeValues[c15_iH] = 0.0;
    }
  }
}

const mxArray *sf_c15_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c15_nameCaptureInfo = NULL;
  c15_nameCaptureInfo = NULL;
  sf_mex_assign(&c15_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c15_nameCaptureInfo;
}

static uint16_T c15_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_b_cont, const
  char_T *c15_identifier)
{
  uint16_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = (const char *)c15_identifier;
  c15_thisId.fParent = NULL;
  c15_thisId.bParentIsCell = false;
  c15_y = c15_b_emlrt_marshallIn(chartInstance, c15_sp, sf_mex_dup(c15_b_cont),
    &c15_thisId);
  sf_mex_destroy(&c15_b_cont);
  return c15_y;
}

static uint16_T c15_b_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_u, const
  emlrtMsgIdentifier *c15_parentId)
{
  uint16_T c15_y;
  const mxArray *c15_mxFi = NULL;
  const mxArray *c15_mxInt = NULL;
  uint16_T c15_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c15_parentId, c15_u, false, 0U, NULL, c15_eml_mx, c15_b_eml_mx);
  sf_mex_assign(&c15_mxFi, sf_mex_dup(c15_u), false);
  sf_mex_assign(&c15_mxInt, sf_mex_call(c15_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c15_mxFi)), false);
  sf_mex_import(c15_parentId, sf_mex_dup(c15_mxInt), &c15_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c15_mxFi);
  sf_mex_destroy(&c15_mxInt);
  c15_y = c15_b_u;
  sf_mex_destroy(&c15_mxFi);
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static uint8_T c15_c_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_b_out_init, const
  char_T *c15_identifier)
{
  uint8_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = (const char *)c15_identifier;
  c15_thisId.fParent = NULL;
  c15_thisId.bParentIsCell = false;
  c15_y = c15_d_emlrt_marshallIn(chartInstance, c15_sp, sf_mex_dup
    (c15_b_out_init), &c15_thisId);
  sf_mex_destroy(&c15_b_out_init);
  return c15_y;
}

static uint8_T c15_d_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c15_sp, const mxArray *c15_u, const
  emlrtMsgIdentifier *c15_parentId)
{
  uint8_T c15_y;
  const mxArray *c15_mxFi = NULL;
  const mxArray *c15_mxInt = NULL;
  uint8_T c15_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c15_parentId, c15_u, false, 0U, NULL, c15_eml_mx, c15_c_eml_mx);
  sf_mex_assign(&c15_mxFi, sf_mex_dup(c15_u), false);
  sf_mex_assign(&c15_mxInt, sf_mex_call(c15_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c15_mxFi)), false);
  sf_mex_import(c15_parentId, sf_mex_dup(c15_mxInt), &c15_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c15_mxFi);
  sf_mex_destroy(&c15_mxInt);
  c15_y = c15_b_u;
  sf_mex_destroy(&c15_mxFi);
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static uint8_T c15_e_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_b_is_active_c15_PWM_28_HalfB, const char_T *
  c15_identifier)
{
  uint8_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = (const char *)c15_identifier;
  c15_thisId.fParent = NULL;
  c15_thisId.bParentIsCell = false;
  c15_y = c15_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_b_is_active_c15_PWM_28_HalfB), &c15_thisId);
  sf_mex_destroy(&c15_b_is_active_c15_PWM_28_HalfB);
  return c15_y;
}

static uint8_T c15_f_emlrt_marshallIn(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  uint8_T c15_y;
  uint8_T c15_b_u;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), &c15_b_u, 1, 3, 0U, 0, 0U, 0);
  c15_y = c15_b_u;
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static const mxArray *c15_chart_data_browse_helper
  (SFc15_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c15_ssIdNumber)
{
  const mxArray *c15_mxData = NULL;
  uint8_T c15_u;
  int16_T c15_i;
  int16_T c15_i1;
  int16_T c15_i2;
  uint16_T c15_u1;
  uint8_T c15_u2;
  uint8_T c15_u3;
  real_T c15_d;
  real_T c15_d1;
  real_T c15_d2;
  real_T c15_d3;
  real_T c15_d4;
  real_T c15_d5;
  c15_mxData = NULL;
  switch (c15_ssIdNumber) {
   case 18U:
    c15_u = *chartInstance->c15_enable;
    c15_d = (real_T)c15_u;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c15_i = *chartInstance->c15_offset;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c15_i1 = *chartInstance->c15_max;
    c15_d1 = (real_T)c15_i1;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c15_i2 = *chartInstance->c15_sum;
    c15_d2 = (real_T)c15_i2;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c15_u1 = *chartInstance->c15_cont;
    c15_d3 = (real_T)c15_u1;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c15_u2 = *chartInstance->c15_init;
    c15_d4 = (real_T)c15_u2;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c15_u3 = *chartInstance->c15_out_init;
    c15_d5 = (real_T)c15_u3;
    sf_mex_assign(&c15_mxData, sf_mex_create("mxData", &c15_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c15_mxData;
}

static int32_T c15__s32_add__(SFc15_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c15_b, int32_T c15_c, int32_T c15_EMLOvCount_src_loc, uint32_T
  c15_ssid_src_loc, int32_T c15_offset_src_loc, int32_T c15_length_src_loc)
{
  int32_T c15_a;
  int32_T c15_PICOffset;
  real_T c15_d;
  observerLogReadPIC(&c15_PICOffset);
  c15_a = c15_b + c15_c;
  if (((c15_a ^ c15_b) & (c15_a ^ c15_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c15_ssid_src_loc,
      c15_offset_src_loc, c15_length_src_loc);
    c15_d = 1.0;
    observerLog(c15_EMLOvCount_src_loc + c15_PICOffset, &c15_d, 1);
  }

  return c15_a;
}

static void init_dsm_address_info(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc15_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c15_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c15_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c15_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c15_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c15_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c15_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c15_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c15_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c15_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c15_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c15_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c15_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c15_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c15_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoWl8QLhvvJFFv"
    "EdiTpoTwlwQAADr0CCS"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c15_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c15_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c15_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c15_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc15_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c15_PWM_28_HalfB
      ((SFc15_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c15_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c15_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc15_PWM_28_HalfB((SFc15_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c15_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iNHWcLIJYARodu0qiIsmyCpxTEm1AKlWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4qgxEiADUMQMv/fN+5+RV+v0PBxb+Ly65XnX8X0Dn7o3HRvZvL",
    "bwTNcb3jfZ/AkKCRv3iSKx9iqHIDE8Ay25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "oxGQSQtD/dRloSHgp8hW2JNH3maTAE1bYDQREraUdTmZDTXWJkTPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgXaENgQ11ue6BYYY8M1pqZnOUh3MgDJOOCOi0NqI6AASdLCB50mIv4fWoFF5GI2IMvsQkQn",
    "oLhunnFJAnpNp/HDMBDFSMcJbMfed4LJufY769GQIvMIhqNu+AjJOJBOmPP5BGy1tCXLMoQnHdl",
    "TOFsBr64L/gsEJqFK/DX05AUVGcChKN00d0jpNozXPkmWYYTG8IOopxfhpCEuzFzNHBwTjBEcoU",
    "QaD1MiOPlJsgu4tZbNxx2XmqpKx8TTYehUsZWtNoCoKc7Y2FT7hXJfCjmTShQnwlLVJDKmGTVmL",
    "cVqz8Eiig116l1eDFQwDn8F8KUJWGK5JDpD2nR+xsVxEUquNjH1M3ma3u/x5GdYRBtSQUCjqAoo",
    "wDeiz1L3lbCHTLvYIRK1Mql4ReJohq1CeHlrRPJFqjD6paCLnJriIlgJjPcJYYiU811g0VTAXy1",
    "U4SmgEoWswjEMPywaxBT7RrrU9xbqbMHPWBE0VS4qi6s6f+975+fPlO5w/M7n8+9sFnloBj7fwd",
    "vhHC/jN+kX8Rm7f+mzNjUx+b0H+q9x+jZy8w227ynn7+y8/3/zni79++vWPzt9g8vbn9agt6VHz",
    "Zvsn1y53bm9l8zuzBjlP+MlSnjnswYJejQL+2wv829lcf3/QDnrjV7uvn+3CD8rwN7ESD/2U77d",
    "6tb7XcvrO1u+6Tn2WpH1XK9oJswuFmxM7PWbz8by+wh+b2fp0/PtkPfl7e/k4FvmrccFfDY9KYb",
    "ZK8vFq9X+wl5cv0v9GLt5uLq0ZMMH+Jzvu7q0nP92/v8KOnZwdO+m9YkBct4IB3X0w6L/sDb57N",
    "DggfLi/3Gfet14vK+ddsdynoudnv3x4+97lHN54T7n6muf+Vcmta99l7yMfG77qPPNy+O2P2I51",
    "74kfGv+nd7l73NfZ/PH8L5YfMR4W3Lazz10gw6KvV2DffybuoH8=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c15_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c15_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c15_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c15_PWM_28_HalfB(SimStruct *S)
{
  SFc15_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc15_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc15_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc15_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c15_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c15_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c15_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c15_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c15_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c15_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c15_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c15_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c15_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c15_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c15_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c15_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c15_JITStateAnimation,
    chartInstance->c15_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c15_PWM_28_HalfB(chartInstance);
}

void c15_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c15_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c15_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c15_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c15_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
