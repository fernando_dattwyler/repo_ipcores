/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c23_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c23_eml_mx;
static const mxArray *c23_b_eml_mx;
static const mxArray *c23_c_eml_mx;

/* Function Declarations */
static void initialize_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c23_update_jit_animation_state_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance);
static void c23_do_animation_call_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_st);
static void sf_gateway_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c23_emlrt_update_log_1(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static int16_T c23_emlrt_update_log_2(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static int16_T c23_emlrt_update_log_3(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static boolean_T c23_emlrt_update_log_4(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static uint16_T c23_emlrt_update_log_5(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static int32_T c23_emlrt_update_log_6(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index);
static void c23_emlrtInitVarDataTables(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c23_dataTables[24],
  emlrtLocationLoggingHistogramType c23_histTables[24]);
static uint16_T c23_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_b_cont, const
  char_T *c23_identifier);
static uint16_T c23_b_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_u, const
  emlrtMsgIdentifier *c23_parentId);
static uint8_T c23_c_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_b_out_init, const
  char_T *c23_identifier);
static uint8_T c23_d_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_u, const
  emlrtMsgIdentifier *c23_parentId);
static uint8_T c23_e_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_b_is_active_c23_PWM_28_HalfB, const char_T *
  c23_identifier);
static uint8_T c23_f_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static const mxArray *c23_chart_data_browse_helper
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c23_ssIdNumber);
static int32_T c23__s32_add__(SFc23_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c23_b, int32_T c23_c, int32_T c23_EMLOvCount_src_loc, uint32_T
  c23_ssid_src_loc, int32_T c23_offset_src_loc, int32_T c23_length_src_loc);
static void init_dsm_address_info(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c23_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c23_st.tls = chartInstance->c23_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c23_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c23_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c23_is_active_c23_PWM_28_HalfB = 0U;
  sf_mex_assign(&c23_c_eml_mx, sf_mex_call(&c23_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c23_b_eml_mx, sf_mex_call(&c23_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c23_eml_mx, sf_mex_call(&c23_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c23_emlrtLocLogSimulated = false;
  c23_emlrtInitVarDataTables(chartInstance,
    chartInstance->c23_emlrtLocationLoggingDataTables,
    chartInstance->c23_emlrtLocLogHistTables);
}

static void initialize_params_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c23_update_jit_animation_state_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c23_do_animation_call_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c23_st;
  const mxArray *c23_y = NULL;
  const mxArray *c23_b_y = NULL;
  uint16_T c23_u;
  const mxArray *c23_c_y = NULL;
  const mxArray *c23_d_y = NULL;
  uint8_T c23_b_u;
  const mxArray *c23_e_y = NULL;
  const mxArray *c23_f_y = NULL;
  c23_st = NULL;
  c23_st = NULL;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_createcellmatrix(3, 1), false);
  c23_b_y = NULL;
  c23_u = *chartInstance->c23_cont;
  c23_c_y = NULL;
  sf_mex_assign(&c23_c_y, sf_mex_create("y", &c23_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c23_b_y, sf_mex_create_fi(sf_mex_dup(c23_eml_mx), sf_mex_dup
    (c23_b_eml_mx), "simulinkarray", c23_c_y, false, false), false);
  sf_mex_setcell(c23_y, 0, c23_b_y);
  c23_d_y = NULL;
  c23_b_u = *chartInstance->c23_out_init;
  c23_e_y = NULL;
  sf_mex_assign(&c23_e_y, sf_mex_create("y", &c23_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c23_d_y, sf_mex_create_fi(sf_mex_dup(c23_eml_mx), sf_mex_dup
    (c23_c_eml_mx), "simulinkarray", c23_e_y, false, false), false);
  sf_mex_setcell(c23_y, 1, c23_d_y);
  c23_f_y = NULL;
  sf_mex_assign(&c23_f_y, sf_mex_create("y",
    &chartInstance->c23_is_active_c23_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c23_y, 2, c23_f_y);
  sf_mex_assign(&c23_st, c23_y, false);
  return c23_st;
}

static void set_sim_state_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_st)
{
  emlrtStack c23_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c23_u;
  c23_b_st.tls = chartInstance->c23_fEmlrtCtx;
  chartInstance->c23_doneDoubleBufferReInit = true;
  c23_u = sf_mex_dup(c23_st);
  *chartInstance->c23_cont = c23_emlrt_marshallIn(chartInstance, &c23_b_st,
    sf_mex_dup(sf_mex_getcell(c23_u, 0)), "cont");
  *chartInstance->c23_out_init = c23_c_emlrt_marshallIn(chartInstance, &c23_b_st,
    sf_mex_dup(sf_mex_getcell(c23_u, 1)), "out_init");
  chartInstance->c23_is_active_c23_PWM_28_HalfB = c23_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c23_u, 2)),
     "is_active_c23_PWM_28_HalfB");
  sf_mex_destroy(&c23_u);
  sf_mex_destroy(&c23_st);
}

static void sf_gateway_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c23_PICOffset;
  uint8_T c23_b_enable;
  int16_T c23_b_offset;
  int16_T c23_b_max;
  int16_T c23_b_sum;
  uint8_T c23_b_init;
  uint8_T c23_a0;
  uint8_T c23_a;
  uint8_T c23_b_a0;
  uint8_T c23_a1;
  uint8_T c23_b_a1;
  boolean_T c23_c;
  int8_T c23_i;
  int8_T c23_i1;
  real_T c23_d;
  uint8_T c23_c_a0;
  uint16_T c23_b_cont;
  uint8_T c23_b_a;
  uint8_T c23_b_out_init;
  uint8_T c23_d_a0;
  uint8_T c23_c_a1;
  uint8_T c23_d_a1;
  boolean_T c23_b_c;
  int8_T c23_i2;
  int8_T c23_i3;
  real_T c23_d1;
  int16_T c23_varargin_1;
  int16_T c23_b_varargin_1;
  int16_T c23_c_varargin_1;
  int16_T c23_d_varargin_1;
  int16_T c23_var1;
  int16_T c23_b_var1;
  int16_T c23_i4;
  int16_T c23_i5;
  boolean_T c23_covSaturation;
  boolean_T c23_b_covSaturation;
  uint16_T c23_hfi;
  uint16_T c23_b_hfi;
  uint16_T c23_u;
  uint16_T c23_u1;
  int16_T c23_e_varargin_1;
  int16_T c23_f_varargin_1;
  int16_T c23_g_varargin_1;
  int16_T c23_h_varargin_1;
  int16_T c23_c_var1;
  int16_T c23_d_var1;
  int16_T c23_i6;
  int16_T c23_i7;
  boolean_T c23_c_covSaturation;
  boolean_T c23_d_covSaturation;
  uint16_T c23_c_hfi;
  uint16_T c23_d_hfi;
  uint16_T c23_u2;
  uint16_T c23_u3;
  uint16_T c23_e_a0;
  uint16_T c23_f_a0;
  uint16_T c23_b0;
  uint16_T c23_b_b0;
  uint16_T c23_c_a;
  uint16_T c23_d_a;
  uint16_T c23_b;
  uint16_T c23_b_b;
  uint16_T c23_g_a0;
  uint16_T c23_h_a0;
  uint16_T c23_c_b0;
  uint16_T c23_d_b0;
  uint16_T c23_e_a1;
  uint16_T c23_f_a1;
  uint16_T c23_b1;
  uint16_T c23_b_b1;
  uint16_T c23_g_a1;
  uint16_T c23_h_a1;
  uint16_T c23_c_b1;
  uint16_T c23_d_b1;
  boolean_T c23_c_c;
  boolean_T c23_d_c;
  int16_T c23_i8;
  int16_T c23_i9;
  int16_T c23_i10;
  int16_T c23_i11;
  int16_T c23_i12;
  int16_T c23_i13;
  int16_T c23_i14;
  int16_T c23_i15;
  int16_T c23_i16;
  int16_T c23_i17;
  int16_T c23_i18;
  int16_T c23_i19;
  int32_T c23_i20;
  int32_T c23_i21;
  int16_T c23_i22;
  int16_T c23_i23;
  int16_T c23_i24;
  int16_T c23_i25;
  int16_T c23_i26;
  int16_T c23_i27;
  int16_T c23_i28;
  int16_T c23_i29;
  int16_T c23_i30;
  int16_T c23_i31;
  int16_T c23_i32;
  int16_T c23_i33;
  int32_T c23_i34;
  int32_T c23_i35;
  int16_T c23_i36;
  int16_T c23_i37;
  int16_T c23_i38;
  int16_T c23_i39;
  int16_T c23_i40;
  int16_T c23_i41;
  int16_T c23_i42;
  int16_T c23_i43;
  real_T c23_d2;
  real_T c23_d3;
  int16_T c23_i_varargin_1;
  int16_T c23_i_a0;
  int16_T c23_j_varargin_1;
  int16_T c23_e_b0;
  int16_T c23_e_var1;
  int16_T c23_k_varargin_1;
  int16_T c23_i44;
  int16_T c23_v;
  boolean_T c23_e_covSaturation;
  int16_T c23_val;
  int16_T c23_c_b;
  int32_T c23_i45;
  uint16_T c23_e_hfi;
  real_T c23_d4;
  int32_T c23_i46;
  real_T c23_d5;
  real_T c23_d6;
  real_T c23_d7;
  int32_T c23_i47;
  int32_T c23_i48;
  int32_T c23_i49;
  real_T c23_d8;
  real_T c23_d9;
  int32_T c23_e_c;
  int32_T c23_l_varargin_1;
  int32_T c23_m_varargin_1;
  int32_T c23_f_var1;
  int32_T c23_i50;
  boolean_T c23_f_covSaturation;
  uint16_T c23_f_hfi;
  observerLogReadPIC(&c23_PICOffset);
  chartInstance->c23_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 4U, (real_T)
                    *chartInstance->c23_init);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 3U, (real_T)
                    *chartInstance->c23_sum);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 2U, (real_T)
                    *chartInstance->c23_max);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 1U, (real_T)
                    *chartInstance->c23_offset);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 0U, (real_T)
                    *chartInstance->c23_enable);
  chartInstance->c23_sfEvent = CALL_EVENT;
  c23_b_enable = *chartInstance->c23_enable;
  c23_b_offset = *chartInstance->c23_offset;
  c23_b_max = *chartInstance->c23_max;
  c23_b_sum = *chartInstance->c23_sum;
  c23_b_init = *chartInstance->c23_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c23_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c23_emlrt_update_log_1(chartInstance, c23_b_enable,
    chartInstance->c23_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c23_emlrt_update_log_2(chartInstance, c23_b_offset,
    chartInstance->c23_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c23_emlrt_update_log_3(chartInstance, c23_b_max,
    chartInstance->c23_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c23_emlrt_update_log_3(chartInstance, c23_b_sum,
    chartInstance->c23_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c23_emlrt_update_log_1(chartInstance, c23_b_init,
    chartInstance->c23_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c23_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c23_covrtInstance, 4U, 0, 0, false);
  c23_a0 = c23_b_enable;
  c23_a = c23_a0;
  c23_b_a0 = c23_a;
  c23_a1 = c23_b_a0;
  c23_b_a1 = c23_a1;
  c23_c = (c23_b_a1 == 0);
  c23_i = (int8_T)c23_b_enable;
  if ((int8_T)(c23_i & 2) != 0) {
    c23_i1 = (int8_T)(c23_i | -2);
  } else {
    c23_i1 = (int8_T)(c23_i & 1);
  }

  if (c23_i1 > 0) {
    c23_d = 3.0;
  } else {
    c23_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c23_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c23_covrtInstance,
        4U, 0U, 0U, c23_d, 0.0, -2, 0U, (int32_T)c23_emlrt_update_log_4
        (chartInstance, c23_c, chartInstance->c23_emlrtLocationLoggingDataTables,
         5)))) {
    c23_b_cont = c23_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c23_emlrtLocationLoggingDataTables, 6);
    c23_b_out_init = c23_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c23_emlrtLocationLoggingDataTables, 7);
  } else {
    c23_c_a0 = c23_b_init;
    c23_b_a = c23_c_a0;
    c23_d_a0 = c23_b_a;
    c23_c_a1 = c23_d_a0;
    c23_d_a1 = c23_c_a1;
    c23_b_c = (c23_d_a1 == 0);
    c23_i2 = (int8_T)c23_b_init;
    if ((int8_T)(c23_i2 & 2) != 0) {
      c23_i3 = (int8_T)(c23_i2 | -2);
    } else {
      c23_i3 = (int8_T)(c23_i2 & 1);
    }

    if (c23_i3 > 0) {
      c23_d1 = 3.0;
    } else {
      c23_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c23_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c23_covrtInstance, 4U, 0U, 1U, c23_d1,
                        0.0, -2, 0U, (int32_T)c23_emlrt_update_log_4
                        (chartInstance, c23_b_c,
                         chartInstance->c23_emlrtLocationLoggingDataTables, 8))))
    {
      c23_b_varargin_1 = c23_b_sum;
      c23_d_varargin_1 = c23_b_varargin_1;
      c23_b_var1 = c23_d_varargin_1;
      c23_i5 = c23_b_var1;
      c23_b_covSaturation = false;
      if (c23_i5 < 0) {
        c23_i5 = 0;
      } else {
        if (c23_i5 > 4095) {
          c23_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 0, 0,
          c23_b_covSaturation);
      }

      c23_b_hfi = (uint16_T)c23_i5;
      c23_u1 = c23_emlrt_update_log_5(chartInstance, c23_b_hfi,
        chartInstance->c23_emlrtLocationLoggingDataTables, 10);
      c23_f_varargin_1 = c23_b_max;
      c23_h_varargin_1 = c23_f_varargin_1;
      c23_d_var1 = c23_h_varargin_1;
      c23_i7 = c23_d_var1;
      c23_d_covSaturation = false;
      if (c23_i7 < 0) {
        c23_i7 = 0;
      } else {
        if (c23_i7 > 4095) {
          c23_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 1, 0,
          c23_d_covSaturation);
      }

      c23_d_hfi = (uint16_T)c23_i7;
      c23_u3 = c23_emlrt_update_log_5(chartInstance, c23_d_hfi,
        chartInstance->c23_emlrtLocationLoggingDataTables, 11);
      c23_f_a0 = c23_u1;
      c23_b_b0 = c23_u3;
      c23_d_a = c23_f_a0;
      c23_b_b = c23_b_b0;
      c23_h_a0 = c23_d_a;
      c23_d_b0 = c23_b_b;
      c23_f_a1 = c23_h_a0;
      c23_b_b1 = c23_d_b0;
      c23_h_a1 = c23_f_a1;
      c23_d_b1 = c23_b_b1;
      c23_d_c = (c23_h_a1 < c23_d_b1);
      c23_i9 = (int16_T)c23_u1;
      c23_i11 = (int16_T)c23_u3;
      c23_i13 = (int16_T)c23_u3;
      c23_i15 = (int16_T)c23_u1;
      if ((int16_T)(c23_i13 & 4096) != 0) {
        c23_i17 = (int16_T)(c23_i13 | -4096);
      } else {
        c23_i17 = (int16_T)(c23_i13 & 4095);
      }

      if ((int16_T)(c23_i15 & 4096) != 0) {
        c23_i19 = (int16_T)(c23_i15 | -4096);
      } else {
        c23_i19 = (int16_T)(c23_i15 & 4095);
      }

      c23_i21 = c23_i17 - c23_i19;
      if (c23_i21 > 4095) {
        c23_i21 = 4095;
      } else {
        if (c23_i21 < -4096) {
          c23_i21 = -4096;
        }
      }

      c23_i23 = (int16_T)c23_u1;
      c23_i25 = (int16_T)c23_u3;
      c23_i27 = (int16_T)c23_u1;
      c23_i29 = (int16_T)c23_u3;
      if ((int16_T)(c23_i27 & 4096) != 0) {
        c23_i31 = (int16_T)(c23_i27 | -4096);
      } else {
        c23_i31 = (int16_T)(c23_i27 & 4095);
      }

      if ((int16_T)(c23_i29 & 4096) != 0) {
        c23_i33 = (int16_T)(c23_i29 | -4096);
      } else {
        c23_i33 = (int16_T)(c23_i29 & 4095);
      }

      c23_i35 = c23_i31 - c23_i33;
      if (c23_i35 > 4095) {
        c23_i35 = 4095;
      } else {
        if (c23_i35 < -4096) {
          c23_i35 = -4096;
        }
      }

      if ((int16_T)(c23_i9 & 4096) != 0) {
        c23_i37 = (int16_T)(c23_i9 | -4096);
      } else {
        c23_i37 = (int16_T)(c23_i9 & 4095);
      }

      if ((int16_T)(c23_i11 & 4096) != 0) {
        c23_i39 = (int16_T)(c23_i11 | -4096);
      } else {
        c23_i39 = (int16_T)(c23_i11 & 4095);
      }

      if ((int16_T)(c23_i23 & 4096) != 0) {
        c23_i41 = (int16_T)(c23_i23 | -4096);
      } else {
        c23_i41 = (int16_T)(c23_i23 & 4095);
      }

      if ((int16_T)(c23_i25 & 4096) != 0) {
        c23_i43 = (int16_T)(c23_i25 | -4096);
      } else {
        c23_i43 = (int16_T)(c23_i25 & 4095);
      }

      if (c23_i37 < c23_i39) {
        c23_d3 = (real_T)((int16_T)c23_i21 <= 1);
      } else if (c23_i41 > c23_i43) {
        if ((int16_T)c23_i35 <= 1) {
          c23_d3 = 3.0;
        } else {
          c23_d3 = 0.0;
        }
      } else {
        c23_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c23_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c23_covrtInstance, 4U, 0U, 2U, c23_d3,
                          0.0, -2, 2U, (int32_T)c23_emlrt_update_log_4
                          (chartInstance, c23_d_c,
                           chartInstance->c23_emlrtLocationLoggingDataTables, 9))))
      {
        c23_i_a0 = c23_b_sum;
        c23_e_b0 = c23_b_offset;
        c23_k_varargin_1 = c23_e_b0;
        c23_v = c23_k_varargin_1;
        c23_val = c23_v;
        c23_c_b = c23_val;
        c23_i45 = c23_i_a0;
        if (c23_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d4 = 1.0;
          observerLog(405 + c23_PICOffset, &c23_d4, 1);
        }

        if (c23_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d5 = 1.0;
          observerLog(405 + c23_PICOffset, &c23_d5, 1);
        }

        c23_i46 = c23_c_b;
        if (c23_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d6 = 1.0;
          observerLog(408 + c23_PICOffset, &c23_d6, 1);
        }

        if (c23_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d7 = 1.0;
          observerLog(408 + c23_PICOffset, &c23_d7, 1);
        }

        if ((c23_i45 & 65536) != 0) {
          c23_i47 = c23_i45 | -65536;
        } else {
          c23_i47 = c23_i45 & 65535;
        }

        if ((c23_i46 & 65536) != 0) {
          c23_i48 = c23_i46 | -65536;
        } else {
          c23_i48 = c23_i46 & 65535;
        }

        c23_i49 = c23__s32_add__(chartInstance, c23_i47, c23_i48, 407, 1U, 323,
          12);
        if (c23_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d8 = 1.0;
          observerLog(413 + c23_PICOffset, &c23_d8, 1);
        }

        if (c23_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c23_d9 = 1.0;
          observerLog(413 + c23_PICOffset, &c23_d9, 1);
        }

        if ((c23_i49 & 65536) != 0) {
          c23_e_c = c23_i49 | -65536;
        } else {
          c23_e_c = c23_i49 & 65535;
        }

        c23_l_varargin_1 = c23_emlrt_update_log_6(chartInstance, c23_e_c,
          chartInstance->c23_emlrtLocationLoggingDataTables, 13);
        c23_m_varargin_1 = c23_l_varargin_1;
        c23_f_var1 = c23_m_varargin_1;
        c23_i50 = c23_f_var1;
        c23_f_covSaturation = false;
        if (c23_i50 < 0) {
          c23_i50 = 0;
        } else {
          if (c23_i50 > 4095) {
            c23_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 2, 0,
            c23_f_covSaturation);
        }

        c23_f_hfi = (uint16_T)c23_i50;
        c23_b_cont = c23_emlrt_update_log_5(chartInstance, c23_f_hfi,
          chartInstance->c23_emlrtLocationLoggingDataTables, 12);
        c23_b_out_init = c23_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 14);
      } else {
        c23_b_cont = c23_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 15);
        c23_b_out_init = c23_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c23_varargin_1 = c23_b_sum;
      c23_c_varargin_1 = c23_varargin_1;
      c23_var1 = c23_c_varargin_1;
      c23_i4 = c23_var1;
      c23_covSaturation = false;
      if (c23_i4 < 0) {
        c23_i4 = 0;
      } else {
        if (c23_i4 > 4095) {
          c23_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 3, 0,
          c23_covSaturation);
      }

      c23_hfi = (uint16_T)c23_i4;
      c23_u = c23_emlrt_update_log_5(chartInstance, c23_hfi,
        chartInstance->c23_emlrtLocationLoggingDataTables, 18);
      c23_e_varargin_1 = c23_b_max;
      c23_g_varargin_1 = c23_e_varargin_1;
      c23_c_var1 = c23_g_varargin_1;
      c23_i6 = c23_c_var1;
      c23_c_covSaturation = false;
      if (c23_i6 < 0) {
        c23_i6 = 0;
      } else {
        if (c23_i6 > 4095) {
          c23_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 4, 0,
          c23_c_covSaturation);
      }

      c23_c_hfi = (uint16_T)c23_i6;
      c23_u2 = c23_emlrt_update_log_5(chartInstance, c23_c_hfi,
        chartInstance->c23_emlrtLocationLoggingDataTables, 19);
      c23_e_a0 = c23_u;
      c23_b0 = c23_u2;
      c23_c_a = c23_e_a0;
      c23_b = c23_b0;
      c23_g_a0 = c23_c_a;
      c23_c_b0 = c23_b;
      c23_e_a1 = c23_g_a0;
      c23_b1 = c23_c_b0;
      c23_g_a1 = c23_e_a1;
      c23_c_b1 = c23_b1;
      c23_c_c = (c23_g_a1 < c23_c_b1);
      c23_i8 = (int16_T)c23_u;
      c23_i10 = (int16_T)c23_u2;
      c23_i12 = (int16_T)c23_u2;
      c23_i14 = (int16_T)c23_u;
      if ((int16_T)(c23_i12 & 4096) != 0) {
        c23_i16 = (int16_T)(c23_i12 | -4096);
      } else {
        c23_i16 = (int16_T)(c23_i12 & 4095);
      }

      if ((int16_T)(c23_i14 & 4096) != 0) {
        c23_i18 = (int16_T)(c23_i14 | -4096);
      } else {
        c23_i18 = (int16_T)(c23_i14 & 4095);
      }

      c23_i20 = c23_i16 - c23_i18;
      if (c23_i20 > 4095) {
        c23_i20 = 4095;
      } else {
        if (c23_i20 < -4096) {
          c23_i20 = -4096;
        }
      }

      c23_i22 = (int16_T)c23_u;
      c23_i24 = (int16_T)c23_u2;
      c23_i26 = (int16_T)c23_u;
      c23_i28 = (int16_T)c23_u2;
      if ((int16_T)(c23_i26 & 4096) != 0) {
        c23_i30 = (int16_T)(c23_i26 | -4096);
      } else {
        c23_i30 = (int16_T)(c23_i26 & 4095);
      }

      if ((int16_T)(c23_i28 & 4096) != 0) {
        c23_i32 = (int16_T)(c23_i28 | -4096);
      } else {
        c23_i32 = (int16_T)(c23_i28 & 4095);
      }

      c23_i34 = c23_i30 - c23_i32;
      if (c23_i34 > 4095) {
        c23_i34 = 4095;
      } else {
        if (c23_i34 < -4096) {
          c23_i34 = -4096;
        }
      }

      if ((int16_T)(c23_i8 & 4096) != 0) {
        c23_i36 = (int16_T)(c23_i8 | -4096);
      } else {
        c23_i36 = (int16_T)(c23_i8 & 4095);
      }

      if ((int16_T)(c23_i10 & 4096) != 0) {
        c23_i38 = (int16_T)(c23_i10 | -4096);
      } else {
        c23_i38 = (int16_T)(c23_i10 & 4095);
      }

      if ((int16_T)(c23_i22 & 4096) != 0) {
        c23_i40 = (int16_T)(c23_i22 | -4096);
      } else {
        c23_i40 = (int16_T)(c23_i22 & 4095);
      }

      if ((int16_T)(c23_i24 & 4096) != 0) {
        c23_i42 = (int16_T)(c23_i24 | -4096);
      } else {
        c23_i42 = (int16_T)(c23_i24 & 4095);
      }

      if (c23_i36 < c23_i38) {
        c23_d2 = (real_T)((int16_T)c23_i20 <= 1);
      } else if (c23_i40 > c23_i42) {
        if ((int16_T)c23_i34 <= 1) {
          c23_d2 = 3.0;
        } else {
          c23_d2 = 0.0;
        }
      } else {
        c23_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c23_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c23_covrtInstance, 4U, 0U, 3U, c23_d2,
                          0.0, -2, 2U, (int32_T)c23_emlrt_update_log_4
                          (chartInstance, c23_c_c,
                           chartInstance->c23_emlrtLocationLoggingDataTables, 17))))
      {
        c23_i_varargin_1 = c23_b_sum;
        c23_j_varargin_1 = c23_i_varargin_1;
        c23_e_var1 = c23_j_varargin_1;
        c23_i44 = c23_e_var1;
        c23_e_covSaturation = false;
        if (c23_i44 < 0) {
          c23_i44 = 0;
        } else {
          if (c23_i44 > 4095) {
            c23_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c23_covrtInstance, 4, 0, 5, 0,
            c23_e_covSaturation);
        }

        c23_e_hfi = (uint16_T)c23_i44;
        c23_b_cont = c23_emlrt_update_log_5(chartInstance, c23_e_hfi,
          chartInstance->c23_emlrtLocationLoggingDataTables, 20);
        c23_b_out_init = c23_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 21);
      } else {
        c23_b_cont = c23_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 22);
        c23_b_out_init = c23_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c23_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c23_cont = c23_b_cont;
  *chartInstance->c23_out_init = c23_b_out_init;
  c23_do_animation_call_c23_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 5U, (real_T)
                    *chartInstance->c23_cont);
  covrtSigUpdateFcn(chartInstance->c23_covrtInstance, 6U, (real_T)
                    *chartInstance->c23_out_init);
}

static void mdl_start_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c23_decisionTxtStartIdx = 0U;
  static const uint32_T c23_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c23_chart_data_browse_helper);
  chartInstance->c23_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c23_RuntimeVar,
    &chartInstance->c23_IsDebuggerActive,
    &chartInstance->c23_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c23_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c23_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c23_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c23_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c23_decisionTxtStartIdx, &c23_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c23_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c23_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c23_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c23_PWM_28_HalfB
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c23_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7727",              /* mexFileName */
    "Thu May 27 10:27:28 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c23_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c23_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c23_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c23_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7727");
    emlrtLocationLoggingPushLog(&c23_emlrtLocationLoggingFileInfo,
      c23_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c23_emlrtLocationLoggingDataTables, c23_emlrtLocationInfo,
      NULL, 0U, c23_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7727");
  }

  sfListenerLightTerminate(chartInstance->c23_RuntimeVar);
  sf_mex_destroy(&c23_eml_mx);
  sf_mex_destroy(&c23_b_eml_mx);
  sf_mex_destroy(&c23_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c23_covrtInstance);
}

static void initSimStructsc23_PWM_28_HalfB(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c23_emlrt_update_log_1(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  real_T c23_d;
  uint8_T c23_u;
  uint8_T c23_localMin;
  real_T c23_d1;
  uint8_T c23_u1;
  uint8_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_d = c23_b_table[0U].SimMin;
    if (c23_d < 2.0) {
      if (c23_d >= 0.0) {
        c23_u = (uint8_T)c23_d;
      } else {
        c23_u = 0U;
      }
    } else if (c23_d >= 2.0) {
      c23_u = 1U;
    } else {
      c23_u = 0U;
    }

    c23_localMin = c23_u;
    c23_d1 = c23_b_table[0U].SimMax;
    if (c23_d1 < 2.0) {
      if (c23_d1 >= 0.0) {
        c23_u1 = (uint8_T)c23_d1;
      } else {
        c23_u1 = 0U;
      }
    } else if (c23_d1 >= 2.0) {
      c23_u1 = 1U;
    } else {
      c23_u1 = 0U;
    }

    c23_localMax = c23_u1;
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c23_in < c23_localMin) {
      c23_localMin = c23_in;
    }

    if (c23_in > c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static int16_T c23_emlrt_update_log_2(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  real_T c23_d;
  int16_T c23_i;
  int16_T c23_localMin;
  real_T c23_d1;
  int16_T c23_i1;
  int16_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_d = muDoubleScalarFloor(c23_b_table[0U].SimMin);
    if (c23_d < 32768.0) {
      if (c23_d >= -32768.0) {
        c23_i = (int16_T)c23_d;
      } else {
        c23_i = MIN_int16_T;
      }
    } else if (c23_d >= 32768.0) {
      c23_i = MAX_int16_T;
    } else {
      c23_i = 0;
    }

    c23_localMin = c23_i;
    c23_d1 = muDoubleScalarFloor(c23_b_table[0U].SimMax);
    if (c23_d1 < 32768.0) {
      if (c23_d1 >= -32768.0) {
        c23_i1 = (int16_T)c23_d1;
      } else {
        c23_i1 = MIN_int16_T;
      }
    } else if (c23_d1 >= 32768.0) {
      c23_i1 = MAX_int16_T;
    } else {
      c23_i1 = 0;
    }

    c23_localMax = c23_i1;
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c23_in < c23_localMin) {
      c23_localMin = c23_in;
    }

    if (c23_in > c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static int16_T c23_emlrt_update_log_3(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  real_T c23_d;
  int16_T c23_i;
  int16_T c23_localMin;
  real_T c23_d1;
  int16_T c23_i1;
  int16_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_d = muDoubleScalarFloor(c23_b_table[0U].SimMin);
    if (c23_d < 2048.0) {
      if (c23_d >= -2048.0) {
        c23_i = (int16_T)c23_d;
      } else {
        c23_i = -2048;
      }
    } else if (c23_d >= 2048.0) {
      c23_i = 2047;
    } else {
      c23_i = 0;
    }

    c23_localMin = c23_i;
    c23_d1 = muDoubleScalarFloor(c23_b_table[0U].SimMax);
    if (c23_d1 < 2048.0) {
      if (c23_d1 >= -2048.0) {
        c23_i1 = (int16_T)c23_d1;
      } else {
        c23_i1 = -2048;
      }
    } else if (c23_d1 >= 2048.0) {
      c23_i1 = 2047;
    } else {
      c23_i1 = 0;
    }

    c23_localMax = c23_i1;
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c23_in < c23_localMin) {
      c23_localMin = c23_in;
    }

    if (c23_in > c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static boolean_T c23_emlrt_update_log_4(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  boolean_T c23_localMin;
  boolean_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_localMin = (c23_b_table[0U].SimMin > 0.0);
    c23_localMax = (c23_b_table[0U].SimMax > 0.0);
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c23_in < (int32_T)c23_localMin) {
      c23_localMin = c23_in;
    }

    if ((int32_T)c23_in > (int32_T)c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static uint16_T c23_emlrt_update_log_5(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  real_T c23_d;
  uint16_T c23_u;
  uint16_T c23_localMin;
  real_T c23_d1;
  uint16_T c23_u1;
  uint16_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_d = c23_b_table[0U].SimMin;
    if (c23_d < 4096.0) {
      if (c23_d >= 0.0) {
        c23_u = (uint16_T)c23_d;
      } else {
        c23_u = 0U;
      }
    } else if (c23_d >= 4096.0) {
      c23_u = 4095U;
    } else {
      c23_u = 0U;
    }

    c23_localMin = c23_u;
    c23_d1 = c23_b_table[0U].SimMax;
    if (c23_d1 < 4096.0) {
      if (c23_d1 >= 0.0) {
        c23_u1 = (uint16_T)c23_d1;
      } else {
        c23_u1 = 0U;
      }
    } else if (c23_d1 >= 4096.0) {
      c23_u1 = 4095U;
    } else {
      c23_u1 = 0U;
    }

    c23_localMax = c23_u1;
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c23_in < c23_localMin) {
      c23_localMin = c23_in;
    }

    if (c23_in > c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static int32_T c23_emlrt_update_log_6(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c23_in, emlrtLocationLoggingDataType c23_table[],
  int32_T c23_index)
{
  boolean_T c23_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c23_b_table;
  real_T c23_d;
  int32_T c23_i;
  int32_T c23_localMin;
  real_T c23_d1;
  int32_T c23_i1;
  int32_T c23_localMax;
  emlrtLocationLoggingHistogramType *c23_histTable;
  real_T c23_inDouble;
  real_T c23_significand;
  int32_T c23_exponent;
  (void)chartInstance;
  c23_isLoggingEnabledHere = (c23_index >= 0);
  if (c23_isLoggingEnabledHere) {
    c23_b_table = (emlrtLocationLoggingDataType *)&c23_table[c23_index];
    c23_d = muDoubleScalarFloor(c23_b_table[0U].SimMin);
    if (c23_d < 65536.0) {
      if (c23_d >= -65536.0) {
        c23_i = (int32_T)c23_d;
      } else {
        c23_i = -65536;
      }
    } else if (c23_d >= 65536.0) {
      c23_i = 65535;
    } else {
      c23_i = 0;
    }

    c23_localMin = c23_i;
    c23_d1 = muDoubleScalarFloor(c23_b_table[0U].SimMax);
    if (c23_d1 < 65536.0) {
      if (c23_d1 >= -65536.0) {
        c23_i1 = (int32_T)c23_d1;
      } else {
        c23_i1 = -65536;
      }
    } else if (c23_d1 >= 65536.0) {
      c23_i1 = 65535;
    } else {
      c23_i1 = 0;
    }

    c23_localMax = c23_i1;
    c23_histTable = c23_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c23_in < c23_localMin) {
      c23_localMin = c23_in;
    }

    if (c23_in > c23_localMax) {
      c23_localMax = c23_in;
    }

    /* Histogram logging. */
    c23_inDouble = (real_T)c23_in;
    c23_histTable->TotalNumberOfValues++;
    if (c23_inDouble == 0.0) {
      c23_histTable->NumberOfZeros++;
    } else {
      c23_histTable->SimSum += c23_inDouble;
      if ((!muDoubleScalarIsInf(c23_inDouble)) && (!muDoubleScalarIsNaN
           (c23_inDouble))) {
        c23_significand = frexp(c23_inDouble, &c23_exponent);
        if (c23_exponent > 128) {
          c23_exponent = 128;
        }

        if (c23_exponent < -127) {
          c23_exponent = -127;
        }

        if (c23_significand < 0.0) {
          c23_histTable->NumberOfNegativeValues++;
          c23_histTable->HistogramOfNegativeValues[127 + c23_exponent]++;
        } else {
          c23_histTable->NumberOfPositiveValues++;
          c23_histTable->HistogramOfPositiveValues[127 + c23_exponent]++;
        }
      }
    }

    c23_b_table[0U].SimMin = (real_T)c23_localMin;
    c23_b_table[0U].SimMax = (real_T)c23_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c23_in;
}

static void c23_emlrtInitVarDataTables(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c23_dataTables[24],
  emlrtLocationLoggingHistogramType c23_histTables[24])
{
  int32_T c23_i;
  int32_T c23_iH;
  (void)chartInstance;
  for (c23_i = 0; c23_i < 24; c23_i++) {
    c23_dataTables[c23_i].SimMin = rtInf;
    c23_dataTables[c23_i].SimMax = rtMinusInf;
    c23_dataTables[c23_i].OverflowWraps = 0;
    c23_dataTables[c23_i].Saturations = 0;
    c23_dataTables[c23_i].IsAlwaysInteger = true;
    c23_dataTables[c23_i].HistogramTable = &c23_histTables[c23_i];
    c23_histTables[c23_i].NumberOfZeros = 0.0;
    c23_histTables[c23_i].NumberOfPositiveValues = 0.0;
    c23_histTables[c23_i].NumberOfNegativeValues = 0.0;
    c23_histTables[c23_i].TotalNumberOfValues = 0.0;
    c23_histTables[c23_i].SimSum = 0.0;
    for (c23_iH = 0; c23_iH < 256; c23_iH++) {
      c23_histTables[c23_i].HistogramOfPositiveValues[c23_iH] = 0.0;
      c23_histTables[c23_i].HistogramOfNegativeValues[c23_iH] = 0.0;
    }
  }
}

const mxArray *sf_c23_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c23_nameCaptureInfo = NULL;
  c23_nameCaptureInfo = NULL;
  sf_mex_assign(&c23_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c23_nameCaptureInfo;
}

static uint16_T c23_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_b_cont, const
  char_T *c23_identifier)
{
  uint16_T c23_y;
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = (const char *)c23_identifier;
  c23_thisId.fParent = NULL;
  c23_thisId.bParentIsCell = false;
  c23_y = c23_b_emlrt_marshallIn(chartInstance, c23_sp, sf_mex_dup(c23_b_cont),
    &c23_thisId);
  sf_mex_destroy(&c23_b_cont);
  return c23_y;
}

static uint16_T c23_b_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_u, const
  emlrtMsgIdentifier *c23_parentId)
{
  uint16_T c23_y;
  const mxArray *c23_mxFi = NULL;
  const mxArray *c23_mxInt = NULL;
  uint16_T c23_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c23_parentId, c23_u, false, 0U, NULL, c23_eml_mx, c23_b_eml_mx);
  sf_mex_assign(&c23_mxFi, sf_mex_dup(c23_u), false);
  sf_mex_assign(&c23_mxInt, sf_mex_call(c23_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c23_mxFi)), false);
  sf_mex_import(c23_parentId, sf_mex_dup(c23_mxInt), &c23_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c23_mxFi);
  sf_mex_destroy(&c23_mxInt);
  c23_y = c23_b_u;
  sf_mex_destroy(&c23_mxFi);
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static uint8_T c23_c_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_b_out_init, const
  char_T *c23_identifier)
{
  uint8_T c23_y;
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = (const char *)c23_identifier;
  c23_thisId.fParent = NULL;
  c23_thisId.bParentIsCell = false;
  c23_y = c23_d_emlrt_marshallIn(chartInstance, c23_sp, sf_mex_dup
    (c23_b_out_init), &c23_thisId);
  sf_mex_destroy(&c23_b_out_init);
  return c23_y;
}

static uint8_T c23_d_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c23_sp, const mxArray *c23_u, const
  emlrtMsgIdentifier *c23_parentId)
{
  uint8_T c23_y;
  const mxArray *c23_mxFi = NULL;
  const mxArray *c23_mxInt = NULL;
  uint8_T c23_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c23_parentId, c23_u, false, 0U, NULL, c23_eml_mx, c23_c_eml_mx);
  sf_mex_assign(&c23_mxFi, sf_mex_dup(c23_u), false);
  sf_mex_assign(&c23_mxInt, sf_mex_call(c23_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c23_mxFi)), false);
  sf_mex_import(c23_parentId, sf_mex_dup(c23_mxInt), &c23_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c23_mxFi);
  sf_mex_destroy(&c23_mxInt);
  c23_y = c23_b_u;
  sf_mex_destroy(&c23_mxFi);
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static uint8_T c23_e_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_b_is_active_c23_PWM_28_HalfB, const char_T *
  c23_identifier)
{
  uint8_T c23_y;
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = (const char *)c23_identifier;
  c23_thisId.fParent = NULL;
  c23_thisId.bParentIsCell = false;
  c23_y = c23_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c23_b_is_active_c23_PWM_28_HalfB), &c23_thisId);
  sf_mex_destroy(&c23_b_is_active_c23_PWM_28_HalfB);
  return c23_y;
}

static uint8_T c23_f_emlrt_marshallIn(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  uint8_T c23_y;
  uint8_T c23_b_u;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_b_u, 1, 3, 0U, 0, 0U, 0);
  c23_y = c23_b_u;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static const mxArray *c23_chart_data_browse_helper
  (SFc23_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c23_ssIdNumber)
{
  const mxArray *c23_mxData = NULL;
  uint8_T c23_u;
  int16_T c23_i;
  int16_T c23_i1;
  int16_T c23_i2;
  uint16_T c23_u1;
  uint8_T c23_u2;
  uint8_T c23_u3;
  real_T c23_d;
  real_T c23_d1;
  real_T c23_d2;
  real_T c23_d3;
  real_T c23_d4;
  real_T c23_d5;
  c23_mxData = NULL;
  switch (c23_ssIdNumber) {
   case 18U:
    c23_u = *chartInstance->c23_enable;
    c23_d = (real_T)c23_u;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c23_i = *chartInstance->c23_offset;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c23_i1 = *chartInstance->c23_max;
    c23_d1 = (real_T)c23_i1;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c23_i2 = *chartInstance->c23_sum;
    c23_d2 = (real_T)c23_i2;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c23_u1 = *chartInstance->c23_cont;
    c23_d3 = (real_T)c23_u1;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c23_u2 = *chartInstance->c23_init;
    c23_d4 = (real_T)c23_u2;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c23_u3 = *chartInstance->c23_out_init;
    c23_d5 = (real_T)c23_u3;
    sf_mex_assign(&c23_mxData, sf_mex_create("mxData", &c23_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c23_mxData;
}

static int32_T c23__s32_add__(SFc23_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c23_b, int32_T c23_c, int32_T c23_EMLOvCount_src_loc, uint32_T
  c23_ssid_src_loc, int32_T c23_offset_src_loc, int32_T c23_length_src_loc)
{
  int32_T c23_a;
  int32_T c23_PICOffset;
  real_T c23_d;
  observerLogReadPIC(&c23_PICOffset);
  c23_a = c23_b + c23_c;
  if (((c23_a ^ c23_b) & (c23_a ^ c23_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c23_ssid_src_loc,
      c23_offset_src_loc, c23_length_src_loc);
    c23_d = 1.0;
    observerLog(c23_EMLOvCount_src_loc + c23_PICOffset, &c23_d, 1);
  }

  return c23_a;
}

static void init_dsm_address_info(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc23_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c23_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c23_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c23_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c23_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c23_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c23_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c23_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c23_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c23_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c23_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c23_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c23_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c23_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c23_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyykXF8QLhvvJFFv"
    "EdiTpoTwlwQAADrtSCR"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c23_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c23_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c23_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c23_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc23_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c23_PWM_28_HalfB
      ((SFc23_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c23_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c23_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc23_PWM_28_HalfB((SFc23_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c23_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5cNDUcLspEARIds2qaJKmyCqxTUm1AClWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfIG4qSZYqk4igxUiADUMQMv/fN+5+RV+v0PBxb+Lz82vOu4nsTn7o3HRvZvL",
    "bwTNcb3rfZ/DEKCRv3iSKx9iqHIDE8BS25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "IxGQSQtD/dQloQHgp8iW2JNH3maTAE1bYDQREraUdTmZDTXWJljPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgHaENgQ11me6BYYY8M1JqZnOUh3MgDJOOCOi0NqI6AASdLCBZ0mIvwfWoFF5GI2IMnsQkQn",
    "oLhunnFJAnpNp/HDEBDFSMcJbMfed4LJufY769GQIvMIhqNueAjJOJBOmPP5BGy1tCXLEoQlHdl",
    "TOFsAr64L/nMExqFK/DX05AUVGcCBKN00d0jpJozXPkmWYYTE8J2qXYvw0hKXZi5mjA4JxgkOUK",
    "INBamRHHyo2QfeWstm44zJzVcnYeBpsvQqWsrUmUBWFOVubCp9wrkthhzLpwgR4ytokhlTDpqzF",
    "OK1ZeCjRwS69y6vBCoaBz2C+FCErDNckB0j7zhNsLOeR1GojYx+Tt9ntLn9ehnWEATUkFIq6gCJ",
    "MA/osdW85W8i0iz0CUSuTqlcEnmbIKpSnh1Y0j6Uao08qmsiZCS6ipcBYjzCWWAnPNBZNFczFch",
    "WOEhpB6BoM49DDskFsgU+0a227WHcTZk6boKliSVFU3fnzvXd2/lx/h/NnJpd/31ngqRXweAtvh",
    "3+4gL9WP4/fyO1bn625kcnvLMh/mduvkZN3uG1XOW/++PWXr/794u+ff/uz8w+YvP15PWpLetS8",
    "2f7JlYud21vZ/OasQc4TfrKUZw67v6BXo4D/mwX+7Wyuf9hvB73xy7uvnt6Fn5Thr2MlfvRTvt/",
    "r1fpeyek7W7/tOvVpkvZdrWgnzC4Ubk7s9JjNx/PqCn9cy9an47/H68l/t5OPY5G/Guf81fCoFG",
    "arJB8vV/8HO3n5Iv03c/F2c2nNgAn2gey4vbOe/HT//go7buTsuJHeKwbEdSsY0Hv3B/0XvcG9h",
    "4N9wod7y33mfev1onLeJcv9X/T87JePb9+7nMMb7ylXX/Pcvyy5de276H3kU8NXnWdeDr/9Cdux",
    "7j3xY+P/8i52j7uVzR/N/2L5EeNhwW07+9wFMiz6egn2vQUfo6B+",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c23_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c23_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c23_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c23_PWM_28_HalfB(SimStruct *S)
{
  SFc23_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc23_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc23_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc23_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c23_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c23_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c23_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c23_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c23_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c23_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c23_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c23_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c23_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c23_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c23_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c23_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c23_JITStateAnimation,
    chartInstance->c23_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c23_PWM_28_HalfB(chartInstance);
}

void c23_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c23_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c23_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c23_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c23_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
