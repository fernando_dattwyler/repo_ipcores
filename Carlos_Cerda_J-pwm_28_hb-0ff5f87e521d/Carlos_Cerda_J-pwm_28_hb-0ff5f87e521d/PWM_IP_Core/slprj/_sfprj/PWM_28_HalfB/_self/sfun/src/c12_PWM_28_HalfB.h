#ifndef __c12_PWM_28_HalfB_h__
#define __c12_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc12_PWM_28_HalfBInstanceStruct
#define typedef_SFc12_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c12_sfEvent;
  boolean_T c12_doneDoubleBufferReInit;
  uint8_T c12_is_active_c12_PWM_28_HalfB;
  uint8_T c12_JITStateAnimation[1];
  uint8_T c12_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c12_emlrtLocationLoggingDataTables[24];
  int32_T c12_IsDebuggerActive;
  int32_T c12_IsSequenceViewerPresent;
  int32_T c12_SequenceViewerOptimization;
  void *c12_RuntimeVar;
  emlrtLocationLoggingHistogramType c12_emlrtLocLogHistTables[24];
  boolean_T c12_emlrtLocLogSimulated;
  uint32_T c12_mlFcnLineNumber;
  void *c12_fcnDataPtrs[7];
  char_T *c12_dataNames[7];
  uint32_T c12_numFcnVars;
  uint32_T c12_ssIds[7];
  uint32_T c12_statuses[7];
  void *c12_outMexFcns[7];
  void *c12_inMexFcns[7];
  CovrtStateflowInstance *c12_covrtInstance;
  void *c12_fEmlrtCtx;
  uint8_T *c12_enable;
  int16_T *c12_offset;
  int16_T *c12_max;
  int16_T *c12_sum;
  uint16_T *c12_cont;
  uint8_T *c12_init;
  uint8_T *c12_out_init;
} SFc12_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc12_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c12_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c12_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c12_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
