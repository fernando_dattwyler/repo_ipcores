#ifndef __c17_PWM_28_HalfB_h__
#define __c17_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc17_PWM_28_HalfBInstanceStruct
#define typedef_SFc17_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c17_sfEvent;
  boolean_T c17_doneDoubleBufferReInit;
  uint8_T c17_is_active_c17_PWM_28_HalfB;
  uint8_T c17_JITStateAnimation[1];
  uint8_T c17_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c17_emlrtLocationLoggingDataTables[24];
  int32_T c17_IsDebuggerActive;
  int32_T c17_IsSequenceViewerPresent;
  int32_T c17_SequenceViewerOptimization;
  void *c17_RuntimeVar;
  emlrtLocationLoggingHistogramType c17_emlrtLocLogHistTables[24];
  boolean_T c17_emlrtLocLogSimulated;
  uint32_T c17_mlFcnLineNumber;
  void *c17_fcnDataPtrs[7];
  char_T *c17_dataNames[7];
  uint32_T c17_numFcnVars;
  uint32_T c17_ssIds[7];
  uint32_T c17_statuses[7];
  void *c17_outMexFcns[7];
  void *c17_inMexFcns[7];
  CovrtStateflowInstance *c17_covrtInstance;
  void *c17_fEmlrtCtx;
  uint8_T *c17_enable;
  int16_T *c17_offset;
  int16_T *c17_max;
  int16_T *c17_sum;
  uint16_T *c17_cont;
  uint8_T *c17_init;
  uint8_T *c17_out_init;
} SFc17_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc17_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c17_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c17_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c17_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
