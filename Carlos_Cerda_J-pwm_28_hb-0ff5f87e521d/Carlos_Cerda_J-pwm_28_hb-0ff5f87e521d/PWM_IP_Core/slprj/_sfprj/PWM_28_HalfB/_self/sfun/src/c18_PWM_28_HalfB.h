#ifndef __c18_PWM_28_HalfB_h__
#define __c18_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc18_PWM_28_HalfBInstanceStruct
#define typedef_SFc18_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c18_sfEvent;
  boolean_T c18_doneDoubleBufferReInit;
  uint8_T c18_is_active_c18_PWM_28_HalfB;
  uint8_T c18_JITStateAnimation[1];
  uint8_T c18_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c18_emlrtLocationLoggingDataTables[24];
  int32_T c18_IsDebuggerActive;
  int32_T c18_IsSequenceViewerPresent;
  int32_T c18_SequenceViewerOptimization;
  void *c18_RuntimeVar;
  emlrtLocationLoggingHistogramType c18_emlrtLocLogHistTables[24];
  boolean_T c18_emlrtLocLogSimulated;
  uint32_T c18_mlFcnLineNumber;
  void *c18_fcnDataPtrs[7];
  char_T *c18_dataNames[7];
  uint32_T c18_numFcnVars;
  uint32_T c18_ssIds[7];
  uint32_T c18_statuses[7];
  void *c18_outMexFcns[7];
  void *c18_inMexFcns[7];
  CovrtStateflowInstance *c18_covrtInstance;
  void *c18_fEmlrtCtx;
  uint8_T *c18_enable;
  int16_T *c18_offset;
  int16_T *c18_max;
  int16_T *c18_sum;
  uint16_T *c18_cont;
  uint8_T *c18_init;
  uint8_T *c18_out_init;
} SFc18_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc18_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c18_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c18_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c18_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
