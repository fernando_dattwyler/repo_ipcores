#ifndef __c26_PWM_28_HalfB_h__
#define __c26_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc26_PWM_28_HalfBInstanceStruct
#define typedef_SFc26_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c26_sfEvent;
  boolean_T c26_doneDoubleBufferReInit;
  uint8_T c26_is_active_c26_PWM_28_HalfB;
  uint8_T c26_JITStateAnimation[1];
  uint8_T c26_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c26_emlrtLocationLoggingDataTables[24];
  int32_T c26_IsDebuggerActive;
  int32_T c26_IsSequenceViewerPresent;
  int32_T c26_SequenceViewerOptimization;
  void *c26_RuntimeVar;
  emlrtLocationLoggingHistogramType c26_emlrtLocLogHistTables[24];
  boolean_T c26_emlrtLocLogSimulated;
  uint32_T c26_mlFcnLineNumber;
  void *c26_fcnDataPtrs[7];
  char_T *c26_dataNames[7];
  uint32_T c26_numFcnVars;
  uint32_T c26_ssIds[7];
  uint32_T c26_statuses[7];
  void *c26_outMexFcns[7];
  void *c26_inMexFcns[7];
  CovrtStateflowInstance *c26_covrtInstance;
  void *c26_fEmlrtCtx;
  uint8_T *c26_enable;
  int16_T *c26_offset;
  int16_T *c26_max;
  int16_T *c26_sum;
  uint16_T *c26_cont;
  uint8_T *c26_init;
  uint8_T *c26_out_init;
} SFc26_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc26_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c26_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c26_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c26_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
