/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c3_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c3_eml_mx;
static const mxArray *c3_b_eml_mx;
static const mxArray *c3_c_eml_mx;

/* Function Declarations */
static void initialize_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c3_update_jit_animation_state_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance);
static void c3_do_animation_call_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_st);
static void sf_gateway_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c3_emlrt_update_log_1(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static int16_T c3_emlrt_update_log_2(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static int16_T c3_emlrt_update_log_3(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static boolean_T c3_emlrt_update_log_4(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static uint16_T c3_emlrt_update_log_5(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static int32_T c3_emlrt_update_log_6(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index);
static void c3_emlrtInitVarDataTables(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c3_dataTables[24],
  emlrtLocationLoggingHistogramType c3_histTables[24]);
static uint16_T c3_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_b_cont, const
  char_T *c3_identifier);
static uint16_T c3_b_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_u, const
  emlrtMsgIdentifier *c3_parentId);
static uint8_T c3_c_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_b_out_init, const
  char_T *c3_identifier);
static uint8_T c3_d_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_u, const
  emlrtMsgIdentifier *c3_parentId);
static uint8_T c3_e_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_b_is_active_c3_PWM_28_HalfB, const char_T
  *c3_identifier);
static uint8_T c3_f_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static const mxArray *c3_chart_data_browse_helper
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c3_ssIdNumber);
static int32_T c3__s32_add__(SFc3_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c3_b, int32_T c3_c, int32_T c3_EMLOvCount_src_loc, uint32_T
  c3_ssid_src_loc, int32_T c3_offset_src_loc, int32_T c3_length_src_loc);
static void init_dsm_address_info(SFc3_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c3_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c3_st.tls = chartInstance->c3_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c3_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c3_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c3_is_active_c3_PWM_28_HalfB = 0U;
  sf_mex_assign(&c3_c_eml_mx, sf_mex_call(&c3_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c3_b_eml_mx, sf_mex_call(&c3_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c3_eml_mx, sf_mex_call(&c3_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c3_emlrtLocLogSimulated = false;
  c3_emlrtInitVarDataTables(chartInstance,
    chartInstance->c3_emlrtLocationLoggingDataTables,
    chartInstance->c3_emlrtLocLogHistTables);
}

static void initialize_params_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c3_update_jit_animation_state_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_do_animation_call_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c3_st;
  const mxArray *c3_y = NULL;
  const mxArray *c3_b_y = NULL;
  uint16_T c3_u;
  const mxArray *c3_c_y = NULL;
  const mxArray *c3_d_y = NULL;
  uint8_T c3_b_u;
  const mxArray *c3_e_y = NULL;
  const mxArray *c3_f_y = NULL;
  c3_st = NULL;
  c3_st = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createcellmatrix(3, 1), false);
  c3_b_y = NULL;
  c3_u = *chartInstance->c3_cont;
  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", &c3_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_b_y, sf_mex_create_fi(sf_mex_dup(c3_eml_mx), sf_mex_dup
    (c3_b_eml_mx), "simulinkarray", c3_c_y, false, false), false);
  sf_mex_setcell(c3_y, 0, c3_b_y);
  c3_d_y = NULL;
  c3_b_u = *chartInstance->c3_out_init;
  c3_e_y = NULL;
  sf_mex_assign(&c3_e_y, sf_mex_create("y", &c3_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_d_y, sf_mex_create_fi(sf_mex_dup(c3_eml_mx), sf_mex_dup
    (c3_c_eml_mx), "simulinkarray", c3_e_y, false, false), false);
  sf_mex_setcell(c3_y, 1, c3_d_y);
  c3_f_y = NULL;
  sf_mex_assign(&c3_f_y, sf_mex_create("y",
    &chartInstance->c3_is_active_c3_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 2, c3_f_y);
  sf_mex_assign(&c3_st, c3_y, false);
  return c3_st;
}

static void set_sim_state_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_st)
{
  emlrtStack c3_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c3_u;
  c3_b_st.tls = chartInstance->c3_fEmlrtCtx;
  chartInstance->c3_doneDoubleBufferReInit = true;
  c3_u = sf_mex_dup(c3_st);
  *chartInstance->c3_cont = c3_emlrt_marshallIn(chartInstance, &c3_b_st,
    sf_mex_dup(sf_mex_getcell(c3_u, 0)), "cont");
  *chartInstance->c3_out_init = c3_c_emlrt_marshallIn(chartInstance, &c3_b_st,
    sf_mex_dup(sf_mex_getcell(c3_u, 1)), "out_init");
  chartInstance->c3_is_active_c3_PWM_28_HalfB = c3_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 2)),
     "is_active_c3_PWM_28_HalfB");
  sf_mex_destroy(&c3_u);
  sf_mex_destroy(&c3_st);
}

static void sf_gateway_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c3_PICOffset;
  uint8_T c3_b_enable;
  int16_T c3_b_offset;
  int16_T c3_b_max;
  int16_T c3_b_sum;
  uint8_T c3_b_init;
  uint8_T c3_a0;
  uint8_T c3_a;
  uint8_T c3_b_a0;
  uint8_T c3_a1;
  uint8_T c3_b_a1;
  boolean_T c3_c;
  int8_T c3_i;
  int8_T c3_i1;
  real_T c3_d;
  uint8_T c3_c_a0;
  uint16_T c3_b_cont;
  uint8_T c3_b_a;
  uint8_T c3_b_out_init;
  uint8_T c3_d_a0;
  uint8_T c3_c_a1;
  uint8_T c3_d_a1;
  boolean_T c3_b_c;
  int8_T c3_i2;
  int8_T c3_i3;
  real_T c3_d1;
  int16_T c3_varargin_1;
  int16_T c3_b_varargin_1;
  int16_T c3_c_varargin_1;
  int16_T c3_d_varargin_1;
  int16_T c3_var1;
  int16_T c3_b_var1;
  int16_T c3_i4;
  int16_T c3_i5;
  boolean_T c3_covSaturation;
  boolean_T c3_b_covSaturation;
  uint16_T c3_hfi;
  uint16_T c3_b_hfi;
  uint16_T c3_u;
  uint16_T c3_u1;
  int16_T c3_e_varargin_1;
  int16_T c3_f_varargin_1;
  int16_T c3_g_varargin_1;
  int16_T c3_h_varargin_1;
  int16_T c3_c_var1;
  int16_T c3_d_var1;
  int16_T c3_i6;
  int16_T c3_i7;
  boolean_T c3_c_covSaturation;
  boolean_T c3_d_covSaturation;
  uint16_T c3_c_hfi;
  uint16_T c3_d_hfi;
  uint16_T c3_u2;
  uint16_T c3_u3;
  uint16_T c3_e_a0;
  uint16_T c3_f_a0;
  uint16_T c3_b0;
  uint16_T c3_b_b0;
  uint16_T c3_c_a;
  uint16_T c3_d_a;
  uint16_T c3_b;
  uint16_T c3_b_b;
  uint16_T c3_g_a0;
  uint16_T c3_h_a0;
  uint16_T c3_c_b0;
  uint16_T c3_d_b0;
  uint16_T c3_e_a1;
  uint16_T c3_f_a1;
  uint16_T c3_b1;
  uint16_T c3_b_b1;
  uint16_T c3_g_a1;
  uint16_T c3_h_a1;
  uint16_T c3_c_b1;
  uint16_T c3_d_b1;
  boolean_T c3_c_c;
  boolean_T c3_d_c;
  int16_T c3_i8;
  int16_T c3_i9;
  int16_T c3_i10;
  int16_T c3_i11;
  int16_T c3_i12;
  int16_T c3_i13;
  int16_T c3_i14;
  int16_T c3_i15;
  int16_T c3_i16;
  int16_T c3_i17;
  int16_T c3_i18;
  int16_T c3_i19;
  int32_T c3_i20;
  int32_T c3_i21;
  int16_T c3_i22;
  int16_T c3_i23;
  int16_T c3_i24;
  int16_T c3_i25;
  int16_T c3_i26;
  int16_T c3_i27;
  int16_T c3_i28;
  int16_T c3_i29;
  int16_T c3_i30;
  int16_T c3_i31;
  int16_T c3_i32;
  int16_T c3_i33;
  int32_T c3_i34;
  int32_T c3_i35;
  int16_T c3_i36;
  int16_T c3_i37;
  int16_T c3_i38;
  int16_T c3_i39;
  int16_T c3_i40;
  int16_T c3_i41;
  int16_T c3_i42;
  int16_T c3_i43;
  real_T c3_d2;
  real_T c3_d3;
  int16_T c3_i_varargin_1;
  int16_T c3_i_a0;
  int16_T c3_j_varargin_1;
  int16_T c3_e_b0;
  int16_T c3_e_var1;
  int16_T c3_k_varargin_1;
  int16_T c3_i44;
  int16_T c3_v;
  boolean_T c3_e_covSaturation;
  int16_T c3_val;
  int16_T c3_c_b;
  int32_T c3_i45;
  uint16_T c3_e_hfi;
  real_T c3_d4;
  int32_T c3_i46;
  real_T c3_d5;
  real_T c3_d6;
  real_T c3_d7;
  int32_T c3_i47;
  int32_T c3_i48;
  int32_T c3_i49;
  real_T c3_d8;
  real_T c3_d9;
  int32_T c3_e_c;
  int32_T c3_l_varargin_1;
  int32_T c3_m_varargin_1;
  int32_T c3_f_var1;
  int32_T c3_i50;
  boolean_T c3_f_covSaturation;
  uint16_T c3_f_hfi;
  observerLogReadPIC(&c3_PICOffset);
  chartInstance->c3_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 4U, (real_T)
                    *chartInstance->c3_init);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 3U, (real_T)
                    *chartInstance->c3_sum);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 2U, (real_T)
                    *chartInstance->c3_max);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 1U, (real_T)
                    *chartInstance->c3_offset);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 0U, (real_T)
                    *chartInstance->c3_enable);
  chartInstance->c3_sfEvent = CALL_EVENT;
  c3_b_enable = *chartInstance->c3_enable;
  c3_b_offset = *chartInstance->c3_offset;
  c3_b_max = *chartInstance->c3_max;
  c3_b_sum = *chartInstance->c3_sum;
  c3_b_init = *chartInstance->c3_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c3_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c3_emlrt_update_log_1(chartInstance, c3_b_enable,
                        chartInstance->c3_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c3_emlrt_update_log_2(chartInstance, c3_b_offset,
                        chartInstance->c3_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c3_emlrt_update_log_3(chartInstance, c3_b_max,
                        chartInstance->c3_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c3_emlrt_update_log_3(chartInstance, c3_b_sum,
                        chartInstance->c3_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c3_emlrt_update_log_1(chartInstance, c3_b_init,
                        chartInstance->c3_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c3_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 0, false);
  c3_a0 = c3_b_enable;
  c3_a = c3_a0;
  c3_b_a0 = c3_a;
  c3_a1 = c3_b_a0;
  c3_b_a1 = c3_a1;
  c3_c = (c3_b_a1 == 0);
  c3_i = (int8_T)c3_b_enable;
  if ((int8_T)(c3_i & 2) != 0) {
    c3_i1 = (int8_T)(c3_i | -2);
  } else {
    c3_i1 = (int8_T)(c3_i & 1);
  }

  if (c3_i1 > 0) {
    c3_d = 3.0;
  } else {
    c3_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c3_covrtInstance,
        4U, 0U, 0U, c3_d, 0.0, -2, 0U, (int32_T)c3_emlrt_update_log_4
        (chartInstance, c3_c, chartInstance->c3_emlrtLocationLoggingDataTables,
         5)))) {
    c3_b_cont = c3_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c3_emlrtLocationLoggingDataTables, 6);
    c3_b_out_init = c3_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c3_emlrtLocationLoggingDataTables, 7);
  } else {
    c3_c_a0 = c3_b_init;
    c3_b_a = c3_c_a0;
    c3_d_a0 = c3_b_a;
    c3_c_a1 = c3_d_a0;
    c3_d_a1 = c3_c_a1;
    c3_b_c = (c3_d_a1 == 0);
    c3_i2 = (int8_T)c3_b_init;
    if ((int8_T)(c3_i2 & 2) != 0) {
      c3_i3 = (int8_T)(c3_i2 | -2);
    } else {
      c3_i3 = (int8_T)(c3_i2 & 1);
    }

    if (c3_i3 > 0) {
      c3_d1 = 3.0;
    } else {
      c3_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c3_covrtInstance, 4U, 0U, 1U, c3_d1, 0.0,
                        -2, 0U, (int32_T)c3_emlrt_update_log_4(chartInstance,
           c3_b_c, chartInstance->c3_emlrtLocationLoggingDataTables, 8)))) {
      c3_b_varargin_1 = c3_b_sum;
      c3_d_varargin_1 = c3_b_varargin_1;
      c3_b_var1 = c3_d_varargin_1;
      c3_i5 = c3_b_var1;
      c3_b_covSaturation = false;
      if (c3_i5 < 0) {
        c3_i5 = 0;
      } else {
        if (c3_i5 > 4095) {
          c3_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 0, 0,
          c3_b_covSaturation);
      }

      c3_b_hfi = (uint16_T)c3_i5;
      c3_u1 = c3_emlrt_update_log_5(chartInstance, c3_b_hfi,
        chartInstance->c3_emlrtLocationLoggingDataTables, 10);
      c3_f_varargin_1 = c3_b_max;
      c3_h_varargin_1 = c3_f_varargin_1;
      c3_d_var1 = c3_h_varargin_1;
      c3_i7 = c3_d_var1;
      c3_d_covSaturation = false;
      if (c3_i7 < 0) {
        c3_i7 = 0;
      } else {
        if (c3_i7 > 4095) {
          c3_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 1, 0,
          c3_d_covSaturation);
      }

      c3_d_hfi = (uint16_T)c3_i7;
      c3_u3 = c3_emlrt_update_log_5(chartInstance, c3_d_hfi,
        chartInstance->c3_emlrtLocationLoggingDataTables, 11);
      c3_f_a0 = c3_u1;
      c3_b_b0 = c3_u3;
      c3_d_a = c3_f_a0;
      c3_b_b = c3_b_b0;
      c3_h_a0 = c3_d_a;
      c3_d_b0 = c3_b_b;
      c3_f_a1 = c3_h_a0;
      c3_b_b1 = c3_d_b0;
      c3_h_a1 = c3_f_a1;
      c3_d_b1 = c3_b_b1;
      c3_d_c = (c3_h_a1 < c3_d_b1);
      c3_i9 = (int16_T)c3_u1;
      c3_i11 = (int16_T)c3_u3;
      c3_i13 = (int16_T)c3_u3;
      c3_i15 = (int16_T)c3_u1;
      if ((int16_T)(c3_i13 & 4096) != 0) {
        c3_i17 = (int16_T)(c3_i13 | -4096);
      } else {
        c3_i17 = (int16_T)(c3_i13 & 4095);
      }

      if ((int16_T)(c3_i15 & 4096) != 0) {
        c3_i19 = (int16_T)(c3_i15 | -4096);
      } else {
        c3_i19 = (int16_T)(c3_i15 & 4095);
      }

      c3_i21 = c3_i17 - c3_i19;
      if (c3_i21 > 4095) {
        c3_i21 = 4095;
      } else {
        if (c3_i21 < -4096) {
          c3_i21 = -4096;
        }
      }

      c3_i23 = (int16_T)c3_u1;
      c3_i25 = (int16_T)c3_u3;
      c3_i27 = (int16_T)c3_u1;
      c3_i29 = (int16_T)c3_u3;
      if ((int16_T)(c3_i27 & 4096) != 0) {
        c3_i31 = (int16_T)(c3_i27 | -4096);
      } else {
        c3_i31 = (int16_T)(c3_i27 & 4095);
      }

      if ((int16_T)(c3_i29 & 4096) != 0) {
        c3_i33 = (int16_T)(c3_i29 | -4096);
      } else {
        c3_i33 = (int16_T)(c3_i29 & 4095);
      }

      c3_i35 = c3_i31 - c3_i33;
      if (c3_i35 > 4095) {
        c3_i35 = 4095;
      } else {
        if (c3_i35 < -4096) {
          c3_i35 = -4096;
        }
      }

      if ((int16_T)(c3_i9 & 4096) != 0) {
        c3_i37 = (int16_T)(c3_i9 | -4096);
      } else {
        c3_i37 = (int16_T)(c3_i9 & 4095);
      }

      if ((int16_T)(c3_i11 & 4096) != 0) {
        c3_i39 = (int16_T)(c3_i11 | -4096);
      } else {
        c3_i39 = (int16_T)(c3_i11 & 4095);
      }

      if ((int16_T)(c3_i23 & 4096) != 0) {
        c3_i41 = (int16_T)(c3_i23 | -4096);
      } else {
        c3_i41 = (int16_T)(c3_i23 & 4095);
      }

      if ((int16_T)(c3_i25 & 4096) != 0) {
        c3_i43 = (int16_T)(c3_i25 | -4096);
      } else {
        c3_i43 = (int16_T)(c3_i25 & 4095);
      }

      if (c3_i37 < c3_i39) {
        c3_d3 = (real_T)((int16_T)c3_i21 <= 1);
      } else if (c3_i41 > c3_i43) {
        if ((int16_T)c3_i35 <= 1) {
          c3_d3 = 3.0;
        } else {
          c3_d3 = 0.0;
        }
      } else {
        c3_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c3_covrtInstance, 4U, 0U, 2U, c3_d3,
                          0.0, -2, 2U, (int32_T)c3_emlrt_update_log_4
                          (chartInstance, c3_d_c,
                           chartInstance->c3_emlrtLocationLoggingDataTables, 9))))
      {
        c3_i_a0 = c3_b_sum;
        c3_e_b0 = c3_b_offset;
        c3_k_varargin_1 = c3_e_b0;
        c3_v = c3_k_varargin_1;
        c3_val = c3_v;
        c3_c_b = c3_val;
        c3_i45 = c3_i_a0;
        if (c3_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d4 = 1.0;
          observerLog(45 + c3_PICOffset, &c3_d4, 1);
        }

        if (c3_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d5 = 1.0;
          observerLog(45 + c3_PICOffset, &c3_d5, 1);
        }

        c3_i46 = c3_c_b;
        if (c3_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d6 = 1.0;
          observerLog(48 + c3_PICOffset, &c3_d6, 1);
        }

        if (c3_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d7 = 1.0;
          observerLog(48 + c3_PICOffset, &c3_d7, 1);
        }

        if ((c3_i45 & 65536) != 0) {
          c3_i47 = c3_i45 | -65536;
        } else {
          c3_i47 = c3_i45 & 65535;
        }

        if ((c3_i46 & 65536) != 0) {
          c3_i48 = c3_i46 | -65536;
        } else {
          c3_i48 = c3_i46 & 65535;
        }

        c3_i49 = c3__s32_add__(chartInstance, c3_i47, c3_i48, 47, 1U, 323, 12);
        if (c3_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d8 = 1.0;
          observerLog(53 + c3_PICOffset, &c3_d8, 1);
        }

        if (c3_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c3_d9 = 1.0;
          observerLog(53 + c3_PICOffset, &c3_d9, 1);
        }

        if ((c3_i49 & 65536) != 0) {
          c3_e_c = c3_i49 | -65536;
        } else {
          c3_e_c = c3_i49 & 65535;
        }

        c3_l_varargin_1 = c3_emlrt_update_log_6(chartInstance, c3_e_c,
          chartInstance->c3_emlrtLocationLoggingDataTables, 13);
        c3_m_varargin_1 = c3_l_varargin_1;
        c3_f_var1 = c3_m_varargin_1;
        c3_i50 = c3_f_var1;
        c3_f_covSaturation = false;
        if (c3_i50 < 0) {
          c3_i50 = 0;
        } else {
          if (c3_i50 > 4095) {
            c3_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 2, 0,
            c3_f_covSaturation);
        }

        c3_f_hfi = (uint16_T)c3_i50;
        c3_b_cont = c3_emlrt_update_log_5(chartInstance, c3_f_hfi,
          chartInstance->c3_emlrtLocationLoggingDataTables, 12);
        c3_b_out_init = c3_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 14);
      } else {
        c3_b_cont = c3_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 15);
        c3_b_out_init = c3_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c3_varargin_1 = c3_b_sum;
      c3_c_varargin_1 = c3_varargin_1;
      c3_var1 = c3_c_varargin_1;
      c3_i4 = c3_var1;
      c3_covSaturation = false;
      if (c3_i4 < 0) {
        c3_i4 = 0;
      } else {
        if (c3_i4 > 4095) {
          c3_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 3, 0,
          c3_covSaturation);
      }

      c3_hfi = (uint16_T)c3_i4;
      c3_u = c3_emlrt_update_log_5(chartInstance, c3_hfi,
        chartInstance->c3_emlrtLocationLoggingDataTables, 18);
      c3_e_varargin_1 = c3_b_max;
      c3_g_varargin_1 = c3_e_varargin_1;
      c3_c_var1 = c3_g_varargin_1;
      c3_i6 = c3_c_var1;
      c3_c_covSaturation = false;
      if (c3_i6 < 0) {
        c3_i6 = 0;
      } else {
        if (c3_i6 > 4095) {
          c3_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 4, 0,
          c3_c_covSaturation);
      }

      c3_c_hfi = (uint16_T)c3_i6;
      c3_u2 = c3_emlrt_update_log_5(chartInstance, c3_c_hfi,
        chartInstance->c3_emlrtLocationLoggingDataTables, 19);
      c3_e_a0 = c3_u;
      c3_b0 = c3_u2;
      c3_c_a = c3_e_a0;
      c3_b = c3_b0;
      c3_g_a0 = c3_c_a;
      c3_c_b0 = c3_b;
      c3_e_a1 = c3_g_a0;
      c3_b1 = c3_c_b0;
      c3_g_a1 = c3_e_a1;
      c3_c_b1 = c3_b1;
      c3_c_c = (c3_g_a1 < c3_c_b1);
      c3_i8 = (int16_T)c3_u;
      c3_i10 = (int16_T)c3_u2;
      c3_i12 = (int16_T)c3_u2;
      c3_i14 = (int16_T)c3_u;
      if ((int16_T)(c3_i12 & 4096) != 0) {
        c3_i16 = (int16_T)(c3_i12 | -4096);
      } else {
        c3_i16 = (int16_T)(c3_i12 & 4095);
      }

      if ((int16_T)(c3_i14 & 4096) != 0) {
        c3_i18 = (int16_T)(c3_i14 | -4096);
      } else {
        c3_i18 = (int16_T)(c3_i14 & 4095);
      }

      c3_i20 = c3_i16 - c3_i18;
      if (c3_i20 > 4095) {
        c3_i20 = 4095;
      } else {
        if (c3_i20 < -4096) {
          c3_i20 = -4096;
        }
      }

      c3_i22 = (int16_T)c3_u;
      c3_i24 = (int16_T)c3_u2;
      c3_i26 = (int16_T)c3_u;
      c3_i28 = (int16_T)c3_u2;
      if ((int16_T)(c3_i26 & 4096) != 0) {
        c3_i30 = (int16_T)(c3_i26 | -4096);
      } else {
        c3_i30 = (int16_T)(c3_i26 & 4095);
      }

      if ((int16_T)(c3_i28 & 4096) != 0) {
        c3_i32 = (int16_T)(c3_i28 | -4096);
      } else {
        c3_i32 = (int16_T)(c3_i28 & 4095);
      }

      c3_i34 = c3_i30 - c3_i32;
      if (c3_i34 > 4095) {
        c3_i34 = 4095;
      } else {
        if (c3_i34 < -4096) {
          c3_i34 = -4096;
        }
      }

      if ((int16_T)(c3_i8 & 4096) != 0) {
        c3_i36 = (int16_T)(c3_i8 | -4096);
      } else {
        c3_i36 = (int16_T)(c3_i8 & 4095);
      }

      if ((int16_T)(c3_i10 & 4096) != 0) {
        c3_i38 = (int16_T)(c3_i10 | -4096);
      } else {
        c3_i38 = (int16_T)(c3_i10 & 4095);
      }

      if ((int16_T)(c3_i22 & 4096) != 0) {
        c3_i40 = (int16_T)(c3_i22 | -4096);
      } else {
        c3_i40 = (int16_T)(c3_i22 & 4095);
      }

      if ((int16_T)(c3_i24 & 4096) != 0) {
        c3_i42 = (int16_T)(c3_i24 | -4096);
      } else {
        c3_i42 = (int16_T)(c3_i24 & 4095);
      }

      if (c3_i36 < c3_i38) {
        c3_d2 = (real_T)((int16_T)c3_i20 <= 1);
      } else if (c3_i40 > c3_i42) {
        if ((int16_T)c3_i34 <= 1) {
          c3_d2 = 3.0;
        } else {
          c3_d2 = 0.0;
        }
      } else {
        c3_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c3_covrtInstance, 4U, 0U, 3U, c3_d2,
                          0.0, -2, 2U, (int32_T)c3_emlrt_update_log_4
                          (chartInstance, c3_c_c,
                           chartInstance->c3_emlrtLocationLoggingDataTables, 17))))
      {
        c3_i_varargin_1 = c3_b_sum;
        c3_j_varargin_1 = c3_i_varargin_1;
        c3_e_var1 = c3_j_varargin_1;
        c3_i44 = c3_e_var1;
        c3_e_covSaturation = false;
        if (c3_i44 < 0) {
          c3_i44 = 0;
        } else {
          if (c3_i44 > 4095) {
            c3_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 5, 0,
            c3_e_covSaturation);
        }

        c3_e_hfi = (uint16_T)c3_i44;
        c3_b_cont = c3_emlrt_update_log_5(chartInstance, c3_e_hfi,
          chartInstance->c3_emlrtLocationLoggingDataTables, 20);
        c3_b_out_init = c3_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 21);
      } else {
        c3_b_cont = c3_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 22);
        c3_b_out_init = c3_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c3_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c3_cont = c3_b_cont;
  *chartInstance->c3_out_init = c3_b_out_init;
  c3_do_animation_call_c3_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 5U, (real_T)
                    *chartInstance->c3_cont);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 6U, (real_T)
                    *chartInstance->c3_out_init);
}

static void mdl_start_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c3_decisionTxtStartIdx = 0U;
  static const uint32_T c3_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c3_chart_data_browse_helper);
  chartInstance->c3_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c3_RuntimeVar,
    &chartInstance->c3_IsDebuggerActive,
    &chartInstance->c3_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c3_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c3_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c3_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c3_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c3_decisionTxtStartIdx, &c3_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c3_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c3_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c3_PWM_28_HalfB
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c3_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7141",              /* mexFileName */
    "Thu May 27 10:27:16 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c3_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c3_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c3_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c3_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7141");
    emlrtLocationLoggingPushLog(&c3_emlrtLocationLoggingFileInfo,
      c3_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c3_emlrtLocationLoggingDataTables, c3_emlrtLocationInfo,
      NULL, 0U, c3_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7141");
  }

  sfListenerLightTerminate(chartInstance->c3_RuntimeVar);
  sf_mex_destroy(&c3_eml_mx);
  sf_mex_destroy(&c3_b_eml_mx);
  sf_mex_destroy(&c3_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c3_covrtInstance);
}

static void initSimStructsc3_PWM_28_HalfB(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c3_emlrt_update_log_1(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  real_T c3_d;
  uint8_T c3_u;
  uint8_T c3_localMin;
  real_T c3_d1;
  uint8_T c3_u1;
  uint8_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_d = c3_b_table[0U].SimMin;
    if (c3_d < 2.0) {
      if (c3_d >= 0.0) {
        c3_u = (uint8_T)c3_d;
      } else {
        c3_u = 0U;
      }
    } else if (c3_d >= 2.0) {
      c3_u = 1U;
    } else {
      c3_u = 0U;
    }

    c3_localMin = c3_u;
    c3_d1 = c3_b_table[0U].SimMax;
    if (c3_d1 < 2.0) {
      if (c3_d1 >= 0.0) {
        c3_u1 = (uint8_T)c3_d1;
      } else {
        c3_u1 = 0U;
      }
    } else if (c3_d1 >= 2.0) {
      c3_u1 = 1U;
    } else {
      c3_u1 = 0U;
    }

    c3_localMax = c3_u1;
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c3_in < c3_localMin) {
      c3_localMin = c3_in;
    }

    if (c3_in > c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static int16_T c3_emlrt_update_log_2(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  real_T c3_d;
  int16_T c3_i;
  int16_T c3_localMin;
  real_T c3_d1;
  int16_T c3_i1;
  int16_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_d = muDoubleScalarFloor(c3_b_table[0U].SimMin);
    if (c3_d < 32768.0) {
      if (c3_d >= -32768.0) {
        c3_i = (int16_T)c3_d;
      } else {
        c3_i = MIN_int16_T;
      }
    } else if (c3_d >= 32768.0) {
      c3_i = MAX_int16_T;
    } else {
      c3_i = 0;
    }

    c3_localMin = c3_i;
    c3_d1 = muDoubleScalarFloor(c3_b_table[0U].SimMax);
    if (c3_d1 < 32768.0) {
      if (c3_d1 >= -32768.0) {
        c3_i1 = (int16_T)c3_d1;
      } else {
        c3_i1 = MIN_int16_T;
      }
    } else if (c3_d1 >= 32768.0) {
      c3_i1 = MAX_int16_T;
    } else {
      c3_i1 = 0;
    }

    c3_localMax = c3_i1;
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c3_in < c3_localMin) {
      c3_localMin = c3_in;
    }

    if (c3_in > c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static int16_T c3_emlrt_update_log_3(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  real_T c3_d;
  int16_T c3_i;
  int16_T c3_localMin;
  real_T c3_d1;
  int16_T c3_i1;
  int16_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_d = muDoubleScalarFloor(c3_b_table[0U].SimMin);
    if (c3_d < 2048.0) {
      if (c3_d >= -2048.0) {
        c3_i = (int16_T)c3_d;
      } else {
        c3_i = -2048;
      }
    } else if (c3_d >= 2048.0) {
      c3_i = 2047;
    } else {
      c3_i = 0;
    }

    c3_localMin = c3_i;
    c3_d1 = muDoubleScalarFloor(c3_b_table[0U].SimMax);
    if (c3_d1 < 2048.0) {
      if (c3_d1 >= -2048.0) {
        c3_i1 = (int16_T)c3_d1;
      } else {
        c3_i1 = -2048;
      }
    } else if (c3_d1 >= 2048.0) {
      c3_i1 = 2047;
    } else {
      c3_i1 = 0;
    }

    c3_localMax = c3_i1;
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c3_in < c3_localMin) {
      c3_localMin = c3_in;
    }

    if (c3_in > c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static boolean_T c3_emlrt_update_log_4(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  boolean_T c3_localMin;
  boolean_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_localMin = (c3_b_table[0U].SimMin > 0.0);
    c3_localMax = (c3_b_table[0U].SimMax > 0.0);
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c3_in < (int32_T)c3_localMin) {
      c3_localMin = c3_in;
    }

    if ((int32_T)c3_in > (int32_T)c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static uint16_T c3_emlrt_update_log_5(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  real_T c3_d;
  uint16_T c3_u;
  uint16_T c3_localMin;
  real_T c3_d1;
  uint16_T c3_u1;
  uint16_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_d = c3_b_table[0U].SimMin;
    if (c3_d < 4096.0) {
      if (c3_d >= 0.0) {
        c3_u = (uint16_T)c3_d;
      } else {
        c3_u = 0U;
      }
    } else if (c3_d >= 4096.0) {
      c3_u = 4095U;
    } else {
      c3_u = 0U;
    }

    c3_localMin = c3_u;
    c3_d1 = c3_b_table[0U].SimMax;
    if (c3_d1 < 4096.0) {
      if (c3_d1 >= 0.0) {
        c3_u1 = (uint16_T)c3_d1;
      } else {
        c3_u1 = 0U;
      }
    } else if (c3_d1 >= 4096.0) {
      c3_u1 = 4095U;
    } else {
      c3_u1 = 0U;
    }

    c3_localMax = c3_u1;
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c3_in < c3_localMin) {
      c3_localMin = c3_in;
    }

    if (c3_in > c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static int32_T c3_emlrt_update_log_6(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c3_in, emlrtLocationLoggingDataType c3_table[],
  int32_T c3_index)
{
  boolean_T c3_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c3_b_table;
  real_T c3_d;
  int32_T c3_i;
  int32_T c3_localMin;
  real_T c3_d1;
  int32_T c3_i1;
  int32_T c3_localMax;
  emlrtLocationLoggingHistogramType *c3_histTable;
  real_T c3_inDouble;
  real_T c3_significand;
  int32_T c3_exponent;
  (void)chartInstance;
  c3_isLoggingEnabledHere = (c3_index >= 0);
  if (c3_isLoggingEnabledHere) {
    c3_b_table = (emlrtLocationLoggingDataType *)&c3_table[c3_index];
    c3_d = muDoubleScalarFloor(c3_b_table[0U].SimMin);
    if (c3_d < 65536.0) {
      if (c3_d >= -65536.0) {
        c3_i = (int32_T)c3_d;
      } else {
        c3_i = -65536;
      }
    } else if (c3_d >= 65536.0) {
      c3_i = 65535;
    } else {
      c3_i = 0;
    }

    c3_localMin = c3_i;
    c3_d1 = muDoubleScalarFloor(c3_b_table[0U].SimMax);
    if (c3_d1 < 65536.0) {
      if (c3_d1 >= -65536.0) {
        c3_i1 = (int32_T)c3_d1;
      } else {
        c3_i1 = -65536;
      }
    } else if (c3_d1 >= 65536.0) {
      c3_i1 = 65535;
    } else {
      c3_i1 = 0;
    }

    c3_localMax = c3_i1;
    c3_histTable = c3_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c3_in < c3_localMin) {
      c3_localMin = c3_in;
    }

    if (c3_in > c3_localMax) {
      c3_localMax = c3_in;
    }

    /* Histogram logging. */
    c3_inDouble = (real_T)c3_in;
    c3_histTable->TotalNumberOfValues++;
    if (c3_inDouble == 0.0) {
      c3_histTable->NumberOfZeros++;
    } else {
      c3_histTable->SimSum += c3_inDouble;
      if ((!muDoubleScalarIsInf(c3_inDouble)) && (!muDoubleScalarIsNaN
           (c3_inDouble))) {
        c3_significand = frexp(c3_inDouble, &c3_exponent);
        if (c3_exponent > 128) {
          c3_exponent = 128;
        }

        if (c3_exponent < -127) {
          c3_exponent = -127;
        }

        if (c3_significand < 0.0) {
          c3_histTable->NumberOfNegativeValues++;
          c3_histTable->HistogramOfNegativeValues[127 + c3_exponent]++;
        } else {
          c3_histTable->NumberOfPositiveValues++;
          c3_histTable->HistogramOfPositiveValues[127 + c3_exponent]++;
        }
      }
    }

    c3_b_table[0U].SimMin = (real_T)c3_localMin;
    c3_b_table[0U].SimMax = (real_T)c3_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c3_in;
}

static void c3_emlrtInitVarDataTables(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c3_dataTables[24],
  emlrtLocationLoggingHistogramType c3_histTables[24])
{
  int32_T c3_i;
  int32_T c3_iH;
  (void)chartInstance;
  for (c3_i = 0; c3_i < 24; c3_i++) {
    c3_dataTables[c3_i].SimMin = rtInf;
    c3_dataTables[c3_i].SimMax = rtMinusInf;
    c3_dataTables[c3_i].OverflowWraps = 0;
    c3_dataTables[c3_i].Saturations = 0;
    c3_dataTables[c3_i].IsAlwaysInteger = true;
    c3_dataTables[c3_i].HistogramTable = &c3_histTables[c3_i];
    c3_histTables[c3_i].NumberOfZeros = 0.0;
    c3_histTables[c3_i].NumberOfPositiveValues = 0.0;
    c3_histTables[c3_i].NumberOfNegativeValues = 0.0;
    c3_histTables[c3_i].TotalNumberOfValues = 0.0;
    c3_histTables[c3_i].SimSum = 0.0;
    for (c3_iH = 0; c3_iH < 256; c3_iH++) {
      c3_histTables[c3_i].HistogramOfPositiveValues[c3_iH] = 0.0;
      c3_histTables[c3_i].HistogramOfNegativeValues[c3_iH] = 0.0;
    }
  }
}

const mxArray *sf_c3_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c3_nameCaptureInfo = NULL;
  c3_nameCaptureInfo = NULL;
  sf_mex_assign(&c3_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c3_nameCaptureInfo;
}

static uint16_T c3_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_b_cont, const
  char_T *c3_identifier)
{
  uint16_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = (const char *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_b_emlrt_marshallIn(chartInstance, c3_sp, sf_mex_dup(c3_b_cont),
    &c3_thisId);
  sf_mex_destroy(&c3_b_cont);
  return c3_y;
}

static uint16_T c3_b_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_u, const
  emlrtMsgIdentifier *c3_parentId)
{
  uint16_T c3_y;
  const mxArray *c3_mxFi = NULL;
  const mxArray *c3_mxInt = NULL;
  uint16_T c3_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c3_parentId, c3_u, false, 0U, NULL, c3_eml_mx, c3_b_eml_mx);
  sf_mex_assign(&c3_mxFi, sf_mex_dup(c3_u), false);
  sf_mex_assign(&c3_mxInt, sf_mex_call(c3_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c3_mxFi)), false);
  sf_mex_import(c3_parentId, sf_mex_dup(c3_mxInt), &c3_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c3_mxFi);
  sf_mex_destroy(&c3_mxInt);
  c3_y = c3_b_u;
  sf_mex_destroy(&c3_mxFi);
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static uint8_T c3_c_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_b_out_init, const
  char_T *c3_identifier)
{
  uint8_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = (const char *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_d_emlrt_marshallIn(chartInstance, c3_sp, sf_mex_dup(c3_b_out_init),
    &c3_thisId);
  sf_mex_destroy(&c3_b_out_init);
  return c3_y;
}

static uint8_T c3_d_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, const mxArray *c3_u, const
  emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_y;
  const mxArray *c3_mxFi = NULL;
  const mxArray *c3_mxInt = NULL;
  uint8_T c3_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c3_parentId, c3_u, false, 0U, NULL, c3_eml_mx, c3_c_eml_mx);
  sf_mex_assign(&c3_mxFi, sf_mex_dup(c3_u), false);
  sf_mex_assign(&c3_mxInt, sf_mex_call(c3_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c3_mxFi)), false);
  sf_mex_import(c3_parentId, sf_mex_dup(c3_mxInt), &c3_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c3_mxFi);
  sf_mex_destroy(&c3_mxInt);
  c3_y = c3_b_u;
  sf_mex_destroy(&c3_mxFi);
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static uint8_T c3_e_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_b_is_active_c3_PWM_28_HalfB, const char_T
  *c3_identifier)
{
  uint8_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = (const char *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_b_is_active_c3_PWM_28_HalfB), &c3_thisId);
  sf_mex_destroy(&c3_b_is_active_c3_PWM_28_HalfB);
  return c3_y;
}

static uint8_T c3_f_emlrt_marshallIn(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_y;
  uint8_T c3_b_u;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_b_u, 1, 3, 0U, 0, 0U, 0);
  c3_y = c3_b_u;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static const mxArray *c3_chart_data_browse_helper
  (SFc3_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c3_ssIdNumber)
{
  const mxArray *c3_mxData = NULL;
  uint8_T c3_u;
  int16_T c3_i;
  int16_T c3_i1;
  int16_T c3_i2;
  uint16_T c3_u1;
  uint8_T c3_u2;
  uint8_T c3_u3;
  real_T c3_d;
  real_T c3_d1;
  real_T c3_d2;
  real_T c3_d3;
  real_T c3_d4;
  real_T c3_d5;
  c3_mxData = NULL;
  switch (c3_ssIdNumber) {
   case 18U:
    c3_u = *chartInstance->c3_enable;
    c3_d = (real_T)c3_u;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c3_i = *chartInstance->c3_offset;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c3_i1 = *chartInstance->c3_max;
    c3_d1 = (real_T)c3_i1;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c3_i2 = *chartInstance->c3_sum;
    c3_d2 = (real_T)c3_i2;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c3_u1 = *chartInstance->c3_cont;
    c3_d3 = (real_T)c3_u1;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c3_u2 = *chartInstance->c3_init;
    c3_d4 = (real_T)c3_u2;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c3_u3 = *chartInstance->c3_out_init;
    c3_d5 = (real_T)c3_u3;
    sf_mex_assign(&c3_mxData, sf_mex_create("mxData", &c3_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c3_mxData;
}

static int32_T c3__s32_add__(SFc3_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c3_b, int32_T c3_c, int32_T c3_EMLOvCount_src_loc, uint32_T
  c3_ssid_src_loc, int32_T c3_offset_src_loc, int32_T c3_length_src_loc)
{
  int32_T c3_a;
  int32_T c3_PICOffset;
  real_T c3_d;
  observerLogReadPIC(&c3_PICOffset);
  c3_a = c3_b + c3_c;
  if (((c3_a ^ c3_b) & (c3_a ^ c3_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c3_ssid_src_loc, c3_offset_src_loc,
      c3_length_src_loc);
    c3_d = 1.0;
    observerLog(c3_EMLOvCount_src_loc + c3_PICOffset, &c3_d, 1);
  }

  return c3_a;
}

static void init_dsm_address_info(SFc3_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc3_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c3_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c3_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c3_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c3_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c3_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c3_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c3_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c3_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c3_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c3_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c3_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c3_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c3_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c3_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyycXxAuG+8kUW8R"
    "2JOmhOSuSAAAOoOIF0="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c3_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c3_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c3_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c3_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c3_PWM_28_HalfB
      ((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c3_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c3_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc3_PWM_28_HalfB((SFc3_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c3_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV9uO00YYdkIWgVpWS28QEhLctVdVORRxVZZ1EjZS0k3xcriLZsd/4lHGM2YO2V2eoy9RnqC",
    "XXPAC3HFR9Q0q9RH6j+Nks47tsARWIHUkx5rx93/zn2fi1To9D8cmPi++87yL+L6ET92bjo1sXl",
    "t4pusN7/ts/hCFhI37RJFYe5VDkBiegJbcGiZFRwxlIYyJISgQFLGJVKaMTbPYcibGbSuo49PPI",
    "0ajIJKWhzsoS8I9wY+RLbGmjzxNpoCaNkBoIiXtKGpzMpprrMyhHwEdaxtXmaDBBDZxaume5YYl",
    "HFpHQDtCG4Ia6xPdAkMM+Oao1ExnqQ5mQBknnBFRaG1EdAAJOtjA0yTE3z1r0Kg8jEZEmR2IyAR",
    "0l41TTikgz8k0fjhgghipGOGtmPtOcFm3Pkd9ejIEXuEQ1G1HARknkglTHv+gjZa2BDng0IQDOy",
    "pnC+CldcF/xuAQVKnfhr6cgCIj2BOlm6YOaR2l0ZpnyTLMsBieEfWIYvw0hKXZi5mjA4Jxgn2UK",
    "INBamRH7ys2QfeWstm44zJzVcnYeBpsvQqWsrUmUBWFOVubCp9wrkth+zLpwgR4ytokhlTDpqzF",
    "OK1ZuC/RwS69y6vBCoaBz2C+FCErDNckB0j7zq/YWE4jqdVGxj4mb7PbXf68DOsIA2pIKBR1AUW",
    "YBvRZ6t5ytpBpF3sEolYmVa8IPM2QVShPD61oHko1Rp9UNJETE1xES4GxHmEssRKeaiyaKpiL5S",
    "ocJTSC0DUYxqGHZYPYAp9o19oeYd1NmDlugqaKJUVRdefPT97J+fPtB5w/M7n8+4cFnloBj7fwd",
    "vgHC/jL9dP4jdy+9dmaG5n89oL8ldx+jZy8w225yvn97es/rv7zzV+//fmu8zeYvP15PWpLetS8",
    "2f7JhbOd25vZ/MasQc4TfrKUZw67u6BXo4D/2gL/VjbX93bbQW/84vbLJ7fhsTL8VazEfT/le1O",
    "v1vdCTt/Z+i3XqY+TtO9qRTthdqFwc2Knx2w+nhdX+ONytj4d/z5cT/7H7Xwci/zVOOWvhkelMJ",
    "sl+Xi++v+8nZcv0v9SLt5uLq0ZMME+kR23tteTn+7fX2HH9Zwd19N7xYC4bgUDenfQf94b3Hkw2",
    "CV8uFPQZz62Xs8q552z3Nei5/9++fz2fcg5vPGRcvU1z/3zklvXvrPeR740fNV55uXwW1+wHeve",
    "Ez83/r13tnvczWz+y/wvlh8xHhbctrPPXSDDoq/nYN9/qB6gSg==",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c3_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c3_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c3_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c3_PWM_28_HalfB(SimStruct *S)
{
  SFc3_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc3_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc3_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc3_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c3_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c3_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c3_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c3_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c3_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c3_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c3_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c3_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c3_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c3_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c3_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c3_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c3_JITStateAnimation,
    chartInstance->c3_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c3_PWM_28_HalfB(chartInstance);
}

void c3_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c3_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c3_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c3_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c3_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
